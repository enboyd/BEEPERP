-- 采购单purchase
DROP TABLE IF EXISTS `biz_purchase`;
CREATE TABLE `biz_purchase`(
  `purchase_id` VARCHAR(20) COLLATE utf8_bin NOT NULL COMMENT '采购单ID',
  `purchase_code` VARCHAR(24) COLLATE utf8_bin NOT NULL COMMENT '采购单编号',
  `purchase_date` DATE DEFAULT '1970-01-01' COMMENT '采购日期',
  `total_price` DECIMAL(10,2) NOT NULL DEFAULT 0.00 COMMENT '采购总额',
  `status` CHAR(1) COLLATE utf8_bin NOT NULL DEFAULT '0' COMMENT '采购状态(0:新单,1:采购中,2:待补齐,3:已采购,4:已取消)',
  `create_by` VARCHAR(64) COLLATE utf8_bin NOT NULL COMMENT '创建者ID',
  `create_by_name` VARCHAR(64) COLLATE utf8_bin NOT NULL COMMENT '创建者名称',
  `create_date` DATETIME NOT NULL COMMENT '创建时间',
  `update_by` VARCHAR(64) COLLATE utf8_bin NOT NULL COMMENT '更新者ID',
  `update_by_name` VARCHAR(64) COLLATE utf8_bin NOT NULL COMMENT '更新者名称',
  `update_date` DATETIME NOT NULL COMMENT '更新时间',
  `remarks` VARCHAR(255) COLLATE utf8_bin DEFAULT NULL COMMENT '备注信息',
  `del_flag` CHAR(1) COLLATE utf8_bin NOT NULL DEFAULT '0' COMMENT '删除标记(0:否,1:是)',
  PRIMARY KEY (`purchase_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT '采购单';

-- 采购明细purchase_detail
DROP TABLE IF EXISTS `biz_purchase_detail`;
CREATE TABLE `biz_purchase_detail`(
  `purchase_detail_id` VARCHAR(20) COLLATE utf8_bin NOT NULL COMMENT '采购明细ID',
  `purchase_id` VARCHAR(20) COLLATE utf8_bin NOT NULL COMMENT '所属采购单',
  `goods_id` VARCHAR(20) COLLATE utf8_bin NOT NULL COMMENT '商品ID',
  `goods_code` VARCHAR(24) COLLATE utf8_bin NOT NULL COMMENT '商品编号',
  `goods_name` VARCHAR(64) COLLATE utf8_bin NOT NULL COMMENT '商品名称',
  `goodes_category_id` VARCHAR(20) COLLATE utf8_bin NOT NULL COMMENT '商品类型',
  `unit_price` DECIMAL(10,2) NOT NULL DEFAULT 0.00 COMMENT '商品单价',
  `expert_amount` INT(10) NOT NULL NULL DEFAULT 1 COMMENT '申请采购数量',
  `actual_amount` INT(10) NOT NULL NULL DEFAULT 0 COMMENT '实际入库数量',
  `different_amount` INT(10) NOT NULL DEFAULT 0 COMMENT '相差数量',
  `total_price` DECIMAL(10,2) NOT NULL DEFAULT 0.00 COMMENT '金额',
  `supplier_id` VARCHAR(20) COLLATE utf8_bin NOT NULL DEFAULT '' COMMENT '供应商',
  `create_by` VARCHAR(64) COLLATE utf8_bin NOT NULL COMMENT '创建者ID',
  `create_by_name` VARCHAR(64) COLLATE utf8_bin NOT NULL COMMENT '创建者名称',
  `create_date` DATETIME NOT NULL COMMENT '创建时间',
  `update_by` VARCHAR(64) COLLATE utf8_bin NOT NULL COMMENT '更新者ID',
  `update_by_name` VARCHAR(64) COLLATE utf8_bin NOT NULL COMMENT '更新者名称',
  `update_date` DATETIME NOT NULL COMMENT '更新时间',
  `remarks` VARCHAR(255) COLLATE utf8_bin DEFAULT NULL COMMENT '备注信息',
  `del_flag` CHAR(1) COLLATE utf8_bin NOT NULL DEFAULT '0' COMMENT '删除标记(0:否,1:是)',
  PRIMARY KEY (`purchase_detail_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT '采购明细';

-- 商品类型goods_category
DROP TABLE IF EXISTS `biz_goods_category`;
CREATE TABLE `biz_goods_category`(
  `goods_category_id` INT(11) NOT NULL AUTO_INCREMENT COMMENT '商品类型ID',
  `goods_category_name` VARCHAR(64) COLLATE utf8_bin NOT NULL COMMENT '商品类型名称',
  `create_by` VARCHAR(64) COLLATE utf8_bin NOT NULL COMMENT '创建者ID',
  `create_by_name` VARCHAR(64) COLLATE utf8_bin NOT NULL COMMENT '创建者名称',
  `create_date` DATETIME NOT NULL COMMENT '创建时间',
  `update_by` VARCHAR(64) COLLATE utf8_bin NOT NULL COMMENT '更新者ID',
  `update_by_name` VARCHAR(64) COLLATE utf8_bin NOT NULL COMMENT '更新者名称',
  `update_date` DATETIME NOT NULL COMMENT '更新时间',
  `remarks` VARCHAR(255) COLLATE utf8_bin DEFAULT NULL COMMENT '备注信息',
  `del_flag` CHAR(1) COLLATE utf8_bin NOT NULL DEFAULT '0' COMMENT '删除标记(0:否,1:是)',
  PRIMARY KEY (`goods_category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT '商品类型';

-- 商品品牌goods_brand
DROP TABLE IF EXISTS `biz_goods_brand`;
CREATE TABLE `biz_goods_brand`(
  `goods_brand_id` INT(11) NOT NULL AUTO_INCREMENT COMMENT '商品品牌ID',
  `goods_brand_name` VARCHAR(64) COLLATE utf8_bin NOT NULL COMMENT '商品品牌名称',
  `create_by` VARCHAR(64) COLLATE utf8_bin NOT NULL COMMENT '创建者ID',
  `create_by_name` VARCHAR(64) COLLATE utf8_bin NOT NULL COMMENT '创建者名称',
  `create_date` DATETIME NOT NULL COMMENT '创建时间',
  `update_by` VARCHAR(64) COLLATE utf8_bin NOT NULL COMMENT '更新者ID',
  `update_by_name` VARCHAR(64) COLLATE utf8_bin NOT NULL COMMENT '更新者名称',
  `update_date` DATETIME NOT NULL COMMENT '更新时间',
  `remarks` VARCHAR(255) COLLATE utf8_bin DEFAULT NULL COMMENT '备注信息',
  `del_flag` CHAR(1) COLLATE utf8_bin NOT NULL DEFAULT '0' COMMENT '删除标记(0:否,1:是)',
  PRIMARY KEY (`goods_brand_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT '商品品牌';

-- 商品goods
DROP TABLE IF EXISTS `biz_goods`;
CREATE TABLE `biz_goods`(
  `goods_id` INT(11) NOT NULL AUTO_INCREMENT COMMENT '商品ID',
  `goods_code` VARCHAR(24) COLLATE utf8_bin NOT NULL COMMENT '商品编号',
  `goods_name` VARCHAR(64) COLLATE utf8_bin NOT NULL COMMENT '商品名称',
  `goods_category_id` INT(11) NOT NULL COMMENT '商品类型',
  `goods_brand_id` INT(11) NOT NULL COMMENT '商品品牌',
  `specification` VARCHAR(64) COLLATE utf8_bin COMMENT '规格',
  `size` VARCHAR(64) COLLATE utf8_bin COMMENT '尺寸',
  `weight` VARCHAR(64) COLLATE utf8_bin COMMENT '重量',
  `unit_price` DECIMAL(10,2) NOT NULL DEFAULT 0.00 COMMENT '进货单价',
  `selling_price` DECIMAL(10,2) NOT NULL DEFAULT 0.00 COMMENT '参考售价',
  `stock_amount` INT(10) NOT NULL DEFAULT 0 COMMENT '库存数量',
  `purchasing_amount` INT(10) NOT NULL DEFAULT 0 COMMENT '采购中数量',
  `warning_amount` INT(10) NOT NULL DEFAULT 0 COMMENT '预警数量(设置为0则不预警)',
  `create_by` VARCHAR(64) COLLATE utf8_bin NOT NULL COMMENT '创建者ID',
  `create_by_name` VARCHAR(64) COLLATE utf8_bin NOT NULL COMMENT '创建者名称',
  `create_date` DATETIME NOT NULL COMMENT '创建时间',
  `update_by` VARCHAR(64) COLLATE utf8_bin NOT NULL COMMENT '更新者ID',
  `update_by_name` VARCHAR(64) COLLATE utf8_bin NOT NULL COMMENT '更新者名称',
  `update_date` DATETIME NOT NULL COMMENT '更新时间',
  `remarks` VARCHAR(255) COLLATE utf8_bin DEFAULT NULL COMMENT '备注信息',
  `del_flag` CHAR(1) COLLATE utf8_bin NOT NULL DEFAULT '0' COMMENT '删除标记(0:否,1:是)',
  PRIMARY KEY (`goods_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT '商品';

-- 供应商supplier
DROP TABLE IF EXISTS `biz_supplier`;
CREATE TABLE `biz_supplier`(
  `supplier_id` INT(11) NOT NULL AUTO_INCREMENT COMMENT '供应商ID',
  `supplier_name` VARCHAR(64) COLLATE utf8_bin NOT NULL COMMENT '供应商名称',
  `contact` VARCHAR(64) COLLATE utf8_bin NOT NULL DEFAULT '' COMMENT '联系人',
  `phone` VARCHAR(18) COLLATE utf8_bin NOT NULL DEFAULT '' COMMENT '联系电话',
  `address` VARCHAR(256) COLLATE utf8_bin COMMENT '地址',
  `create_by` VARCHAR(64) COLLATE utf8_bin NOT NULL COMMENT '创建者ID',
  `create_by_name` VARCHAR(64) COLLATE utf8_bin NOT NULL COMMENT '创建者名称',
  `create_date` DATETIME NOT NULL COMMENT '创建时间',
  `update_by` VARCHAR(64) COLLATE utf8_bin NOT NULL COMMENT '更新者ID',
  `update_by_name` VARCHAR(64) COLLATE utf8_bin NOT NULL COMMENT '更新者名称',
  `update_date` DATETIME NOT NULL COMMENT '更新时间',
  `remarks` VARCHAR(255) COLLATE utf8_bin DEFAULT NULL COMMENT '备注信息',
  `del_flag` CHAR(1) COLLATE utf8_bin NOT NULL DEFAULT '0' COMMENT '删除标记(0:否,1:是)',
  PRIMARY KEY (`supplier_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT '供应商';

-- 仓库storage
DROP TABLE IF EXISTS `biz_storage`;
CREATE TABLE `biz_storage`(
  `storage_id` INT(11) NOT NULL AUTO_INCREMENT COMMENT '仓库ID',
  `storage_name` VARCHAR(64) COLLATE utf8_bin NOT NULL COMMENT '仓库名称',
  `contact` VARCHAR(64) COLLATE utf8_bin NOT NULL DEFAULT '' COMMENT '联系人',
  `phone` VARCHAR(18) COLLATE utf8_bin NOT NULL DEFAULT '' COMMENT '联系电话',
  `type` TINYINT(1) NOT NULL DEFAULT '0' COMMENT '仓库类型',
  `province` VARCHAR(10) NOT NULL DEFAULT '' COMMENT '省',
  `city` VARCHAR(10) NOT NULL DEFAULT '' COMMENT '市',
  `district` VARCHAR(10) NOT NULL DEFAULT '' COMMENT '区',
  `address` VARCHAR(256) COLLATE utf8_bin COMMENT '地址',
  `create_by` VARCHAR(64) COLLATE utf8_bin NOT NULL COMMENT '创建者ID',
  `create_by_name` VARCHAR(64) COLLATE utf8_bin NOT NULL COMMENT '创建者名称',
  `create_date` DATETIME NOT NULL COMMENT '创建时间',
  `update_by` VARCHAR(64) COLLATE utf8_bin NOT NULL COMMENT '更新者ID',
  `update_by_name` VARCHAR(64) COLLATE utf8_bin NOT NULL COMMENT '更新者名称',
  `update_date` DATETIME NOT NULL COMMENT '更新时间',
  `remarks` VARCHAR(255) COLLATE utf8_bin DEFAULT NULL COMMENT '备注信息',
  `del_flag` CHAR(1) COLLATE utf8_bin NOT NULL DEFAULT '0' COMMENT '删除标记(0:否,1:是)',
  PRIMARY KEY (`storage_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT '仓库';

-- 入库单stock_in
DROP TABLE IF EXISTS `biz_stock_in`;
CREATE TABLE `biz_stock_in`(
  `stock_in_id` VARCHAR(20) COLLATE utf8_bin NOT NULL COMMENT '入库单ID',
  `stock_in_code` VARCHAR(24) COLLATE utf8_bin NOT NULL COMMENT '入库单编号',
  `stock_in_date` DATE DEFAULT '1970-01-01' COMMENT '入库日期',
  `create_by` VARCHAR(64) COLLATE utf8_bin NOT NULL COMMENT '创建者ID',
  `create_by_name` VARCHAR(64) COLLATE utf8_bin NOT NULL COMMENT '创建者名称',
  `create_date` DATETIME NOT NULL COMMENT '创建时间',
  `update_by` VARCHAR(64) COLLATE utf8_bin NOT NULL COMMENT '更新者ID',
  `update_by_name` VARCHAR(64) COLLATE utf8_bin NOT NULL COMMENT '更新者名称',
  `update_date` DATETIME NOT NULL COMMENT '更新时间',
  `remarks` VARCHAR(255) COLLATE utf8_bin DEFAULT NULL COMMENT '备注信息',
  `del_flag` CHAR(1) COLLATE utf8_bin NOT NULL DEFAULT '0' COMMENT '删除标记(0:否,1:是)',
  PRIMARY KEY (`stock_in_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT '入库单';

-- 入库明细stock_in_detail
DROP TABLE IF EXISTS `biz_stock_in_detail`;
CREATE TABLE `biz_stock_in_detail`(
  `stock_in_detail_id` VARCHAR(20) COLLATE utf8_bin NOT NULL COMMENT '入库明细ID',
  `stock_in_id` VARCHAR(20) COLLATE utf8_bin NOT NULL COMMENT '所属入库单',
  `goods_id` VARCHAR(20) COLLATE utf8_bin NOT NULL COMMENT '商品ID',
  `goods_code` VARCHAR(24) COLLATE utf8_bin NOT NULL COMMENT '商品编号',
  `goods_name` VARCHAR(64) COLLATE utf8_bin NOT NULL COMMENT '商品名称',
  `goodes_category_id` VARCHAR(20) COLLATE utf8_bin NOT NULL COMMENT '商品类型',
  `expert_amount` INT(10) NOT NULL NULL DEFAULT 1 COMMENT '申请采购数量',
  `actual_amount` INT(10) NOT NULL NULL DEFAULT 0 COMMENT '实际入库数量',
  `different_amount` INT(10) NOT NULL DEFAULT 0 COMMENT '相差数量',
  `supplier_id` VARCHAR(20) COLLATE utf8_bin NOT NULL DEFAULT '' COMMENT '供应商',
  `create_by` VARCHAR(64) COLLATE utf8_bin NOT NULL COMMENT '创建者ID',
  `create_by_name` VARCHAR(64) COLLATE utf8_bin NOT NULL COMMENT '创建者名称',
  `create_date` DATETIME NOT NULL COMMENT '创建时间',
  `update_by` VARCHAR(64) COLLATE utf8_bin NOT NULL COMMENT '更新者ID',
  `update_by_name` VARCHAR(64) COLLATE utf8_bin NOT NULL COMMENT '更新者名称',
  `update_date` DATETIME NOT NULL COMMENT '更新时间',
  `remarks` VARCHAR(255) COLLATE utf8_bin DEFAULT NULL COMMENT '备注信息',
  `del_flag` CHAR(1) COLLATE utf8_bin NOT NULL DEFAULT '0' COMMENT '删除标记(0:否,1:是)',
  PRIMARY KEY (`stock_in_detail_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT '入库明细';

-- 出库单stock_out

-- 库存调拨单stock_transfer

-- 订单order

-- 客户