/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.sys.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.thinkgem.jeesite.common.config.Global;
import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.web.BaseController;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.modules.sys.entity.SysAutoname;
import com.thinkgem.jeesite.modules.sys.service.SysAutonameService;

/**
 * 自动编码Controller
 * @author xuchen
 * @version 2018-08-29
 */
@Controller
@RequestMapping(value = "${adminPath}/sys/sysAutoname")
public class SysAutonameController extends BaseController {

	@Autowired
	private SysAutonameService sysAutonameService;
	
	@ModelAttribute
	public SysAutoname get(@RequestParam(required=false) String id) {
		SysAutoname entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = sysAutonameService.get(id);
		}
		if (entity == null){
			entity = new SysAutoname();
		}
		return entity;
	}
	
	@RequiresPermissions("sys:sysAutoname:view")
	@RequestMapping(value = {"list", ""})
	public String list(SysAutoname sysAutoname, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<SysAutoname> page = sysAutonameService.findPage(new Page<SysAutoname>(request, response), sysAutoname); 
		model.addAttribute("page", page);
		return "modules/sys/sysAutonameList";
	}

	@RequiresPermissions("sys:sysAutoname:view")
	@RequestMapping(value = "form")
	public String form(SysAutoname sysAutoname, Model model) {
		model.addAttribute("sysAutoname", sysAutoname);
		return "modules/sys/sysAutonameForm";
	}

	@RequiresPermissions("sys:sysAutoname:edit")
	@RequestMapping(value = "save")
	public String save(SysAutoname sysAutoname, Model model, RedirectAttributes redirectAttributes) {
		if (!beanValidator(model, sysAutoname)){
			return form(sysAutoname, model);
		}
		sysAutonameService.save(sysAutoname);
		addMessage(redirectAttributes, "保存自动编码成功");
		return "redirect:"+Global.getAdminPath()+"/sys/sysAutoname/?repage";
	}
	
	@RequiresPermissions("sys:sysAutoname:edit")
	@RequestMapping(value = "delete")
	public String delete(SysAutoname sysAutoname, RedirectAttributes redirectAttributes) {
		sysAutonameService.delete(sysAutoname);
		addMessage(redirectAttributes, "删除自动编码成功");
		return "redirect:"+Global.getAdminPath()+"/sys/sysAutoname/?repage";
	}


}