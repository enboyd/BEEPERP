/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.sys.dao;

import com.thinkgem.jeesite.common.persistence.CrudDao;
import com.thinkgem.jeesite.common.persistence.annotation.MyBatisDao;
import com.thinkgem.jeesite.modules.sys.entity.SysAutoname;
import org.apache.ibatis.annotations.Param;

/**
 * 自动编码DAO接口
 * @author xuchen
 * @version 2018-08-29
 */
@MyBatisDao
public interface SysAutonameDao extends CrudDao<SysAutoname> {

    SysAutoname getAutoNameByTable(@Param("tableName") String tableName);

    void resetNum();
}