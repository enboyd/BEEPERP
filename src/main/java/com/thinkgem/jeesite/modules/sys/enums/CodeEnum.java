package com.thinkgem.jeesite.modules.sys.enums;

public interface CodeEnum<T> {

    T getCode();
}
