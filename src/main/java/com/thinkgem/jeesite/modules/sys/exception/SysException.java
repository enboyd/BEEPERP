package com.thinkgem.jeesite.modules.sys.exception;

import com.thinkgem.jeesite.modules.sys.enums.ResultEnum;
import lombok.Getter;

@Getter
public class SysException extends RuntimeException {

    private Integer code;

    public SysException(ResultEnum resultEnum) {
        super(resultEnum.getMsg());
        this.code = resultEnum.getCode();
    }

    public SysException(Integer code, String msg) {
        super(msg);
        this.code = code;
    }
}
