/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.sys.entity;

import org.hibernate.validator.constraints.Length;

import com.thinkgem.jeesite.common.persistence.DataEntity;
import org.omg.PortableInterceptor.INACTIVE;

/**
 * 自动编码Entity
 * @author xuchen
 * @version 2018-08-29
 */
public class SysAutoname extends DataEntity<SysAutoname> {
	
	private static final long serialVersionUID = 1L;
	private String tableName;		// 表名
	private String logogramName;		// 简写名
	private Integer autoNum;		// 自增数
	
	public SysAutoname() {
		super();
	}

	public SysAutoname(String id){
		super(id);
	}

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}
	
	public String getLogogramName() {
		return logogramName;
	}

	public void setLogogramName(String logogramName) {
		this.logogramName = logogramName;
	}
	
	public Integer getAutoNum() {
		return autoNum;
	}

	public void setAutoNum(Integer autoNum) {
		this.autoNum = autoNum;
	}
	
}