/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.sys.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.service.CrudService;
import com.thinkgem.jeesite.modules.sys.entity.SysAutoname;
import com.thinkgem.jeesite.modules.sys.dao.SysAutonameDao;

/**
 * 自动编码Service
 * @author xuchen
 * @version 2018-08-29
 */
@Service
@Transactional(readOnly = true)
public class SysAutonameService extends CrudService<SysAutonameDao, SysAutoname> {

	@Autowired
	private SysAutonameDao sysAutonameDao;

	public SysAutoname get(String id) {
		return super.get(id);
	}
	
	public List<SysAutoname> findList(SysAutoname sysAutoname) {
		return super.findList(sysAutoname);
	}
	
	public Page<SysAutoname> findPage(Page<SysAutoname> page, SysAutoname sysAutoname) {
		return super.findPage(page, sysAutoname);
	}
	
	@Transactional(readOnly = false)
	public void save(SysAutoname sysAutoname) {
		super.save(sysAutoname);
	}
	
	@Transactional(readOnly = false)
	public void delete(SysAutoname sysAutoname) {
		super.delete(sysAutoname);
	}

	public SysAutoname getAutoNameByTable(String tableName) {
		return sysAutonameDao.getAutoNameByTable(tableName);
	}

	@Transactional(readOnly = false)
	public void resetNum() {
		sysAutonameDao.resetNum();
	}
}