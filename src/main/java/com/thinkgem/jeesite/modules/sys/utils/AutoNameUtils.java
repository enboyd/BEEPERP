package com.thinkgem.jeesite.modules.sys.utils;

import com.thinkgem.jeesite.common.utils.DateUtils;
import com.thinkgem.jeesite.common.utils.ObjectUtils;
import com.thinkgem.jeesite.modules.sys.entity.SysAutoname;
import com.thinkgem.jeesite.modules.sys.service.SysAutonameService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 自动编码工具类
 * @author : xuchen
 * @date : 2018/8/29 10:21
 */
public class AutoNameUtils {

    /*@Autowired
    private static SysAutonameService autoNameService;*/

    /**
     * 生成对应表的编码--普通表
     * xuchen
     * @param tableName
     * @return
     */
    public static String createAutoNameByTableName(String tableName){
        SysAutonameService autonameService=(SysAutonameService) ObjectUtils.getObject("sysAutonameService");
        //自动编码实体类
        SysAutoname autoName = autonameService.getAutoNameByTable(tableName);
        //最终生成的编码
        StringBuffer sb = new StringBuffer();
        if(autoName != null && StringUtils.isNotEmpty(autoName.getId())){
            //表简写
            String logogramName = autoName.getLogogramName();
            //自增数
            Integer autoNum = autoName.getAutoNum();
            if(autoNum == null){
                autoNum = 1;
            }else{
                autoNum += 1;
            }
            autoName.setAutoNum(autoNum);
            //当前日期--yyyyMMdd
            String date = DateUtils.getDate();
            String dateReplace = date.replace("-", "");
            if(StringUtils.isNotEmpty(logogramName) && null != autoNum && StringUtils.isNotEmpty(date)) {
                sb.append(logogramName).append(dateReplace);
                String autoNumStr = autoNum + "";
                while (autoNumStr.length() < 3) {
                    autoNumStr = "0" + autoNumStr;
                }
                sb.append(autoNumStr);
            }
            autonameService.save(autoName);
        }
        return sb.toString();
    }


    public static String createAutoNameByTableName1(String tableName){
        SysAutonameService autonameService=(SysAutonameService) ObjectUtils.getObject("sysAutonameService");
        //自动编码实体类
        SysAutoname autoName = autonameService.getAutoNameByTable(tableName);
        //最终生成的编码
        StringBuffer sb = new StringBuffer();
        if(autoName != null && StringUtils.isNotEmpty(autoName.getId())){

            //自增数
            Integer autoNum = autoName.getAutoNum();
            if(autoNum == null){
                autoNum = 1;
            }else{
                autoNum += 1;
            }
            autoName.setAutoNum(autoNum);
            //当前日期--yyyyMMdd
            String date = DateUtils.getDate();
            String dateReplace = date.replace("-", "");
            if(null != autoNum && StringUtils.isNotEmpty(date)) {
                sb.append(dateReplace);
                String autoNumStr = autoNum + "";
                while (autoNumStr.length() < 3) {
                    autoNumStr = "0" + autoNumStr;
                }
                sb.append(autoNumStr);
            }
            autonameService.save(autoName);
        }
        return sb.toString();
    }

    /**
     * 商品表生成编码
     * @param front 编码开头
     * @param tableName 表名
     * @return
     */
    public static String createAutoNameByTableName(String front, String tableName) {
        SysAutonameService autonameService=(SysAutonameService) ObjectUtils.getObject("sysAutonameService");
        //最终生成的编码
        StringBuffer sb = new StringBuffer();
        if(StringUtils.isNotEmpty(front) && StringUtils.isNotEmpty(tableName)){
            //自动编码实体类
            SysAutoname autoName = autonameService.getAutoNameByTable(tableName);
            if(autoName != null && StringUtils.isNotEmpty(autoName.getId())){
                //自增数
                Integer autoNum = autoName.getAutoNum();
                if(autoNum == null){
                    autoNum = 1;
                }else{
                    autoNum += 1;
                }
                autoName.setAutoNum(autoNum);
                if(null != autoNum) {
                    sb.append(front);
                    String autoNumStr = autoNum + "";
                    while (autoNumStr.length() < 4) {
                        autoNumStr = "0" + autoNumStr;
                    }
                    sb.append(autoNumStr);
                }
                autonameService.save(autoName);
            }
        }
        return sb.toString();
    }
}
