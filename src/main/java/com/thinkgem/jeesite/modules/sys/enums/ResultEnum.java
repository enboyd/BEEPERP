package com.thinkgem.jeesite.modules.sys.enums;

import lombok.*;

/**
 * Description:
 * Author: LTY
 * Create Time: 2018-08-09 14:03
 */
@Getter
@NoArgsConstructor
@AllArgsConstructor
public enum ResultEnum {

    SUCCESS(0, "成功"),
    ERROR(1, "异常"),

    UN_LOGIN(9,"暂未登陆，请重新登陆"),
    LOGIN_SUCCESS(10, "登录成功"),
    LOGIN_FAIL(11, "登录失败"),
    LOGIN_FAIL_UNAUTHORIZED_OPENID(12, "登录失败，未授权的微信号"),
    LOGIN_FAIL_UNREGISTERED_ACCOUNT(13, "登录失败，未注册的账号"),
    LOGIN_FAIL_WRONG_PASSWORD(14, "登录失败，登录密码错误"),
    API_AUTH_FAIL(15, "接口授权认证失败"),
    API_REQUEST_EXPIRED(16, "接口请求过期"),
    API_REQUIRED_PARAM_LOST(17, "接口验证必要参数缺失"),
    API_SIGN_VALIDATE_ERROR(18, "接口签名验证失败"),
    API_PARAM_FORMAT_ERROR(19, "接口参数格式错误"),

    LOGOUT_SUCCESS(20, "登出成功"),

    JSON_FORMAT_ERROR(31, "JSON格式错误"),

    BARCODE_NUMBER(41, "托盘上商品数量不足"),
    BARCODE_INFO(42, "暂无此条码，请重新扫描！"),

    FORM_VALIDATE_PARAM_ERROR(61, "表单验证参数有误"),

    STORAGE_NOT_ALLOW(70,"21区的商品不允许出库"),

    PRODUCT_NOT_EXIST(80, "商品不存在"),
    PRODUCT_STOCK_ERROR(81, "库存不足"),
    PRODUCT_STATUS_ERROR(82, "商品状态错误"),
    PRODUCT_CATEGORY_ERROR(83, "不是当前商品，请进行确认"),

    ORDER_NOT_EXIST(92, "订单不存在"),
    ORDER_DETAILS_NOT_EXIST(93, "订单明细不存在"),
    ORDER_STATUS_ERROR(94, "订单状态错误"),
    ORDER_UPDATE_FAIL(95, "订单更新失败"),

    ORDER_DETAILED_BEYOND(96, "该商品的出库数已大于实际出库数"),
    ORDER_DETAILED_INSUFFICIENT(97, "有未出库的商品，请仔细检查"),

    ORDER_DETAIL_ERROR(101, "订单详情错误"),

    ;


    private Integer code;

    private String msg;
}
