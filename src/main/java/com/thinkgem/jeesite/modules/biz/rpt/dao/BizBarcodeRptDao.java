package com.thinkgem.jeesite.modules.biz.rpt.dao;

import com.thinkgem.jeesite.common.persistence.CrudDao;
import com.thinkgem.jeesite.common.persistence.annotation.MyBatisDao;
import com.thinkgem.jeesite.modules.biz.rpt.entity.BizBarcodeRpt;
import com.thinkgem.jeesite.modules.biz.rpt.entity.BizGoodsRpt;

import java.util.List;

/**
 * Created by Administrator on 2018/10/15.
 */
@MyBatisDao
public interface BizBarcodeRptDao extends CrudDao<BizBarcodeRpt> {

    List<BizBarcodeRpt> findListAll(BizBarcodeRpt bizBarcodeRpt);

    List<BizGoodsRpt> selectByGoodsCode(BizGoodsRpt bizGoodsRpt);

    Integer selectByGoodsCodeCount(BizGoodsRpt bizGoodsRpt);
}
