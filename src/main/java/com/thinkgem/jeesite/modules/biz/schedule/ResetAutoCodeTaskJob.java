package com.thinkgem.jeesite.modules.biz.schedule;

import com.thinkgem.jeesite.common.utils.ObjectUtils;
import com.thinkgem.jeesite.modules.sys.service.SysAutonameService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

/**
 * 重置自动生成编码的自增数
 * @author : xuchen
 * @date : 2018/8/28 17:47
 */
@Service
@Lazy(false)
public class ResetAutoCodeTaskJob {

    @SuppressWarnings("unchecked")
    @Scheduled(cron = "0 50 14 * * ?")//每天凌晨1点整
    public void job1() {
        SysAutonameService sysAutonameService=(SysAutonameService) ObjectUtils.getObject("sysAutonameService");
        sysAutonameService.resetNum();
    }

}
