/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.biz.dao;

import com.thinkgem.jeesite.common.persistence.CrudDao;
import com.thinkgem.jeesite.common.persistence.annotation.MyBatisDao;
import com.thinkgem.jeesite.modules.biz.entity.BizStockLog;
import com.thinkgem.jeesite.modules.biz.vo.BizStockLogVo;

import java.util.List;

/**
 * 期出期入的logDAO接口
 * @author tiancheng
 * @version 2018-09-13
 */
@MyBatisDao
public interface BizStockLogDao extends CrudDao<BizStockLog> {

    List<BizStockLogVo> findListApi(BizStockLog bizStockLog);
}