/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.biz.service;

import java.util.Date;
import java.util.List;

import com.thinkgem.jeesite.common.utils.IdGen;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.modules.biz.entity.BizBuyingRequisitionDetail;
import com.thinkgem.jeesite.modules.biz.entity.BizGoods;
import com.thinkgem.jeesite.modules.biz.entity.BizPurchaseDetail;
import com.thinkgem.jeesite.modules.sys.entity.User;
import com.thinkgem.jeesite.modules.sys.utils.AutoNameUtils;
import com.thinkgem.jeesite.modules.sys.utils.UserUtils;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.service.CrudService;
import com.thinkgem.jeesite.modules.biz.entity.BizBuyingRequisition;
import com.thinkgem.jeesite.modules.biz.dao.BizBuyingRequisitionDao;

/**
 * 请购单的controllerService
 * @author xuchen
 * @version 2018-07-28
 */
@Service
@Transactional(readOnly = true)
public class BizBuyingRequisitionService extends CrudService<BizBuyingRequisitionDao, BizBuyingRequisition> {

	@Autowired
	private BizBuyingRequisitionDao bizBuyingRequisitionDao;
	@Autowired
	private BizBuyingRequisitionDetailService bizBuyingRequisitionDetailService;
	@Autowired
	private BizGoodsService bizGoodsService;

	public BizBuyingRequisition get(String id) {
		return super.get(id);
	}
	
	public List<BizBuyingRequisition> findList(BizBuyingRequisition bizBuyingRequisition) {
		return super.findList(bizBuyingRequisition);
	}
	
	public Page<BizBuyingRequisition> findPage(Page<BizBuyingRequisition> page, BizBuyingRequisition bizBuyingRequisition) {
		return super.findPage(page,bizBuyingRequisition);
		/*List<BizBuyingRequisition> list = bizBuyingRequisitionDao.findList(bizBuyingRequisition);
		bizBuyingRequisition.setPage(page);
		page.setList(list);
		return page;*/
	}
	
	@Transactional(readOnly = false)
	public void save(BizBuyingRequisition bizBuyingRequisition) {
		String buyingId = "";
		if(null == bizBuyingRequisition.getBuyingId() || bizBuyingRequisition.getBuyingId().equals("")){
			buyingId = IdGen.getUniqueKey();
			bizBuyingRequisition.setBuyingId(buyingId);
			//bizBuyingRequisition.setBuyingCode(IdGen.getUniqueCode("QG",""));
			String buyingAutoCode = AutoNameUtils.createAutoNameByTableName("biz_buying_requisition");
			bizBuyingRequisition.setBuyingCode(buyingAutoCode);
			bizBuyingRequisition.setBuyingDate(new Date());
			bizBuyingRequisition.setStatus("5");
			bizBuyingRequisition.setCreateBy(UserUtils.getUser());
			bizBuyingRequisition.setCreateDate(new Date());
			bizBuyingRequisition.setUpdateBy(UserUtils.getUser());
			bizBuyingRequisition.setUpdateDate(new Date());
/*			//添加操作日志
			bizPurchaseLogService.addBizBuyingRequisitionLog("10",buyingId);*/
			bizBuyingRequisitionDao.insert(bizBuyingRequisition);
		}else{
			buyingId = bizBuyingRequisition.getBuyingId();
			User user = UserUtils.getUser();
			if (StringUtils.isNotBlank(user.getId())){
				bizBuyingRequisition.setUpdateBy(user);
			}
			bizBuyingRequisition.setUpdateDate(new Date());
			bizBuyingRequisitionDao.update(bizBuyingRequisition);
		}
		//保存请购单明细(判断是不是修改，是修改的话，先删除再添加)
		BizBuyingRequisitionDetail bizBuyingRequisitionDetail  = new BizBuyingRequisitionDetail();
		bizBuyingRequisitionDetail.setBuyingId(buyingId);
		//当前请购单下的采购明细
		List<BizBuyingRequisitionDetail> buyingRequisitionDetails = bizBuyingRequisitionDetailService.findList(bizBuyingRequisitionDetail);
		if(null != buyingRequisitionDetails && buyingRequisitionDetails.size() > 0){
			//删除当前请购单下的采购明细
			bizBuyingRequisitionDetailService.deleteByBuyingId(buyingId);
		}
		//获取页面上的请购单明细
		List<BizBuyingRequisitionDetail> bizBuyingRequisitionList = bizBuyingRequisition.getBizBuyingRequisitionList();
		if(bizBuyingRequisitionList != null){
			for (BizBuyingRequisitionDetail buyingRequisitionDetail:bizBuyingRequisitionList) {
				//获取商品的信息
				//BizGoods bizGoods = bizGoodsService.get(buyingRequisitionDetail.getGoodsId());
				//前台页面删除行的时候将采购单的delFlag 设置为1
				if (null != buyingRequisitionDetail.getDelFlag() && buyingRequisitionDetail.getDelFlag().equals("0")) {
					buyingRequisitionDetail.setOutExpertAmount(0);
					buyingRequisitionDetail.setSurplusExpertAmount(buyingRequisitionDetail.getExpertAmount());
					buyingRequisitionDetail.setBuyingDetailId(IdGen.getUniqueKey());
					buyingRequisitionDetail.setBuyingId(buyingId);
					buyingRequisitionDetail.setSubmitOutExpertAmount(0);
					bizBuyingRequisitionDetailService.save(buyingRequisitionDetail);
				}
			}
		}

	}


	@Transactional(readOnly = false)
	public void delete(BizBuyingRequisition bizBuyingRequisition) {
		super.delete(bizBuyingRequisition);
	}

	/**
	 * 审批请购单
	 * @param bizBuyingId
	 */
	@Transactional(readOnly = false)
	public void toExamine(String bizBuyingId) {
		bizBuyingRequisitionDao.toExamine(bizBuyingId);
	}

	/**
	 * 驳回请购单
	 * @param bizBuyingId
	 */
	@Transactional(readOnly = false)
	public void reject(String bizBuyingId) {
		bizBuyingRequisitionDao.reject(bizBuyingId);
	}

	/**
	 * 重新提交请购单
	 * @param bizBuyingId
	 */
	@Transactional(readOnly = false)
	public void resubmit(String bizBuyingId) {
		bizBuyingRequisitionDao.resubmit(bizBuyingId);
	}

	/*
	 * 提交审批
	 */
	@Transactional(readOnly = false)
	public void submit(String buyingId) {
		bizBuyingRequisitionDao.submit(buyingId);
	}
}