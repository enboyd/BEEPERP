/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.biz.entity;

import com.thinkgem.jeesite.common.utils.excel.annotation.ExcelField;
import org.hibernate.validator.constraints.Length;
import com.thinkgem.jeesite.modules.sys.entity.User;
import javax.validation.constraints.NotNull;

import com.thinkgem.jeesite.common.persistence.DataEntity;

import java.util.Date;

/**
 * 采购列表单明细的controllerEntity
 * @author tiancheng
 * @version 2018-07-13
 */
public class BizPurchaseDetail extends DataEntity<BizPurchaseDetail> {
	
	private static final long serialVersionUID = 1L;
	private String purchaseDetailId;		// 采购明细ID
	private String purchaseId;		// 所属采购单
	private String goodsId;		// 商品ID
	private String buyingDetailId;   // 请购单的id
	private String goodsCode;		// 商品编号
	private String goodsName;		// 商品名称
	private String goodsBrandId;		// 商品品牌
	private String goodsCategoryId;		// 商品类型
	private String unitPrice;		// 采购单价
	private String expertAmount;		// 申请采购数量 -- 采购数量
	private String actualAmount;		// 实际入库数量
	private String differentAmount;		// 相差数量
	private String totalPrice;		// 金额
	private String supplierId;		// 供应商
	private String supplierName; //供应商name
	private String goodsBrandName;		// 商品品牌
	private String goodsCategoryName;		// 商品类型
	private String stockAmount; //库存数量
	private String purchasingAmount; //采购中数量
	private String warningAmount; //预警数量
	private Integer requisitionAmount;		//请购数量
	private String specification;		// 规格
	private String unit;	//单位
	private Integer minPurchaseQuantity;	//最小起购数量
	private String taxRate;	//税率
	private Date demandTime;		// 需求时间

	/*
		导出使用字段
	 */
	private Integer num;	//序号
	private String purchaseType;	//业务类型
	private String purchaseCode;	//采购单编号
	private Date purchaseDate;	//采购日期
	private String createName;	//制单人


	@ExcelField(title="规格型号", align=1, sort=80)
	public String getSpecification() {
		return specification;
	}

	public void setSpecification(String specification) {
		this.specification = specification;
	}

	@ExcelField(title="主计量", align=1, sort=90)
	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public Integer getRequisitionAmount() {
		return requisitionAmount;
	}

	public void setRequisitionAmount(Integer requisitionAmount) {
		this.requisitionAmount = requisitionAmount;
	}

	public BizPurchaseDetail() {
		super();
	}

	public BizPurchaseDetail(String id){
		super(id);
	}

	@Length(min=1, max=20, message="采购明细ID长度必须介于 1 和 20 之间")
	public String getPurchaseDetailId() {
		return purchaseDetailId;
	}

	public void setPurchaseDetailId(String purchaseDetailId) {
		this.purchaseDetailId = purchaseDetailId;
	}
	
	@Length(min=1, max=20, message="所属采购单长度必须介于 1 和 20 之间")
	public String getPurchaseId() {
		return purchaseId;
	}

	public void setPurchaseId(String purchaseId) {
		this.purchaseId = purchaseId;
	}
	
	@Length(min=1, max=20, message="商品ID长度必须介于 1 和 20 之间")
	public String getGoodsId() {
		return goodsId;
	}

	public void setGoodsId(String goodsId) {
		this.goodsId = goodsId;
	}

	@Length(min=1, max=24, message="商品编号长度必须介于 1 和 24 之间")
	@ExcelField(title="存货编码", align=1, sort=60)
	public String getGoodsCode() {
		return goodsCode;
	}

	public void setGoodsCode(String goodsCode) {
		this.goodsCode = goodsCode;
	}

	@Length(min=1, max=64, message="商品名称长度必须介于 1 和 64 之间")
	@ExcelField(title="存货名称", align=1, sort=70)
	public String getGoodsName() {
		return goodsName;
	}

	public void setGoodsName(String goodsName) {
		this.goodsName = goodsName;
	}

	@Length(min=1, max=20, message="商品类型长度必须介于 1 和 20 之间")
	public String getGoodsCategoryId() {
		return goodsCategoryId;
	}

	public void setGoodsCategoryId(String goodsCategoryId) {
		this.goodsCategoryId = goodsCategoryId;
	}

	@ExcelField(title="采购单价", align=1, sort=110)
	public String getUnitPrice() {
		return unitPrice;
	}

	public void setUnitPrice(String unitPrice) {
		this.unitPrice = unitPrice;
	}
	
	@Length(min=0, max=10, message="申请采购数量长度必须介于 0 和 10 之间")
	@ExcelField(title="数量", align=1, sort=100)
	public String getExpertAmount() {
		return expertAmount;
	}

	public void setExpertAmount(String expertAmount) {
		this.expertAmount = expertAmount;
	}
	
	@Length(min=0, max=10, message="实际入库数量长度必须介于 0 和 10 之间")
	public String getActualAmount() {
		return actualAmount;
	}

	public void setActualAmount(String actualAmount) {
		this.actualAmount = actualAmount;
	}
	
	@Length(min=1, max=10, message="相差数量长度必须介于 1 和 10 之间")
	public String getDifferentAmount() {
		return differentAmount;
	}

	public void setDifferentAmount(String differentAmount) {
		this.differentAmount = differentAmount;
	}

	@ExcelField(title="金额", align=1, sort=130)
	public String getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(String totalPrice) {
		this.totalPrice = totalPrice;
	}
	
	@Length(min=1, max=20, message="供应商长度必须介于 1 和 20 之间")
	public String getSupplierId() {
		return supplierId;
	}

	public void setSupplierId(String supplierId) {
		this.supplierId = supplierId;
	}

	public String getGoodsBrandId() {
		return goodsBrandId;
	}

	public void setGoodsBrandId(String goodsBrandId) {
		this.goodsBrandId = goodsBrandId;
	}

	public String getGoodsBrandName() {
		return goodsBrandName;
	}

	public void setGoodsBrandName(String goodsBrandName) {
		this.goodsBrandName = goodsBrandName;
	}

	public String getGoodsCategoryName() {
		return goodsCategoryName;
	}

	public void setGoodsCategoryName(String goodsCategoryName) {
		this.goodsCategoryName = goodsCategoryName;
	}

	public String getStockAmount() {
		return stockAmount;
	}

	public void setStockAmount(String stockAmount) {
		this.stockAmount = stockAmount;
	}

	public String getPurchasingAmount() {
		return purchasingAmount;
	}

	public void setPurchasingAmount(String purchasingAmount) {
		this.purchasingAmount = purchasingAmount;
	}

	@ExcelField(title="供应商", align=1, sort=50)
	public String getSupplierName() {
		return supplierName;
	}

	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}

	public String getWarningAmount() {
		return warningAmount;
	}

	public void setWarningAmount(String warningAmount) {
		this.warningAmount = warningAmount;
	}

	public String getBuyingDetailId() {
		return buyingDetailId;
	}

	public void setBuyingDetailId(String buyingDetailId) {
		this.buyingDetailId = buyingDetailId;
	}

	public Integer getMinPurchaseQuantity() {
		return minPurchaseQuantity;
	}

	public void setMinPurchaseQuantity(Integer minPurchaseQuantity) {
		this.minPurchaseQuantity = minPurchaseQuantity;
	}

	@ExcelField(title="税率", align=1, sort=120)
	public String getTaxRate() {
		return taxRate;
	}

	public void setTaxRate(String taxRate) {
		this.taxRate = taxRate;
	}

	@ExcelField(title="序号", align=1, sort=10)
	public Integer getNum() {
		return num;
	}

	public void setNum(Integer num) {
		this.num = num;
	}

	@ExcelField(title="业务类型", align=1, dictType = "biz_purchase_type", sort=20)
	public String getPurchaseType() {
		return purchaseType;
	}

	public void setPurchaseType(String purchaseType) {
		this.purchaseType = purchaseType;
	}

	@ExcelField(title="采购单编号", align=1, sort=30)
	public String getPurchaseCode() {
		return purchaseCode;
	}

	public void setPurchaseCode(String purchaseCode) {
		this.purchaseCode = purchaseCode;
	}

	@ExcelField(title="日期", align=1, sort=40)
	public Date getPurchaseDate() {
		return purchaseDate;
	}

	public void setPurchaseDate(Date purchaseDate) {
		this.purchaseDate = purchaseDate;
	}

	@ExcelField(title="制单人", align=1, sort=140)
	public String getCreateName() {
		return createName;
	}

	public void setCreateName(String createName) {
		this.createName = createName;
	}

	public Date getDemandTime() {
		return demandTime;
	}

	public void setDemandTime(Date demandTime) {
		this.demandTime = demandTime;
	}
}