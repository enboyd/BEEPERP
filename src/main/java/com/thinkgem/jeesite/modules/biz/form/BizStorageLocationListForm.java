/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.biz.form;

import lombok.Data;

/**
 * 库位管理Entity
 * @author xuchen
 * @version 2018-07-17
 */
@Data
public class BizStorageLocationListForm {

	private String type;
}