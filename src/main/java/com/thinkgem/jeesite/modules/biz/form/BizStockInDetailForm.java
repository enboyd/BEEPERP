package com.thinkgem.jeesite.modules.biz.form;

import lombok.Data;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * Description:
 * Author: LTY
 * Create Time: 2018-08-09 17:09
 */
@Data
public class BizStockInDetailForm {

    @NotEmpty(message = "采购单明细ID必填")
    private String purchaseDetailId;

    @NotEmpty(message = "生产日期必填")
    private String manufactureDate;

    private String expirationDate;

    private String storageLocationId;//仓库库位的id

    @NotEmpty(message = "实际入库数量必填")
    private String actualAmount;

    private String remarks;  //采购单明细入库时备注


    // 手机端传的多余的参数
    private String purchaseId;
    private String goodsId;
    private String goodsName;
    private String expertAmount;
    private String differentAmount;
    private String goodsCode;
    private String type;
}
