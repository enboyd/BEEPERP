/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.biz.dao;

import com.thinkgem.jeesite.common.persistence.CrudDao;
import com.thinkgem.jeesite.common.persistence.annotation.MyBatisDao;
import com.thinkgem.jeesite.modules.biz.entity.BizOrderdetail;
import com.thinkgem.jeesite.modules.biz.vo.BizOrderDetailVo;

import java.util.List;
import java.util.Map;

/**
 * 订单DAO接口
 * @author tiancheng
 * @version 2018-09-17
 */
@MyBatisDao
public interface BizOrderdetailDao extends CrudDao<BizOrderdetail> {


    List<BizOrderDetailVo> findListApi(BizOrderdetail bizOrderdetail);

    List<String> findListByIsDeliver(BizOrderdetail  bizOrderdetail);

    Integer deleteByOrderId(String orderId);

    List<BizOrderdetail> findListByOrderId(String orderId);

    List<BizOrderdetail> findListReceive(BizOrderdetail bizOrderdetail);
}