/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.biz.service;

import java.util.Date;
import java.util.List;

import com.thinkgem.jeesite.common.utils.IdGen;
import com.thinkgem.jeesite.modules.biz.entity.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.service.CrudService;
import com.thinkgem.jeesite.modules.biz.dao.BizStockInDetailDao;

/**
 * 入库单明细的ControllerService
 * @author tiancheng
 * @version 2018-07-16
 */
@Service
@Transactional(readOnly = true)
public class BizStockInDetailService extends CrudService<BizStockInDetailDao, BizStockInDetail> {

	@Autowired
	private BizStockInService BizStockInService;
	@Autowired
	private BizGoodsService bizGoodsService;
	@Autowired
	private BizPurchaseDetailService bizPurchaseDetailService;
	@Autowired
	private BizPurchaseService bizPurchaseService;
	@Autowired
	private BizStockInDetailDao bizStockInDetailDao;

	public BizStockInDetail get(String id) {
		return super.get(id);
	}
	
	public List<BizStockInDetail> findList(BizStockInDetail bizStockInDetail) {
		return super.findList(bizStockInDetail);
	}
	
	public Page<BizStockInDetail> findPage(Page<BizStockInDetail> page, BizStockInDetail bizStockInDetail) {
		return super.findPage(page, bizStockInDetail);
	}

	public List<BizStockInDetail> findListNo(BizStockInDetail bizStockInDetail){
		return bizStockInDetailDao.findListNo(bizStockInDetail);
	}

	@Transactional(readOnly = false)
	public void save(BizStockInDetail bizStockInDetail) {
		super.save(bizStockInDetail);
	}
	
	@Transactional(readOnly = false)
	public void delete(BizStockInDetail bizStockInDetail) {
		super.delete(bizStockInDetail);
	}

	
	 /**
	  * 添加入库单和明细
	  * @author tiancheng  
	  * @date 2018/7/16 15:37
	  * @param bizPurchase 采购单的信息
	  * @return   
	  */  
	@Transactional(readOnly = false)
	public void warehousing(BizPurchase bizPurchase){
		//添加入库单
		String stockInId = IdGen.uuid();
		BizStockIn bizStockIn = new BizStockIn();
		bizStockIn.setStockInId(stockInId);
		bizStockIn.setStockInCode(IdGen.getUniqueCode("RK",""));
		bizStockIn.setPurchaseId(bizPurchase.getPurchaseId());
		bizStockIn.setStockInDate(new Date());
		bizStockIn.setRemarks(bizPurchase.getRemarks());
		BizStockInService.save(bizStockIn);
		//定义一个开光，判断商品是否全部到货
		boolean a = true;
		//入库单明细对应的采购单明细
		List<BizPurchaseDetail> BizPurchaseDetailList = bizPurchase.getPurchaseDetailList();
		for (BizPurchaseDetail bizPurchaseDetail:BizPurchaseDetailList) {
			if(Integer.valueOf(bizPurchaseDetail.getDifferentAmount()) > 0){
				a = false;
			}
			//添加商品的库存和采购中的数量
			BizGoods goods = bizGoodsService.get(bizPurchaseDetail.getGoodsId());
			Integer num = goods.getStockAmount() + Integer.valueOf(bizPurchaseDetail.getActualAmount());
			goods.setStockAmount(num);//库存数量
			Integer purchasingAmount = Integer.valueOf(goods.getPurchasingAmount())-Integer.valueOf(bizPurchaseDetail.getActualAmount());
			goods.setPurchasingAmount(purchasingAmount.toString());//采购中数量
			bizGoodsService.updateInfo(goods);

			//修改采购单明细
			BizPurchaseDetail bizPurchaseDetailAll = bizPurchaseDetailService.get(bizPurchaseDetail.getPurchaseDetailId());
			bizPurchaseDetailAll.setDifferentAmount(bizPurchaseDetail.getDifferentAmount());//相差数量
			//bizPurchaseDetail.getActualAmount() 这是入库单的实际入库数
			Integer sum = Integer.valueOf(bizPurchaseDetailAll.getActualAmount()) + Integer.valueOf(bizPurchaseDetail.getActualAmount());
			bizPurchaseDetailAll.setActualAmount(sum.toString());
			bizPurchaseDetailService.updateAmount(bizPurchaseDetailAll);

			//添加入库单明细
			BizStockInDetail bizStockInDetail = new BizStockInDetail();
			bizStockInDetail.setStockInId(stockInId);
			bizStockInDetail.setStockInDetailId(IdGen.getUniqueKey());
			bizStockInDetail.setPurchaseDetailId(bizPurchaseDetail.getPurchaseDetailId());
			bizStockInDetail.setGoodsId(bizPurchaseDetail.getGoodsId());
			bizStockInDetail.setActualAmount(bizPurchaseDetail.getActualAmount());
			super.save(bizStockInDetail);
		}

		//修改采购单的状态 (11,部分入库)
		if(a){
			bizPurchase.setStatus("12");
		}else{
			bizPurchase.setStatus("11");
		}
		bizPurchaseService.updateStatus(bizPurchase);
	}


	@Transactional(readOnly = false)
    public void updateActualAmount(BizStockInDetail bizStockInDetail) {
		bizStockInDetailDao.updateActualAmount(bizStockInDetail);
    }


	 /**
	  * 查询入库单还有没有待分配托盘的入库单明细
	  * @author tiancheng  
	  * @date 2018/9/6 17:48
	  * @param
	  * @return   
	  */  
	public Integer selectCountByStockInId(String stockInId){
		return bizStockInDetailDao.selectCountByStockInId(stockInId);
	}

	/**
	 * 查询入库单明细Page
	 * @param bizStockInDetailPage
	 * @param bizStockIn
	 * @return
	 */
    public Page<BizStockInDetail> findPageByParent(Page<BizStockInDetail> bizStockInDetailPage, BizStockIn bizStockIn) {
		BizStockInDetail bizStockInDetail = new BizStockInDetail();
		bizStockInDetail.setPage(bizStockInDetailPage);
		return bizStockInDetailPage.setList(bizStockInDetailDao.findListByParent(bizStockIn));
	}

	/**
	 * 查询入库单明细List
	 * @param bizStockIn
	 * @return
	 */
	public List<BizStockInDetail> findListByParent(BizStockIn bizStockIn) {
		return bizStockInDetailDao.findListByParent(bizStockIn);
	}
}