package com.thinkgem.jeesite.modules.biz.schedule;

import com.thinkgem.jeesite.common.config.Global;
import com.thinkgem.jeesite.common.utils.DateUtils;
import com.thinkgem.jeesite.common.utils.FileUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;

/**
 * 删除昨天的单一的条形码
 */
@Service
@Lazy(false)
public class RemoveTxm {

    @Scheduled(cron = "0 0 0 * * ?")//每天凌晨1点整
    public void removeTxmFile(){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = DateUtils.getDateBefore(new Date(),1);
        String exportDir = FileUtils.path(Global.getUserfilesBaseDir() + Global.USERFILES_BASE_URL + "1/files/exportFiles/" + simpleDateFormat.format(date) + "/");
        // String exportDir = FileUtils.path(Global.getUserfilesBaseDir() + Global.USERFILES_BASE_URL + "1/files/exportFiles/2018-09-11/");

        //判断是否存在次文件夹
        File file = new File(exportDir);
        if (file.exists()) {
            LinkedList<File> list = new LinkedList<File>();
            File[] files = file.listFiles();
            for (File file2 : files) {
                if (!file2.isDirectory()) {
                    //每天删除单一的条形码
                    if(file2.getAbsolutePath().indexOf("TXM") != -1){
                        FileUtils.delFile(file2.getAbsolutePath());
                    }
                }
            }
        }

    }
}
