package com.thinkgem.jeesite.modules.biz.rpt.entity;

import com.thinkgem.jeesite.common.persistence.DataEntity;
import com.thinkgem.jeesite.common.utils.excel.annotation.ExcelField;
import lombok.Data;

import java.util.Date;

/**
 * 期初入库信息
 * Created by xuchen on 2018/10/17.
 */
public class BizInitialStorage extends DataEntity<BizInitialStorage> {

    private String stockInId; //入库单ID
    private String stockInCode;  //单号(入库单编号)
    private Date inDate;   //入库日期
    private Date stockInDateStart; // 入库日期开始时间
    private Date stockInDateEnd; // 入库日期结束时间
    private String barId; //条码Id
    private String barcode; //条码编号
    private String goodsId; //存货id
    private String goodsCode; //存货编码
    private String goodsName; //存货名称
    private String batch;   //批次
    private Integer number;     //数量
    private String storageLocId;    //货位Id
    private String storageLocName;    //货位名称
    private String operatorId;  //操作人Id
    private String operatorName;  //操作人Name
    private String remarksType; //导出备注
    private String classifyId;	//商品分类ID
    private String classifyName;	//商品分类Name

    public String getStockInId() {
        return stockInId;
    }

    public void setStockInId(String stockInId) {
        this.stockInId = stockInId;
    }

    @ExcelField(title="单号", align=1, sort=1)
    public String getStockInCode() {
        return stockInCode;
    }

    public void setStockInCode(String stockInCode) {
        this.stockInCode = stockInCode;
    }

    @ExcelField(title="入库日期", align=1, sort=2)
    public Date getInDate() {
        return inDate;
    }

    public void setInDate(Date inDate) {
        this.inDate = inDate;
    }

    public Date getStockInDateStart() {
        return stockInDateStart;
    }

    public void setStockInDateStart(Date stockInDateStart) {
        this.stockInDateStart = stockInDateStart;
    }

    public Date getStockInDateEnd() {
        return stockInDateEnd;
    }

    public void setStockInDateEnd(Date stockInDateEnd) {
        this.stockInDateEnd = stockInDateEnd;
    }

    public String getBarId() {
        return barId;
    }

    public void setBarId(String barId) {
        this.barId = barId;
    }

    @ExcelField(title="条码号", align=1, sort=3)
    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public String getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(String goodsId) {
        this.goodsId = goodsId;
    }

    @ExcelField(title="存货编码", align=1, sort=4)
    public String getGoodsCode() {
        return goodsCode;
    }

    public void setGoodsCode(String goodsCode) {
        this.goodsCode = goodsCode;
    }

    @ExcelField(title="存货名称", align=1, sort=5)
    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    @ExcelField(title="批次", align=1, sort=6)
    public String getBatch() {
        return batch;
    }

    public void setBatch(String batch) {
        this.batch = batch;
    }

    @ExcelField(title="数量", align=1, sort=7)
    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public String getStorageLocId() {
        return storageLocId;
    }

    public void setStorageLocId(String storageLocId) {
        this.storageLocId = storageLocId;
    }

    @ExcelField(title="货位", align=1, sort=8)
    public String getStorageLocName() {
        return storageLocName;
    }

    public void setStorageLocName(String storageLocName) {
        this.storageLocName = storageLocName;
    }

    public String getOperatorId() {
        return operatorId;
    }

    public void setOperatorId(String operatorId) {
        this.operatorId = operatorId;
    }

    @ExcelField(title="操作人", align=1, sort=9)
    public String getOperatorName() {
        return operatorName;
    }

    public void setOperatorName(String operatorName) {
        this.operatorName = operatorName;
    }

    @ExcelField(title="备注", align=1, sort=10)
    public String getRemarksType() {
        return "期初入库";
    }

    public void setRemarksType(String remarksType) {
        this.remarksType = remarksType;
    }

    public String getClassifyId() {
        return classifyId;
    }

    public void setClassifyId(String classifyId) {
        this.classifyId = classifyId;
    }

    public String getClassifyName() {
        return classifyName;
    }

    public void setClassifyName(String classifyName) {
        this.classifyName = classifyName;
    }
}
