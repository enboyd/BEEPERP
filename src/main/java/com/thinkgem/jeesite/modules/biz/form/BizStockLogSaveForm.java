package com.thinkgem.jeesite.modules.biz.form;

import lombok.Data;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * Created by Administrator on 2018/9/13.
 */
@Data
public class BizStockLogSaveForm {

    //@NotEmpty(message = "条码编号必填")
    private String barcode;

    //@NotEmpty(message = "商品编号必填")
    private String goodsCode;

    @NotEmpty(message = "数量必填")
    private String number;

    @NotEmpty(message = "操作类型明细必填")
    private String typeInfo;   //类型明细 （1，酱料的期出，2，标配的期出，31，酱料扫码的期入，32，酱料编号的期入，4，标配的期入）

    @NotEmpty(message = "操作人id必填")
    private String userId;

    //@NotEmpty(message = "库位必填")
    private String storageLocationId;

    //@NotEmpty(message = "生产日期必填")
    private String manufactureDate;
}
