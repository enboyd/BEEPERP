/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.biz.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.thinkgem.jeesite.modules.biz.dao.BizOrderDao;
import com.thinkgem.jeesite.modules.biz.dao.BizOrderdetailDao;
import com.thinkgem.jeesite.modules.biz.entity.BizBarcode;
import com.thinkgem.jeesite.modules.biz.entity.BizOrder;
import com.thinkgem.jeesite.modules.biz.entity.BizOrderdetail;
import com.thinkgem.jeesite.modules.biz.form.BizBarcodeLogForm;
import com.thinkgem.jeesite.modules.biz.vo.ResultVO;
import com.thinkgem.jeesite.modules.sys.entity.User;
import com.thinkgem.jeesite.modules.sys.enums.ResultEnum;
import com.thinkgem.jeesite.modules.sys.utils.ResultVOUtil;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.service.CrudService;
import com.thinkgem.jeesite.modules.biz.entity.BizBarcodeLog;
import com.thinkgem.jeesite.modules.biz.dao.BizBarcodeLogDao;

/**
 * 出库日志Service
 * @author tiancheng
 * @version 2018-09-19
 */
@Service
@Transactional(readOnly = true)
public class BizBarcodeLogService extends CrudService<BizBarcodeLogDao, BizBarcodeLog> {

	@Autowired
	private BizBarcodeService bizBarcodeService;
	@Autowired
	private BizBarcodeLogDao bizBarcodeLogDao;
	@Autowired
	private BizOrderdetailDao bizOrderdetailDao;

	public BizBarcodeLog get(String id) {
		return super.get(id);
	}
	
	public List<BizBarcodeLog> findList(BizBarcodeLog bizBarcodeLog) {
		return super.findList(bizBarcodeLog);
	}
	
	public Page<BizBarcodeLog> findPage(Page<BizBarcodeLog> page, BizBarcodeLog bizBarcodeLog) {
		return super.findPage(page, bizBarcodeLog);
	}
	
	@Transactional(readOnly = false)
	public ResultVO save(BizBarcodeLogForm bizBarcodeLogForm) {
		//参数传递
		BizBarcodeLog bizBarcodeLog = new BizBarcodeLog();
		User user = new User();
		user.setId(bizBarcodeLogForm.getUserId());
		bizBarcodeLog.setCreateBy(user);
		BeanUtils.copyProperties(bizBarcodeLogForm,bizBarcodeLog);
		//返回要该商品要出库的数量
		Integer sum = 0;
		//查询该订单该商品的明细
		BizOrderdetail bizOrderdetail = new BizOrderdetail();
		bizOrderdetail.setOutboundOrderPid(bizBarcodeLogForm.getOrderId());
		bizOrderdetail.setProjectdetailId(bizBarcodeLogForm.getGoodsId());
		List<BizOrderdetail> orderdetailList = bizOrderdetailDao.findList(bizOrderdetail);

		//查询是否是二次扫码
		List<BizBarcodeLog> bizBarcodeLogList = bizBarcodeLogDao.findList(bizBarcodeLog);
		//获取条码的信息
		BizBarcode bizBarcode = bizBarcodeService.get(bizBarcodeLogForm.getBarcodeId());
		if(null == bizBarcodeLogList || bizBarcodeLogList.size() == 0){
			//不是二次扫码
			//获取到订单明细
			bizOrderdetail = orderdetailList.get(0);
			//查询当前的出库单下的该商品即将出库数
			sum = bizBarcodeLogDao.selectSumByOrderIdAndGoodsId(bizBarcodeLog);
			sum = (null == sum)?0:sum;
			//判断是否超出了要发货的数量
			sum += Integer.valueOf(bizBarcodeLogForm.getNumber());
			if(bizOrderdetail.getQuantity() < sum){
				return ResultVOUtil.error(ResultEnum.ORDER_DETAILED_BEYOND.getCode(),"该商品的出库数已大于实际出库数！");
			}
			if(null != bizBarcode){
				Integer wait = Integer.valueOf(bizBarcode.getWaitNumber()) + Integer.valueOf(bizBarcodeLogForm.getNumber());
				//判断托盘上的数量
				if(Integer.valueOf(bizBarcode.getSurplusNumber()) < Integer.valueOf(bizBarcodeLogForm.getNumber())){
					return ResultVOUtil.error(ResultEnum.BARCODE_NUMBER.getCode(),"托盘上商品数量不足");
				}
				Integer surplus = Integer.valueOf(bizBarcode.getSurplusNumber()) - Integer.valueOf(bizBarcodeLogForm.getNumber());
				bizBarcode.setWaitNumber(wait.toString());
				bizBarcode.setSurplusNumber(surplus.toString());
				bizBarcodeService.save(bizBarcode);
			}

		}else{
			//是二次扫码，将以前扫码的进行修改
			//获取日志信息
			bizBarcodeLog = bizBarcodeLogList.get(0);

			Integer number = Integer.valueOf(bizBarcodeLog.getNumber());  // 日志中的出库数量
			Integer chu = Integer.valueOf(bizBarcodeLogForm.getNumber()); // 实际出库数量

			if(null != bizBarcode){
				//修改条码上的待出库数和剩余数
				Integer waitNumber = Integer.valueOf(bizBarcode.getWaitNumber());
				Integer surplusNumber = Integer.valueOf(bizBarcode.getSurplusNumber());

				if(chu > surplusNumber){
					return ResultVOUtil.error(ResultEnum.BARCODE_NUMBER.getCode(),"托盘上商品数量不足");
				}

				if(chu > number){
					waitNumber = waitNumber + (chu - number);
					surplusNumber = surplusNumber - (chu - number);
				}else{
					waitNumber = waitNumber - (number - chu);
					surplusNumber = surplusNumber + (number - chu);
				}
				bizBarcode.setWaitNumber(waitNumber.toString());
				bizBarcode.setSurplusNumber(surplusNumber.toString());

				bizBarcodeService.save(bizBarcode);
			}
			//重复扫码的情况下，计算已出库数
			//获取到订单明细
			bizOrderdetail = orderdetailList.get(0);
			//查询当前的出库单下的该商品即将出库数
			sum = bizBarcodeLogDao.selectSumByOrderIdAndGoodsId(bizBarcodeLog);
			sum = sum - number + chu;
			//判断是否超出了要发货的数量
			if(bizOrderdetail.getQuantity() < sum){
				return ResultVOUtil.error(ResultEnum.ORDER_DETAILED_BEYOND.getCode(),"当前发货数已大于需要发货的数量！");
			}
		}
		//修改实际出库的数量
		bizBarcodeLog.setNumber(bizBarcodeLogForm.getNumber());
		//添加商品的信息
		bizBarcodeLog.setGoodsId(bizBarcodeLogForm.getGoodsId());
		super.save(bizBarcodeLog);


		Map map = new HashMap();
		map.put("sum",sum);
		return ResultVOUtil.success(map);
	}
	
	@Transactional(readOnly = false)
	public void delete(BizBarcodeLog bizBarcodeLog) {
		super.delete(bizBarcodeLog);
	}


	public List<BizBarcodeLog> getAllByGoodsIdAndOrderId(BizBarcodeLog bizBarcodeLog) {
		return bizBarcodeLogDao.getAllByGoodsIdAndOrderId(bizBarcodeLog);
	}
}