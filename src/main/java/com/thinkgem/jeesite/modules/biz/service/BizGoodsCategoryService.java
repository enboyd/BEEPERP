/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.biz.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.thinkgem.jeesite.common.service.TreeService;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.modules.biz.entity.BizGoodsCategory;
import com.thinkgem.jeesite.modules.biz.dao.BizGoodsCategoryDao;

/**
 * 产品类型Service
 * @author xuchen
 * @version 2018-08-28
 */
@Service
@Transactional(readOnly = true)
public class BizGoodsCategoryService extends TreeService<BizGoodsCategoryDao, BizGoodsCategory> {

	@Autowired
	private BizGoodsCategoryDao bizGoodsCategoryDao;

	public BizGoodsCategory get(String id) {
		return super.get(id);
	}
	
	public List<BizGoodsCategory> findList(BizGoodsCategory bizGoodsCategory) {
		if (StringUtils.isNotBlank(bizGoodsCategory.getParentIds())){
			bizGoodsCategory.setParentIds(","+bizGoodsCategory.getParentIds()+",");
		}
		return super.findList(bizGoodsCategory);
	}
	
	@Transactional(readOnly = false)
	public void save(BizGoodsCategory bizGoodsCategory) {
		super.save(bizGoodsCategory);
	}
	
	@Transactional(readOnly = false)
	public void delete(BizGoodsCategory bizGoodsCategory) {
		super.delete(bizGoodsCategory);
	}


}