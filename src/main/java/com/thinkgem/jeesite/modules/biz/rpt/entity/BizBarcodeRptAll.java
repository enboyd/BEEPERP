package com.thinkgem.jeesite.modules.biz.rpt.entity;

import com.thinkgem.jeesite.common.persistence.DataEntity;
import com.thinkgem.jeesite.common.utils.excel.annotation.ExcelField;
import lombok.Data;

/**
 * Created by Administrator on 2018/10/15.
 */
public class BizBarcodeRptAll extends DataEntity<BizBarcodeRptAll> {

    private String number;

    private String goodsName;

    private String goodsCode;

    private String specification;

    private String unit;

    private String storageLocationName;

    private Integer sum;

    private String manufactureDate;

    private String pici;

    private String type; // 1,现存量报表  2，货位现存量报表

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    @ExcelField(title="商品名称", align=1, sort=25)
    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    @ExcelField(title="商品编号", align=1, sort=20)
    public String getGoodsCode() {
        return goodsCode;
    }

    public void setGoodsCode(String goodsCode) {
        this.goodsCode = goodsCode;
    }

    @ExcelField(title="规格型号", align=1, sort=30)
    public String getSpecification() {
        return specification;
    }

    public void setSpecification(String specification) {
        this.specification = specification;
    }

    @ExcelField(title="单位", align=1, sort=35)
    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getStorageLocationName() {
        return storageLocationName;
    }

    public void setStorageLocationName(String storageLocationName) {
        this.storageLocationName = storageLocationName;
    }

    @ExcelField(title="数量", align=1, sort=40)
    public Integer getSum() {
        return sum;
    }

    public void setSum(Integer sum) {
        this.sum = sum;
    }

    public String getManufactureDate() {
        return manufactureDate;
    }

    public void setManufactureDate(String manufactureDate) {
        this.manufactureDate = manufactureDate;
    }

    public String getPici() {
        return pici;
    }

    public void setPici(String pici) {
        this.pici = pici;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
