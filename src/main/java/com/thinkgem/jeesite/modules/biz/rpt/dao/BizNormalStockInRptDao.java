package com.thinkgem.jeesite.modules.biz.rpt.dao;

import com.thinkgem.jeesite.common.persistence.CrudDao;
import com.thinkgem.jeesite.common.persistence.annotation.MyBatisDao;
import com.thinkgem.jeesite.modules.biz.rpt.entity.BizNormalStockInRpt;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

/**
 * Created by xuchen on 2018/10/23.
 */
@MyBatisDao
public interface BizNormalStockInRptDao extends CrudDao<BizNormalStockInRpt> {

    List<BizNormalStockInRpt> findAllListByDate(@Param("stockInDateStart") Date stockInDateStart,
                                                @Param("stockInDateEnd") Date stockInDateEnd,
                                                @Param("goodsCode") String goodsCode,
                                                @Param("classifyId") String classifyId);
}
