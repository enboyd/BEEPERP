/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.biz.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.thinkgem.jeesite.common.service.TreeService;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.modules.biz.entity.BizGoodsClassify;
import com.thinkgem.jeesite.modules.biz.dao.BizGoodsClassifyDao;

/**
 * 商品分类Service
 * @author xuchen
 * @version 2018-10-12
 */
@Service
@Transactional(readOnly = true)
public class BizGoodsClassifyService extends TreeService<BizGoodsClassifyDao, BizGoodsClassify> {

	@Autowired
	private BizGoodsClassifyDao bizGoodsClassify;

	public BizGoodsClassify get(String id) {
		return super.get(id);
	}
	
	public List<BizGoodsClassify> findList(BizGoodsClassify bizGoodsClassify) {
		if (StringUtils.isNotBlank(bizGoodsClassify.getParentIds())){
			bizGoodsClassify.setParentIds(","+bizGoodsClassify.getParentIds()+",");
		}
		return super.findList(bizGoodsClassify);
	}
	
	@Transactional(readOnly = false)
	public void save(BizGoodsClassify bizGoodsClassify) {
		super.save(bizGoodsClassify);
	}
	
	@Transactional(readOnly = false)
	public void delete(BizGoodsClassify bizGoodsClassify) {
		super.delete(bizGoodsClassify);
	}

	/**
	 * 获取所有父级商品分类
	 * @return
	 */
	public List<BizGoodsClassify> findAllParentList() {
		return bizGoodsClassify.findAllParentList();
	}

	/**
	 * 根据父级Id查询所有子级
	 * @param pid
	 * @return
	 */
	public List<BizGoodsClassify> findChildListByPId(String pid) {
		return bizGoodsClassify.findChildListByPId(pid);
	}
}