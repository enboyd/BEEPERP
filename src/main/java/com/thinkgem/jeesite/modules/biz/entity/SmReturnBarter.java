/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.biz.entity;

import java.util.List;

import com.google.common.collect.Lists;
import com.thinkgem.jeesite.common.persistence.DataEntity;

/**
 * 退换货Entity
 * @author xuchen
 * @version 2018-09-10
 */
public class SmReturnBarter extends DataEntity<SmReturnBarter> {
	
	private static final long serialVersionUID = 1L;
	private String name;		// 编码
	private String category;		// 分类(退/换)
	private String goodsType;		// 退货类型
	private String customerId;		// 客户id
	private String customerName;		// 客户名字
	private String phone;		// 手机号
	private String address;		// 收货地址
	private String auditStatus;		// 审核状态
	private String deliverType;		// 发货方式
	private List<SmReturnBarterDetail> smReturnDetailList = Lists.newArrayList();		// 退货商品
	private List<SmReturnBarterDetail> smBarterDetailList = Lists.newArrayList();		// 换货商品
	public SmReturnBarter() {
		super();
	}

	public SmReturnBarter(String id){
		super(id);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}
	
	public String getGoodsType() {
		return goodsType;
	}

	public void setGoodsType(String goodsType) {
		this.goodsType = goodsType;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}
	
	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}
	
	public String getAuditStatus() {
		return auditStatus;
	}

	public void setAuditStatus(String auditStatus) {
		this.auditStatus = auditStatus;
	}

	public List<SmReturnBarterDetail> getSmReturnDetailList() {
		return smReturnDetailList;
	}

	public void setSmReturnDetailList(List<SmReturnBarterDetail> smReturnDetailList) {
		this.smReturnDetailList = smReturnDetailList;
	}

	public List<SmReturnBarterDetail> getSmBarterDetailList() {
		return smBarterDetailList;
	}

	public void setSmBarterDetailList(List<SmReturnBarterDetail> smBarterDetailList) {
		this.smBarterDetailList = smBarterDetailList;
	}

	public String getDeliverType() {
		return deliverType;
	}

	public void setDeliverType(String deliverType) {
		this.deliverType = deliverType;
	}
}