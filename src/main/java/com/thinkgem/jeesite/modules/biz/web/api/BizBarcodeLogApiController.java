/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.biz.web.api;

import com.thinkgem.jeesite.common.web.BaseController;
import com.thinkgem.jeesite.modules.biz.form.BizBarcodeLogForm;
import com.thinkgem.jeesite.modules.biz.service.BizBarcodeLogService;
import com.thinkgem.jeesite.modules.biz.vo.ResultVO;
import com.thinkgem.jeesite.modules.sys.enums.ResultEnum;
import com.thinkgem.jeesite.modules.sys.service.SystemService;
import com.thinkgem.jeesite.modules.sys.utils.ResultVOUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

/**
 * 出库日志Controller
 * @author tiancheng
 * @version 2018-09-19
 */
@Controller
@Slf4j
@RequestMapping(value = "${adminPath}/api/biz/bizBarcodeLog")
public class BizBarcodeLogApiController extends BaseController {

    @Autowired
    private SystemService systemService;
    @Autowired
    private BizBarcodeLogService bizBarcodeLogService;


     /**
      * 添加出库日志
      * @author tiancheng  
      * @date 2018/9/19 11:15
      * @param
      * @return   
      */  
    @RequestMapping(value = "save", method = RequestMethod.POST)
    @ResponseBody
    public ResultVO get(@Valid BizBarcodeLogForm bizBarcodeLogForm, BindingResult bindingResult, HttpServletRequest request, HttpServletResponse response) {

        if (bindingResult.hasErrors()) {
            log.error("【添加出库日志失败】表单校验参数不正确：bizBarcodeLogForm => {}", bizBarcodeLogForm);
            return ResultVOUtil.error(ResultEnum.FORM_VALIDATE_PARAM_ERROR.getCode(),
                    bindingResult.getFieldError().getDefaultMessage());
        }

        ResultVO resultVO = systemService.checkSign(request, BizBarcodeLogForm.class, bizBarcodeLogForm);

        if (resultVO.getCode() == 0) {
            return bizBarcodeLogService.save(bizBarcodeLogForm);
        }
        return resultVO;
    }

}