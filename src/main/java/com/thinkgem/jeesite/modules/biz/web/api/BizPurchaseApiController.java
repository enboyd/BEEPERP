/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.biz.web.api;

import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.common.web.BaseController;
import com.thinkgem.jeesite.modules.biz.form.BizPurchaseGetFrom;
import com.thinkgem.jeesite.modules.biz.form.BizPurchaseListFrom;
import com.thinkgem.jeesite.modules.biz.service.BizPurchaseService;
import com.thinkgem.jeesite.modules.biz.vo.BizPurchaseVo;
import com.thinkgem.jeesite.modules.biz.vo.ResultVO;
import com.thinkgem.jeesite.modules.sys.enums.ResultEnum;
import com.thinkgem.jeesite.modules.sys.service.SystemService;
import com.thinkgem.jeesite.modules.sys.utils.ResultVOUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 手机端请购单的controllerController
 * @author tiancheng
 * @version 2018-08-24
 */
@Controller
@Slf4j
@RequestMapping(value = "${adminPath}/api/biz/bizPurchase")
public class BizPurchaseApiController extends BaseController {

	@Autowired
	private BizPurchaseService bizPurchaseService;

	@Autowired
	private SystemService systemService;

	 /**
	  * 手机端查询采购单的列表
	  * @author tiancheng  
	  * @date 2018/8/25 14:39
	  * @param
	  * @return   
	  */  
	@RequestMapping(value = "list", method = RequestMethod.GET)
	@ResponseBody
	public ResultVO list(@Valid BizPurchaseListFrom bizPurchaseListFrom, BindingResult bindingResult, HttpServletRequest request) {

		if (bindingResult.hasErrors()) {
			log.error("【查询采购单列表异常】表单校验参数不正确：bizPurchaseFrom => {}", bizPurchaseListFrom);
			return ResultVOUtil.error(ResultEnum.FORM_VALIDATE_PARAM_ERROR.getCode(),
					bindingResult.getFieldError().getDefaultMessage());
		}

		ResultVO resultVO = systemService.checkSign(request, BizPurchaseListFrom.class, bizPurchaseListFrom);

		if (resultVO.getCode() == 0) {
			//返回的采购单的集合(审核通过)
			List<BizPurchaseVo> bizBuyingRequisitionList = bizPurchaseService.findListApi(bizPurchaseListFrom);
			Map map = new HashMap();
			map.put("bizBuyingRequisitionList",bizBuyingRequisitionList);
			return ResultVOUtil.success(map);
		}
		return resultVO;
	}

	/**
	 * 手机端查询采购单的详情
	 * @author tiancheng
	 * @date 2018/8/25 14:39
	 * @param
	 * @return
	 */
	@RequestMapping(value = "get", method = RequestMethod.GET)
	@ResponseBody
	public ResultVO get(@Valid BizPurchaseGetFrom bizPurchaseGetFrom, BindingResult bindingResult, HttpServletRequest request) {

		if (!StringUtils.isNotBlank(bizPurchaseGetFrom.getPurchaseId())) {
			log.error("【查询采购单详情异常】表单校验参数不正确：bizPurchaseFrom => {}", bizPurchaseGetFrom);
			return ResultVOUtil.error(ResultEnum.FORM_VALIDATE_PARAM_ERROR.getCode(),
					bindingResult.getFieldError().getDefaultMessage());
		}

		ResultVO resultVO = systemService.checkSign(request, BizPurchaseGetFrom.class, bizPurchaseGetFrom);

		if (resultVO.getCode() == 0) {
		//返回的采购单的集合
		Map map = bizPurchaseService.getApi(bizPurchaseGetFrom);
		return ResultVOUtil.success(map);
		}
		return resultVO;
	}


}