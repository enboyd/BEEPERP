package com.thinkgem.jeesite.modules.biz.form;

import lombok.Data;
import lombok.NonNull;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * Description:
 * Author: xuchen
 * Create Time: 2018-10-07
 */
@Data
public class BizGoodsAllListForm {
    /**
     * 表单中会传入后端什么字段，就写什么字段，一个都不要多写
     */
    private int pageSize;   //页大小
    private int pageNo;    //页码
    private String classifyId;	//分类(01是酱料，02是设备，03是餐具，04是宣传品)
    private String searchKey;   //搜索关键字

}
