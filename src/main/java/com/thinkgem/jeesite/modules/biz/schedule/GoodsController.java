package com.thinkgem.jeesite.modules.biz.schedule;

import com.thinkgem.jeesite.common.utils.ObjectUtils;
import com.thinkgem.jeesite.modules.biz.service.BizGoodsService;
import org.springframework.context.annotation.Lazy;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

/**
 * 每天商品的条码打印数量变为0
 */
@Service
@Lazy(false)
public class GoodsController {

    @Scheduled(cron = "0 0 0 * * ?")//每天凌晨1点整
    public void job1() {
        BizGoodsService bizGoodsService = (BizGoodsService) ObjectUtils.getObject("bizGoodsService");
        //每天清空day_num,变成0
        bizGoodsService.updateDayNum0();
    }
}
