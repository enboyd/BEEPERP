/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.biz.dao;

import com.thinkgem.jeesite.common.persistence.CrudDao;
import com.thinkgem.jeesite.common.persistence.annotation.MyBatisDao;
import com.thinkgem.jeesite.modules.biz.entity.SmReturnBarter;

/**
 * 退换货DAO接口
 * @author xuchen
 * @version 2018-09-10
 */
@MyBatisDao
public interface SmReturnBarterDao extends CrudDao<SmReturnBarter> {

	void submitAudit(SmReturnBarter smReturnBarter);

    void approve(SmReturnBarter smReturnBarter);

    void dismiss(SmReturnBarter smReturnBarter);

    void realDelete(SmReturnBarter smReturnBarter);
}