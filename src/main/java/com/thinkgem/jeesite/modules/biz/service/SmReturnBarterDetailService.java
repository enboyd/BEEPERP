/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.biz.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.service.CrudService;
import com.thinkgem.jeesite.modules.biz.entity.SmReturnBarterDetail;
import com.thinkgem.jeesite.modules.biz.dao.SmReturnBarterDetailDao;

/**
 * 退换货明细Service
 * @author xuchen
 * @version 2018-09-11
 */
@Service
@Transactional(readOnly = true)
public class SmReturnBarterDetailService extends CrudService<SmReturnBarterDetailDao, SmReturnBarterDetail> {

	public SmReturnBarterDetail get(String id) {
		return super.get(id);
	}
	
	public List<SmReturnBarterDetail> findList(SmReturnBarterDetail smReturnBarterDetail) {
		return super.findList(smReturnBarterDetail);
	}
	
	public Page<SmReturnBarterDetail> findPage(Page<SmReturnBarterDetail> page, SmReturnBarterDetail smReturnBarterDetail) {
		return super.findPage(page, smReturnBarterDetail);
	}
	
	@Transactional(readOnly = false)
	public void save(SmReturnBarterDetail smReturnBarterDetail) {
		super.save(smReturnBarterDetail);
	}
	
	@Transactional(readOnly = false)
	public void delete(SmReturnBarterDetail smReturnBarterDetail) {
		super.delete(smReturnBarterDetail);
	}
	
}