/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.biz.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.thinkgem.jeesite.modules.biz.entity.BizGoods;
import com.thinkgem.jeesite.modules.biz.service.BizPurchaseService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.thinkgem.jeesite.common.config.Global;
import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.web.BaseController;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.modules.biz.entity.BizPurchaseDetail;
import com.thinkgem.jeesite.modules.biz.service.BizPurchaseDetailService;

/**
 * 采购列表单明细的controllerController
 * @author tiancheng
 * @version 2018-07-13
 */
@Controller
@RequestMapping(value = "${adminPath}/biz/bizPurchaseDetail")
public class BizPurchaseDetailController extends BaseController {

	@Autowired
	private BizPurchaseDetailService bizPurchaseDetailService;
	@Autowired
	private BizPurchaseService bizPurchaseService;
	
	@ModelAttribute
	public BizPurchaseDetail get(@RequestParam(required=false) String id) {
		BizPurchaseDetail entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = bizPurchaseDetailService.get(id);
		}
		if (entity == null){
			entity = new BizPurchaseDetail();
		}
		return entity;
	}
	
	@RequiresPermissions("biz:bizPurchaseDetail:view")
	@RequestMapping(value = {"list", ""})
	public String list(BizPurchaseDetail bizPurchaseDetail, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<BizPurchaseDetail> page = bizPurchaseDetailService.findPage(new Page<BizPurchaseDetail>(request, response), bizPurchaseDetail); 
		model.addAttribute("page", page);
		return "modules/biz/bizPurchaseDetailList";
	}

	@RequiresPermissions("biz:bizPurchaseDetail:view")
	@RequestMapping(value = "form")
	public String form(BizPurchaseDetail bizPurchaseDetail, Model model) {
		model.addAttribute("bizPurchaseDetail", bizPurchaseDetail);
		return "modules/biz/bizPurchaseDetailForm";
	}

	@RequiresPermissions("biz:bizPurchaseDetail:edit")
	@RequestMapping(value = "save")
	public String save(BizPurchaseDetail bizPurchaseDetail, Model model, RedirectAttributes redirectAttributes) {
		if (!beanValidator(model, bizPurchaseDetail)){
			return form(bizPurchaseDetail, model);
		}
		bizPurchaseDetailService.save(bizPurchaseDetail);
		addMessage(redirectAttributes, "保存采购列表单明细成功");
		return "redirect:"+Global.getAdminPath()+"/biz/bizPurchaseDetail/?repage";
	}
	
	@RequiresPermissions("biz:bizPurchaseDetail:edit")
	@RequestMapping(value = "delete")
	public String delete(BizPurchaseDetail bizPurchaseDetail, RedirectAttributes redirectAttributes) {
		bizPurchaseDetailService.delete(bizPurchaseDetail);
		addMessage(redirectAttributes, "删除采购列表单明细成功");
		return "redirect:"+Global.getAdminPath()+"/biz/bizPurchaseDetail/?repage";
	}

}