/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.biz.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.thinkgem.jeesite.modules.biz.entity.BizBarcode;
import com.thinkgem.jeesite.modules.biz.entity.BizGoods;
import com.thinkgem.jeesite.modules.biz.service.BizBarcodeService;
import com.thinkgem.jeesite.modules.biz.service.BizGoodsService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.thinkgem.jeesite.common.config.Global;
import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.web.BaseController;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.modules.biz.entity.BizDiscardedGoods;
import com.thinkgem.jeesite.modules.biz.service.BizDiscardedGoodsService;

/**
 * 废弃商品Controller
 * @author xuchen
 * @version 2018-11-02
 */
@Controller
@RequestMapping(value = "${adminPath}/biz/bizDiscardedGoods")
public class BizDiscardedGoodsController extends BaseController {

	@Autowired
	private BizDiscardedGoodsService bizDiscardedGoodsService;
	@Autowired
	private BizGoodsService bizGoodsService;
	@Autowired
	private BizBarcodeService bizBarcodeService;
	
	@ModelAttribute
	public BizDiscardedGoods get(@RequestParam(required=false) String id) {
		BizDiscardedGoods entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = bizDiscardedGoodsService.get(id);
		}
		if (entity == null){
			entity = new BizDiscardedGoods();
		}
		return entity;
	}
	
	@RequiresPermissions("biz:bizDiscardedGoods:view")
	@RequestMapping(value = {"list", ""})
	public String list(BizDiscardedGoods bizDiscardedGoods, HttpServletRequest request, HttpServletResponse response, Model model) {
		String url = "";
		String type = request.getParameter("type");
		if(StringUtils.isNotEmpty(type)){
			if("1".equals(type)){
				//过期待废弃
				bizDiscardedGoods.setType("10");
				url = "modules/biz/bizDiscardedGoodsList2";
			}
		}else{
			//废弃
			bizDiscardedGoods.setStorageLocationId("452d92f95a4746cfbaa1cc0b63218a80");
			url = "modules/biz/bizDiscardedGoodsList";
		}
		Page<BizDiscardedGoods> page = bizDiscardedGoodsService.findPage(new Page<BizDiscardedGoods>(request, response), bizDiscardedGoods); 
		model.addAttribute("page", page);
		return url;
	}


	@ResponseBody
	@RequestMapping(value = "discarding")
	public String discarding(HttpServletRequest request, HttpServletResponse response, Model model) {

		String msg = "操作失败";
		boolean flag = false;

		String ids = request.getParameter("ids");
		if(StringUtils.isNotEmpty(ids)){
			String[] discardIdArray = ids.split(",");
			if(discardIdArray != null && discardIdArray.length > 0){
				for (String discardId : discardIdArray) {
					BizDiscardedGoods discardedGoods = bizDiscardedGoodsService.get(discardId);
					if(discardedGoods != null){
						Integer number = discardedGoods.getNumber();	//废弃数量
						//废弃
						discardedGoods.setType("20");	//过期废弃
						discardedGoods.setStorageLocationId("452d92f95a4746cfbaa1cc0b63218a80");	//移动到报废区
						discardedGoods.setStorageLocationName("报废区");
						//设置条码的实际库存和商品的实际库存
						String barcodeId = discardedGoods.getBarcodeId();
						BizBarcode bizBarcode = bizBarcodeService.get(barcodeId);
						String goodsCode = discardedGoods.getGoodsCode();
						BizGoods bizGoods = null;
						try {
							bizGoods = bizGoodsService.getByGoodsCode(goodsCode);
						} catch (Exception e) {
							e.printStackTrace();
						}
						if(bizBarcode != null){
							String bizBarcodeNumber = bizBarcode.getNumber();
							if(StringUtils.isNotEmpty(bizBarcodeNumber)){
								Integer surplusStock = Integer.valueOf(bizBarcodeNumber) - number;
								if(surplusStock != null && surplusStock == 0){
									bizBarcodeService.delete(bizBarcode);
									flag = true;
								}else{
									bizBarcode.setNumber(surplusStock+"");
									bizBarcodeService.save(bizBarcode);
									flag = true;
								}
							}
						}
						if(bizGoods != null){
							Integer stockAmount = bizGoods.getStockAmount();
							if(stockAmount != null){
								bizGoods.setStockAmount(stockAmount-number);
								bizGoodsService.update(bizGoods);
								flag = true;
							}
						}
						bizDiscardedGoodsService.update(discardedGoods);
					}
				}
			}
		}
		if(flag){
			msg = "操作成功!";
		}


		return msg;
	}
}