/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.biz.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.thinkgem.jeesite.common.config.Global;
import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.web.BaseController;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.modules.biz.entity.BizBuyingRequisitionDetail;
import com.thinkgem.jeesite.modules.biz.service.BizBuyingRequisitionDetailService;

/**
 * 请购单明细的controllerController
 * @author xuchen
 * @version 2018-07-28
 */
@Controller
@RequestMapping(value = "${adminPath}/biz/bizBuyingRequisitionDetail")
public class BizBuyingRequisitionDetailController extends BaseController {

	@Autowired
	private BizBuyingRequisitionDetailService bizBuyingRequisitionDetailService;
	
	@ModelAttribute
	public BizBuyingRequisitionDetail get(@RequestParam(required=false) String id) {
		BizBuyingRequisitionDetail entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = bizBuyingRequisitionDetailService.get(id);
		}
		if (entity == null){
			entity = new BizBuyingRequisitionDetail();
		}
		return entity;
	}
	
	@RequiresPermissions("biz:bizBuyingRequisitionDetail:view")
	@RequestMapping(value = {"list", ""})
	public String list(BizBuyingRequisitionDetail bizBuyingRequisitionDetail, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<BizBuyingRequisitionDetail> page = bizBuyingRequisitionDetailService.findPage(new Page<BizBuyingRequisitionDetail>(request, response), bizBuyingRequisitionDetail); 
		model.addAttribute("page", page);
		return "modules/biz/bizBuyingRequisitionDetailList";
	}

	@RequiresPermissions("biz:bizBuyingRequisitionDetail:view")
	@RequestMapping(value = "form")
	public String form(BizBuyingRequisitionDetail bizBuyingRequisitionDetail, Model model) {
		model.addAttribute("bizBuyingRequisitionDetail", bizBuyingRequisitionDetail);
		return "modules/biz/bizBuyingRequisitionDetailForm";
	}

	@RequiresPermissions("biz:bizBuyingRequisitionDetail:edit")
	@RequestMapping(value = "save")
	public String save(BizBuyingRequisitionDetail bizBuyingRequisitionDetail, Model model, RedirectAttributes redirectAttributes) {
		if (!beanValidator(model, bizBuyingRequisitionDetail)){
			return form(bizBuyingRequisitionDetail, model);
		}
		bizBuyingRequisitionDetailService.save(bizBuyingRequisitionDetail);
		addMessage(redirectAttributes, "保存请购单明细成功");
		return "redirect:"+Global.getAdminPath()+"/biz/bizBuyingRequisitionDetail/?repage";
	}
	
	@RequiresPermissions("biz:bizBuyingRequisitionDetail:edit")
	@RequestMapping(value = "delete")
	public String delete(BizBuyingRequisitionDetail bizBuyingRequisitionDetail, RedirectAttributes redirectAttributes) {
		bizBuyingRequisitionDetailService.delete(bizBuyingRequisitionDetail);
		addMessage(redirectAttributes, "删除请购单明细成功");
		return "redirect:"+Global.getAdminPath()+"/biz/bizBuyingRequisitionDetail/?repage";
	}

}