/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.biz.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.thinkgem.jeesite.common.utils.DateUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.thinkgem.jeesite.common.config.Global;
import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.web.BaseController;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.modules.biz.entity.BizBarcode;
import com.thinkgem.jeesite.modules.biz.service.BizBarcodeService;

import java.text.SimpleDateFormat;

/**
 * 条形码Controller
 * @author tiancheng
 * @version 2018-09-05
 */
@Controller
@RequestMapping(value = "${adminPath}/biz/bizBarcode")
public class BizBarcodeController extends BaseController {

	@Autowired
	private BizBarcodeService bizBarcodeService;
	
	@ModelAttribute
	public BizBarcode get(@RequestParam(required=false) String id) {
		BizBarcode entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = bizBarcodeService.get(id);
		}
		if (entity == null){
			entity = new BizBarcode();
		}
		return entity;
	}
	
	@RequiresPermissions("biz:bizBarcode:view")
	@RequestMapping(value = {"list", ""})
	public String list(BizBarcode bizBarcode, HttpServletRequest request, HttpServletResponse response, Model model) {
		//生产日期转换类型
		if(null != bizBarcode.getManufactureDateDa()){
			String date = DateUtils.getDateStr(bizBarcode.getManufactureDateDa());
			bizBarcode.setManufactureDate(date);
		}
		Page<BizBarcode> page = bizBarcodeService.findPage(new Page<BizBarcode>(request, response), bizBarcode); 
		model.addAttribute("page", page);
		return "modules/biz/bizBarcodeList";
	}

	@RequiresPermissions("biz:bizBarcode:view")
	@RequestMapping(value = "form")
	public String form(BizBarcode bizBarcode, Model model) {
		model.addAttribute("bizBarcode", bizBarcode);
		return "modules/biz/bizBarcodeForm";
	}

	@RequiresPermissions("biz:bizBarcode:edit")
	@RequestMapping(value = "save")
	public String save(BizBarcode bizBarcode, Model model, RedirectAttributes redirectAttributes) {
		if (!beanValidator(model, bizBarcode)){
			return form(bizBarcode, model);
		}
		bizBarcodeService.save(bizBarcode);
		addMessage(redirectAttributes, "保存条形码成功");
		return "redirect:"+Global.getAdminPath()+"/biz/bizBarcode/?repage";
	}
	
	
	 /**
	  * 删除条形码
	  * @author tiancheng  
	  * @date 2018/9/6 15:07
	  * @param
	  * @return   
	  */  
	@RequestMapping(value = "delete")
	@ResponseBody
	public String delete(String barcodeId) {
		BizBarcode bizBarcode = new BizBarcode();
		bizBarcode.setId(barcodeId);
		bizBarcodeService.delete(bizBarcode);
		return "success";
	}


	/**
	 * 条码打印次数增加
	 * @author xuchen
	 * @date 2018/12/03 14:45
	 * @param
	 * @return
	 */
	@RequestMapping(value = "printNumberAdd")
	@ResponseBody
	public String printNumberAdd(String barCode) {
		bizBarcodeService.printNumberAdd(barCode);
		return "success";
	}

}