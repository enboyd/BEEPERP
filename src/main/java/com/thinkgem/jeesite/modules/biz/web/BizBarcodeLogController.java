/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.biz.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.thinkgem.jeesite.common.config.Global;
import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.web.BaseController;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.modules.biz.entity.BizBarcodeLog;
import com.thinkgem.jeesite.modules.biz.service.BizBarcodeLogService;

/**
 * 出库日志Controller
 * @author tiancheng
 * @version 2018-09-19
 */
@Controller
@RequestMapping(value = "${adminPath}/biz/bizBarcodeLog")
public class BizBarcodeLogController extends BaseController {

	@Autowired
	private BizBarcodeLogService bizBarcodeLogService;
	
	@ModelAttribute
	public BizBarcodeLog get(@RequestParam(required=false) String id) {
		BizBarcodeLog entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = bizBarcodeLogService.get(id);
		}
		if (entity == null){
			entity = new BizBarcodeLog();
		}
		return entity;
	}
	
	@RequiresPermissions("biz:bizBarcodeLog:view")
	@RequestMapping(value = {"list", ""})
	public String list(BizBarcodeLog bizBarcodeLog, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<BizBarcodeLog> page = bizBarcodeLogService.findPage(new Page<BizBarcodeLog>(request, response), bizBarcodeLog); 
		model.addAttribute("page", page);
		return "modules/biz/bizBarcodeLogList";
	}

	@RequiresPermissions("biz:bizBarcodeLog:view")
	@RequestMapping(value = "form")
	public String form(BizBarcodeLog bizBarcodeLog, Model model) {
		model.addAttribute("bizBarcodeLog", bizBarcodeLog);
		return "modules/biz/bizBarcodeLogForm";
	}

	@RequiresPermissions("biz:bizBarcodeLog:edit")
	@RequestMapping(value = "save")
	public String save(BizBarcodeLog bizBarcodeLog, Model model, RedirectAttributes redirectAttributes) {
		if (!beanValidator(model, bizBarcodeLog)){
			return form(bizBarcodeLog, model);
		}
		bizBarcodeLogService.save(bizBarcodeLog);
		addMessage(redirectAttributes, "保存出库日志成功");
		return "redirect:"+Global.getAdminPath()+"/biz/bizBarcodeLog/?repage";
	}
	
	@RequiresPermissions("biz:bizBarcodeLog:edit")
	@RequestMapping(value = "delete")
	public String delete(BizBarcodeLog bizBarcodeLog, RedirectAttributes redirectAttributes) {
		bizBarcodeLogService.delete(bizBarcodeLog);
		addMessage(redirectAttributes, "删除出库日志成功");
		return "redirect:"+Global.getAdminPath()+"/biz/bizBarcodeLog/?repage";
	}

}