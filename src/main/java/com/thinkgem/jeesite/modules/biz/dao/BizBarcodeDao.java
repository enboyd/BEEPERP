/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.biz.dao;

import com.thinkgem.jeesite.common.persistence.CrudDao;
import com.thinkgem.jeesite.common.persistence.annotation.MyBatisDao;
import com.thinkgem.jeesite.modules.biz.entity.BizBarcode;
import com.thinkgem.jeesite.modules.biz.vo.BizBarcodeVO;

import java.util.List;
import java.util.Map;

/**
 * 条形码DAO接口
 * @author tiancheng
 * @version 2018-09-05
 */
@MyBatisDao
public interface BizBarcodeDao extends CrudDao<BizBarcode> {


    BizBarcodeVO getApiByName(String name);


    List<Map> findListInfo(BizBarcode bizBarcode);

    Map getInfo(BizBarcode bizBarcode);

    void updateStorageLocationId(BizBarcode bizBarcode);

    BizBarcode getByName(String name);

    List<BizBarcodeVO> findListApi(BizBarcode bizBarcode);

    List<BizBarcodeVO> findListApiNo21(BizBarcode bizBarcode);

    Integer selectCountByGoodsId(BizBarcode bizBarcode);

    BizBarcode getAll(String barcodeId);

    void printNumberAdd(String barCode);
}