/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.biz.dao;

import com.thinkgem.jeesite.common.persistence.CrudDao;
import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.persistence.annotation.MyBatisDao;
import com.thinkgem.jeesite.modules.biz.entity.BizBuyingRequisition;
import com.thinkgem.jeesite.modules.biz.entity.BizBuyingRequisitionDetail;
import com.thinkgem.jeesite.modules.biz.entity.BizGoods;

import java.util.List;

/**
 * 请购单明细的controllerDAO接口
 * @author xuchen
 * @version 2018-07-28
 */
@MyBatisDao
public interface BizBuyingRequisitionDetailDao extends CrudDao<BizBuyingRequisitionDetail> {

    void deleteByBuyingId(String buyingId);

    Integer selectCountByBuyingId(String buyingId);

    List<BizBuyingRequisitionDetail> findListByParent(BizBuyingRequisition bizBuyingRequisition);
}