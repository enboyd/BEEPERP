/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.biz.web.api;

import com.thinkgem.jeesite.common.web.BaseController;
import com.thinkgem.jeesite.modules.biz.entity.BizStorageLocation;
import com.thinkgem.jeesite.modules.biz.form.BizStockLogListForm;
import com.thinkgem.jeesite.modules.biz.form.BizStorageLocationListForm;
import com.thinkgem.jeesite.modules.biz.service.BizStorageLocationService;
import com.thinkgem.jeesite.modules.biz.vo.BizStorageLocationVo;
import com.thinkgem.jeesite.modules.biz.vo.ResultVO;
import com.thinkgem.jeesite.modules.sys.enums.ResultEnum;
import com.thinkgem.jeesite.modules.sys.service.SystemService;
import com.thinkgem.jeesite.modules.sys.utils.ResultVOUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 库位管理Controller
 * @author xuchen
 * @version 2018-07-17
 */
@Controller
@Slf4j
@RequestMapping(value = "${adminPath}/api/biz/bizStorageLocation")
public class BizStorageLocationApiController extends BaseController {

	@Autowired
	private BizStorageLocationService bizStorageLocationService;
	@Autowired
	private SystemService systemService;

	 /**
	  * 手机端查询仓库货位的列表
	  * @author tiancheng
	  * @date 2018/8/30 10:50
	  * @param
	  * @return
	  */
	@RequestMapping(value = "list", method = RequestMethod.GET)
	@ResponseBody
	public ResultVO listForApi(@Valid BizStorageLocationListForm bizStorageLocationListForm, BindingResult bindingResult, HttpServletRequest request) {
		if (bindingResult.hasErrors()) {
			log.error("【查询库位列表异常】表单校验参数不正确：bizStorageLocationListForm => {}", bizStorageLocationListForm);
			return ResultVOUtil.error(ResultEnum.FORM_VALIDATE_PARAM_ERROR.getCode(),
					bindingResult.getFieldError().getDefaultMessage());
		}
		ResultVO resultVO = systemService.checkSign(request, BizStorageLocationListForm.class, bizStorageLocationListForm);

		if (resultVO.getCode() == 0) {
			Map map = new HashMap();
			BizStorageLocation bizStorageLocation = new BizStorageLocation();
			BizStorageLocation parent = new BizStorageLocation();
			parent.setId("2f97dc778304481ea1c2175b7e0440b0");
			bizStorageLocation.setParent(parent);
			//仓库货位的列表
			List<BizStorageLocationVo> bizStorageLocations = bizStorageLocationService.findListApi(bizStorageLocation);
			//查询下级的库位
			for (BizStorageLocationVo bizStorageLocationVo:bizStorageLocations) {
				BizStorageLocation storageLocation = new BizStorageLocation();
				parent.setId(bizStorageLocationVo.getId());
				storageLocation.setParent(parent);
				//type 为 1 为入库单的添加，酱料默认到21区
				if(bizStorageLocationVo.getName().equals("酱料区") && bizStorageLocationListForm.getType().equals("1")){
					storageLocation.setName("21区");
				}
				List<BizStorageLocationVo> bizStorageLocationList = bizStorageLocationService.findListApi(storageLocation);
				bizStorageLocationVo.setBizStorageLocationVoList(bizStorageLocationList);
			}
			if(null != bizStorageLocations){
				map.put("bizStorageLocations",bizStorageLocations);
			}
			return ResultVOUtil.success(map);

		}
		return resultVO;
	}
	
}