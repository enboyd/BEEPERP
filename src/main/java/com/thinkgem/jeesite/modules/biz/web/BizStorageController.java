/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.biz.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.thinkgem.jeesite.common.config.Global;
import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.web.BaseController;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.modules.biz.entity.BizStorage;
import com.thinkgem.jeesite.modules.biz.service.BizStorageService;

/**
 * 仓库管理明细的controllerController
 * @author xuchen
 * @version 2018-07-16
 */
@Controller
@RequestMapping(value = "${adminPath}/biz/bizStorage")
public class BizStorageController extends BaseController {

	@Autowired
	private BizStorageService bizStorageService;
	
	@ModelAttribute
	public BizStorage get(@RequestParam(required=false) String id) {
		BizStorage entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = bizStorageService.get(id);
		}
		if (entity == null){
			entity = new BizStorage();
		}
		return entity;
	}
	
	@RequiresPermissions("biz:bizStorage:view")
	@RequestMapping(value = {"list", ""})
	public String list(BizStorage bizStorage, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<BizStorage> page = bizStorageService.findPage(new Page<BizStorage>(request, response), bizStorage); 
		model.addAttribute("page", page);
		return "modules/biz/bizStorageList";
	}

	@RequiresPermissions("biz:bizStorage:view")
	@RequestMapping(value = "form")
	public String form(BizStorage bizStorage, Model model) {
		model.addAttribute("bizStorage", bizStorage);
		return "modules/biz/bizStorageForm";
	}

	@RequiresPermissions(value={"biz:bizStorage:edit","biz:bizStorage:add"},logical= Logical.OR)
	@RequestMapping(value = "save")
	public String save(BizStorage bizStorage, Model model, RedirectAttributes redirectAttributes) {
		if (!beanValidator(model, bizStorage)){
			return form(bizStorage, model);
		}
		bizStorageService.save(bizStorage);
		addMessage(redirectAttributes, "保存仓库管理成功");
		return "redirect:"+Global.getAdminPath()+"/biz/bizStorage/?repage";
	}
	
	@RequiresPermissions("biz:bizStorage:remove")
	@RequestMapping(value = "delete")
	public String delete(BizStorage bizStorage, RedirectAttributes redirectAttributes) {
		bizStorageService.delete(bizStorage);
		addMessage(redirectAttributes, "删除仓库管理成功");
		return "redirect:"+Global.getAdminPath()+"/biz/bizStorage/?repage";
	}

}