package com.thinkgem.jeesite.modules.biz.rpt.entity;

import com.thinkgem.jeesite.common.persistence.DataEntity;
import com.thinkgem.jeesite.common.utils.excel.annotation.ExcelField;
import lombok.Data;

import java.util.Date;

/**
 * 正常出库报表
 * Created by xuchen on 2018/10/16.
 */

public class BizNormalOutGoing extends DataEntity<BizNormalOutGoing> {

    private String orderID; //订单id
    private String orderCode;  //单号(订单编号)
    private Date outDate;   //出库日期
    private Date beginOutDate;   //开始出库日期(用于查询)
    private Date endOutDate;   //结束出库日期(用于查询)
    private String barId; //条码Id
    private String barcode; //条码编号
    private String goodsId; //存货id
    private String goodsCode; //存货编码
    private String goodsName; //存货名称
    private String batch;   //批次
    private Integer number;     //数量
    private String storageLocId;    //货位Id
    private String storageLocName;    //货位名称
    private String operatorId;  //操作人Id
    private String operatorName;  //操作人Name
    private String remarksType; //导出备注
    private String classifyId;	//商品分类ID
    private String classifyName;	//商品分类Name
    private String unitPrice;	//出库单价
    private String customerName;	//出库单价
    private String idNumber;	//出库单价
    private String specification;		// 规格
    private String address;		// 收货地址


    public String getOrderID() {
        return orderID;
    }

    public void setOrderID(String orderID) {
        this.orderID = orderID;
    }

    @ExcelField(title="单号", align=1, sort=10)
    public String getOrderCode() {
        return orderCode;
    }

    public void setOrderCode(String orderCode) {
        this.orderCode = orderCode;
    }

    @ExcelField(title="出库日期", align=1, sort=20)
    public Date getOutDate() {
        return outDate;
    }

    public void setOutDate(Date outDate) {
        this.outDate = outDate;
    }

    public Date getBeginOutDate() {
        return beginOutDate;
    }

    public void setBeginOutDate(Date beginOutDate) {
        this.beginOutDate = beginOutDate;
    }

    public Date getEndOutDate() {
        return endOutDate;
    }

    public void setEndOutDate(Date endOutDate) {
        this.endOutDate = endOutDate;
    }

    public String getBarId() {
        return barId;
    }

    public void setBarId(String barId) {
        this.barId = barId;
    }

    @ExcelField(title="条码号", align=1, sort=30)
    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public String getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(String goodsId) {
        this.goodsId = goodsId;
    }

    @ExcelField(title="存货编码", align=1, sort=40)
    public String getGoodsCode() {
        return goodsCode;
    }

    public void setGoodsCode(String goodsCode) {
        this.goodsCode = goodsCode;
    }

    @ExcelField(title="存货名称", align=1, sort=50)
    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    @ExcelField(title="批次", align=1, sort=60)
    public String getBatch() {
        return batch;
    }

    public void setBatch(String batch) {
        this.batch = batch;
    }

    @ExcelField(title="数量", align=1, sort=80)
    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public String getStorageLocId() {
        return storageLocId;
    }

    public void setStorageLocId(String storageLocId) {
        this.storageLocId = storageLocId;
    }

    @ExcelField(title="货位", align=1, sort=90)
    public String getStorageLocName() {
        return storageLocName;
    }

    public void setStorageLocName(String storageLocName) {
        this.storageLocName = storageLocName;
    }

    public String getOperatorId() {
        return operatorId;
    }

    public void setOperatorId(String operatorId) {
        this.operatorId = operatorId;
    }

    @ExcelField(title="操作人", align=1, sort=100)
    public String getOperatorName() {
        return operatorName;
    }

    public void setOperatorName(String operatorName) {
        this.operatorName = operatorName;
    }

    @ExcelField(title="备注", align=1, sort=110)
    public String getRemarksType() {
        return "正常出库";
    }

    public void setRemarksType(String remarksType) {
        this.remarksType = remarksType;
    }

    public String getClassifyId() {
        return classifyId;
    }

    public void setClassifyId(String classifyId) {
        this.classifyId = classifyId;
    }

    @ExcelField(title="分类", align=1, sort=51)
    public String getClassifyName() {
        return classifyName;
    }

    public void setClassifyName(String classifyName) {
        this.classifyName = classifyName;
    }

    @ExcelField(title="出库单价", align=1, sort=70)
    public String getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(String unitPrice) {
        this.unitPrice = unitPrice;
    }

    @ExcelField(title="客户", align=1, sort=120)
    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    @ExcelField(title="身份证号码", align=1, sort=130)
    public String getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }

    @ExcelField(title="规格", align=1, sort=55)
    public String getSpecification() {
        return specification;
    }

    public void setSpecification(String specification) {
        this.specification = specification;
    }

    @ExcelField(title="收货地址", align=1, sort=140)
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
