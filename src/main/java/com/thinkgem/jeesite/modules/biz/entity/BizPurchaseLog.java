/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.biz.entity;

import org.hibernate.validator.constraints.Length;
import com.thinkgem.jeesite.modules.sys.entity.User;

import com.thinkgem.jeesite.common.persistence.DataEntity;

/**
 * 采购单日志的ControllerEntity
 * @author tiancheng
 * @version 2018-07-17
 */
public class BizPurchaseLog extends DataEntity<BizPurchaseLog> {
	
	private static final long serialVersionUID = 1L;
	private String purchaseId;		// 采购单id
	private String type;		// 操作

	private String createName;//操作人的name

	public BizPurchaseLog() {
		super();
	}

	public BizPurchaseLog(String id){
		super(id);
	}

	@Length(min=0, max=20, message="采购单id长度必须介于 0 和 20 之间")
	public String getPurchaseId() {
		return purchaseId;
	}

	public void setPurchaseId(String purchaseId) {
		this.purchaseId = purchaseId;
	}
	
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getCreateName() {
		return createName;
	}

	public void setCreateName(String createName) {
		this.createName = createName;
	}
}