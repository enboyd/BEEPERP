package com.thinkgem.jeesite.modules.biz.rpt.entity;

import com.thinkgem.jeesite.common.persistence.DataEntity;
import com.thinkgem.jeesite.common.utils.excel.annotation.ExcelField;

/**
 * Created by Administrator on 2018/10/15.
 */
public class BizBarcodeRpt extends DataEntity<BizBarcodeRpt> {

    private String number;

    private String goodsName;

    private String goodsCode;

    private String specification;

    private String unit;

    private String storageLocationName;

    private Integer sum;

    private String manufactureDate;

    private String pici;

    private String type; // 1,现存量报表  2，货位现存量报表

    private String classifyId;	//商品分类ID
    private String classifyName;	//商品分类Name

    @ExcelField(title="数量", align=1, sort=45)
    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    @ExcelField(title="商品名称", align=1, sort=15)
    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    @ExcelField(title="商品编码", align=1, sort=10)
    public String getGoodsCode() {
        return goodsCode;
    }

    public void setGoodsCode(String goodsCode) {
        this.goodsCode = goodsCode;
    }

    @ExcelField(title="规格型号", align=1, sort=20)
    public String getSpecification() {
        return specification;
    }

    public void setSpecification(String specification) {
        this.specification = specification;
    }

    @ExcelField(title="单位", align=1, sort=25)
    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    @ExcelField(title="货位名称", align=1, sort=40)
    public String getStorageLocationName() {
        return storageLocationName;
    }

    public void setStorageLocationName(String storageLocationName) {
        this.storageLocationName = storageLocationName;
    }

    public Integer getSum() {
        return sum;
    }

    public void setSum(Integer sum) {
        this.sum = sum;
    }

    @ExcelField(title="生产日期", align=1, sort=30)
    public String getManufactureDate() {
        return manufactureDate;
    }

    public void setManufactureDate(String manufactureDate) {
        this.manufactureDate = manufactureDate;
    }

    @ExcelField(title="批次", align=1, sort=35)
    public String getPici() {
        return pici;
    }

    public void setPici(String pici) {
        this.pici = pici;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getClassifyId() {
        return classifyId;
    }

    public void setClassifyId(String classifyId) {
        this.classifyId = classifyId;
    }

    @ExcelField(title="商品分类", align=1, sort=16)
    public String getClassifyName() {
        return classifyName;
    }

    public void setClassifyName(String classifyName) {
        this.classifyName = classifyName;
    }
}
