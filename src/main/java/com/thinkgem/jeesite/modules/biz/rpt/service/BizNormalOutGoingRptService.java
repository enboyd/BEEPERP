package com.thinkgem.jeesite.modules.biz.rpt.service;

import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.modules.biz.entity.*;
import com.thinkgem.jeesite.modules.biz.rpt.dao.BizNormalOutGoingRptDao;
import com.thinkgem.jeesite.modules.biz.rpt.entity.BizInitialStorage;
import com.thinkgem.jeesite.modules.biz.rpt.entity.BizNormalOutGoing;
import com.thinkgem.jeesite.modules.biz.service.*;
import com.thinkgem.jeesite.modules.sys.utils.UserUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author : xuchen
 * @date : 2018/10/22 9:13
 */
@Service
@Transactional(readOnly = true)
public class BizNormalOutGoingRptService {

    @Autowired
    private BizNormalOutGoingRptDao bizNormalOutGoingRptDao;

    public Page<BizNormalOutGoing> findPage(Page<BizNormalOutGoing> page, BizNormalOutGoing bizNormalOutGoing) {
        bizNormalOutGoing.setPage(page);
        List<BizNormalOutGoing> bizNormalOutGoingList = bizNormalOutGoingRptDao.findList(bizNormalOutGoing);
        page.setList(bizNormalOutGoingList);
        return page;
    }

    public List<BizNormalOutGoing> findAllListByDate(Date beginOutDate,Date endOutDate,String goodsCode,String classifyId) {
        return bizNormalOutGoingRptDao.findAllListByDate(beginOutDate,endOutDate,goodsCode,classifyId);
    }
}
