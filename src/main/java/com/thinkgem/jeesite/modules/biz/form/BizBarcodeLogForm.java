/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.biz.form;

import lombok.Data;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * 出库日志Entity
 * @author tiancheng
 * @version 2018-09-19
 */
@Data
public class BizBarcodeLogForm {

	@NotEmpty(message = "订单id必填")
	private String orderId;

	private String barcodeId;

	@NotEmpty(message = "出库数必填")
	private String number;

	@NotEmpty(message = "用户的id必填")
	private String userId;

	@NotEmpty(message = "商品的id必填")
	private String goodsId;


}