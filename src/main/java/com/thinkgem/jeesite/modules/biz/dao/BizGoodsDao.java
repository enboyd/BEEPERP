/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.biz.dao;

import com.thinkgem.jeesite.common.persistence.CrudDao;
import com.thinkgem.jeesite.common.persistence.annotation.MyBatisDao;
import com.thinkgem.jeesite.modules.biz.entity.BizGoods;
import com.thinkgem.jeesite.modules.biz.vo.BizGoodsVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 商品的ControllerDAO接口
 * @author tiancheng
 * @version 2018-07-12
 */
@MyBatisDao
public interface BizGoodsDao extends CrudDao<BizGoods> {

     /**
      * 根据集合查询商品
      * @author tiancheng
      * @date 2018/7/13 11:23
      * @param id 商品的id集合
      * @return
      */
    List<BizGoods> selectGoodsListByIds(List<String> id);


     /**
      * 修改库存和采购数
      * @author tiancheng
      * @date 2018/7/14 11:54
      * @param bizGoods
      * @return
      */
    void updateInfo(BizGoods bizGoods);

    List<BizGoods> findAllWarningGoodsList(BizGoods bizGoods);

    List<BizGoodsVO> findListForApi(BizGoods bizGoods);
    List<BizGoodsVO> findAllListForApi(@Param("startIndex") int startIndex,
                                       @Param("pageSize")int pageSize,
                                       @Param("classifyId")String classifyId,
                                       @Param("searchKey") String searchKey);

     /**
      * 手机端查看商品的详情
      * @author tiancheng
      * @date 2018/8/31 14:11
      * @param
      * @return
      */
    BizGoodsVO getApi(String id);

     /**
      * 修改商品中单日商品生成条码的数量
      * @author tiancheng
      * @date 2018/9/11 14:41
      * @param
      * @return
      */
    void updateDayNum(BizGoods bizGoods);

     /**
      * 每天清空day_num,变成0
      * @author tiancheng  
      * @date 2018/9/11 15:08
      * @param
      * @return   
      */  
    void updateDayNum0();

    /**
     * 查找商品编码在数据库的数量
     * @param bizGoodsCode
     * @return
     */
    Integer getCountGoodsCode(String bizGoodsCode);

     /**
      * 修改库存
      * @author tiancheng
      * @date 2018/9/26 11:25
      * @param
      * @return
      */
    void updateStockAmount(BizGoods bizGoods);

    BizGoods selectInfoByGoodsCode(String goodsCode);

    Integer getStockAmountByGoodsCode(String goodsCode);

    /**
     * 通过商品code查找商品
     * @param goodsCode
     * @return
     */
    BizGoods getByGoodsCode(String goodsCode);

    /**
     * 通过商品code修改商品
     * @param bizGoods
     * @return
     */
    BizGoods updateByGoodsCode(BizGoods bizGoods);

    /**
     * 通过商品编码查询商品数
     * @param goodsCode
     * @return
     */
    Integer findCountByGoodsCode(String goodsCode);


    List<Map<String,Object>> findListMap(BizGoods bizGoods);
}