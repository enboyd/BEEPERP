/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.biz.vo;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 移库Entity
 * @author tiancheng
 * @version 2018-09-12
 */
@Data
public class BizMovePositionLogVo implements Serializable {

	private static final long serialVersionUID = 1L;
	private String name;		// 移库的编号
	private String goodsName;		// 商品的id
	private String number;		// 数量
	private String storageLocationFrontName;		// 移库前的库位
	private String storageLocationAfterName;		// 移库后的库位
	private Date createDate;
	private String createByName;

}