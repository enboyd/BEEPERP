package com.thinkgem.jeesite.modules.biz.form;

import lombok.Data;
import org.hibernate.validator.constraints.NotEmpty;

import java.util.List;
import java.util.Map;

/**
 * Description:
 * Author: tianCheng
 * Create Time: 2018-08-09 17:09
 */
@Data
public class BizGoodsGetByCodeForm {


    private String code;		// 商品编号

    private String goodsName;		// 商品名称

    private List<BizGoodsGetByCodeForm> smProductInformationFormList;

}
