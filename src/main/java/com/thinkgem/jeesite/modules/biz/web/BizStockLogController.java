/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.biz.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.thinkgem.jeesite.common.config.Global;
import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.web.BaseController;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.modules.biz.entity.BizStockLog;
import com.thinkgem.jeesite.modules.biz.service.BizStockLogService;

/**
 * 期出期入的logController
 * @author tiancheng
 * @version 2018-09-13
 */
@Controller
@RequestMapping(value = "${adminPath}/biz/bizStockLog")
public class BizStockLogController extends BaseController {

	@Autowired
	private BizStockLogService bizStockLogService;
	
	@ModelAttribute
	public BizStockLog get(@RequestParam(required=false) String id) {
		BizStockLog entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = bizStockLogService.get(id);
		}
		if (entity == null){
			entity = new BizStockLog();
		}
		return entity;
	}
	
	@RequiresPermissions("biz:bizStockLog:view")
	@RequestMapping(value = {"list", ""})
	public String list(BizStockLog bizStockLog, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<BizStockLog> page = bizStockLogService.findPage(new Page<BizStockLog>(request, response), bizStockLog);
		model.addAttribute("page", page);
		return "modules/biz/bizStockLogList";
	}

	@RequiresPermissions("biz:bizStockLog:view")
	@RequestMapping(value = "form")
	public String form(BizStockLog bizStockLog, Model model) {
		model.addAttribute("bizStockLog", bizStockLog);
		return "modules/biz/bizStockLogForm";
	}

	@RequiresPermissions("biz:bizStockLog:edit")
	@RequestMapping(value = "save")
	public String save(BizStockLog bizStockLog, Model model, RedirectAttributes redirectAttributes) {
		if (!beanValidator(model, bizStockLog)){
			return form(bizStockLog, model);
		}
		bizStockLogService.save(bizStockLog);
		addMessage(redirectAttributes, "保存期出期入成功");
		return "redirect:"+Global.getAdminPath()+"/biz/bizStockLog/?repage";
	}
	
	@RequiresPermissions("biz:bizStockLog:edit")
	@RequestMapping(value = "delete")
	public String delete(BizStockLog bizStockLog, RedirectAttributes redirectAttributes) {
		bizStockLogService.delete(bizStockLog);
		addMessage(redirectAttributes, "删除期出期入成功");
		return "redirect:"+Global.getAdminPath()+"/biz/bizStockLog/?repage";
	}

}