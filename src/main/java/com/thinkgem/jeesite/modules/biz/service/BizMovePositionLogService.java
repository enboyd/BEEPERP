/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.biz.service;

import java.util.List;

import com.thinkgem.jeesite.modules.biz.dao.BizBarcodeDao;
import com.thinkgem.jeesite.modules.biz.dao.BizStorageLocationDao;
import com.thinkgem.jeesite.modules.biz.entity.BizBarcode;
import com.thinkgem.jeesite.modules.biz.entity.BizStorageLocation;
import com.thinkgem.jeesite.modules.biz.form.BizMovePositionLogForm;
import com.thinkgem.jeesite.modules.biz.form.BizMovePositionLogListForm;
import com.thinkgem.jeesite.modules.biz.vo.BizMovePositionLogVo;
import com.thinkgem.jeesite.modules.sys.entity.User;
import com.thinkgem.jeesite.modules.sys.utils.AutoNameUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.service.CrudService;
import com.thinkgem.jeesite.modules.biz.entity.BizMovePositionLog;
import com.thinkgem.jeesite.modules.biz.dao.BizMovePositionLogDao;

/**
 * 移库Service
 * @author tiancheng
 * @version 2018-09-12
 */
@Service
@Transactional(readOnly = true)
public class BizMovePositionLogService extends CrudService<BizMovePositionLogDao, BizMovePositionLog> {

	@Autowired
	private BizBarcodeDao bizBarcodeDao;
	@Autowired
	private BizMovePositionLogDao bizMovePositionLogDao;
	@Autowired
	private BizStorageLocationDao bizStorageLocationDao;

	public BizMovePositionLog get(String id) {
		return super.get(id);
	}
	
	public List<BizMovePositionLog> findList(BizMovePositionLog bizMovePositionLog) {
		return super.findList(bizMovePositionLog);
	}

	 /**
	  * 手机端查询移库的列表
	  * @author tiancheng
	  * @date 2018/9/12 15:05
	  * @param
	  * @return
	  */
	public List<BizMovePositionLogVo> findListApi(BizMovePositionLogListForm bizMovePositionLogListForm){
		BizMovePositionLog bizMovePositionLog = new BizMovePositionLog();
		bizMovePositionLog.setPageSize(bizMovePositionLogListForm.getPageSize());
		bizMovePositionLog.setPageNo((bizMovePositionLogListForm.getPageNo()-1)*bizMovePositionLogListForm.getPageSize());
		List<BizMovePositionLogVo> bizMovePositionLogVoList = bizMovePositionLogDao.findListApi(bizMovePositionLog);
		return bizMovePositionLogVoList;
	}


	public Page<BizMovePositionLog> findPage(Page<BizMovePositionLog> page, BizMovePositionLog bizMovePositionLog) {
		return super.findPage(page, bizMovePositionLog);
	}
	
	@Transactional(readOnly = false)
	public void save(BizMovePositionLogForm bizMovePositionLogForm) {
		//参数传递
		BizMovePositionLog bizMovePositionLog = new BizMovePositionLog();
		BeanUtils.copyProperties(bizMovePositionLogForm, bizMovePositionLog);
		//修改条形码的信息（移库是整盘移动）
		BizBarcode bizBarcode = new BizBarcode();
		bizBarcode.setName(bizMovePositionLogForm.getBarcode());
		//修改条码移库后的库位
		bizBarcode.setStorageLocationId(bizMovePositionLogForm.getStorageLocationIdAfter());
		//查询库位的名称
		BizStorageLocation bizStorageLocation = bizStorageLocationDao.get(bizMovePositionLogForm.getStorageLocationIdAfter());
		bizBarcode.setStorageLocationName(bizStorageLocation.getName());
		bizBarcodeDao.updateStorageLocationId(bizBarcode);

		String code = AutoNameUtils.createAutoNameByTableName("biz_move_position_log");
		bizMovePositionLog.setName(code);
		//添加移库的日志
		User user = new User();
		user.setId(bizMovePositionLogForm.getUserId());
		bizMovePositionLog.setCreateBy(user);
		bizMovePositionLog.setUpdateBy(user);
		super.save(bizMovePositionLog);
	}
	
	@Transactional(readOnly = false)
	public void delete(BizMovePositionLog bizMovePositionLog) {
		super.delete(bizMovePositionLog);
	}
	
}