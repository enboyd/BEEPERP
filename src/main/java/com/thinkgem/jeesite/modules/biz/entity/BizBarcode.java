/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.biz.entity;

import org.hibernate.validator.constraints.Length;
import com.thinkgem.jeesite.modules.sys.entity.User;
import javax.validation.constraints.NotNull;

import com.thinkgem.jeesite.common.persistence.DataEntity;

import java.util.Date;

/**
 * 条形码Entity
 * @author tiancheng
 * @version 2018-09-05
 */
public class BizBarcode extends DataEntity<BizBarcode> {
	
	private static final long serialVersionUID = 1L;
	private String name;		// 条形码编码
	private String stockInDetailId;		// 入库单明细的id
	private String number;		// 条码上的数量
	private String url;		// 条形码保存到路径
	private String txmPath;  //单纯条形码的路径（当天跑定时删除）
	private User createByName;		// 创建者名称
	private User updateByName;		// 更新者名称
	private String waitNumber;   // 等待出库数
	private String surplusNumber;   //剩余数

	private String stockInId; //入库单id
	private String goodsId; //商品的id
	private String goodsName; //商品的名称
	private String goodsCode; //商品的编号
	private String manufactureDate;  //生产日期
	private Date manufactureDateDa;  //生产日期 date
	private String storageLocationName;  //库位的name
	private String storageLocationId;  //库位的id

	private String stockAmount; // 库存
	private Integer printNumber;	//打印次数

	public BizBarcode() {
		super();
	}

	public BizBarcode(String id){
		super(id);
	}

	@Length(min=1, max=64, message="条形码编码长度必须介于 1 和 64 之间")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	@Length(min=1, max=32, message="入库单明细的id长度必须介于 1 和 32 之间")
	public String getStockInDetailId() {
		return stockInDetailId;
	}

	public void setStockInDetailId(String stockInDetailId) {
		this.stockInDetailId = stockInDetailId;
	}
	
	@Length(min=0, max=11, message="条码上的数量长度必须介于 0 和 11 之间")
	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}
	
	@Length(min=0, max=255, message="条形码保存到路径长度必须介于 0 和 255 之间")
	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
	
	@NotNull(message="创建者名称不能为空")
	public User getCreateByName() {
		return createByName;
	}

	public void setCreateByName(User createByName) {
		this.createByName = createByName;
	}
	
	@NotNull(message="更新者名称不能为空")
	public User getUpdateByName() {
		return updateByName;
	}

	public void setUpdateByName(User updateByName) {
		this.updateByName = updateByName;
	}

	public String getTxmPath() {
		return txmPath;
	}

	public void setTxmPath(String txmPath) {
		this.txmPath = txmPath;
	}

	public String getStockInId() {
		return stockInId;
	}

	public void setStockInId(String stockInId) {
		this.stockInId = stockInId;
	}

	public String getGoodsName() {
		return goodsName;
	}

	public void setGoodsName(String goodsName) {
		this.goodsName = goodsName;
	}

	public String getGoodsCode() {
		return goodsCode;
	}

	public void setGoodsCode(String goodsCode) {
		this.goodsCode = goodsCode;
	}

	public String getStorageLocationName() {
		return storageLocationName;
	}

	public void setStorageLocationName(String storageLocationName) {
		this.storageLocationName = storageLocationName;
	}

	public String getManufactureDate() {
		return manufactureDate;
	}

	public void setManufactureDate(String manufactureDate) {
		this.manufactureDate = manufactureDate;
	}

	public Date getManufactureDateDa() {
		return manufactureDateDa;
	}

	public void setManufactureDateDa(Date manufactureDateDa) {
		this.manufactureDateDa = manufactureDateDa;
	}

	public String getStorageLocationId() {
		return storageLocationId;
	}

	public void setStorageLocationId(String storageLocationId) {
		this.storageLocationId = storageLocationId;
	}

	public String getGoodsId() {
		return goodsId;
	}

	public void setGoodsId(String goodsId) {
		this.goodsId = goodsId;
	}

	public String getStockAmount() {
		return stockAmount;
	}

	public void setStockAmount(String stockAmount) {
		this.stockAmount = stockAmount;
	}

	public String getWaitNumber() {
		return waitNumber;
	}

	public void setWaitNumber(String waitNumber) {
		this.waitNumber = waitNumber;
	}

	public String getSurplusNumber() {
		return surplusNumber;
	}

	public void setSurplusNumber(String surplusNumber) {
		this.surplusNumber = surplusNumber;
	}

	public Integer getPrintNumber() {
		return printNumber;
	}

	public void setPrintNumber(Integer printNumber) {
		this.printNumber = printNumber;
	}
}