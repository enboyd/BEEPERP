/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.biz.vo;

import lombok.Data;
import java.io.Serializable;

/**
 * 采购列表单明细的controllerEntity
 * @author tiancheng
 * @version 2018-07-13
 */
@Data
public class BizPurchaseDetailVo implements Serializable {

	private String purchaseDetailId;		// 采购明细ID
	private String purchaseId;		// 所属采购单
	private String goodsId;		// 商品ID
	private String goodsName;		// 商品名称
	private String expertAmount;		// 申请采购数量
	private String differentAmount;		// 相差数量
	private String goodsCode;  //采购单的编号
	private String type;  //采购单商品的类型
}