/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.biz.dao;

import com.thinkgem.jeesite.common.persistence.CrudDao;
import com.thinkgem.jeesite.common.persistence.annotation.MyBatisDao;
import com.thinkgem.jeesite.modules.biz.entity.BizBuyingRequisition;

/**
 * 请购单的controllerDAO接口
 * @author xuchen
 * @version 2018-07-28
 */
@MyBatisDao
public interface BizBuyingRequisitionDao extends CrudDao<BizBuyingRequisition> {

    void toExamine(String bizBuyingId);

    void reject(String bizBuyingId);

    void resubmit(String bizBuyingId);

    void submit(String bizBuyingId);

    void complete(String bizBuyingId);

    void updateInfo(BizBuyingRequisition bizBuyingRequisition);
}