/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.biz.web;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.thinkgem.jeesite.common.utils.HttpClientUtils;
import com.thinkgem.jeesite.modules.biz.entity.SmReturnBarter;
import com.thinkgem.jeesite.modules.biz.entity.SmReturnBarterDetail;
import com.thinkgem.jeesite.modules.biz.service.SmReturnBarterDetailService;
import com.thinkgem.jeesite.modules.biz.service.SmReturnBarterService;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.thinkgem.jeesite.common.config.Global;
import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.web.BaseController;
import com.thinkgem.jeesite.common.utils.StringUtils;


/**
 * 退换货Controller
 * @author xuchen
 * @version 2018-09-10
 */
@Controller
@RequestMapping(value = "${adminPath}/biz/smReturnBarter")
public class SmReturnBarterController extends BaseController {

	@Autowired
	private SmReturnBarterService smReturnBarterService;

	@Autowired
	private SmReturnBarterDetailService smReturnBarterDetailService;
	
	@ModelAttribute
	public SmReturnBarter get(@RequestParam(required=false) String id) {
		SmReturnBarter entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = smReturnBarterService.get(id);
		}
		if (entity == null){
			entity = new SmReturnBarter();
		}
		return entity;
	}
	
	@RequiresPermissions("sm:smReturnBarter:view")
	@RequestMapping(value = {"list", ""})
	public String list(SmReturnBarter smReturnBarter, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<SmReturnBarter> page = smReturnBarterService.findPage(new Page<SmReturnBarter>(request, response), smReturnBarter); 
		model.addAttribute("page", page);
		return "modules/biz/smReturnBarterList";
	}

	@RequiresPermissions("sm:smReturnBarter:view")
	@RequestMapping(value = "form")
	public String form(SmReturnBarter smReturnBarter, Model model) {
		if(smReturnBarter != null){
			String id = smReturnBarter.getId();
			if(StringUtils.isNotEmpty(id)){
				SmReturnBarterDetail smReturnDetail = new SmReturnBarterDetail();
				smReturnDetail.setReturnBarterId(id);
				smReturnDetail.setType("10");
				List<SmReturnBarterDetail> smReturnDetailList = smReturnBarterDetailService.findList(smReturnDetail);
				model.addAttribute("smReturnDetailList", smReturnDetailList);
				SmReturnBarterDetail smBarterDetail = new SmReturnBarterDetail();
				smBarterDetail.setReturnBarterId(id);
				smBarterDetail.setType("20");
				List<SmReturnBarterDetail> smBarterDetailList = smReturnBarterDetailService.findList(smBarterDetail);
				model.addAttribute("smBarterDetailList", smBarterDetailList);
			}
		}
		model.addAttribute("smReturnBarter", smReturnBarter);
		return "modules/biz/smReturnBarterForm";
	}

	@RequiresPermissions(value = {"sm:smReturnBarter:edit", "sm:smReturnBarter:add"}, logical = Logical.OR)
	@RequestMapping(value = "save")
	public String save(SmReturnBarter smReturnBarter, Model model, RedirectAttributes redirectAttributes, HttpServletRequest request) {
		if (!beanValidator(model, smReturnBarter)){
			return form(smReturnBarter, model);
		}
		smReturnBarterService.save(smReturnBarter);
		addMessage(redirectAttributes, "保存退换货成功");
		return "redirect:"+Global.getAdminPath()+"/biz/smReturnBarter/?repage";
	}
	
	@RequiresPermissions("sm:smReturnBarter:remove")
	@RequestMapping(value = "delete")
	public String delete(SmReturnBarter smReturnBarter, RedirectAttributes redirectAttributes) {
		smReturnBarterService.delete(smReturnBarter);
		addMessage(redirectAttributes, "删除退换货成功");
		return "redirect:"+Global.getAdminPath()+"/biz/smReturnBarter/?repage";
	}


	/**
	 * 审批通过
	 * @param request
	 * @param smReturnBarter
	 * @param redirectAttributes
	 * @return
	 */
	@RequiresPermissions("sm:smReturnBarter:approve")
	@RequestMapping(value = "approve")
	@ResponseBody
	public String approve(HttpServletRequest request,SmReturnBarter smReturnBarter, RedirectAttributes redirectAttributes) {
		String msg = "操作失败";
		String source = request.getParameter("source");
		try{
			String path = Global.getConfig("crm.interface.path");
			String url = path+"/a/sm/smReturnBarter/updateStatus";
			String param = "id="+smReturnBarter.getId()+"&auditStatus=20";
			//请求CRM修改退换货单
			String result = HttpClientUtils.sendGet(url, param);
			if(StringUtils.isNotEmpty(result) && result.equals("0")){
				smReturnBarterService.approve(smReturnBarter);
				if("20".equals(smReturnBarter.getCategory())){
					msg = "审核通过,生成出库单成功！";
				}else{
					msg = "审核通过";
				}
			}
		}catch (Exception e){
			e.printStackTrace();
		}
		return msg;
	}

	/**
	 * 审核驳回
	 * @param smReturnBarter
	 * @param redirectAttributes
	 * @return
	 */
	@RequiresPermissions("sm:smReturnBarter:dismiss")
	@RequestMapping(value = "dismiss")
	@ResponseBody
	public String dismiss(SmReturnBarter smReturnBarter, RedirectAttributes redirectAttributes) {
		String msg = "操作失败";
		try{
			//请求CRM修改退换货单
			String path = Global.getConfig("crm.interface.path");
			String url = path+"/a/sm/smReturnBarter/updateStatus";
			String param = "id="+smReturnBarter.getId()+"&auditStatus=30";
			//请求CRM修改退换货单
			String result = HttpClientUtils.sendGet(url, param);
			if(StringUtils.isNotEmpty(result) && result.equals("0")){
				smReturnBarterService.dismiss(smReturnBarter);
				msg = "操作成功";
			}
		}catch (Exception e){
			e.printStackTrace();
		}

		return msg;
	}



}