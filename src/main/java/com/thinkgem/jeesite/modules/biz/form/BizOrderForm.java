/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.biz.form;

import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * 订单Entity
 * @author tiancheng
 * @version 2018-09-17
 */
@Data
public class BizOrderForm {

	private String id;
	private String teamName;   //团队名称
	private String sellerName;  //招商名称
	private String customerId;		// 客户ID
	private String customerName;		// 客户名称
	private String projectId;		// 所属项目ID
	private String consigneeName;		// 收货人姓名 导出
	private String moneyMakerName;		// 打款人姓名
	private String phone;		// 电话
	private String address;		// 地址
	private String customerRemarks;		// 发货时备注
	private String name;		// 订货编号
	private Date orderTime;		// 订货时间
	private String discount;		// 折扣
	private String orderAmount;		// 订货金额
	private String couponAmount;		// 优惠券金额
	private String amountReceivable;		// 应收金额
	private String amountCollected;		// 实收金额
	private String settlementMethod;		// 结算方式
	private List<BizOrderdetailForm> SmOrderdetailList; // 订货单明细
	private String deliverType;//发货类型
	private String createBy;//创建人
	private String createByName;//创建人
	private Date createDate;//创建时间
	private String orderGenre;	//订货单类型
	private String auditorName;	//审核人名称
	private Date auditDate;	//审核日期
	private String remarks;	//备注
	private String idNumber;	//身份证号码

}