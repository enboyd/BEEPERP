/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.biz.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.service.CrudService;
import com.thinkgem.jeesite.modules.biz.entity.BizGoodsBrand;
import com.thinkgem.jeesite.modules.biz.dao.BizGoodsBrandDao;

/**
 * 商品品牌的ControllerService
 * @author tiancheng
 * @version 2018-07-12
 */
@Service
@Transactional(readOnly = true)
public class BizGoodsBrandService extends CrudService<BizGoodsBrandDao, BizGoodsBrand> {

	public BizGoodsBrand get(String id) {
		return super.get(id);
	}
	
	public List<BizGoodsBrand> findList(BizGoodsBrand bizGoodsBrand) {
		return super.findList(bizGoodsBrand);
	}
	
	public Page<BizGoodsBrand> findPage(Page<BizGoodsBrand> page, BizGoodsBrand bizGoodsBrand) {
		return super.findPage(page, bizGoodsBrand);
	}

	public List<BizGoodsBrand> findAllList(BizGoodsBrand bizGoodsBrand) {
		return dao.findAllList(bizGoodsBrand);
	}
	
	@Transactional(readOnly = false)
	public void save(BizGoodsBrand bizGoodsBrand) {
		super.save(bizGoodsBrand);
	}
	
	@Transactional(readOnly = false)
	public void delete(BizGoodsBrand bizGoodsBrand) {
		super.delete(bizGoodsBrand);
	}
	
}