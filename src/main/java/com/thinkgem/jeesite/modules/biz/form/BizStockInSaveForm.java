package com.thinkgem.jeesite.modules.biz.form;

import lombok.Data;

import java.util.List;

/**
 * Description:
 * Author: LTY
 * Create Time: 2018-08-09 17:09
 */
@Data
public class BizStockInSaveForm {

    private String purchaseId;//采购单的id

    //入库单明细的list
    private List<BizStockInDetailForm> bizStockInDetailFormList;

    private String createById;  //创建人id
    private String createByName;  //创建人name

}
