/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.biz.web.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.thinkgem.jeesite.common.config.Global;
import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.common.web.BaseController;
import com.thinkgem.jeesite.modules.biz.entity.BizOrder;
import com.thinkgem.jeesite.modules.biz.form.*;
import com.thinkgem.jeesite.modules.biz.service.BizOrderService;
import com.thinkgem.jeesite.modules.biz.vo.BizOrderVo;
import com.thinkgem.jeesite.modules.biz.vo.BizPurchaseVo;
import com.thinkgem.jeesite.modules.biz.vo.ResultVO;
import com.thinkgem.jeesite.modules.sys.enums.ResultEnum;
import com.thinkgem.jeesite.modules.sys.service.SystemService;
import com.thinkgem.jeesite.modules.sys.utils.ResultVOUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 订单Controller
 * @author tiancheng
 * @version 2018-09-17
 */
@Controller
@Slf4j
@RequestMapping(value = "${adminPath}/api/biz/bizOrder")
public class BizOrderApiController extends BaseController {

	@Autowired
	private BizOrderService bizOrderService;
	@Autowired
	private SystemService systemService;


	 /**
	  * 出库单的列表
	  * @author tiancheng
	  * @date 2018/9/18 15:31
	  * @param
	  * @return
	  */
	 @RequestMapping(value = "get", method = RequestMethod.GET)
	 @ResponseBody
	 public ResultVO get(@Valid BizOrderGetForm bizOrderGetForm, BindingResult bindingResult, HttpServletRequest request) {

		 if (bindingResult.hasErrors()) {
			 log.error("【查询出库单详情异常】表单校验参数不正确：bizOrderGetForm => {}", bizOrderGetForm);
			 return ResultVOUtil.error(ResultEnum.FORM_VALIDATE_PARAM_ERROR.getCode(),
					 bindingResult.getFieldError().getDefaultMessage());
		 }

		 ResultVO resultVO = systemService.checkSign(request, BizOrderGetForm.class, bizOrderGetForm);
		 if (resultVO.getCode() == 0) {
			 //返回的出库单详情
			 BizOrderVo bizOrderVo = bizOrderService.getApi(bizOrderGetForm.getName());
			 if(null == bizOrderVo){
				 return ResultVOUtil.error(ResultEnum.BARCODE_INFO.getCode(),"暂无此条码，请重新扫描！");
			 }
			 return ResultVOUtil.success(bizOrderVo);
		 }
		 return resultVO;
	 }

	 /**
	 * 接受CRM的订单推送
	 * @author tiancheng
	 * @date 2018/9/17 10:53
	 * @param
	 * @return
	 */
	@RequestMapping(value = "save", method = RequestMethod.POST)
	@ResponseBody
	public ResultVO save(@RequestBody String code) throws IOException {
		ObjectMapper objectMapper = new ObjectMapper();
		BizOrderForm bizOrderForm = objectMapper.readValue(code, BizOrderForm.class);
		//保存退换货单
		ResultVO resultVO = new ResultVO();
		try{
			bizOrderService.saveApi(bizOrderForm);
			return ResultVOUtil.success();
		}
		catch (Exception e){
			return ResultVOUtil.error(1,"失败");
		}
	}



	 /**
	  * 出库单的出库
	  * @author tiancheng
	  * @date 2018/9/19 15:59
	  * @param
	  * @return
	  */
	 @RequestMapping(value = "out", method = RequestMethod.POST)
	 @ResponseBody
	 public ResultVO out(@Valid BizOrderOutForm bizOrderOutForm, BindingResult bindingResult, HttpServletRequest request) {

		 if (bindingResult.hasErrors()) {
			 log.error("【查询出库单详情异常】表单校验参数不正确：bizOrderOutForm => {}", bizOrderOutForm);
			 return ResultVOUtil.error(ResultEnum.FORM_VALIDATE_PARAM_ERROR.getCode(),
					 bindingResult.getFieldError().getDefaultMessage());
		 }

		 ResultVO resultVO = systemService.checkSign(request, BizOrderOutForm.class, bizOrderOutForm);
		 if (resultVO.getCode() == 0) {
			 return bizOrderService.out(bizOrderOutForm);
		 }
		 return resultVO;
	 }


	 /**
	  * 财务审核通过后修改订单的审核状态
	  * @author tiancheng  
	  * @date 2018/11/13 15:59
	  * @param
	  * @return   
	  */  
	@RequestMapping(value = "orderApprovalAdopt", method = RequestMethod.GET)
	@ResponseBody
	public ResultVO orderApprovalAdopt(@Valid BizOrderForm bizOrderForm){
		//审批通过指定订货单
		try{
			bizOrderService.updateOrderApprovalAdopt(bizOrderForm.getId());
			return ResultVOUtil.success();
		}
		catch (Exception e){
			return ResultVOUtil.error(1,"失败");
		}
	}

	 /**
	  * 优惠申请单的审核通过
	  * @author tiancheng
	  * @date 2018/11/26 15:17
	  * @param
	  * @return
	  */
	@RequestMapping(value = "orderApprovalAdoptYY", method = RequestMethod.GET)
	@ResponseBody
	public ResultVO orderApprovalAdoptYY(@Valid BizOrderForm bizOrderForm){
		//审批通过指定订货单
		try{
			bizOrderService.updateOrderApprovalAdoptYY(bizOrderForm.getId());
			return ResultVOUtil.success();
		}
		catch (Exception e){
			return ResultVOUtil.error(1,"失败");
		}
	}


	 /**
	  * CRM删除订单或者撤销申请时，同步作废ERP的订单
	  * @author tiancheng
	  * @date 2018/11/15 9:01
	  * @param
	  * @return
	  */
	@RequestMapping(value = "discarded", method = RequestMethod.GET)
	@ResponseBody
	public ResultVO discarded(@Valid String name){
		//审批通过指定订货单
		try{
			//根据订单的编号查询是否存在已出库的订单
			Integer num = bizOrderService.selectCountByName(name);
			//判断是否是已出库
			if(num > 0){
				return ResultVOUtil.error(2,"订单已出库！");
			}
			bizOrderService.discarded(name);
			return ResultVOUtil.success("0");
		}
		catch (Exception e){
			return ResultVOUtil.error(1,"失败");
		}
	}


	/**
	 * 接受CRM的优惠申请单推送
	 * @author tiancheng
	 * @date 2018/9/17 10:53
	 * @param
	 * @return
	 */
	@RequestMapping(value = "saveDiscountsOrder", method = RequestMethod.POST)
	@ResponseBody
	public ResultVO saveDiscountsOrder(@RequestBody String code) throws IOException {
		ObjectMapper objectMapper = new ObjectMapper();
		BizOrderForm bizOrderForm = objectMapper.readValue(code, BizOrderForm.class);
		try{
			bizOrderService.saveDiscountsOrder(bizOrderForm);
			return ResultVOUtil.success();
		}
		catch (Exception e){
			return ResultVOUtil.error(1,"失败");
		}
	}
}