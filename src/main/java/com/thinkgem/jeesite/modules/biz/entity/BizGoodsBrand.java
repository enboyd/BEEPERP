/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.biz.entity;

import org.hibernate.validator.constraints.Length;
import com.thinkgem.jeesite.modules.sys.entity.User;
import javax.validation.constraints.NotNull;

import com.thinkgem.jeesite.common.persistence.DataEntity;

/**
 * 商品品牌的ControllerEntity
 * @author tiancheng
 * @version 2018-07-12
 */
public class BizGoodsBrand extends DataEntity<BizGoodsBrand> {
	
	private static final long serialVersionUID = 1L;
	private String goodsBrandId;		// 商品品牌ID
	private String goodsBrandName;		// 商品品牌名称

	public BizGoodsBrand() {
		super();
	}

	public BizGoodsBrand(String id){
		super(id);
	}

	@Length(min=1, max=11, message="商品品牌ID长度必须介于 1 和 11 之间")
	public String getGoodsBrandId() {
		return goodsBrandId;
	}

	public void setGoodsBrandId(String goodsBrandId) {
		this.goodsBrandId = goodsBrandId;
	}
	
	@Length(min=1, max=64, message="商品品牌名称长度必须介于 1 和 64 之间")
	public String getGoodsBrandName() {
		return goodsBrandName;
	}

	public void setGoodsBrandName(String goodsBrandName) {
		this.goodsBrandName = goodsBrandName;
	}

}