/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.biz.web.api;

import com.thinkgem.jeesite.common.web.BaseController;
import com.thinkgem.jeesite.modules.biz.entity.BizGoods;
import com.thinkgem.jeesite.modules.biz.entity.BizMovePositionLog;
import com.thinkgem.jeesite.modules.biz.form.BizMovePositionLogForm;
import com.thinkgem.jeesite.modules.biz.form.BizMovePositionLogListForm;
import com.thinkgem.jeesite.modules.biz.service.BizMovePositionLogService;
import com.thinkgem.jeesite.modules.biz.vo.BizMovePositionLogVo;
import com.thinkgem.jeesite.modules.biz.vo.ResultVO;
import com.thinkgem.jeesite.modules.sys.enums.ResultEnum;
import com.thinkgem.jeesite.modules.sys.service.SystemService;
import com.thinkgem.jeesite.modules.sys.utils.ResultVOUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 移库Controller
 * @author tiancheng
 * @version 2018-09-12
 */
@Controller
@Slf4j
@RequestMapping(value = "${adminPath}/api/biz/bizMovePositionLog")
public class BizMovePositionLogApiController extends BaseController {

	@Autowired
	private BizMovePositionLogService bizMovePositionLogService;

	@Autowired
	private SystemService systemService;

	/**
	 * 移库的记录
	 * @author tiancheng
	 * @date 2018/9/12 10:20
	 * @param
	 * @return
	 */
	@RequestMapping(value = "list", method = RequestMethod.GET)
	@ResponseBody
	public ResultVO list(@Valid BizMovePositionLogListForm bizMovePositionLogListForm, BindingResult bindingResult, HttpServletRequest request, HttpServletResponse response) {

		if (bindingResult.hasErrors()) {
			log.error("【添加移库失败】表单校验参数不正确：bizMovePositionLogListForm => {}", bizMovePositionLogListForm);
			return ResultVOUtil.error(ResultEnum.FORM_VALIDATE_PARAM_ERROR.getCode(),
					bindingResult.getFieldError().getDefaultMessage());
		}

		ResultVO resultVO = systemService.checkSign(request, BizMovePositionLogListForm.class, bizMovePositionLogListForm);

		if (resultVO.getCode() == 0) {
			List<BizMovePositionLogVo> bizMovePositionLogVoList = bizMovePositionLogService.findListApi(bizMovePositionLogListForm);
			Map map = new HashMap();
			map.put("info",bizMovePositionLogVoList);
			return ResultVOUtil.success(map);
		}
		return resultVO;
	}


	 /**
	  * 添加移库
	  * @author tiancheng
	  * @date 2018/9/12 10:20
	  * @param
	  * @return
	  */
	@RequestMapping(value = "save", method = RequestMethod.POST)
	@ResponseBody
	public ResultVO save(@Valid BizMovePositionLogForm bizMovePositionLogForm, BindingResult bindingResult, HttpServletRequest request, HttpServletResponse response) {

		if (bindingResult.hasErrors()) {
			log.error("【添加移库失败】表单校验参数不正确：bizMovePositionLogForm => {}", bizMovePositionLogForm);
			return ResultVOUtil.error(ResultEnum.FORM_VALIDATE_PARAM_ERROR.getCode(),
					bindingResult.getFieldError().getDefaultMessage());
		}

		ResultVO resultVO = systemService.checkSign(request, BizMovePositionLogForm.class, bizMovePositionLogForm);

		if (resultVO.getCode() == 0) {
			bizMovePositionLogService.save(bizMovePositionLogForm);
			return ResultVOUtil.success();
		}
		return resultVO;
	}

}