package com.thinkgem.jeesite.modules.biz.web.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.thinkgem.jeesite.modules.biz.form.BizReturnBarterForm;
import com.thinkgem.jeesite.modules.biz.form.BizStockInSaveCopyForm;
import com.thinkgem.jeesite.modules.biz.service.SmReturnBarterService;
import com.thinkgem.jeesite.modules.biz.vo.ResultVO;
import com.thinkgem.jeesite.modules.sys.service.SystemService;
import com.thinkgem.jeesite.modules.sys.utils.ResultVOUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.beans.BeanUtils;
import java.net.URLEncoder;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * @author : xuchen
 * @date : 2018/9/12 14:40
 */
@Controller
@Slf4j
@RequestMapping(value = "${adminPath}/api/biz/returnBarter")
public class BizReturnBarterApiController {

    @Autowired
    private SystemService systemService;
    @Autowired
    private SmReturnBarterService smReturnBarterService;

    /**
     * 保存来自CRM退换货对象
     * @author xuchen
     * @date 2018/8/25 14:39
     * @param
     * @return
     */
    @RequestMapping(value = "save", method = RequestMethod.POST)

    @ResponseBody
    public ResultVO save(@RequestBody String code , HttpServletRequest request) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        BizReturnBarterForm bizReturnBarterForm = objectMapper.readValue(code, BizReturnBarterForm.class);
        //保存退换货
        ResultVO resultVO = new ResultVO();
        try{
            smReturnBarterService.saveFromCrm(bizReturnBarterForm);
            return ResultVOUtil.success();
        }
        catch (Exception e){
            return ResultVOUtil.error(1,"失败");
        }
    }
}
