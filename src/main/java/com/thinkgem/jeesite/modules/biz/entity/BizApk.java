/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.biz.entity;

import org.hibernate.validator.constraints.Length;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;

import com.thinkgem.jeesite.common.persistence.DataEntity;

/**
 * 安卓apkEntity
 * @author tiancheng
 * @version 2018-08-28
 */
public class BizApk extends DataEntity<BizApk> {
	
	private static final long serialVersionUID = 1L;
	private String appType;		// 升级的类型（android）
	private String version;		// 当前内部的大版本号
	private String versionCode;		// 当前对外展示的版本号
	private String apkUrl;		// apk升级地址
	private String upgradePoint;		// 更新信息
	private String status;		// apk的状态（0，正常，1，禁用）
	private Date createTime;		// 创建时间
	private Date updateTime;		// 修改时间
	private String isUpdate;		// 当前版本是否需要更新(0为不更新，1为更新，2为强制更新
	
	public BizApk() {
		super();
	}

	public BizApk(String id){
		super(id);
	}

	@Length(min=0, max=32, message="升级的类型（android）长度必须介于 0 和 32 之间")
	public String getAppType() {
		return appType;
	}

	public void setAppType(String appType) {
		this.appType = appType;
	}
	
	@Length(min=0, max=255, message="当前内部的大版本号长度必须介于 0 和 255 之间")
	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}
	
	@Length(min=0, max=255, message="当前对外展示的版本号长度必须介于 0 和 255 之间")
	public String getVersionCode() {
		return versionCode;
	}

	public void setVersionCode(String versionCode) {
		this.versionCode = versionCode;
	}
	
	@Length(min=0, max=255, message="apk升级地址长度必须介于 0 和 255 之间")
	public String getApkUrl() {
		return apkUrl;
	}

	public void setApkUrl(String apkUrl) {
		this.apkUrl = apkUrl;
	}
	
	@Length(min=0, max=255, message="更新信息长度必须介于 0 和 255 之间")
	public String getUpgradePoint() {
		return upgradePoint;
	}

	public void setUpgradePoint(String upgradePoint) {
		this.upgradePoint = upgradePoint;
	}
	
	@Length(min=0, max=1, message="apk的状态（0，正常，1，禁用）长度必须介于 0 和 1 之间")
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	
	@Length(min=0, max=1, message="当前版本是否需要更新(0为不更新，1为更新，2为强制更新长度必须介于 0 和 1 之间")
	public String getIsUpdate() {
		return isUpdate;
	}

	public void setIsUpdate(String isUpdate) {
		this.isUpdate = isUpdate;
	}
	
}