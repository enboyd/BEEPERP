/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.biz.entity;

import javax.validation.constraints.NotNull;
import org.hibernate.validator.constraints.Length;
import com.thinkgem.jeesite.modules.sys.entity.Area;
import com.thinkgem.jeesite.modules.sys.entity.User;

import com.thinkgem.jeesite.common.persistence.DataEntity;

/**
 * 仓库管理明细的controllerEntity
 * @author xuchen
 * @version 2018-07-16
 */
public class BizStorage extends DataEntity<BizStorage> {
	
	private static final long serialVersionUID = 1L;
	private Integer storageId;		// 仓库ID
	private String storageName;		// 仓库名称
	private String contact;		// 联系人
	private String phone;		// 联系电话
	private String type;		// 仓库类型
	private Area province;		// 省
	private Area city;		// 市
	private Area district;		// 区
	private String address;		// 地址

	public BizStorage() {
		super();
	}

	public BizStorage(String id){
		super(id);
	}

	public Integer getStorageId() {
		return storageId;
	}

	public void setStorageId(Integer storageId) {
		this.storageId = storageId;
	}
	
	@Length(min=1, max=64, message="仓库名称长度必须介于 1 和 64 之间")
	public String getStorageName() {
		return storageName;
	}

	public void setStorageName(String storageName) {
		this.storageName = storageName;
	}
	
	@Length(min=1, max=64, message="联系人长度必须介于 1 和 64 之间")
	public String getContact() {
		return contact;
	}

	public void setContact(String contact) {
		this.contact = contact;
	}
	
	@Length(min=1, max=18, message="联系电话长度必须介于 1 和 18 之间")
	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}
	
	@Length(min=1, max=1, message="仓库类型长度必须介于 1 和 1 之间")
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	@NotNull(message="省不能为空")
	public Area getProvince() {
		return province;
	}

	public void setProvince(Area province) {
		this.province = province;
	}
	
	@NotNull(message="市不能为空")
	public Area getCity() {
		return city;
	}

	public void setCity(Area city) {
		this.city = city;
	}
	
	@NotNull(message="区不能为空")
	public Area getDistrict() {
		return district;
	}

	public void setDistrict(Area district) {
		this.district = district;
	}
	
	@Length(min=0, max=256, message="地址长度必须介于 0 和 256 之间")
	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}
	
}