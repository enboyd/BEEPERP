package com.thinkgem.jeesite.modules.biz.form;

import lombok.Data;

/**
 * Description:
 * Author: LTY
 * Create Time: 2018-08-09 17:09
 */
@Data
public class BizGoodsGetForm {

    private String goodsId;		// 商品ID

}
