package com.thinkgem.jeesite.modules.biz.form;

import lombok.Data;
import org.hibernate.validator.constraints.NotEmpty;

import java.util.List;

/**
 * Description:
 * Author: LTY
 * Create Time: 2018-08-09 17:09
 */
@Data
public class BizStockInForm {

    private String purchaseId;//采购单的id

    //入库单明细的list
    private List<BizStockInDetailForm> bizStockInDetailFormList;

    private String createById;  //创建人id
    private String createByName;  //创建人name

    private String stockInId;  //入库单的id

    private int pageSize;		//条数
    private int pageNo;			//页码
}
