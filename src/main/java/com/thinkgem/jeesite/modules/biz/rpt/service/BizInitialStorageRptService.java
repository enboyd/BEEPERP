package com.thinkgem.jeesite.modules.biz.rpt.service;

import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.modules.biz.entity.BizBarcode;
import com.thinkgem.jeesite.modules.biz.entity.BizStockLog;
import com.thinkgem.jeesite.modules.biz.rpt.dao.BizInitialStorageRptDao;
import com.thinkgem.jeesite.modules.biz.rpt.entity.BizInitialOutage;
import com.thinkgem.jeesite.modules.biz.rpt.entity.BizInitialStorage;
import com.thinkgem.jeesite.modules.biz.service.BizBarcodeService;
import com.thinkgem.jeesite.modules.biz.service.BizStockLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 期初入库信息
 * @author : xuchen
 * @date : 2018/10/22 9:13
 */
@Service
@Transactional(readOnly = true)
public class BizInitialStorageRptService {

    @Autowired
    private BizInitialStorageRptDao bizInitialStorageRptDao;

    public Page<BizInitialStorage> findPage(Page<BizInitialStorage> page, BizInitialStorage bizInitialStorage) {
        bizInitialStorage.setPage(page);
        List<BizInitialStorage> bizInitialStorageList = bizInitialStorageRptDao.findList(bizInitialStorage);
        page.setList(bizInitialStorageList);
        return page;
    }

    public List<BizInitialStorage> findAllListByDate(Date stockInDateStart, Date stockInDateEnd,String goodsCode,String classifyId) {
        return bizInitialStorageRptDao.findAllListByDate(stockInDateStart,stockInDateEnd,goodsCode,classifyId);
    }
}
