package com.thinkgem.jeesite.modules.biz.rpt.entity;

import com.thinkgem.jeesite.common.persistence.DataEntity;
import com.thinkgem.jeesite.common.utils.excel.annotation.ExcelField;
import lombok.Data;

import java.util.Date;

/**
 * 期初出库信息
 * Created by xuchen on 2018/10/17.
 */
public class BizInitialOutage extends DataEntity<BizInitialOutage> {

    private String stockOutId; //出库单ID
    private String stockOutCode;  //单号(出库单编号)
    private Date outDate;   //出库日期
    private Date beginOutDate;   //开始出库日期(用于查询)
    private Date endOutDate;   //结束出库日期(用于查询)
    private String barId; //条码Id
    private String barcode; //条码编号
    private String goodsId; //存货id
    private String goodsCode; //存货编码
    private String goodsName; //存货名称
    private String batch;   //批次
    private Integer number;     //数量
    private String storageLocId;    //货位Id
    private String storageLocName;    //货位名称
    private String operatorId;  //操作人Id
    private String operatorName;  //操作人Name
    private String remarksType; //导出备注

    private String classifyId;	//商品分类ID
    private String classifyName;	//商品分类Name

    public String getStockOutId() {
        return stockOutId;
    }

    public void setStockOutId(String stockOutId) {
        this.stockOutId = stockOutId;
    }

    @ExcelField(title="单号", align=1, sort=1)
    public String getStockOutCode() {
        return stockOutCode;
    }

    public void setStockOutCode(String stockOutCode) {
        this.stockOutCode = stockOutCode;
    }

    @ExcelField(title="出库日期", align=1, sort=2)
    public Date getOutDate() {
        return outDate;
    }

    public void setOutDate(Date outDate) {
        this.outDate = outDate;
    }

    public Date getBeginOutDate() {
        return beginOutDate;
    }

    public void setBeginOutDate(Date beginOutDate) {
        this.beginOutDate = beginOutDate;
    }

    public Date getEndOutDate() {
        return endOutDate;
    }

    public void setEndOutDate(Date endOutDate) {
        this.endOutDate = endOutDate;
    }

    public String getBarId() {
        return barId;
    }

    public void setBarId(String barId) {
        this.barId = barId;
    }

    @ExcelField(title="条码号", align=1, sort=3)
    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public String getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(String goodsId) {
        this.goodsId = goodsId;
    }

    @ExcelField(title="存货编码", align=1, sort=4)
    public String getGoodsCode() {
        return goodsCode;
    }

    public void setGoodsCode(String goodsCode) {
        this.goodsCode = goodsCode;
    }

    @ExcelField(title="存货名称", align=1, sort=5)
    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    @ExcelField(title="批次", align=1, sort=6)
    public String getBatch() {
        return batch;
    }

    public void setBatch(String batch) {
        this.batch = batch;
    }

    @ExcelField(title="数量", align=1, sort=7)
    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public String getStorageLocId() {
        return storageLocId;
    }

    public void setStorageLocId(String storageLocId) {
        this.storageLocId = storageLocId;
    }

    @ExcelField(title="货位", align=1, sort=8)
    public String getStorageLocName() {
        return storageLocName;
    }

    public void setStorageLocName(String storageLocName) {
        this.storageLocName = storageLocName;
    }

    public String getOperatorId() {
        return operatorId;
    }

    public void setOperatorId(String operatorId) {
        this.operatorId = operatorId;
    }

    @ExcelField(title="操作人", align=1, sort=9)
    public String getOperatorName() {
        return operatorName;
    }

    public void setOperatorName(String operatorName) {
        this.operatorName = operatorName;
    }

    @ExcelField(title="备注", align=1, sort=10)
    public String getRemarksType() {
        return "期初出库";
    }

    public void setRemarksType(String remarksType) {
        this.remarksType = remarksType;
    }

    public String getClassifyId() {
        return classifyId;
    }

    public void setClassifyId(String classifyId) {
        this.classifyId = classifyId;
    }

    public String getClassifyName() {
        return classifyName;
    }

    public void setClassifyName(String classifyName) {
        this.classifyName = classifyName;
    }
}
