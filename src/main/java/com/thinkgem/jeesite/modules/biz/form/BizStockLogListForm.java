package com.thinkgem.jeesite.modules.biz.form;

import lombok.Data;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * Created by Administrator on 2018/9/13.
 */
@Data
public class BizStockLogListForm {

    private int pageSize;		//条数
    private int pageNo;			//页码
}
