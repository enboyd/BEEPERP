/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.biz.service;

import java.util.List;

import com.thinkgem.jeesite.common.utils.IdGen;
import com.thinkgem.jeesite.modules.sys.utils.UserUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.service.CrudService;
import com.thinkgem.jeesite.modules.biz.entity.BizStorage;
import com.thinkgem.jeesite.modules.biz.dao.BizStorageDao;

/**
 * 仓库管理明细的controllerService
 * @author xuchen
 * @version 2018-07-16
 */
@Service
@Transactional(readOnly = true)
public class BizStorageService extends CrudService<BizStorageDao, BizStorage> {

	@Autowired
	private BizStorageDao bizStorageDao;

	public BizStorage get(String id) {
		return super.get(id);
	}
	
	public List<BizStorage> findList(BizStorage bizStorage) {
		return super.findList(bizStorage);
	}
	
	public Page<BizStorage> findPage(Page<BizStorage> page, BizStorage bizStorage) {
		return super.findPage(page, bizStorage);
	}
	
	@Transactional(readOnly = false)
	public void save(BizStorage bizStorage) {
		//新增的时候添加信息
		/*if(null == bizStorage.getStorageId() || bizStorage.getStorageId().equals("")){
			bizStorage.setStorageId(IdGen.getUniqueKey());
		}*/
		super.save(bizStorage);
	}
	
	@Transactional(readOnly = false)
	public void delete(BizStorage bizStorage) {
		bizStorageDao.deleteByStorageId(bizStorage.getStorageId());
	}
	
}