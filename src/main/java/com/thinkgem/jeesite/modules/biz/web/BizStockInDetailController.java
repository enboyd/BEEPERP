/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.biz.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.thinkgem.jeesite.modules.biz.entity.BizPurchase;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.thinkgem.jeesite.common.config.Global;
import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.web.BaseController;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.modules.biz.entity.BizStockInDetail;
import com.thinkgem.jeesite.modules.biz.service.BizStockInDetailService;

/**
 * 入库单明细的ControllerController
 * @author tiancheng
 * @version 2018-07-16
 */
@Controller
@RequestMapping(value = "${adminPath}/biz/bizStockInDetail")
public class BizStockInDetailController extends BaseController {

	@Autowired
	private BizStockInDetailService bizStockInDetailService;
	
	@ModelAttribute
	public BizStockInDetail get(@RequestParam(required=false) String id) {
		BizStockInDetail entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = bizStockInDetailService.get(id);
		}
		if (entity == null){
			entity = new BizStockInDetail();
		}
		return entity;
	}
	
	@RequiresPermissions("biz:bizStockInDetail:view")
	@RequestMapping(value = {"list", ""})
	public String list(BizStockInDetail bizStockInDetail, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<BizStockInDetail> page = bizStockInDetailService.findPage(new Page<BizStockInDetail>(request, response), bizStockInDetail); 
		model.addAttribute("page", page);
		return "modules/biz/bizStockInDetailList";
	}

	@RequiresPermissions("biz:bizStockInDetail:view")
	@RequestMapping(value = "form")
	public String form(BizStockInDetail bizStockInDetail, Model model) {
		model.addAttribute("bizStockInDetail", bizStockInDetail);
		return "modules/biz/bizStockInDetailForm";
	}

	@RequiresPermissions(value={"biz:bizStockInDetail:add","biz:bizStockInDetail:edit"},logical= Logical.OR)
	@RequestMapping(value = "save")
	public String save(BizStockInDetail bizStockInDetail, Model model, RedirectAttributes redirectAttributes) {
		if (!beanValidator(model, bizStockInDetail)){
			return form(bizStockInDetail, model);
		}
		bizStockInDetailService.save(bizStockInDetail);
		addMessage(redirectAttributes, "保存入库单明细成功");
		return "redirect:"+Global.getAdminPath()+"/biz/bizStockInDetail/?repage";
	}
	
	@RequiresPermissions("biz:bizStockInDetail:remove")
	@RequestMapping(value = "delete")
	public String delete(BizStockInDetail bizStockInDetail, RedirectAttributes redirectAttributes) {
		bizStockInDetailService.delete(bizStockInDetail);
		addMessage(redirectAttributes, "删除入库单明细成功");
		return "redirect:"+Global.getAdminPath()+"/biz/bizStockInDetail/?repage";
	}

	
	 /**
	  * 入库
	  * @author tiancheng  
	  * @date 2018/7/16 15:21
	  * @param bizPurchase
	  * @return   
	  */
	@RequiresPermissions("biz:bizStockInDetail:add")
	@RequestMapping(value = "warehousing")
	public String warehousing(BizPurchase bizPurchase, RedirectAttributes redirectAttributes) {
		try {
			bizStockInDetailService.warehousing(bizPurchase);
			addMessage(redirectAttributes, "保存入库单明细成功!");
		} catch (Exception e) {
			addMessage(redirectAttributes, "保存入库单明细失败!");
			e.printStackTrace();
		}

		return "redirect:"+Global.getAdminPath()+"/biz/bizPurchase/list?type=1";
	}
	
}