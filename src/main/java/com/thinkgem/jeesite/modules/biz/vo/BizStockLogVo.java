/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.biz.vo;

import lombok.Data;

import java.util.Date;

/**
 * 期出期入的logEntity
 * @author tiancheng
 * @version 2018-09-13
 */
@Data
public class BizStockLogVo {

	private static final long serialVersionUID = 1L;
	private String name; //编号
	private String goodsName;  //商品名称
	private String goodsCode;  //商品编号
	private String storageLocationName;  //库位名称
	private Date createDate;  //创建时间
	private String createByName;  //创建人名称
	private String number;  // 数量
	private String type;  // 类型（0，期初，1，期入，2，正常发货）

}