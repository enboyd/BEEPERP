/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.biz.web.api;

import com.thinkgem.jeesite.common.web.BaseController;
import com.thinkgem.jeesite.modules.biz.dao.BizBarcodeLogDao;
import com.thinkgem.jeesite.modules.biz.entity.BizBarcode;
import com.thinkgem.jeesite.modules.biz.entity.BizBarcodeLog;
import com.thinkgem.jeesite.modules.biz.form.BizBarcodeByOrderForm;
import com.thinkgem.jeesite.modules.biz.form.BizBarcodeForm;
import com.thinkgem.jeesite.modules.biz.form.BizBarcodeListForm;
import com.thinkgem.jeesite.modules.biz.form.BizGoodsGetForm;
import com.thinkgem.jeesite.modules.biz.service.BizBarcodeService;
import com.thinkgem.jeesite.modules.biz.vo.BizBarcodeVO;
import com.thinkgem.jeesite.modules.biz.vo.BizGoodsVO;
import com.thinkgem.jeesite.modules.biz.vo.ResultVO;
import com.thinkgem.jeesite.modules.sys.enums.ResultEnum;
import com.thinkgem.jeesite.modules.sys.service.SystemService;
import com.thinkgem.jeesite.modules.sys.utils.ResultVOUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 条形码Controller
 * @author tiancheng
 * @version 2018-09-05
 */
@Controller
@Slf4j
@RequestMapping(value = "${adminPath}/api/biz/bizBarcode")
public class BizBarcodeApiController extends BaseController {

	@Autowired
	private BizBarcodeService bizBarcodeService;
	@Autowired
	private SystemService systemService;
	@Autowired
	private BizBarcodeLogDao bizBarcodeLogDao;

	 /**
	  * 手机端的扫码
	  * @author tiancheng
	  * @date 2018/9/8 14:59
	  * @param
	  * @return
	  */
	@RequestMapping(value = "get", method = RequestMethod.GET)
	@ResponseBody
	public ResultVO get(@Valid BizBarcodeForm bizBarcodeForm, BindingResult bindingResult, HttpServletRequest request, HttpServletResponse response) {

		if (bindingResult.hasErrors()) {
			log.error("【获取条码的详情失败】表单校验参数不正确：bizBarcodeForm => {}", bizBarcodeForm);
			return ResultVOUtil.error(ResultEnum.FORM_VALIDATE_PARAM_ERROR.getCode(),
					bindingResult.getFieldError().getDefaultMessage());
		}

		ResultVO resultVO = systemService.checkSign(request, BizBarcodeForm.class, bizBarcodeForm);

		if (resultVO.getCode() == 0) {
			BizBarcodeVO bizBarcode = bizBarcodeService.getApiByName(bizBarcodeForm.getName());
			if(null != bizBarcode){
				return ResultVOUtil.success(bizBarcode);
			}
			return ResultVOUtil.error(ResultEnum.BARCODE_INFO.getCode(),"暂无此条码，请重新扫描！");
		}
		return resultVO;
	}


	 /**
	  * 出库单的扫码
	  * @author tiancheng  
	  * @date 2018/9/19 15:31
	  * @param
	  * @return   
	  */  
	@RequestMapping(value = "getByOrder", method = RequestMethod.GET)
	@ResponseBody
	public ResultVO getByOrder(@Valid BizBarcodeByOrderForm bizBarcodeByOrderForm, BindingResult bindingResult, HttpServletRequest request, HttpServletResponse response) {

		if (bindingResult.hasErrors()) {
			log.error("【获取条码的详情失败】表单校验参数不正确：bizBarcodeByOrderForm => {}", bizBarcodeByOrderForm);
			return ResultVOUtil.error(ResultEnum.FORM_VALIDATE_PARAM_ERROR.getCode(),
					bindingResult.getFieldError().getDefaultMessage());
		}

		ResultVO resultVO = systemService.checkSign(request, BizBarcodeByOrderForm.class, bizBarcodeByOrderForm);

		if (resultVO.getCode() == 0) {
			return bizBarcodeService.getApiByName(bizBarcodeByOrderForm);
		}
		return resultVO;
	}


	 /**
	  * 根据商品的id查询条码的列表（按照生产日期排序）
	  * @author tiancheng
	  * @date 2018/9/19 9:13
	  * @param
	  * @return
	  */
	@RequestMapping(value = "list", method = RequestMethod.GET)
	@ResponseBody
	public ResultVO list(@Valid BizBarcodeListForm bizBarcodeListForm, BindingResult bindingResult, HttpServletRequest request, HttpServletResponse response) {

		if (bindingResult.hasErrors()) {
			log.error("【获取条码的列表失败】表单校验参数不正确：bizBarcodeListForm => {}", bizBarcodeListForm);
			return ResultVOUtil.error(ResultEnum.FORM_VALIDATE_PARAM_ERROR.getCode(),
					bindingResult.getFieldError().getDefaultMessage());
		}
		ResultVO resultVO = systemService.checkSign(request, BizBarcodeListForm.class, bizBarcodeListForm);
		if (resultVO.getCode() == 0) {
			List<BizBarcodeVO> bizBarcodeVOList = bizBarcodeService.findListApiNo21(bizBarcodeListForm);

			//获取当前订单，当前商品的本次出库数
			BizBarcodeLog bizBarcodeLog = new BizBarcodeLog();
			bizBarcodeLog.setOrderId(bizBarcodeListForm.getOrderId());
			bizBarcodeLog.setGoodsId(bizBarcodeListForm.getGoodsId());
			Integer sum = bizBarcodeLogDao.selectSumByOrderIdAndGoodsId(bizBarcodeLog);

			Map map = new HashMap();
			map.put("bizBarcodeVOList",bizBarcodeVOList);
			map.put("sum",sum);
			return ResultVOUtil.success(map);
		}
		return resultVO;
	}



}