package com.thinkgem.jeesite.modules.biz.rpt.dao;

import com.thinkgem.jeesite.common.persistence.CrudDao;
import com.thinkgem.jeesite.common.persistence.annotation.MyBatisDao;
import com.thinkgem.jeesite.modules.biz.rpt.entity.BizInitialOutage;
import com.thinkgem.jeesite.modules.biz.rpt.entity.BizInitialStorage;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

/**
 * Created by xuchen on 2018/10/23.
 */
@MyBatisDao
public interface BizInitialOutageRptDao extends CrudDao<BizInitialOutage> {

    List<BizInitialOutage> findAllListByDate(@Param("beginOutDate") Date beginOutDate,
                                             @Param("endOutDate") Date endOutDate,
                                             @Param("goodsCode") String goodsCode,
                                             @Param("classifyId") String classifyId);
}
