/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.biz.dao;

import com.thinkgem.jeesite.common.persistence.CrudDao;
import com.thinkgem.jeesite.common.persistence.annotation.MyBatisDao;
import com.thinkgem.jeesite.modules.biz.entity.BizStorage;

/**
 * 仓库管理明细的controllerDAO接口
 * @author xuchen
 * @version 2018-07-16
 */
@MyBatisDao
public interface BizStorageDao extends CrudDao<BizStorage> {

    void deleteByStorageId(Integer storageId);

    BizStorage findByName(String storageName);
}