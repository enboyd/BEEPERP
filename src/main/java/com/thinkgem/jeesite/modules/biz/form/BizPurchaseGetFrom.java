/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.biz.form;

import lombok.Data;

/**
 * 请购单的Entity
 * @author tiancheng
 * @version 2018-08-24
 */
@Data
public class BizPurchaseGetFrom{
	
	private String purchaseId;		// 请购单ID

}