package com.thinkgem.jeesite.modules.biz.vo;

import lombok.Data;

import java.io.Serializable;

/**
 * Description:
 * Author: stc
 * Create Time: 2018-08-09 19:46
 */
@Data
public class BizBarcodeVO implements Serializable {

    private String id;
    private String name;
    private String url;
    private String number;
    private String manufactureDate;
    private String goodsName;
    private String goodsCode;
    private String storageLocationName;
    private String storageLocationId;
    private String surplusNumber;
    private String goodsId;
}
