package com.thinkgem.jeesite.modules.biz.form;

import lombok.Data;

import java.util.Date;

/**
 * @author : xuchen
 * @date : 2018/9/12 15:02
 */
@Data
public class BizReturnBarterDetailForm {
    private String returnBarterId;		// 退换货单id
    private String goodsId;		// 商品id
    private String goodsCode;		// 商品id
    private String goodsName;		// 商品名称
    private String specifications;		// 规格
    private Integer quantity;		// 数量
    private String type;		// 退货/换货
    private String createBy;    //创建人id
    private String createByName;    //创建人name
    private Date createDate;    //创建日期
    private String updateBy;    //更新人id
    private String updateByName;    //更新人name
    private Date updateDate;    //更新日期
    private String remarks;     //备注
    private Double unitPrice;	//单价
    private Integer stockAmount;	//库存
    private String isDeliver;  // 是否发货（0，立即发货，1，等待发货）
}
