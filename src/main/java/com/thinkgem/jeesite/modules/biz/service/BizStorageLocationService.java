/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.biz.service;

import java.util.List;

import com.thinkgem.jeesite.modules.biz.vo.BizStorageLocationVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.thinkgem.jeesite.common.service.TreeService;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.modules.biz.entity.BizStorageLocation;
import com.thinkgem.jeesite.modules.biz.dao.BizStorageLocationDao;

/**
 * 库位管理Service
 * @author xuchen
 * @version 2018-07-17
 */
@Service
@Transactional(readOnly = true)
public class BizStorageLocationService extends TreeService<BizStorageLocationDao, BizStorageLocation> {

	@Autowired
	private BizStorageLocationDao bizStorageLocationDao;

	public BizStorageLocation get(String id) {
		return super.get(id);
	}
	
	public List<BizStorageLocation> findList(BizStorageLocation bizStorageLocation) {
		if (StringUtils.isNotBlank(bizStorageLocation.getParentIds())){
			bizStorageLocation.setParentIds(","+bizStorageLocation.getParentIds()+",");
		}
		return super.findList(bizStorageLocation);
	}

	 /**
	  * 手机端查询仓库货位的列表
	  * @author tiancheng
	  * @date 2018/8/30 10:47
	  * @param
	  * @return
	  */
	public List<BizStorageLocationVo> findListApi(BizStorageLocation bizStorageLocation){
		return bizStorageLocationDao.findListApi(bizStorageLocation);
	}
	
	@Transactional(readOnly = false)
	public void save(BizStorageLocation bizStorageLocation) {
		super.save(bizStorageLocation);
	}
	
	@Transactional(readOnly = false)
	public void delete(BizStorageLocation bizStorageLocation) {
		if (bizStorageLocation != null){
			bizStorageLocationDao.deleteById(bizStorageLocation);
		}
	}
	
}