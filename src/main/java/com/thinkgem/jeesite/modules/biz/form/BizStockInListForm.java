package com.thinkgem.jeesite.modules.biz.form;

import lombok.Data;

import java.util.List;

/**
 * Description:
 * Author: LTY
 * Create Time: 2018-08-09 17:09
 */
@Data
public class BizStockInListForm {

    private int pageSize;		//条数
    private int pageNo;			//页码
}
