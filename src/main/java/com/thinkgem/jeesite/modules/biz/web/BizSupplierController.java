/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.biz.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.thinkgem.jeesite.common.config.Global;
import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.web.BaseController;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.modules.biz.entity.BizSupplier;
import com.thinkgem.jeesite.modules.biz.service.BizSupplierService;

/**
 * 供应商的ControllerController
 * @author tiancheng
 * @version 2018-07-13
 */
@Controller
@RequestMapping(value = "${adminPath}/biz/bizSupplier")
public class BizSupplierController extends BaseController {

	@Autowired
	private BizSupplierService bizSupplierService;
	
	@ModelAttribute
	public BizSupplier get(@RequestParam(required=false) String id) {
		BizSupplier entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = bizSupplierService.get(id);
		}
		if (entity == null){
			entity = new BizSupplier();
		}
		return entity;
	}
	
	@RequiresPermissions("biz:bizSupplier:view")
	@RequestMapping(value = {"list", ""})
	public String list(BizSupplier bizSupplier, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<BizSupplier> page = bizSupplierService.findPage(new Page<BizSupplier>(request, response), bizSupplier); 
		model.addAttribute("page", page);
		return "modules/biz/bizSupplierList";
	}

	@RequiresPermissions("biz:bizSupplier:view")
	@RequestMapping(value = "form")
	public String form(BizSupplier bizSupplier, Model model) {
		model.addAttribute("bizSupplier", bizSupplier);
		return "modules/biz/bizSupplierForm";
	}

	@RequiresPermissions(value={"biz:bizSupplier:add","biz:bizSupplier:edit"},logical= Logical.OR)
	@RequestMapping(value = "save")
	public String save(BizSupplier bizSupplier, Model model, RedirectAttributes redirectAttributes) {
		if (!beanValidator(model, bizSupplier)){
			return form(bizSupplier, model);
		}
		bizSupplierService.save(bizSupplier);
		addMessage(redirectAttributes, "保存供应商成功");
		return "redirect:"+Global.getAdminPath()+"/biz/bizSupplier/?repage";
	}
	
	@RequiresPermissions("biz:bizSupplier:remove")
	@RequestMapping(value = "delete")
	public String delete(BizSupplier bizSupplier, RedirectAttributes redirectAttributes) {
		bizSupplierService.delete(bizSupplier);
		addMessage(redirectAttributes, "删除供应商成功");
		return "redirect:"+Global.getAdminPath()+"/biz/bizSupplier/?repage";
	}


	@RequiresPermissions(value = {"biz:supplier:dismissApproval","biz:supplier:passApproval","biz:supplier:startApproval"},logical = Logical.OR)
	@RequestMapping(value = "approval")
	public String approval(BizSupplier bizSupplier, RedirectAttributes redirectAttributes,String type) {
		if(StringUtils.isNotEmpty(type)){
			if("anew".equals(type)){
				bizSupplierService.approval(bizSupplier.getSupplierId(),"5");
				addMessage(redirectAttributes, "提交成功");
			}else if("pass".equals(type)){
				bizSupplierService.approval(bizSupplier.getSupplierId(),"15");
			}else if("dismiss".equals(type)){
				bizSupplierService.approval(bizSupplier.getSupplierId(),"10");
				addMessage(redirectAttributes, "驳回成功");
			}
		}
		return "redirect:"+Global.getAdminPath()+"/biz/bizSupplier/?repage";
	}
}