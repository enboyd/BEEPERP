/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.biz.web.api;

import com.thinkgem.jeesite.common.web.BaseController;
import com.thinkgem.jeesite.modules.biz.form.BizStockLogListForm;
import com.thinkgem.jeesite.modules.biz.form.BizStockLogSaveForm;
import com.thinkgem.jeesite.modules.biz.service.BizStockLogService;
import com.thinkgem.jeesite.modules.biz.vo.BizStockLogVo;
import com.thinkgem.jeesite.modules.biz.vo.ResultVO;
import com.thinkgem.jeesite.modules.sys.enums.ResultEnum;
import com.thinkgem.jeesite.modules.sys.service.SystemService;
import com.thinkgem.jeesite.modules.sys.utils.ResultVOUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 期出期入的logController
 * @author tiancheng
 * @version 2018-09-13
 */
@Controller
@Slf4j
@RequestMapping(value = "${adminPath}/api/biz/bizStockLog")
public class BizStockLogApiController extends BaseController {

	@Autowired
	private BizStockLogService bizStockLogService;
	@Autowired
	private SystemService systemService;


	/**
	 * 手机端的期初期入的列表
	 * @author tiancheng
	 * @date 2018/9/13 10:23
	 * @param
	 * @return
	 */
	@RequestMapping(value = "list", method = RequestMethod.GET)
	@ResponseBody
	public ResultVO list(@Valid BizStockLogListForm bizStockLogListForm, BindingResult bindingResult, HttpServletRequest request) {
		if (bindingResult.hasErrors()) {
			log.error("【查询期初期入列表异常】表单校验参数不正确：bizStockLogListForm => {}", bizStockLogListForm);
			return ResultVOUtil.error(ResultEnum.FORM_VALIDATE_PARAM_ERROR.getCode(),
					bindingResult.getFieldError().getDefaultMessage());
		}

		ResultVO resultVO = systemService.checkSign(request, BizStockLogListForm.class, bizStockLogListForm);

		if (resultVO.getCode() == 0) {
			List<BizStockLogVo> bizStockLogVoList = bizStockLogService.findListApi(bizStockLogListForm);
			Map map = new HashMap();
			map.put("info",bizStockLogVoList);
			return ResultVOUtil.success(map);
		}
		return resultVO;
	}

	 /**
	  * 手机端的期初期入的添加
	  * @author tiancheng
	  * @date 2018/9/13 10:23
	  * @param
	  * @return
	  */
	@RequestMapping(value = "save", method = RequestMethod.POST)
	@ResponseBody
	public ResultVO save(@Valid BizStockLogSaveForm bizStockLogSaveForm, BindingResult bindingResult, HttpServletRequest request) {
		if (bindingResult.hasErrors()) {
			log.error("【添加期初期入异常】表单校验参数不正确：bizStockLogSaveForm => {}", bizStockLogSaveForm);
			return ResultVOUtil.error(ResultEnum.FORM_VALIDATE_PARAM_ERROR.getCode(),
					bindingResult.getFieldError().getDefaultMessage());
		}
		if(null != bizStockLogSaveForm.getManufactureDate()){
			try {
				bizStockLogSaveForm.setManufactureDate(URLEncoder.encode(bizStockLogSaveForm.getManufactureDate(), "utf-8"));
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
		}

		ResultVO resultVO = systemService.checkSign(request, BizStockLogSaveForm.class, bizStockLogSaveForm);

		if (resultVO.getCode() == 0) {
			return bizStockLogService.save(bizStockLogSaveForm);
		}
		return resultVO;
	}
}