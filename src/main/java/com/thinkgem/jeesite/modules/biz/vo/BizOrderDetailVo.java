/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.biz.vo;

import lombok.Data;

/**
 * 订单Entity
 * @author tiancheng
 * @version 2018-09-17
 */
@Data
public class BizOrderDetailVo {

	private String id;   // 订单明细的id
	private String goodsName;		// 商品名称
	private String specifications;   // 规格
	private String code;   // 商品编号
	private String quantity;   // 数量
	private String goodsId;  //商品id
	private Integer actualNumber;  //实际出库数
	private String isDeliver;   //是否发货（0，可发货，1，等待发货，3，已发货）
	private String goodsType; // 出库时判断是条码出库还是设备出库
}