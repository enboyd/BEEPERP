/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.biz.web.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.thinkgem.jeesite.common.utils.MD5util;
import com.thinkgem.jeesite.common.web.BaseController;
import com.thinkgem.jeesite.modules.biz.form.*;
import com.thinkgem.jeesite.modules.biz.service.BizStockInService;
import com.thinkgem.jeesite.modules.biz.vo.BizStockInVo;
import com.thinkgem.jeesite.modules.biz.vo.ResultVO;
import com.thinkgem.jeesite.modules.sys.enums.ResultEnum;
import com.thinkgem.jeesite.modules.sys.service.SystemService;
import com.thinkgem.jeesite.modules.sys.utils.ResultVOUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.IOException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 手机端入库单的Controller
 * @author tiancheng
 * @version 2018-08-24
 */
@Controller
@Slf4j
@RequestMapping(value = "${adminPath}/api/biz/bizStockIn")
public class BizStockInApiController extends BaseController {

	@Autowired
	private BizStockInService bizStockInService;
	@Autowired
	private SystemService systemService;


	 /**
	  * 手机端查询入库单的列表
	  * @author tiancheng
	  * @date 2018/8/29 10:09
	  * @param
	  * @return
	  */
	@RequestMapping(value = "list", method = RequestMethod.GET)
	@ResponseBody
	public ResultVO list(@Valid BizStockInListForm bizStockInListForm, BindingResult bindingResult, HttpServletRequest request) {

		if (bindingResult.hasErrors()) {
			log.error("【查询入库单列表异常】表单校验参数不正确：bizStockInListForm => {}", bizStockInListForm);
			return ResultVOUtil.error(ResultEnum.FORM_VALIDATE_PARAM_ERROR.getCode(),
					bindingResult.getFieldError().getDefaultMessage());
		}

		ResultVO resultVO = systemService.checkSign(request, BizStockInListForm.class, bizStockInListForm);

		if (resultVO.getCode() == 0) {
			List<BizStockInVo> bizStockInVoList = bizStockInService.findListApi(bizStockInListForm);
			Map map = new HashMap();
			if(null != bizStockInVoList && bizStockInVoList.size() > 0){
				map.put("bizStockInVoList",bizStockInVoList);
				return ResultVOUtil.success(map);
			}
		}
		return resultVO;
	}


	 /**
	  * 手机端查询入库单的详情
	  * @author tiancheng
	  * @date 2018/8/29 10:09
	  * @param
	  * @return
	  */
	@RequestMapping(value = "get", method = RequestMethod.GET)
	@ResponseBody
	public ResultVO get(@Valid BizStockInGetForm bizStockInGetForm, BindingResult bindingResult, HttpServletRequest request) {

		if (bindingResult.hasErrors()) {
			log.error("【查询入库单详情异常】表单校验参数不正确：bizStockInGetForm => {}", bizStockInGetForm);
			return ResultVOUtil.error(ResultEnum.FORM_VALIDATE_PARAM_ERROR.getCode(),
					bindingResult.getFieldError().getDefaultMessage());
		}

		ResultVO resultVO = systemService.checkSign(request, BizStockInGetForm.class, bizStockInGetForm);

		if (resultVO.getCode() == 0) {
			BizStockInVo bizStockInVo = bizStockInService.getApi(bizStockInGetForm);
			return ResultVOUtil.success(bizStockInVo);
		}
		return resultVO;
	}

	 /**
	  * 手机端添加入库单
	  * @author tiancheng  
	  * @date 2018/8/25 14:39
	  * @param
	  * @return   
	  */  
	@RequestMapping(value = "save", method = RequestMethod.POST)
	@ResponseBody
	public ResultVO save(@RequestBody String code , HttpServletRequest request) throws IOException {
		ObjectMapper objectMapper = new ObjectMapper();
		BizStockInSaveForm bizStockInSaveForm = objectMapper.readValue(code, BizStockInSaveForm.class);
		//复制信息，只验证入库单的信息
		BizStockInSaveCopyForm bizStockInSaveCopyForm = new BizStockInSaveCopyForm();
		BeanUtils.copyProperties(bizStockInSaveForm, bizStockInSaveCopyForm);
		//创建人（汉字加密不能通过，先将汉字URL加密）
		bizStockInSaveCopyForm.setCreateByName(URLEncoder.encode(bizStockInSaveCopyForm.getCreateByName(), "utf-8"));

//		if (bindingResult.hasErrors()) {
//			log.error("【查询采购单列表异常】表单校验参数不正确：bizStockInForm => {}", bizStockInForm);
//			return ResultVOUtil.error(ResultEnum.FORM_VALIDATE_PARAM_ERROR.getCode(),
//					bindingResult.getFieldError().getDefaultMessage());
//		}

		ResultVO resultVO = systemService.checkSign(request, BizStockInSaveCopyForm.class, bizStockInSaveCopyForm);

		if (resultVO.getCode() == 0) {
			try {
				bizStockInService.saveApi(bizStockInSaveForm);
				return ResultVOUtil.success();
			} catch (Exception e) {
				e.printStackTrace();
			}
			return ResultVOUtil.error();
		}
 		return resultVO;
	}



}