/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.biz.dao;

import com.thinkgem.jeesite.common.persistence.CrudDao;
import com.thinkgem.jeesite.common.persistence.annotation.MyBatisDao;
import com.thinkgem.jeesite.modules.biz.entity.BizMovePositionLog;
import com.thinkgem.jeesite.modules.biz.vo.BizMovePositionLogVo;

import java.util.List;

/**
 * 移库DAO接口
 * @author tiancheng
 * @version 2018-09-12
 */
@MyBatisDao
public interface BizMovePositionLogDao extends CrudDao<BizMovePositionLog> {

    List<BizMovePositionLogVo> findListApi(BizMovePositionLog bizMovePositionLog);
}