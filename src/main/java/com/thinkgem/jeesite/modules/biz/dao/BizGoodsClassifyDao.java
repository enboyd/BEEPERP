/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.biz.dao;

import com.thinkgem.jeesite.common.persistence.TreeDao;
import com.thinkgem.jeesite.common.persistence.annotation.MyBatisDao;
import com.thinkgem.jeesite.modules.biz.entity.BizGoodsClassify;

import java.util.List;

/**
 * 商品分类DAO接口
 * @author xuchen
 * @version 2018-10-12
 */
@MyBatisDao
public interface BizGoodsClassifyDao extends TreeDao<BizGoodsClassify> {

    List<BizGoodsClassify> findAllParentList();

    List<BizGoodsClassify> findChildListByPId(String pid);

    BizGoodsClassify findByName(String classifyName);
}