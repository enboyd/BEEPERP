package com.thinkgem.jeesite.modules.biz.form;

import lombok.Data;

import java.util.List;

/**
 * Description:
 * Author: LTY
 * Create Time: 2018-08-09 17:09
 */
@Data
public class BizStockInGetForm {

    private String stockInId;  //入库单的id

}
