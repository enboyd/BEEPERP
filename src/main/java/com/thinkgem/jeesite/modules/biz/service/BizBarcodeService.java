/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.biz.service;

import java.util.List;

import com.thinkgem.jeesite.modules.biz.form.BizBarcodeByOrderForm;
import com.thinkgem.jeesite.modules.biz.form.BizBarcodeForm;
import com.thinkgem.jeesite.modules.biz.form.BizBarcodeListForm;
import com.thinkgem.jeesite.modules.biz.vo.BizBarcodeVO;
import com.thinkgem.jeesite.modules.biz.vo.ResultVO;
import com.thinkgem.jeesite.modules.sys.enums.ResultEnum;
import com.thinkgem.jeesite.modules.sys.utils.ResultVOUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.service.CrudService;
import com.thinkgem.jeesite.modules.biz.entity.BizBarcode;
import com.thinkgem.jeesite.modules.biz.dao.BizBarcodeDao;

/**
 * 条形码Service
 * @author tiancheng
 * @version 2018-09-05
 */
@Service
@Transactional(readOnly = true)
public class BizBarcodeService extends CrudService<BizBarcodeDao, BizBarcode> {

	@Autowired
	private BizBarcodeDao bizBarcodeDao;

	public BizBarcode get(String id) {
		return super.get(id);
	}

	public BizBarcodeVO getApiByName(String name) {
		return bizBarcodeDao.getApiByName(name);
	}

	 /**
	  * 出库单的扫码
	  * @author tiancheng  
	  * @date 2018/9/19 15:43
	  * @param
	  * @return   
	  */  
	public ResultVO getApiByName(BizBarcodeByOrderForm bizBarcodeByOrderForm) {
		BizBarcodeVO bizBarcodeVO = bizBarcodeDao.getApiByName(bizBarcodeByOrderForm.getName());
		if(null == bizBarcodeVO){
			return ResultVOUtil.error(ResultEnum.BARCODE_INFO.getCode(),"暂无此条码，请重新扫描！");
		}
		if(bizBarcodeVO.getStorageLocationName().equals("21区")){
			return ResultVOUtil.error(ResultEnum.STORAGE_NOT_ALLOW.getCode(),"21区的商品不允许出库!");
		}
		if(bizBarcodeByOrderForm.getGoodsId().equals(bizBarcodeVO.getGoodsId())){
			return ResultVOUtil.success(bizBarcodeVO);
		}else{
			return ResultVOUtil.error(ResultEnum.PRODUCT_CATEGORY_ERROR.getCode(),"不是当前商品，请进行确认！");
		}

	}

	public List<BizBarcode> findList(BizBarcode bizBarcode) {
		return super.findList(bizBarcode);
	}

	public List<BizBarcode> findAllList(BizBarcode bizBarcode) {
		return bizBarcodeDao.findAllList(bizBarcode);
	}
	
	public Page<BizBarcode> findPage(Page<BizBarcode> page, BizBarcode bizBarcode) {
		return super.findPage(page, bizBarcode);
	}
	
	@Transactional(readOnly = false)
	public void save(BizBarcode bizBarcode) {
		super.save(bizBarcode);
	}
	
	@Transactional(readOnly = false)
	public void delete(BizBarcode bizBarcode) {
		super.delete(bizBarcode);
	}


	public List<BizBarcodeVO> findListApi(BizBarcodeListForm bizBarcodeListForm){
		BizBarcode bizBarcode = new BizBarcode();
		bizBarcode.setGoodsId(bizBarcodeListForm.getGoodsId());
		return bizBarcodeDao.findListApi(bizBarcode);
	}

    public BizBarcode getAll(String barcodeId) {
		return bizBarcodeDao.getAll(barcodeId);
    }

	public List<BizBarcodeVO> findListApiNo21(BizBarcodeListForm bizBarcodeListForm){
		BizBarcode bizBarcode = new BizBarcode();
		bizBarcode.setGoodsId(bizBarcodeListForm.getGoodsId());
		return bizBarcodeDao.findListApiNo21(bizBarcode);
	}

	/**
	 * 条码打印次数增加
	 * @author xuchen
	 * @date 2018/12/03 14:45
	 * @param
	 * @return
	 */
	@Transactional(readOnly = false)
    public void printNumberAdd(String barCode) {
		bizBarcodeDao.printNumberAdd(barCode);
    }
}