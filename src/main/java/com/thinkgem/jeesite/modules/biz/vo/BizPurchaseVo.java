/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.biz.vo;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * 请购单的Entity
 * @author xuchen
 * @version 2018-07-28
 */
@Data
public class BizPurchaseVo implements Serializable {

	private static final long serialVersionUID = 1L;
	private String purchaseId;		// 采购单ID
	private String purchaseCode;		// 采购单编号
	private Date purchaseDate;		// 采购日期
	private String totalPrice;		// 采购总额
	private String status;		// 请购单状态  待提交、待审批、已驳回、已通过、完成
	private String detailGoodsName;   //明细的商品名
	private List<BizPurchaseDetailVo> bizPurchaseDetailVoList; //采购单的明细
	private List<BizStockInVo> bizStockInVoList;  //采购单下的入库单
}