/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.biz.vo;

import lombok.Data;

import java.util.List;

/**
 * 订单Entity
 * @author tiancheng
 * @version 2018-09-17
 */
@Data
public class BizOrderVo {

	private String id;   // 订单的id
	private String teamName;		// 团队名称
	private String consigneeName;   // 收货人姓名
	private String address;   // 地址
	private String phone; //手机号
	private String orderStatus; // 订单的状态 （0，待出库，1，已出库)
	private List<BizOrderDetailVo> bizOrderDetailVoList;  // 订单明细
}