/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.biz.dao;

import com.thinkgem.jeesite.common.persistence.CrudDao;
import com.thinkgem.jeesite.common.persistence.annotation.MyBatisDao;
import com.thinkgem.jeesite.modules.biz.entity.BizDiscardedGoods;

/**
 * 废弃商品DAO接口
 * @author xuchen
 * @version 2018-11-02
 */
@MyBatisDao
public interface BizDiscardedGoodsDao extends CrudDao<BizDiscardedGoods> {
	
}