/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.biz.dao;

import com.thinkgem.jeesite.common.persistence.CrudDao;
import com.thinkgem.jeesite.common.persistence.annotation.MyBatisDao;
import com.thinkgem.jeesite.modules.biz.entity.BizStockIn;
import com.thinkgem.jeesite.modules.biz.vo.BizStockInVo;

import java.util.List;

/**
 * 入库单的ControllerDAO接口
 * @author tianCheng
 * @version 2018-07-16
 */
@MyBatisDao
public interface BizStockInDao extends CrudDao<BizStockIn> {

    List<BizStockInVo> findListApi(BizStockIn bizStockIn);

    BizStockInVo getApi(String stockInId);

    void updateStatus(BizStockIn bizStockIn);
}