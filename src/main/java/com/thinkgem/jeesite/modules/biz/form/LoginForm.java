package com.thinkgem.jeesite.modules.biz.form;

import lombok.Data;

/**
 * 用户登录Form
 * @author : xuchen
 * @date : 2018/8/24 15:20
 */
@Data
public class LoginForm {

    
    private String userName;		// 用户名
    private String password;		// 密码
}
