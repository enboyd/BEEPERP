/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.biz.dao;

import com.thinkgem.jeesite.common.persistence.CrudDao;
import com.thinkgem.jeesite.common.persistence.annotation.MyBatisDao;
import com.thinkgem.jeesite.modules.biz.entity.BizPurchaseLog;

/**
 * 采购单日志的ControllerDAO接口
 * @author tiancheng
 * @version 2018-07-17
 */
@MyBatisDao
public interface BizPurchaseLogDao extends CrudDao<BizPurchaseLog> {
	
}