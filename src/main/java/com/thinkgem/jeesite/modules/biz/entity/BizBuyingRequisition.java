/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.biz.entity;

import org.hibernate.validator.constraints.Length;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import javax.validation.constraints.NotNull;
import com.thinkgem.jeesite.modules.sys.entity.User;

import com.thinkgem.jeesite.common.persistence.DataEntity;

/**
 * 请购单的Entity
 * @author xuchen
 * @version 2018-07-28
 */
public class BizBuyingRequisition extends DataEntity<BizBuyingRequisition> {
	
	private static final long serialVersionUID = 1L;
	private String buyingId;		// 请购单ID
	private String buyingCode;		// 请购单编号
	private Date buyingDate;		// 请购日期
	private String status;		// 请购单状态  待提交、待审批、已驳回、已通过、完成
	private Date beginBuyingDate;		// 开始 请购日期
	private Date endBuyingDate;		// 结束 请购日期
	private String type;	//请购单类型(正常请购;其他请购)

	public List<BizBuyingRequisitionDetail> bizBuyingRequisitionList;//明细

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public BizBuyingRequisition() {
		super();
	}

	public BizBuyingRequisition(String id){
		super(id);
	}

	public String getBuyingId() {
		return buyingId;
	}

	public void setBuyingId(String buyingId) {
		this.buyingId = buyingId;
	}
	
	public String getBuyingCode() {
		return buyingCode;
	}

	public void setBuyingCode(String buyingCode) {
		this.buyingCode = buyingCode;
	}
	
	public Date getBuyingDate() {
		return buyingDate;
	}

	public void setBuyingDate(Date buyingDate) {
		this.buyingDate = buyingDate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getBeginBuyingDate() {
		return beginBuyingDate;
	}

	public void setBeginBuyingDate(Date beginBuyingDate) {
		this.beginBuyingDate = beginBuyingDate;
	}
	
	public Date getEndBuyingDate() {
		return endBuyingDate;
	}

	public void setEndBuyingDate(Date endBuyingDate) {
		this.endBuyingDate = endBuyingDate;
	}

	public List<BizBuyingRequisitionDetail> getBizBuyingRequisitionList() {
		return bizBuyingRequisitionList;
	}

	public void setBizBuyingRequisitionList(List<BizBuyingRequisitionDetail> bizBuyingRequisitionList) {
		this.bizBuyingRequisitionList = bizBuyingRequisitionList;
	}
}