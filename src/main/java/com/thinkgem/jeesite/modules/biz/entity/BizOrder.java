/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.biz.entity;

import com.google.common.collect.Lists;
import org.hibernate.validator.constraints.Length;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;

import com.thinkgem.jeesite.common.persistence.DataEntity;

/**
 * 订单Entity
 * @author tiancheng
 * @version 2018-09-17
 */
public class BizOrder extends DataEntity<BizOrder> {

	private static final long serialVersionUID = 1L;
	private String teamId;        // 团队ID
	private String teamName;        // 团队名称
	private String sellerId;        // 销售ID
	private String sellerName;        // 销售名称
	private String customerServiceRep;        // 专属客服ID
	private String customerServiceName;        // 专属客服名称
	private String customerId;        // 客户ID
	private String customerName;        // 客户名称
	private String projectId;        // 所属项目ID
	private String systemOutLibrary;        // 系统出库
	private String consigneeName;        // 收货人姓名
	private String moneyMakerName;        // 打款人姓名
	private String phone;        // 电话
	private String address;        // 地址
	private String customerRemarks;        // 发货时备注
	private String name;        // 订货编号
	private Date orderTime;        // 订货时间
	private String discount;        // 折扣
	private String orderAmount;        // 订货金额
	private String couponAmount;        // 优惠券金额
	private String amountReceivable;        // 应收金额
	private String amountCollected;        // 实收金额
	private String number;        // 数量
	private String settlementMethod;        // 结算方式
	private String orderType;        // 出库类别(1:退换货,0:正常出库)
	private String haveColdChain;        // 有无冷链
	private String allocationId;        // 配货人ID
	private String allocationName;        // 配货人name
	private String procInsId;        // 审批实例
	private String taskState;        // 任务状态

	private String materielId;        // 货物ID
	private String materielName;        // 产品名称
	private String code;        // 产品编码
	private String unitPrice;        // 单价
	private String specifications;        // 规格
	private String profit;        // 利润
	private String orderStatus;  // 订单的状态 （0，待出库，1，已出库）
	private String deliverType;  // 发货方式（0，正常发货,1，立即发货，2，补全发货）
	private String createByName;    //创建人
	private String[] orderIds; //订单的id数组
	private String content; //审核意见
	private String auditorName;    //审核人名称
	private Date auditDate;    //审核日期

	private List<BizOrderdetail> bizOrderDetailList = Lists.newArrayList();        // 出库单明细

	private String tmUrl;  // 条码的路径
	private String orderApprovalStatus;    //订单审批状态
	private String orderGenre;    //出库单类型(10:标配，20:酱料，30:设备，40:冷链)
	private String isCanOut;    //是否可以出库 1:可以出库  0:不可以出库
	private String noIsCanOut;  // 不选择是否可出库

	private Date pushDate; //由CRM推送来的时间
	private String idNumber;    //身份证号码

	public BizOrder() {
		super();
	}

	public BizOrder(String id) {
		super(id);
	}

	@Length(min = 0, max = 64, message = "团队ID长度必须介于 0 和 64 之间")
	public String getTeamId() {
		return teamId;
	}

	public void setTeamId(String teamId) {
		this.teamId = teamId;
	}

	@Length(min = 0, max = 64, message = "团队名称长度必须介于 0 和 64 之间")
	public String getTeamName() {
		return teamName;
	}

	public void setTeamName(String teamName) {
		this.teamName = teamName;
	}

	@Length(min = 0, max = 64, message = "销售ID长度必须介于 0 和 64 之间")
	public String getSellerId() {
		return sellerId;
	}

	public void setSellerId(String sellerId) {
		this.sellerId = sellerId;
	}

	@Length(min = 0, max = 64, message = "销售名称长度必须介于 0 和 64 之间")
	public String getSellerName() {
		return sellerName;
	}

	public void setSellerName(String sellerName) {
		this.sellerName = sellerName;
	}

	@Length(min = 0, max = 64, message = "专属客服ID长度必须介于 0 和 64 之间")
	public String getCustomerServiceRep() {
		return customerServiceRep;
	}

	public void setCustomerServiceRep(String customerServiceRep) {
		this.customerServiceRep = customerServiceRep;
	}

	@Length(min = 0, max = 64, message = "专属客服名称长度必须介于 0 和 64 之间")
	public String getCustomerServiceName() {
		return customerServiceName;
	}

	public void setCustomerServiceName(String customerServiceName) {
		this.customerServiceName = customerServiceName;
	}

	@Length(min = 0, max = 64, message = "客户ID长度必须介于 0 和 64 之间")
	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	@Length(min = 0, max = 64, message = "客户名称长度必须介于 0 和 64 之间")
	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	@Length(min = 0, max = 64, message = "所属项目ID长度必须介于 0 和 64 之间")
	public String getProjectId() {
		return projectId;
	}

	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}

	@Length(min = 0, max = 64, message = "系统出库长度必须介于 0 和 64 之间")
	public String getSystemOutLibrary() {
		return systemOutLibrary;
	}

	public void setSystemOutLibrary(String systemOutLibrary) {
		this.systemOutLibrary = systemOutLibrary;
	}

	@Length(min = 0, max = 64, message = "收货人姓名长度必须介于 0 和 64 之间")
	public String getConsigneeName() {
		return consigneeName;
	}

	public void setConsigneeName(String consigneeName) {
		this.consigneeName = consigneeName;
	}

	@Length(min = 0, max = 64, message = "打款人姓名长度必须介于 0 和 64 之间")
	public String getMoneyMakerName() {
		return moneyMakerName;
	}

	public void setMoneyMakerName(String moneyMakerName) {
		this.moneyMakerName = moneyMakerName;
	}

	@Length(min = 0, max = 64, message = "电话长度必须介于 0 和 64 之间")
	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	@Length(min = 0, max = 500, message = "地址长度必须介于 0 和 500 之间")
	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	@Length(min = 0, max = 64, message = "发货时备注长度必须介于 0 和 64 之间")
	public String getCustomerRemarks() {
		return customerRemarks;
	}

	public void setCustomerRemarks(String customerRemarks) {
		this.customerRemarks = customerRemarks;
	}

	@Length(min = 0, max = 64, message = "订货编号长度必须介于 0 和 64 之间")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	public Date getOrderTime() {
		return orderTime;
	}

	public void setOrderTime(Date orderTime) {
		this.orderTime = orderTime;
	}

	@Length(min = 0, max = 64, message = "折扣长度必须介于 0 和 64 之间")
	public String getDiscount() {
		return discount;
	}

	public void setDiscount(String discount) {
		this.discount = discount;
	}

	@Length(min = 0, max = 64, message = "订货金额长度必须介于 0 和 64 之间")
	public String getOrderAmount() {
		return orderAmount;
	}

	public void setOrderAmount(String orderAmount) {
		this.orderAmount = orderAmount;
	}

	@Length(min = 0, max = 64, message = "优惠券金额长度必须介于 0 和 64 之间")
	public String getCouponAmount() {
		return couponAmount;
	}

	public void setCouponAmount(String couponAmount) {
		this.couponAmount = couponAmount;
	}

	@Length(min = 0, max = 64, message = "应收金额长度必须介于 0 和 64 之间")
	public String getAmountReceivable() {
		return amountReceivable;
	}

	public void setAmountReceivable(String amountReceivable) {
		this.amountReceivable = amountReceivable;
	}

	@Length(min = 0, max = 64, message = "实收金额长度必须介于 0 和 64 之间")
	public String getAmountCollected() {
		return amountCollected;
	}

	public void setAmountCollected(String amountCollected) {
		this.amountCollected = amountCollected;
	}

	@Length(min = 0, max = 64, message = "货物ID长度必须介于 0 和 64 之间")
	public String getMaterielId() {
		return materielId;
	}

	public void setMaterielId(String materielId) {
		this.materielId = materielId;
	}

	@Length(min = 0, max = 9, message = "数量长度必须介于 0 和 9 之间")
	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	@Length(min = 0, max = 64, message = "结算方式长度必须介于 0 和 64 之间")
	public String getSettlementMethod() {
		return settlementMethod;
	}

	public void setSettlementMethod(String settlementMethod) {
		this.settlementMethod = settlementMethod;
	}

	@Length(min = 0, max = 64, message = "产品名称长度必须介于 0 和 64 之间")
	public String getMaterielName() {
		return materielName;
	}

	public void setMaterielName(String materielName) {
		this.materielName = materielName;
	}

	@Length(min = 0, max = 64, message = "产品编码长度必须介于 0 和 64 之间")
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getUnitPrice() {
		return unitPrice;
	}

	public void setUnitPrice(String unitPrice) {
		this.unitPrice = unitPrice;
	}

	@Length(min = 0, max = 64, message = "规格长度必须介于 0 和 64 之间")
	public String getSpecifications() {
		return specifications;
	}

	public void setSpecifications(String specifications) {
		this.specifications = specifications;
	}

	@Length(min = 0, max = 64, message = "订单类型(首签、续签)长度必须介于 0 和 64 之间")
	public String getOrderType() {
		return orderType;
	}

	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}

	@Length(min = 0, max = 64, message = "有无冷链长度必须介于 0 和 64 之间")
	public String getHaveColdChain() {
		return haveColdChain;
	}

	public void setHaveColdChain(String haveColdChain) {
		this.haveColdChain = haveColdChain;
	}

	@Length(min = 0, max = 64, message = "配货人ID长度必须介于 0 和 64 之间")
	public String getAllocationId() {
		return allocationId;
	}

	public void setAllocationId(String allocationId) {
		this.allocationId = allocationId;
	}

	@Length(min = 0, max = 64, message = "配货人name长度必须介于 0 和 64 之间")
	public String getAllocationName() {
		return allocationName;
	}

	public void setAllocationName(String allocationName) {
		this.allocationName = allocationName;
	}

	public String getProfit() {
		return profit;
	}

	public void setProfit(String profit) {
		this.profit = profit;
	}

	@Length(min = 0, max = 64, message = "审批实例长度必须介于 0 和 64 之间")
	public String getProcInsId() {
		return procInsId;
	}

	public void setProcInsId(String procInsId) {
		this.procInsId = procInsId;
	}

	@Length(min = 0, max = 32, message = "任务状态长度必须介于 0 和 32 之间")
	public String getTaskState() {
		return taskState;
	}

	public void setTaskState(String taskState) {
		this.taskState = taskState;
	}

	public List<BizOrderdetail> getBizOrderDetailList() {
		return bizOrderDetailList;
	}

	public void setBizOrderDetailList(List<BizOrderdetail> bizOrderDetailList) {
		this.bizOrderDetailList = bizOrderDetailList;
	}

	public String getTmUrl() {
		return tmUrl;
	}

	public void setTmUrl(String tmUrl) {
		this.tmUrl = tmUrl;
	}

	public String getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}

	public String getDeliverType() {
		return deliverType;
	}

	public void setDeliverType(String deliverType) {
		this.deliverType = deliverType;
	}

	public String getCreateByName() {
		return createByName;
	}

	public void setCreateByName(String createByName) {
		this.createByName = createByName;
	}

	public String[] getOrderIds() {
		return orderIds;
	}

	public void setOrderIds(String[] orderIds) {
		this.orderIds = orderIds;
	}

	public String getOrderApprovalStatus() {
		return orderApprovalStatus;
	}

	public void setOrderApprovalStatus(String orderApprovalStatus) {
		this.orderApprovalStatus = orderApprovalStatus;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getOrderGenre() {
		return orderGenre;
	}

	public void setOrderGenre(String orderGenre) {
		this.orderGenre = orderGenre;
	}

	public String getIsCanOut() {
		return isCanOut;
	}

	public void setIsCanOut(String isCanOut) {
		this.isCanOut = isCanOut;
	}

	public String getAuditorName() {
		return auditorName;
	}

	public void setAuditorName(String auditorName) {
		this.auditorName = auditorName;
	}

	public Date getAuditDate() {
		return auditDate;
	}

	public void setAuditDate(Date auditDate) {
		this.auditDate = auditDate;
	}

	public String getNoIsCanOut() {
		return noIsCanOut;
	}

	public void setNoIsCanOut(String noIsCanOut) {
		this.noIsCanOut = noIsCanOut;
	}

	public Date getPushDate() {
		return pushDate;
	}

	public void setPushDate(Date pushDate) {
		this.pushDate = pushDate;
	}

	public String getIdNumber() {
		return idNumber;
	}

	public void setIdNumber(String idNumber) {
		this.idNumber = idNumber;
	}
}

