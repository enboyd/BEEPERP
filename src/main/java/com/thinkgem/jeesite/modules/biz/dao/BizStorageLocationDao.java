/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.biz.dao;

import com.thinkgem.jeesite.common.persistence.TreeDao;
import com.thinkgem.jeesite.common.persistence.annotation.MyBatisDao;
import com.thinkgem.jeesite.modules.biz.entity.BizStorageLocation;
import com.thinkgem.jeesite.modules.biz.vo.BizStorageLocationVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 库位管理DAO接口
 * @author xuchen
 * @version 2018-07-17
 */
@MyBatisDao
public interface BizStorageLocationDao extends TreeDao<BizStorageLocation> {

    void deleteById(BizStorageLocation bizStorageLocation);

    List<BizStorageLocationVo> findListApi(BizStorageLocation bizStorageLocation);
}