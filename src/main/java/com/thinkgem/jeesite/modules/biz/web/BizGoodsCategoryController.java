/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.biz.web;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.thinkgem.jeesite.common.config.Global;
import com.thinkgem.jeesite.common.web.BaseController;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.modules.biz.entity.BizGoodsCategory;
import com.thinkgem.jeesite.modules.biz.service.BizGoodsCategoryService;

/**
 * 项目Controller
 * @author xuchen
 * @version 2018-08-28
 */
@Controller
@RequestMapping(value = "${adminPath}/biz/bizGoodsCategory")
public class BizGoodsCategoryController extends BaseController {

	@Autowired
	private BizGoodsCategoryService bizGoodsCategoryService;
	
	@ModelAttribute
	public BizGoodsCategory get(@RequestParam(required=false) String id) {
		BizGoodsCategory entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = bizGoodsCategoryService.get(id);
		}
		if (entity == null){
			entity = new BizGoodsCategory();
		}
		return entity;
	}
	
	@RequiresPermissions("biz:bizGoodsCategory:view")
	@RequestMapping(value = {"list", ""})
	public String list(BizGoodsCategory bizGoodsCategory, HttpServletRequest request, HttpServletResponse response, Model model) {
		List<BizGoodsCategory> list = bizGoodsCategoryService.findList(bizGoodsCategory); 
		model.addAttribute("list", list);
		return "modules/biz/bizGoodsCategoryList";
	}

	@RequiresPermissions("biz:bizGoodsCategory:view")
	@RequestMapping(value = "form")
	public String form(BizGoodsCategory bizGoodsCategory, Model model) {
		if (bizGoodsCategory.getParent()!=null && StringUtils.isNotBlank(bizGoodsCategory.getParent().getId())){
			bizGoodsCategory.setParent(bizGoodsCategoryService.get(bizGoodsCategory.getParent().getId()));
			// 获取排序号，最末节点排序号+30
			if (StringUtils.isBlank(bizGoodsCategory.getId())){
				BizGoodsCategory bizGoodsCategoryChild = new BizGoodsCategory();
				bizGoodsCategoryChild.setParent(new BizGoodsCategory(bizGoodsCategory.getParent().getId()));
				List<BizGoodsCategory> list = bizGoodsCategoryService.findList(bizGoodsCategory); 
				if (list.size() > 0){
					bizGoodsCategory.setSort(list.get(list.size()-1).getSort());
					if (bizGoodsCategory.getSort() != null){
						bizGoodsCategory.setSort(bizGoodsCategory.getSort() + 30);
					}
				}
			}
		}
		if (bizGoodsCategory.getSort() == null){
			bizGoodsCategory.setSort(30);
		}
		model.addAttribute("bizGoodsCategory", bizGoodsCategory);
		return "modules/biz/bizGoodsCategoryForm";
	}

	@RequiresPermissions("biz:bizGoodsCategory:edit")
	@RequestMapping(value = "save")
	public String save(BizGoodsCategory bizGoodsCategory, Model model, RedirectAttributes redirectAttributes) {
		if (!beanValidator(model, bizGoodsCategory)){
			return form(bizGoodsCategory, model);
		}
		bizGoodsCategoryService.save(bizGoodsCategory);
		addMessage(redirectAttributes, "保存项目成功");
		return "redirect:"+Global.getAdminPath()+"/biz/bizGoodsCategory/?repage";
	}
	
	@RequiresPermissions("biz:bizGoodsCategory:edit")
	@RequestMapping(value = "delete")
	public String delete(BizGoodsCategory bizGoodsCategory, RedirectAttributes redirectAttributes) {
		bizGoodsCategoryService.delete(bizGoodsCategory);
		addMessage(redirectAttributes, "删除项目成功");
		return "redirect:"+Global.getAdminPath()+"/biz/bizGoodsCategory/?repage";
	}

	@RequiresPermissions("user")
	@ResponseBody
	@RequestMapping(value = "treeData")
	public List<Map<String, Object>> treeData(@RequestParam(required=false) String extId, HttpServletResponse response, String isEnabled) {
		List<Map<String, Object>> mapList = Lists.newArrayList();
		BizGoodsCategory bizGoodsCategory = new BizGoodsCategory();
		if(StringUtils.isNotEmpty(isEnabled) && "true".equals(isEnabled)){
			bizGoodsCategory.setIsenable("1");
		}
 		List<BizGoodsCategory> list = bizGoodsCategoryService.findList(bizGoodsCategory);
		for (int i=0; i<list.size(); i++){
			BizGoodsCategory e = list.get(i);
			if (StringUtils.isBlank(extId) || (extId!=null && !extId.equals(e.getId()) && e.getParentIds().indexOf(","+extId+",")==-1)){
				Map<String, Object> map = Maps.newHashMap();
				map.put("id", e.getId());
				map.put("pId", e.getParentId());
				map.put("name", e.getName());
				mapList.add(map);
			}
		}
		return mapList;
	}

	
}