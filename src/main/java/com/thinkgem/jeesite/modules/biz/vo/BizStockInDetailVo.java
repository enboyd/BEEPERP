/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.biz.vo;

import lombok.Data;

import java.util.Date;

/**
 * 入库单明细的ControllerEntity
 * @author tiancheng
 * @version 2018-07-16
 */
@Data
public class BizStockInDetailVo{
	
	private String stockInDetailId;		// 入库明细ID
	private String stockInId;		// 所属入库单
	private String goodsId;		// 商品ID
	private String actualAmount;		// 实际入库数量
	private String goodsName;//商品名称
	private String manufactureDate;  //生产日期
	private Integer expirationDate;  //保质期的天数
	private String storageLocationName;

}