package com.thinkgem.jeesite.modules.biz.form;

import lombok.Data;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * Description:
 * Author: LTY
 * Create Time: 2018-08-09 17:09
 */
@Data
public class BizGoodsForm {
    /**
     * 表单中会传入后端什么字段，就写什么字段，一个都不要多写
     */
    private String goodsId;		// 商品ID
    private String goodsCode;		// 商品编号

    private String goodsName;		// 商品名称
    private Integer goodsCategoryId;		// 商品类型
    private Integer goodsBrandId;		// 商品品牌

    private String goodsBrandName;//商品品牌名称
    private String goodsCategoryName;//商品类型名称

    private String supplierName;//供应商的name

    private String storageName; //仓库id

}
