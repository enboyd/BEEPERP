/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.biz.dao;

import com.thinkgem.jeesite.common.persistence.TreeDao;
import com.thinkgem.jeesite.common.persistence.annotation.MyBatisDao;
import com.thinkgem.jeesite.modules.biz.entity.BizGoodsCategory;

import java.util.List;

/**
 * 产品类型DAO接口
 * @author xuchen
 * @version 2018-08-28
 */
@MyBatisDao
public interface BizGoodsCategoryDao extends TreeDao<BizGoodsCategory> {

    BizGoodsCategory getByName(String categoryName);

}