/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.biz.service;

import java.util.Date;
import java.util.List;

import com.thinkgem.jeesite.modules.sys.utils.UserUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.service.CrudService;
import com.thinkgem.jeesite.modules.biz.entity.BizDiscardedGoods;
import com.thinkgem.jeesite.modules.biz.dao.BizDiscardedGoodsDao;

/**
 * 废弃商品Service
 * @author xuchen
 * @version 2018-11-02
 */
@Service
@Transactional(readOnly = true)
public class BizDiscardedGoodsService extends CrudService<BizDiscardedGoodsDao, BizDiscardedGoods> {

	@Autowired
	private BizDiscardedGoodsDao bizDiscardedGoodsDao;

	public BizDiscardedGoods get(String id) {
		return super.get(id);
	}
	
	public List<BizDiscardedGoods> findList(BizDiscardedGoods bizDiscardedGoods) {
		return super.findList(bizDiscardedGoods);
	}
	
	public Page<BizDiscardedGoods> findPage(Page<BizDiscardedGoods> page, BizDiscardedGoods bizDiscardedGoods) {
		return super.findPage(page, bizDiscardedGoods);
	}
	
	@Transactional(readOnly = false)
	public void save(BizDiscardedGoods bizDiscardedGoods) {
		super.save(bizDiscardedGoods);
	}

	@Transactional(readOnly = false)
	public void insert(BizDiscardedGoods bizDiscardedGoods) {
		bizDiscardedGoodsDao.insert(bizDiscardedGoods);
	}

	@Transactional(readOnly = false)
	public void update(BizDiscardedGoods bizDiscardedGoods) {
		bizDiscardedGoods.setUpdateDate(new Date());
		bizDiscardedGoods.setUpdateBy(UserUtils.getUser());
		bizDiscardedGoods.setUpdateName(UserUtils.getUser().getName());
		bizDiscardedGoodsDao.update(bizDiscardedGoods);
	}
	
	@Transactional(readOnly = false)
	public void delete(BizDiscardedGoods bizDiscardedGoods) {
		super.delete(bizDiscardedGoods);
	}
	
}