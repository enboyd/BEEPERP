/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.biz.dao;

import com.thinkgem.jeesite.common.persistence.CrudDao;
import com.thinkgem.jeesite.common.persistence.annotation.MyBatisDao;
import com.thinkgem.jeesite.modules.biz.entity.BizOrder;
import com.thinkgem.jeesite.modules.biz.vo.BizOrderVo;

import java.util.List;

/**
 * 订单DAO接口
 * @author tiancheng
 * @version 2018-09-17
 */
@MyBatisDao
public interface BizOrderDao extends CrudDao<BizOrder> {

    BizOrderVo getApi(String name);

    Integer updateStatus(BizOrder bizOrder);

    void updateOrderApprovalAdopt(String id);

    void realDelete(BizOrder bizOrderF);

    void updateById(BizOrder bizOrder);

    List<BizOrder> findReceiveList(BizOrder bizOrder);

    Integer selectCountByName(BizOrder bizOrder);

    List<BizOrder> selectByName(BizOrder bizOrder);

    void realDeleteByName(String name);
}