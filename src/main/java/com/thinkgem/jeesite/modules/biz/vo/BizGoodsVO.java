package com.thinkgem.jeesite.modules.biz.vo;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

/**
 * Description:
 * Author: LTY
 * Create Time: 2018-08-09 19:46
 */
@Data
public class BizGoodsVO implements Serializable {

    private static final long serialVersionUID = -7544967596754825073L;

    private String id;
    private String delFlag;

    private String goodsId;		// 商品ID
    private String goodsCode;		// 商品编号
    private String goodsName;		// 商品名称
    private String goodsCategoryId;		// 商品类型
    private String specification;		// 规格
    private String size;		// 尺寸
    private String weight;		// 重量
    private Double unitPrice;		// 进货价单
    private Double sellingPrice;		// 参考售价
    private Integer stockAmount;		// 库存数量
    private String purchasingAmount;		// 采购中数量
    private Integer warningAmount;		// 预警数量(设置为0则不预警)

    private String goodsCategoryName;//商品类型名称

    private String supplierId; //供应商的id
    private String supplierName;//供应商的name

    private Integer storageId; //仓库id
    private String storageName; //仓库id

    private String unit;	//单位
}
