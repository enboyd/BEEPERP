/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.biz.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.thinkgem.jeesite.common.config.Global;
import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.web.BaseController;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.modules.biz.entity.SmReturnBarterDetail;
import com.thinkgem.jeesite.modules.biz.service.SmReturnBarterDetailService;

/**
 * 退换货明细Controller
 * @author xuchen
 * @version 2018-09-11
 */
@Controller
@RequestMapping(value = "${adminPath}/biz/smReturnBarterDetail")
public class SmReturnBarterDetailController extends BaseController {

	@Autowired
	private SmReturnBarterDetailService smReturnBarterDetailService;
	
	@ModelAttribute
	public SmReturnBarterDetail get(@RequestParam(required=false) String id) {
		SmReturnBarterDetail entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = smReturnBarterDetailService.get(id);
		}
		if (entity == null){
			entity = new SmReturnBarterDetail();
		}
		return entity;
	}
	
	@RequiresPermissions("sm:smReturnBarterDetail:view")
	@RequestMapping(value = {"list", ""})
	public String list(SmReturnBarterDetail smReturnBarterDetail, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<SmReturnBarterDetail> page = smReturnBarterDetailService.findPage(new Page<SmReturnBarterDetail>(request, response), smReturnBarterDetail); 
		model.addAttribute("page", page);
		return "modules/biz/smReturnBarterDetailList";
	}

	@RequiresPermissions("sm:smReturnBarterDetail:view")
	@RequestMapping(value = "form")
	public String form(SmReturnBarterDetail smReturnBarterDetail, Model model) {
		model.addAttribute("smReturnBarterDetail", smReturnBarterDetail);
		return "modules/biz/smReturnBarterDetailForm";
	}

	@RequiresPermissions("sm:smReturnBarterDetail:edit")
	@RequestMapping(value = "save")
	public String save(SmReturnBarterDetail smReturnBarterDetail, Model model, RedirectAttributes redirectAttributes) {
		if (!beanValidator(model, smReturnBarterDetail)){
			return form(smReturnBarterDetail, model);
		}
		smReturnBarterDetailService.save(smReturnBarterDetail);
		addMessage(redirectAttributes, "保存退换货明细成功");
		return "redirect:"+Global.getAdminPath()+"/biz/smReturnBarterDetail/?repage";
	}
	
	@RequiresPermissions("sm:smReturnBarterDetail:edit")
	@RequestMapping(value = "delete")
	public String delete(SmReturnBarterDetail smReturnBarterDetail, RedirectAttributes redirectAttributes) {
		smReturnBarterDetailService.delete(smReturnBarterDetail);
		addMessage(redirectAttributes, "删除退换货明细成功");
		return "redirect:"+Global.getAdminPath()+"/biz/smReturnBarterDetail/?repage";
	}

}