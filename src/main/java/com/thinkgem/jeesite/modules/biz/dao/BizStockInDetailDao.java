/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.biz.dao;

import com.thinkgem.jeesite.common.persistence.CrudDao;
import com.thinkgem.jeesite.common.persistence.annotation.MyBatisDao;
import com.thinkgem.jeesite.modules.biz.entity.BizStockIn;
import com.thinkgem.jeesite.modules.biz.entity.BizStockInDetail;
import com.thinkgem.jeesite.modules.biz.vo.BizStockInDetailVo;

import java.util.List;

/**
 * 入库单明细的ControllerDAO接口
 * @author tiancheng
 * @version 2018-07-16
 */
@MyBatisDao
public interface BizStockInDetailDao extends CrudDao<BizStockInDetail> {

    List<BizStockInDetailVo> findListApi(BizStockInDetail bizStockInDetail);


    List<BizStockInDetail> findListNo(BizStockInDetail bizStockInDetail);
    /**
     * pc端修改入库数量
     * @param bizStockInDetail
     */
    void updateActualAmount(BizStockInDetail bizStockInDetail);

    Integer selectCountByStockInId(String stockInId);

    Integer selectCountStorageLocationId(String storageLocationId);

    List<BizStockInDetail> findListByParent(BizStockIn bizStockIn);
}