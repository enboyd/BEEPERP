/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.biz.entity;

import com.thinkgem.jeesite.common.persistence.ActEntity;
import com.thinkgem.jeesite.common.utils.excel.annotation.ExcelField;


/**
 * 商品的ControllerEntity
 * @author tiancheng
 * @version 2018-07-12
 */
public class BizGoods extends ActEntity<BizGoods> {
	
	private static final long serialVersionUID = 1L;
	private String table = "biz_goods";// 表名
	private String goodsId;		// 商品ID
	private String goodsCode;		// 商品编号
	private String goodsName;		// 商品名称
	private String goodsCategoryId;		// 商品类型Id
	private String goodsCategoryName;	//商品类型名称
	private String specification;		// 规格
	private Integer stockAmount;		// 库存数量
	private String purchasingAmount;		// 采购中数量
	private Integer warningAmount;		// 预警数量(设置为0则不预警)
	private String supplierId; //供应商的id
	private String supplierName;//供应商的name
	private Integer storageId; //仓库id
	private String storageName; //仓库id
	private String unit;	//单位
	private String dayNum;  //当日商品的入库条码个数（生成条码编号时使用）
	private String classifyId;	//商品分类ID
	private String classifyName;	//商品分类Name
	private String expirationDate;	//保质期/月
	private boolean isCanSave;//是否可以导入到数据库
	private String errorMsg;//不可导入到数据库的错误提示
	private Integer sureStockAmount; //可用库存
	private Integer waitStockAmount;  //等待发货的数量
	private Integer minPurchaseQuantity;	//最小起购数量
	private String minPurchaseQuantityStr;	//最小起购数量(导入使用)

	public BizGoods() {
		super();
	}

	public BizGoods(String id){
		super(id);
	}

	public String getTable() {
		return table;
	}

	public void setTable(String table) {
		this.table = table;
	}

	public String getGoodsId() {
		return goodsId;
	}

	public void setGoodsId(String goodsId) {
		this.goodsId = goodsId;
	}

	@ExcelField(title="商品编号", align=1, sort=10)
	public String getGoodsCode() {
		return goodsCode;
	}

	public void setGoodsCode(String goodsCode) {
		this.goodsCode = goodsCode;
	}

	@ExcelField(title="商品名称", align=1, sort=15)
	public String getGoodsName() {
		return goodsName;
	}

	public void setGoodsName(String goodsName) {
		this.goodsName = goodsName;
	}

	@ExcelField(title="规格", align=1, sort=19)
	public String getSpecification() {
		return specification;
	}

	public void setSpecification(String specification) {
		this.specification = specification;
	}

	@ExcelField(title="实际库存", align=1, sort=50)
	public Integer getStockAmount() {
		return stockAmount;
	}


	public void setStockAmount(Integer stockAmount) {
		this.stockAmount = stockAmount;
	}

	public String getPurchasingAmount() {
		return purchasingAmount;
	}

	public void setPurchasingAmount(String purchasingAmount) {
		this.purchasingAmount = purchasingAmount;
	}
	
	public Integer getWarningAmount() {
		return warningAmount;
	}

	public void setWarningAmount(Integer warningAmount) {
		this.warningAmount = warningAmount;
	}

	public String getSupplierId() {
		return supplierId;
	}

	public void setSupplierId(String supplierId) {
		this.supplierId = supplierId;
	}

	@ExcelField(title="供应商名字", align=1, sort=40)
	public String getSupplierName() {
		return supplierName;
	}

	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}

	public Integer getStorageId() {
		return storageId;
	}

	public void setStorageId(Integer storageId) {
		this.storageId = storageId;
	}

	@ExcelField(title="仓库", align=1, sort=25)
	public String getStorageName() {
		return storageName;
	}

	public void setStorageName(String storageName) {
		this.storageName = storageName;
	}

	@ExcelField(title="单位", align=1, sort=18)
	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public String getGoodsCategoryId() {
		return goodsCategoryId;
	}

	public void setGoodsCategoryId(String goodsCategoryId) {
		this.goodsCategoryId = goodsCategoryId;
	}

	@ExcelField(title="项目", align=1, sort=20)
	public String getGoodsCategoryName() {
		return goodsCategoryName;
	}

	public void setGoodsCategoryName(String goodsCategoryName) {
		this.goodsCategoryName = goodsCategoryName;
	}

	public String getDayNum() {
		return dayNum;
	}

	public void setDayNum(String dayNum) {
		this.dayNum = dayNum;
	}

	@ExcelField(title="保质期", align=1, sort=30)
	public String getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(String expirationDate) {
		this.expirationDate = expirationDate;
	}

	public boolean getIsCanSave() {
		return isCanSave;
	}

	public void setIsCanSave(boolean isCanSave) {
		this.isCanSave = isCanSave;
	}

	public String getErrorMsg() {
		return errorMsg;
	}

	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}

	public Integer getSureStockAmount() {
		return sureStockAmount;
	}

	public void setSureStockAmount(Integer sureStockAmount) {
		this.sureStockAmount = sureStockAmount;
	}

	public Integer getWaitStockAmount() {
		return waitStockAmount;
	}

	public void setWaitStockAmount(Integer waitStockAmount) {
		this.waitStockAmount = waitStockAmount;
	}

	public String getClassifyId() {
		return classifyId;
	}

	public void setClassifyId(String classifyId) {
		this.classifyId = classifyId;
	}

	@ExcelField(title="商品分类", align=1, sort=35)
	public String getClassifyName() {
		return classifyName;
	}

	public void setClassifyName(String classifyName) {
		this.classifyName = classifyName;
	}

	public Integer getMinPurchaseQuantity() {
		return minPurchaseQuantity;
	}

	public void setMinPurchaseQuantity(Integer minPurchaseQuantity) {
		this.minPurchaseQuantity = minPurchaseQuantity;
	}

	@ExcelField(title="最小起购数量", align=1, sort=40)
	public String getMinPurchaseQuantityStr() {
		return minPurchaseQuantityStr;
	}

	public void setMinPurchaseQuantityStr(String minPurchaseQuantityStr) {
		this.minPurchaseQuantityStr = minPurchaseQuantityStr;
	}
}