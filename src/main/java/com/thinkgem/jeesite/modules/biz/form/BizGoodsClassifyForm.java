package com.thinkgem.jeesite.modules.biz.form;

import lombok.Data;

/**
 * Description:
 * Author: xuchen
 * Create Time: 2018-10-07
 */
@Data
public class BizGoodsClassifyForm {
    private String type;
}
