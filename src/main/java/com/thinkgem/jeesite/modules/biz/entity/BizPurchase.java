/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.biz.entity;

import org.hibernate.validator.constraints.Length;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;

import com.thinkgem.jeesite.common.persistence.DataEntity;

/**
 * 采购单的controllerEntity
 * @author tiancheng
 * @version 2018-07-13
 */
public class BizPurchase extends DataEntity<BizPurchase> {
	
	private static final long serialVersionUID = 1L;
	private String purchaseId;		// 采购单ID
	private String purchaseCode;		// 采购单编号
	private Date purchaseDate;		// 采购日期
	private Double totalPrice;		// 采购总额
	private String status;		// 采购状态
	private Date beginPurchaseDate;		// 开始 采购日期
	private Date endPurchaseDate;		// 结束 采购日期
	private String type; // 用于判断是哪个页面
	private String purchaseType;	//采购单类型
	private String buyingId;		// 请购单ID
	private String buyingCode;  // 请购单编号
	private int pageSize;		//条数
	private int pageNo;			//页码
	private String  financialStatus;	//财务状态(待结算\部分结算\已结算)
	private Double settledAmount;	//已结算金额
	private Double outstandingAmount;	//待结算金额
	private String warehouse; //仓库
	public List<BizPurchaseDetail> purchaseDetailList;//明细
	private String auditStatus;	//采购单审核状态

	public String getFinancialStatus() {
		return financialStatus;
	}

	public void setFinancialStatus(String financialStatus) {
		this.financialStatus = financialStatus;
	}

	public String getPurchaseType() {
		return purchaseType;
	}

	public void setPurchaseType(String purchaseType) {
		this.purchaseType = purchaseType;
	}

	public String getBuyingId() {
		return buyingId;
	}

	public void setBuyingId(String buyingId) {
		this.buyingId = buyingId;
	}

	public BizPurchase() {
		super();
	}

	public BizPurchase(String id){
		super(id);
	}

	public String getPurchaseId() {
		return purchaseId;
	}

	public void setPurchaseId(String purchaseId) {
		this.purchaseId = purchaseId;
	}
	
	@Length(min=1, max=24, message="采购单编号长度必须介于 1 和 24 之间")
	public String getPurchaseCode() {
		return purchaseCode;
	}

	public void setPurchaseCode(String purchaseCode) {
		this.purchaseCode = purchaseCode;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	public Date getPurchaseDate() {
		return purchaseDate;
	}

	public void setPurchaseDate(Date purchaseDate) {
		this.purchaseDate = purchaseDate;
	}
	
	public Double getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(Double totalPrice) {
		this.totalPrice = totalPrice;
	}
	
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getBeginPurchaseDate() {
		return beginPurchaseDate;
	}

	public void setBeginPurchaseDate(Date beginPurchaseDate) {
		this.beginPurchaseDate = beginPurchaseDate;
	}
	
	public Date getEndPurchaseDate() {
		return endPurchaseDate;
	}

	public void setEndPurchaseDate(Date endPurchaseDate) {
		this.endPurchaseDate = endPurchaseDate;
	}

	public List<BizPurchaseDetail> getPurchaseDetailList() {
		return purchaseDetailList;
	}

	public void setPurchaseDetailList(List<BizPurchaseDetail> purchaseDetailList) {
		this.purchaseDetailList = purchaseDetailList;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public int getPageNo() {
		return pageNo;
	}

	public void setPageNo(int pageNo) {
		this.pageNo = pageNo;
	}

	public Double getSettledAmount() {
		return settledAmount;
	}

	public void setSettledAmount(Double settledAmount) {
		this.settledAmount = settledAmount;
	}

	public Double getOutstandingAmount() {
		return outstandingAmount;
	}

	public void setOutstandingAmount(Double outstandingAmount) {
		this.outstandingAmount = outstandingAmount;
	}

	public String getWarehouse() {
		return warehouse;
	}

	public void setWarehouse(String warehouse) {
		this.warehouse = warehouse;
	}

	public String getBuyingCode() {
		return buyingCode;
	}

	public void setBuyingCode(String buyingCode) {
		this.buyingCode = buyingCode;
	}

	public String getAuditStatus() {
		return auditStatus;
	}

	public void setAuditStatus(String auditStatus) {
		this.auditStatus = auditStatus;
	}

}