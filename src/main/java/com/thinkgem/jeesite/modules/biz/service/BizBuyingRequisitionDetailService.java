/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.biz.service;

import java.util.List;

import com.thinkgem.jeesite.modules.biz.entity.BizBuyingRequisition;
import com.thinkgem.jeesite.modules.biz.entity.BizGoods;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.service.CrudService;
import com.thinkgem.jeesite.modules.biz.entity.BizBuyingRequisitionDetail;
import com.thinkgem.jeesite.modules.biz.dao.BizBuyingRequisitionDetailDao;

/**
 * 请购单明细的controllerService
 * @author xuchen
 * @version 2018-07-28
 */
@Service
@Transactional(readOnly = true)
public class BizBuyingRequisitionDetailService extends CrudService<BizBuyingRequisitionDetailDao, BizBuyingRequisitionDetail> {

	@Autowired
	private BizBuyingRequisitionDetailDao buyingRequisitionDetailDao;

	public BizBuyingRequisitionDetail get(String id) {
		return super.get(id);
	}
	
	public List<BizBuyingRequisitionDetail> findList(BizBuyingRequisitionDetail bizBuyingRequisitionDetail) {
		return super.findList(bizBuyingRequisitionDetail);
	}
	
	public Page<BizBuyingRequisitionDetail> findPage(Page<BizBuyingRequisitionDetail> page, BizBuyingRequisitionDetail bizBuyingRequisitionDetail) {
		return super.findPage(page, bizBuyingRequisitionDetail);
	}
	
	@Transactional(readOnly = false)
	public void save(BizBuyingRequisitionDetail bizBuyingRequisitionDetail) {
		super.save(bizBuyingRequisitionDetail);
	}
	
	@Transactional(readOnly = false)
	public void delete(BizBuyingRequisitionDetail bizBuyingRequisitionDetail) {
		super.delete(bizBuyingRequisitionDetail);
	}

	/**
	 * 根据采购单物理删除
	 * @author xuchen
	 * @date 2018/7/29 10:42
	 * @param
	 * @return
	 */
	@Transactional(readOnly = false)
    public void deleteByBuyingId(String buyingId) {
		dao.deleteByBuyingId(buyingId);
    }

	/**
	 * 根据请购单查询请购单详情List
	 * xuchen
	 * @param bizBuyingRequisition
	 * @return
	 */
	public List<BizBuyingRequisitionDetail> findListByParent(BizBuyingRequisition bizBuyingRequisition) {
		return buyingRequisitionDetailDao.findListByParent(bizBuyingRequisition);
    }

	/**
	 * 查询请购单详情page
	 * @param bizBuyingRequisitionDetailPage
	 * @param buyingRequisition
	 * @return
	 */
	public Page<BizBuyingRequisitionDetail> findPageByParent(Page<BizBuyingRequisitionDetail> bizBuyingRequisitionDetailPage,
															 BizBuyingRequisition buyingRequisition) {
		BizBuyingRequisitionDetail buyingRequisitionDetail = new BizBuyingRequisitionDetail();
		buyingRequisitionDetail.setPage(bizBuyingRequisitionDetailPage);
		bizBuyingRequisitionDetailPage.setList(buyingRequisitionDetailDao.findListByParent(buyingRequisition));
		return bizBuyingRequisitionDetailPage;
	}

	@Transactional(readOnly = false)
    public void update(BizBuyingRequisitionDetail bizBuyingRequisitionDetail) {
		buyingRequisitionDetailDao.update(bizBuyingRequisitionDetail);
    }
}