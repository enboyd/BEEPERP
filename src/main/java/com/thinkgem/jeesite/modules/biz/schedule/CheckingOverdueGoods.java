package com.thinkgem.jeesite.modules.biz.schedule;

import com.thinkgem.jeesite.common.utils.DateUtils;
import com.thinkgem.jeesite.common.utils.IdGen;
import com.thinkgem.jeesite.modules.biz.entity.BizBarcode;
import com.thinkgem.jeesite.modules.biz.entity.BizDiscardedGoods;
import com.thinkgem.jeesite.modules.biz.entity.BizGoods;
import com.thinkgem.jeesite.modules.biz.service.BizBarcodeService;
import com.thinkgem.jeesite.modules.biz.service.BizDiscardedGoodsService;
import com.thinkgem.jeesite.modules.biz.service.BizGoodsService;
import com.thinkgem.jeesite.modules.sys.entity.User;
import com.thinkgem.jeesite.modules.sys.utils.UserUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * 检查过期商品
 * @author : xuchen
 * @date : 2018/11/2 9:09
 */
@Service
@Lazy(false)
public class CheckingOverdueGoods {

    @Autowired
    private BizBarcodeService bizBarcodeService;
    @Autowired
    private BizGoodsService bizGoodsService;
    @Autowired
    private BizDiscardedGoodsService bizDiscardedGoodsService;

    @Scheduled(cron = "0 0 01 * * ?")//每天凌晨1点整
    public void job1() {
        //获取所有条码list
        List<BizBarcode> bizBarcodeList = bizBarcodeService.findAllList(new BizBarcode());
        if(bizBarcodeList != null && bizBarcodeList.size() > 0){

            //当前日期
            Date nowDate = new Date();

            for (BizBarcode bizBarcode : bizBarcodeList) {
                if(bizBarcode != null){
                    //生产日期数据库默认 xxxx年xx月xx日
                    String manufactureDate = bizBarcode.getManufactureDate();
                    if(StringUtils.isNotEmpty(manufactureDate)){
                        manufactureDate = manufactureDate.replace("年","/").replace("月","/").replace("日","");
                        //转换为Date类型
                        Date manufactureDateD = DateUtils.parseDate(manufactureDate);

                        //获取该条码上的商品的 保质期/月
                        Integer expirationNum = null;
                        String goodsId = bizBarcode.getGoodsId();
                        if(StringUtils.isNotEmpty(goodsId)){
                            BizGoods bizGoods = bizGoodsService.get(goodsId);
                            if(bizGoods != null){
                                String expirationDate = bizGoods.getExpirationDate();
                                try{
                                    expirationNum = Integer.valueOf(expirationDate);
                                }catch (Exception e){
                                    e.printStackTrace();
                                }
                                //计算当前日期和生产日期相差的天数
                                double distanceOfTwoDate = DateUtils.getDistanceOfTwoDate(manufactureDateD, nowDate);
                                if(expirationNum != null){
                                    //过期
                                    if (expirationNum*30.4 < distanceOfTwoDate){
                                        //将该条码上的商品库存数量减去当前条码上的数量,当前条码剩余数清空,创建废弃商品对象
                                        String surplusNumber = bizBarcode.getSurplusNumber();
                                        //条码上的可用库存
                                        Integer surplusNum = Integer.valueOf(surplusNumber);
                                        if(surplusNum != null && surplusNum > 0){
                                            bizBarcode.setSurplusNumber("0");

                                            //商品的可用库存
                                            Integer sureStockAmount = bizGoods.getSureStockAmount();
                                            bizGoods.setSureStockAmount(sureStockAmount-surplusNum);


                                            //创建废弃商品对象
                                            BizDiscardedGoods bizDiscardedGoods = new BizDiscardedGoods();
                                            bizDiscardedGoods.setBarcodeId(bizBarcode.getId());
                                            bizDiscardedGoods.setBarcodeName(bizBarcode.getName());
                                            bizDiscardedGoods.setBatch(bizBarcode.getName().substring(0,bizBarcode.getName().length() -6));
                                            bizDiscardedGoods.setGoodsId(bizGoods.getGoodsId());
                                            bizDiscardedGoods.setGoodsCode(bizGoods.getGoodsCode());
                                            bizDiscardedGoods.setGoodsName(bizGoods.getGoodsName());
                                            bizDiscardedGoods.setNumber(surplusNum);
                                            bizDiscardedGoods.setExpirationDate(expirationNum);
                                            bizDiscardedGoods.setManufactureDate(manufactureDateD);
                                            bizDiscardedGoods.setSpecification(bizGoods.getSpecification());
                                            bizDiscardedGoods.setStorageLocationId(bizBarcode.getStorageLocationId());
                                            bizDiscardedGoods.setStorageLocationName(bizBarcode.getStorageLocationName());
                                            bizDiscardedGoods.setType("10");
                                            User admin = UserUtils.get("1");
                                            bizDiscardedGoods.setCreateBy(admin);
                                            bizDiscardedGoods.setCreateName(admin.getName());
                                            bizDiscardedGoods.setUpdateBy(admin);
                                            bizDiscardedGoods.setUpdateName(admin.getName());
                                            bizDiscardedGoods.setCreateDate(new Date());
                                            bizDiscardedGoods.setUpdateDate(new Date());
                                            bizDiscardedGoods.setId(IdGen.uuid());
                                            bizDiscardedGoodsService.insert(bizDiscardedGoods);

                                            bizBarcodeService.save(bizBarcode);
                                            bizGoodsService.update(bizGoods);
                                            //通知内容拼接  TODO
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            //发通知 TODO
        }
    }
}
