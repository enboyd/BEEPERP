/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.biz.form;

import lombok.Data;

/**
 * 请购单的Entity
 * @author tiancheng
 * @version 2018-08-24
 */
@Data
public class BizPurchaseListFrom {
	
	private String status;		// 请购单状态  待提交、待审批、已驳回、已通过、完成
	private int pageSize;		//条数
	private int pageNo;			//页码
}