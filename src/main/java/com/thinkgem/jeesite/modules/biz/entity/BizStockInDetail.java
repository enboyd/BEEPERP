/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.biz.entity;

import com.thinkgem.jeesite.common.utils.excel.annotation.ExcelField;
import org.hibernate.validator.constraints.Length;
import com.thinkgem.jeesite.modules.sys.entity.User;
import javax.validation.constraints.NotNull;

import com.thinkgem.jeesite.common.persistence.DataEntity;

import java.util.Date;
import java.util.List;

/**
 * 入库单明细的ControllerEntity
 * @author tiancheng
 * @version 2018-07-16
 */
public class BizStockInDetail extends DataEntity<BizStockInDetail> {
	
	private static final long serialVersionUID = 1L;
	private String stockInDetailId;		// 入库明细ID
	private String stockInId;		// 所属入库单
	private String purchaseDetailId;		// 采购单明细的id
	private String goodsId;		// 商品ID
	private String goodsName;//商品名称
	private String actualAmount;		// 实际入库数量
	private String waitDistributionAmount;   //待分配托盘数
	private String specification;  // 商品的规格
	private String manufactureDate;//生产日期
	private String expirationDate;//保质期天数
	private String storageLocationId;//仓库库位的id
	private String storageLocationName;  //仓库库位的name
	private String status; // 入库单明细是否全部入库(0,部分到货，1，全部到货)
	private List<BizBarcode> bizBarcodeList; //入库单明细的条形码
	private String bizBarcodeNum;  //入库单明细下面的条码数
	private Double unitPrice;		// 采购单价
	/*
		导出需要字段
	 */
	private Integer num;	//序号
	private Date stockInDate;		// 入库日期
	private String stockInCode;		// 入库单编号
	private String supplierName;	//供应商的name
	private String createName;	//制单人
	private String goodsCode;		// 商品编号
	private String unit;	//单位


	public BizStockInDetail() {
		super();
	}

	public BizStockInDetail(String id){
		super(id);
	}

	@Length(min=1, max=20, message="入库明细ID长度必须介于 1 和 20 之间")
	public String getStockInDetailId() {
		return stockInDetailId;
	}

	public void setStockInDetailId(String stockInDetailId) {
		this.stockInDetailId = stockInDetailId;
	}
	
	@Length(min=1, max=20, message="所属入库单长度必须介于 1 和 20 之间")
	public String getStockInId() {
		return stockInId;
	}

	public void setStockInId(String stockInId) {
		this.stockInId = stockInId;
	}
	
	@Length(min=0, max=32, message="采购单明细的id长度必须介于 0 和 32 之间")
	public String getPurchaseDetailId() {
		return purchaseDetailId;
	}

	public void setPurchaseDetailId(String purchaseDetailId) {
		this.purchaseDetailId = purchaseDetailId;
	}
	
	@Length(min=1, max=20, message="商品ID长度必须介于 1 和 20 之间")
	public String getGoodsId() {
		return goodsId;
	}

	public void setGoodsId(String goodsId) {
		this.goodsId = goodsId;
	}
	
	@Length(min=0, max=10, message="实际入库数量长度必须介于 0 和 10 之间")
	@ExcelField(title="入库数量", align=1, sort=100)
	public String getActualAmount() {
		return actualAmount;
	}

	public void setActualAmount(String actualAmount) {
		this.actualAmount = actualAmount;
	}

	@ExcelField(title="存货名称", align=1, sort=70)
	public String getGoodsName() {
		return goodsName;
	}

	public void setGoodsName(String goodsName) {
		this.goodsName = goodsName;
	}

	public String getManufactureDate() {
		return manufactureDate;
	}

	public void setManufactureDate(String manufactureDate) {
		this.manufactureDate = manufactureDate;
	}

	public String getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(String expirationDate) {
		this.expirationDate = expirationDate;
	}

	@ExcelField(title="规格", align=1, sort=80)
	public String getSpecification() {
		return specification;
	}

	public void setSpecification(String specification) {
		this.specification = specification;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public List<BizBarcode> getBizBarcodeList() {
		return bizBarcodeList;
	}

	public void setBizBarcodeList(List<BizBarcode> bizBarcodeList) {
		this.bizBarcodeList = bizBarcodeList;
	}

	public String getWaitDistributionAmount() {
		return waitDistributionAmount;
	}

	public void setWaitDistributionAmount(String waitDistributionAmount) {
		this.waitDistributionAmount = waitDistributionAmount;
	}

	public String getBizBarcodeNum() {
		return bizBarcodeNum;
	}

	public void setBizBarcodeNum(String bizBarcodeNum) {
		this.bizBarcodeNum = bizBarcodeNum;
	}

	public String getStorageLocationId() {
		return storageLocationId;
	}

	public void setStorageLocationId(String storageLocationId) {
		this.storageLocationId = storageLocationId;
	}

	public String getStorageLocationName() {
		return storageLocationName;
	}

	public void setStorageLocationName(String storageLocationName) {
		this.storageLocationName = storageLocationName;
	}

	@ExcelField(title="入库日期", align=1, sort=20)
	public Date getStockInDate() {
		return stockInDate;
	}

	public void setStockInDate(Date stockInDate) {
		this.stockInDate = stockInDate;
	}

	@ExcelField(title="入库单号", align=1, sort=30)
	public String getStockInCode() {
		return stockInCode;
	}

	public void setStockInCode(String stockInCode) {
		this.stockInCode = stockInCode;
	}

	@ExcelField(title="供应商", align=1, sort=40)
	public String getSupplierName() {
		return supplierName;
	}

	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}

	@ExcelField(title="制单人", align=1, sort=50)
	public String getCreateName() {
		return createName;
	}

	public void setCreateName(String createName) {
		this.createName = createName;
	}

	@ExcelField(title="存货编码", align=1, sort=60)
	public String getGoodsCode() {
		return goodsCode;
	}

	public void setGoodsCode(String goodsCode) {
		this.goodsCode = goodsCode;
	}

	@ExcelField(title="单位", align=1, sort=90)
	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	@ExcelField(title="序号", align=1, sort=10)
	public Integer getNum() {
		return num;
	}

	public void setNum(Integer num) {
		this.num = num;
	}

	public Double getUnitPrice() {
		return unitPrice;
	}

	public void setUnitPrice(Double unitPrice) {
		this.unitPrice = unitPrice;
	}
}