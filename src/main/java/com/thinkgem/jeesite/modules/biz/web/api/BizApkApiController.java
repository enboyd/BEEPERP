/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.biz.web.api;

import com.thinkgem.jeesite.common.web.BaseController;
import com.thinkgem.jeesite.modules.biz.entity.BizApk;
import com.thinkgem.jeesite.modules.biz.form.BizStockInForm;
import com.thinkgem.jeesite.modules.biz.service.BizApkService;
import com.thinkgem.jeesite.modules.biz.vo.ResultVO;
import com.thinkgem.jeesite.modules.sys.enums.ResultEnum;
import com.thinkgem.jeesite.modules.sys.service.SystemService;
import com.thinkgem.jeesite.modules.sys.utils.ResultVOUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;

/**
 * 手机端Apk的操作
 * @author tiancheng
 * @version 2018-08-24
 */
@Controller
@Slf4j
@RequestMapping(value = "${adminPath}/api/biz/bizApk")
public class BizApkApiController extends BaseController {


	@Autowired
	BizApkService bizApkService;
	 /**
	  * 手机端查询Apk
	  * @author tiancheng  
	  * @date 2018/8/25 14:39
	  * @param
	  * @return   
	  */  
	@RequestMapping(value = "get")
	@ResponseBody
	public ResultVO get() {
		BizApk bizApk = bizApkService.findListApi();
		return ResultVOUtil.success(bizApk);
	}



}