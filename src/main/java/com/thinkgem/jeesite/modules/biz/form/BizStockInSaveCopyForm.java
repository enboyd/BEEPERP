package com.thinkgem.jeesite.modules.biz.form;

import lombok.Data;

import java.util.List;

/**
 * Description:
 * Author: LTY
 * Create Time: 2018-08-09 17:09
 */
@Data
public class BizStockInSaveCopyForm {

   private String purchaseId;//采购单的id

    private String createById;  //创建人id
    private String createByName;  //创建人name

}
