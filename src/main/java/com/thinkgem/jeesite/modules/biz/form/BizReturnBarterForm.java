package com.thinkgem.jeesite.modules.biz.form;

import com.google.common.collect.Lists;
import com.thinkgem.jeesite.modules.biz.entity.SmReturnBarterDetail;
import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * @author : xuchen
 * @date : 2018/9/12 14:42
 */
@Data
public class BizReturnBarterForm {

    private String id;  //id
    private String name;		// 编码
    private String category;		// 分类(退/换)
    private String goodsType;		// 退货类型
    private String customerId;		// 客户id
    private String customerName;		// 客户名字
    private String phone;		// 手机号
    private String address;		// 收货地址
    private String auditStatus;		// 审核状态
    private String createBy;    //创建人id
    private String createByName;    //创建人name
    private Date createDate;    //创建日期
    private String updateBy;    //更新人id
    private String updateByName;    //更新人name
    private Date updateDate;    //更新日期
    private String remarks;     //备注
    private String deliverType;		// 发货方式
    private List<BizReturnBarterDetailForm> bizReturnBarterDetailList = Lists.newArrayList();		// 退换货商品
}
