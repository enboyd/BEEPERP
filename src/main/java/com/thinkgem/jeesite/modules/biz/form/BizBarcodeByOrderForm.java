/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.biz.form;

import lombok.Data;

/**
 * 条形码Entity
 * @author tiancheng
 * @version 2018-09-05
 */
@Data
public class BizBarcodeByOrderForm {

	private String name;		// 条形码编码
	private String goodsId;    //商品的id
}