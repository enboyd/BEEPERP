/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.biz.form;

import lombok.Data;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * 条形码Entity
 * @author tiancheng
 * @version 2018-09-05
 */
@Data
public class BizBarcodeListForm {

	@NotEmpty(message = "商品的id必填")
	private String goodsId;		// 商品的id

	@NotEmpty(message = "订单的id必填")
	private String orderId;		// 订单的id
}