/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.biz.form;

import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * 订单Entity
 * @author tiancheng
 * @version 2018-09-17
 */
@Data
public class BizOrderOutForm {

	private String orderId;
}