/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.biz.dao;

import com.thinkgem.jeesite.common.persistence.CrudDao;
import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.persistence.annotation.MyBatisDao;
import com.thinkgem.jeesite.modules.biz.entity.BizPurchase;
import com.thinkgem.jeesite.modules.biz.vo.BizPurchaseVo;

import java.util.List;

/**
 * 采购单的controllerDAO接口
 * @author tiancheng
 * @version 2018-07-13
 */
@MyBatisDao
public interface BizPurchaseDao extends CrudDao<BizPurchase> {

    void updateStatus(BizPurchase bizPurchase);

    List<BizPurchaseVo> findListApi(BizPurchase bizPurchase);

    BizPurchaseVo getApi(String purchaseId);

    List<BizPurchase> findListForFinance(BizPurchase bizPurchase);

    void updateAuditStatus(BizPurchase bizPurchase);
}