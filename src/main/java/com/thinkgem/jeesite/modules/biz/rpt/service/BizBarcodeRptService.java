package com.thinkgem.jeesite.modules.biz.rpt.service;

import com.thinkgem.jeesite.common.persistence .Page;
import com.thinkgem.jeesite.common.service.CrudService;
import com.thinkgem.jeesite.modules.biz.rpt.dao.BizBarcodeRptDao;
import com.thinkgem.jeesite.modules.biz.rpt.entity.BizBarcodeRpt;
import com.thinkgem.jeesite.modules.biz.rpt.entity.BizGoodsRpt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Administrator on 2018/10/15.
 */
@Service
@Transactional(readOnly = true)
public class BizBarcodeRptService extends CrudService<BizBarcodeRptDao, BizBarcodeRpt> {

    @Autowired
    private BizBarcodeRptDao bizBarcodeRptDao;

    public List<BizBarcodeRpt> findList(BizBarcodeRpt bizBarcodeRpt) {
        return super.findList(bizBarcodeRpt);
    }

    public Page<BizBarcodeRpt> findPage(Page<BizBarcodeRpt> page, BizBarcodeRpt bizBarcodeRpt) {
        return super.findPage(page, bizBarcodeRpt);
    }

    public Page<BizGoodsRpt> findRptPage(Page<BizGoodsRpt> page, BizGoodsRpt bizGoodsRpt) {
        bizGoodsRpt.setPage(page);
        List<BizGoodsRpt> bizBarcodeRptList = bizBarcodeRptDao.selectByGoodsCode(bizGoodsRpt);
        page.setList(bizBarcodeRptList);
        return page;
    }

    public Integer selectByGoodsCodeCount(BizGoodsRpt bizGoodsRpt){
        return bizBarcodeRptDao.selectByGoodsCodeCount(bizGoodsRpt);
    }
}
