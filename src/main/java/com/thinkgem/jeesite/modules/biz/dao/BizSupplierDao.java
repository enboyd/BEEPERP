/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.biz.dao;

import com.thinkgem.jeesite.common.persistence.CrudDao;
import com.thinkgem.jeesite.common.persistence.annotation.MyBatisDao;
import com.thinkgem.jeesite.modules.biz.entity.BizSupplier;
import org.apache.ibatis.annotations.Param;

/**
 * 供应商的ControllerDAO接口
 * @author tiancheng
 * @version 2018-07-13
 */
@MyBatisDao
public interface BizSupplierDao extends CrudDao<BizSupplier> {

    /**
     * 审批供应商	XC
     * @param after
     * @param supplierId
     */
    void approval(@Param("supplierId") String supplierId, @Param("after") String after);

    BizSupplier findByName(String supplierName);
}