/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.biz.form;

import lombok.Data;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * 移库Entity
 * @author tiancheng
 * @version 2018-09-12
 */
@Data
public class BizMovePositionLogForm{

	@NotEmpty(message = "条码的编号必填")
	private String barcode;

	@NotEmpty(message = "商品的id必填")
	private String goodsId;

	@NotEmpty(message = "数量必填")
	private String number;

	@NotEmpty(message = "移库前的库位必填")
	private String storageLocationIdFront;

	@NotEmpty(message = "移库后的库位必填")
	private String storageLocationIdAfter;

	@NotEmpty(message = "当前登录人id必填")
	private String userId;

}