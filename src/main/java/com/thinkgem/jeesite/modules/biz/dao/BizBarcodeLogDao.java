/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.biz.dao;

import com.thinkgem.jeesite.common.persistence.CrudDao;
import com.thinkgem.jeesite.common.persistence.annotation.MyBatisDao;
import com.thinkgem.jeesite.modules.biz.entity.BizBarcodeLog;
import org.apache.ibatis.annotations.Param;
import org.springframework.security.access.method.P;

import java.util.List;

/**
 * 出库日志DAO接口
 * @author tiancheng
 * @version 2018-09-19
 */
@MyBatisDao
public interface BizBarcodeLogDao extends CrudDao<BizBarcodeLog> {

    Integer selectSumByOrderIdAndGoodsId(BizBarcodeLog bizBarcodeLog);

    List<BizBarcodeLog> getAllByGoodsIdAndOrderId(BizBarcodeLog bizBarcodeLog);

    List<BizBarcodeLog> selectGroupGoodsId(String orderId);
}