/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.biz.dao;

import com.thinkgem.jeesite.common.persistence.CrudDao;
import com.thinkgem.jeesite.common.persistence.annotation.MyBatisDao;
import com.thinkgem.jeesite.modules.biz.entity.SmReturnBarterDetail;

import java.util.ArrayList;
import java.util.List;

/**
 * 退换货明细DAO接口
 * @author xuchen
 * @version 2018-09-11
 */
@MyBatisDao
public interface SmReturnBarterDetailDao extends CrudDao<SmReturnBarterDetail> {

	void deleteByReturnBarterId(String returnBarterId);

	/**
	 * 获取换货商品
	 * @param smReturnBarterId
	 * @return
	 */
	List<SmReturnBarterDetail> getBarterDetailListByPid(String smReturnBarterId);
	
}