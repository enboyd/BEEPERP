/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.biz.service;

import java.util.List;

import com.thinkgem.jeesite.modules.biz.entity.BizPurchase;
import com.thinkgem.jeesite.modules.biz.vo.BizPurchaseDetailVo;
import com.thinkgem.jeesite.modules.sys.utils.UserUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.service.CrudService;
import com.thinkgem.jeesite.modules.biz.entity.BizPurchaseDetail;
import com.thinkgem.jeesite.modules.biz.dao.BizPurchaseDetailDao;

/**
 * 采购列表单明细的controllerService
 * @author tiancheng
 * @version 2018-07-13
 */
@Service
@Transactional(readOnly = true)
public class BizPurchaseDetailService extends CrudService<BizPurchaseDetailDao, BizPurchaseDetail> {

	@Autowired
	BizPurchaseDetailDao bizPurchaseDetailDao;

	public BizPurchaseDetail get(String id) {
		return super.get(id);
	}
	
	public List<BizPurchaseDetail> findList(BizPurchaseDetail bizPurchaseDetail) {
		return super.findList(bizPurchaseDetail);
	}
	
	public Page<BizPurchaseDetail> findPage(Page<BizPurchaseDetail> page, BizPurchaseDetail bizPurchaseDetail) {
		return super.findPage(page, bizPurchaseDetail);
	}
	
	@Transactional(readOnly = false)
	public void save(BizPurchaseDetail bizPurchaseDetail) {
		if(bizPurchaseDetail != null && StringUtils.isNotEmpty(bizPurchaseDetail.getPurchaseDetailId())){
			bizPurchaseDetail.setUpdateBy(UserUtils.getUser());
			bizPurchaseDetailDao.update(bizPurchaseDetail);
		}else{
			super.save(bizPurchaseDetail);
		}

	}
	
	@Transactional(readOnly = false)
	public void delete(BizPurchaseDetail bizPurchaseDetail) {
		super.delete(bizPurchaseDetail);
	}

	 /**
	  * 根据采购单物理删除
	  * @author tiancheng
	  * @date 2018/7/14 10:42
	  * @param
	  * @return
	  */
	@Transactional(readOnly = false)
	public void deleteByPurchaseId(String purchaseId) {
		dao.deleteByPurchaseId(purchaseId);
	}

	 /**
	  * 根据采购单逻辑删除
	  * @author tiancheng
	  * @date 2018/7/14 10:42
	  * @param
	  * @return
	  */
	@Transactional(readOnly = false)
	public void deleteByPurchaseIdLJ(String purchaseId) {
		dao.deleteByPurchaseIdLJ(purchaseId);
	}

	 /**
	  * 修改实际入库数量和相差数量
	  * @author tiancheng  
	  * @date 2018/7/16 14:04
	  * @param
	  * @return   
	  */  
	@Transactional(readOnly = false)
	public void updateAmount(BizPurchaseDetail bizPurchaseDetail) {
		dao.updateAmount(bizPurchaseDetail);
	}


	 /**
	  * 查询采购单下面的明细是否全部到货
	  * @author tiancheng
	  * @date 2018/7/16 14:29
	  * @param purchaseId 采购单id
	  * @return
	  */
	public Integer selectByPurchaseIdAndDifferentAmount(String purchaseId){
		return dao.selectByPurchaseIdAndDifferentAmount(purchaseId);
	}

	 /**
	  * 手机端查询代购单下的采购单列表
	  * @author tiancheng
	  * @date 2018/8/25 15:15
	  * @param
	  * @return
	  */
	public List<BizPurchaseDetailVo> findListApi(String purchaseId){
		BizPurchaseDetail bizPurchaseDetail = new BizPurchaseDetail();
		bizPurchaseDetail.setPurchaseId(purchaseId);
		return bizPurchaseDetailDao.findListApi(bizPurchaseDetail);
	}

	/**
	 * 查询采购单明细Page
	 * xuchen
	 * @param bizPurchaseDetailPage
	 * @param bizPurchase
	 * @return
	 */
    public Page<BizPurchaseDetail> findPageByParent(Page<BizPurchaseDetail> bizPurchaseDetailPage, BizPurchase bizPurchase) {
		BizPurchaseDetail bizPurchaseDetail = new BizPurchaseDetail();
		bizPurchaseDetail.setPage(bizPurchaseDetailPage);
		return bizPurchaseDetailPage.setList(bizPurchaseDetailDao.findListByParent(bizPurchase));
    }

	/**
	 * 查询采购单明细List
	 * xuchen
	 * @param bizPurchase
	 * @return
	 */
	public List<BizPurchaseDetail> findListByParent(BizPurchase bizPurchase) {
    	return bizPurchaseDetailDao.findListByParent(bizPurchase);
	}


	 /**
	  * 查询采购单中相差数不为0的条数
	  * @author tiancheng  
	  * @date 2018/11/24 13:40
	  * @param
	  * @return   
	  */  
	public Integer selectCountByPurchaseId(String purchaseId){
		return bizPurchaseDetailDao.selectCountByPurchaseId(purchaseId);
	}

	/**
	  * 根据父级采购id并根据供应商id分组查询供应商idList
	  * @author xuchen
	  * @date 2018/12/05 9:08
	  * @param
	  * @return
	  */
	public List<String> selectByParIdGroupBySuppiler(String purchaseId){
		return bizPurchaseDetailDao.selectByParIdGroupBySuppiler(purchaseId);
	}


	/**
	 * 更新采购明细
	 * @param bizPurchaseDetaii
	 */
	@Transactional(readOnly = false)
	public void update(BizPurchaseDetail bizPurchaseDetaii) {
		bizPurchaseDetailDao.update(bizPurchaseDetaii);
	}
}