/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.biz.entity;

import org.hibernate.validator.constraints.Length;

import com.thinkgem.jeesite.common.persistence.DataEntity;

/**
 * 移库Entity
 * @author tiancheng
 * @version 2018-09-12
 */
public class BizMovePositionLog extends DataEntity<BizMovePositionLog> {
	
	private static final long serialVersionUID = 1L;
	private String name;		// 移库的编号
	private String goodsId;		// 商品的id
	private String number;		// 数量
	private String storageLocationIdFront;		// 移库前的库位
	private String storageLocationIdAfter;		// 移库后的库位
	private int pageSize;		//条数
	private int pageNo;			//页码

	public BizMovePositionLog() {
		super();
	}

	public BizMovePositionLog(String id){
		super(id);
	}

	@Length(min=0, max=255, message="移库的编号长度必须介于 0 和 255 之间")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	@Length(min=0, max=32, message="商品的id长度必须介于 0 和 32 之间")
	public String getGoodsId() {
		return goodsId;
	}

	public void setGoodsId(String goodsId) {
		this.goodsId = goodsId;
	}
	
	@Length(min=0, max=32, message="数量长度必须介于 0 和 32 之间")
	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}
	
	@Length(min=0, max=32, message="移库前的库位长度必须介于 0 和 32 之间")
	public String getStorageLocationIdFront() {
		return storageLocationIdFront;
	}

	public void setStorageLocationIdFront(String storageLocationIdFront) {
		this.storageLocationIdFront = storageLocationIdFront;
	}
	
	@Length(min=0, max=32, message="移库后的库位长度必须介于 0 和 32 之间")
	public String getStorageLocationIdAfter() {
		return storageLocationIdAfter;
	}

	public void setStorageLocationIdAfter(String storageLocationIdAfter) {
		this.storageLocationIdAfter = storageLocationIdAfter;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public int getPageNo() {
		return pageNo;
	}

	public void setPageNo(int pageNo) {
		this.pageNo = pageNo;
	}
}