/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.biz.entity;

import com.thinkgem.jeesite.common.utils.excel.annotation.ExcelField;
import org.hibernate.validator.constraints.Length;
import javax.validation.constraints.NotNull;
import com.thinkgem.jeesite.modules.sys.entity.User;

import com.thinkgem.jeesite.common.persistence.DataEntity;

import java.util.Date;

/**
 * 请购单明细的Entity
 * @author xuchen
 * @version 2018-07-28
 */
public class BizBuyingRequisitionDetail extends DataEntity<BizBuyingRequisitionDetail> {
	private static final long serialVersionUID = 1L;
	private String buyingDetailId;		// 请购单明细ID
	private String buyingId;		// 所属请购单ID
	private String goodsId;		// 商品ID
	private Integer expertAmount;		// 申请请购数量

	private String supplierId;		// 供应商
	private String goodsCode;		// 商品编号
	private String goodsName;		// 商品名称
	private String goodsBrandId;		// 商品品牌
	private String goodsBrandName;		// 商品品牌
	private String goodsCategoryId;		// 商品类型
	private String goodsCategoryName;		// 商品类型
	private String supplierName; //供应商name
	private String goodsUnitPrice; //商品的单价
	private String sellingPrice; //参考售价
	private String stockAmount; //库存数量
	private String purchasingAmount; //采购中数量
	private String warningAmount; //预警数量
	private String specification;		// 规格
	private String unit;	//单位
	private Integer minPurchaseQuantity;	//最小起购数量
	private Integer submitOutExpertAmount;	//提交的采购单明细对应数量

	private Integer outExpertAmount;  //已采购数
	private Integer surplusExpertAmount;  //剩余数
	private Date demandTime;		// 需求时间

	/*
	 * 只用于导出
	 */
	private String type;	//业务类型
	private String buyingCode;	//所属请购单编码
	private Date buyingDate;	//请购日期
	private String createName;	//制单人
	private Integer num;	//序号

	@ExcelField(title="规格型号", align=1, sort=60)
	public String getSpecification() {
		return specification;
	}

	public void setSpecification(String specification) {
		this.specification = specification;
	}

	@ExcelField(title="主计量", align=1, sort=70)
	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public BizBuyingRequisitionDetail() {
		super();
	}

	public BizBuyingRequisitionDetail(String id){
		super(id);
	}

	public String getBuyingDetailId() {
		return buyingDetailId;
	}

	public void setBuyingDetailId(String buyingDetailId) {
		this.buyingDetailId = buyingDetailId;
	}

	public String getBuyingId() {
		return buyingId;
	}

	public void setBuyingId(String buyingId) {
		this.buyingId = buyingId;
	}

	public String getGoodsId() {
		return goodsId;
	}

	public void setGoodsId(String goodsId) {
		this.goodsId = goodsId;
	}

	@ExcelField(title="数量", align=1, sort=80)
	public Integer getExpertAmount() {
		return expertAmount;
	}

	public void setExpertAmount(Integer expertAmount) {
		this.expertAmount = expertAmount;
	}

	public String getSupplierId() {
		return supplierId;
	}

	public void setSupplierId(String supplierId) {
		this.supplierId = supplierId;
	}

	@ExcelField(title="存货编码", align=1, sort=40)
	public String getGoodsCode() {
		return goodsCode;
	}

	public void setGoodsCode(String goodsCode) {
		this.goodsCode = goodsCode;
	}

	@ExcelField(title="存货名称", align=1, sort=50)
	public String getGoodsName() {
		return goodsName;
	}

	public void setGoodsName(String goodsName) {
		this.goodsName = goodsName;
	}

	public String getGoodsBrandId() {
		return goodsBrandId;
	}

	public void setGoodsBrandId(String goodsBrandId) {
		this.goodsBrandId = goodsBrandId;
	}

	public String getGoodsCategoryId() {
		return goodsCategoryId;
	}

	public void setGoodsCategoryId(String goodsCategoryId) {
		this.goodsCategoryId = goodsCategoryId;
	}

	@ExcelField(title="供应商", align=1, sort=90)
	public String getSupplierName() {
		return supplierName;
	}

	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}

	public String getGoodsBrandName() {
		return goodsBrandName;
	}

	public void setGoodsBrandName(String goodsBrandName) {
		this.goodsBrandName = goodsBrandName;
	}

	public String getGoodsCategoryName() {
		return goodsCategoryName;
	}

	public void setGoodsCategoryName(String goodsCategoryName) {
		this.goodsCategoryName = goodsCategoryName;
	}

	public String getGoodsUnitPrice() {
		return goodsUnitPrice;
	}

	public void setGoodsUnitPrice(String goodsUnitPrice) {
		this.goodsUnitPrice = goodsUnitPrice;
	}

	public String getSellingPrice() {
		return sellingPrice;
	}

	public void setSellingPrice(String sellingPrice) {
		this.sellingPrice = sellingPrice;
	}

	public String getStockAmount() {
		return stockAmount;
	}

	public void setStockAmount(String stockAmount) {
		this.stockAmount = stockAmount;
	}

	public String getPurchasingAmount() {
		return purchasingAmount;
	}

	public void setPurchasingAmount(String purchasingAmount) {
		this.purchasingAmount = purchasingAmount;
	}

	public String getWarningAmount() {
		return warningAmount;
	}

	public void setWarningAmount(String warningAmount) {
		this.warningAmount = warningAmount;
	}


	public Integer getOutExpertAmount() {
		return outExpertAmount;
	}

	public void setOutExpertAmount(Integer outExpertAmount) {
		this.outExpertAmount = outExpertAmount;
	}

	public Integer getSurplusExpertAmount() {
		return surplusExpertAmount;
	}

	public void setSurplusExpertAmount(Integer surplusExpertAmount) {
		this.surplusExpertAmount = surplusExpertAmount;
	}
	public Integer getMinPurchaseQuantity() {
		return minPurchaseQuantity;
	}

	public void setMinPurchaseQuantity(Integer minPurchaseQuantity) {
		this.minPurchaseQuantity = minPurchaseQuantity;
	}

	@ExcelField(title="业务类型", align=1, dictType = "biz_buying_requisition_type", sort=10)
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@ExcelField(title="单据号", align=1, sort=20)
	public String getBuyingCode() {
		return buyingCode;
	}

	public void setBuyingCode(String buyingCode) {
		this.buyingCode = buyingCode;
	}

	@ExcelField(title="日期", align=1, sort=30)
	public Date getBuyingDate() {
		return buyingDate;
	}

	public void setBuyingDate(Date buyingDate) {
		this.buyingDate = buyingDate;
	}

	@ExcelField(title="制单人", align=1, sort=100)
	public String getCreateName() {
		return createName;
	}

	public void setCreateName(String createName) {
		this.createName = createName;
	}

	@ExcelField(title="序号", align=1, sort=5)
	public Integer getNum() {
		return num;
	}

	public void setNum(Integer num) {
		this.num = num;
	}

	public Date getDemandTime() {
		return demandTime;
	}

	public void setDemandTime(Date demandTime) {
		this.demandTime = demandTime;
	}

	public Integer getSubmitOutExpertAmount() {
		return submitOutExpertAmount;
	}

	public void setSubmitOutExpertAmount(Integer submitOutExpertAmount) {
		this.submitOutExpertAmount = submitOutExpertAmount;
	}
}