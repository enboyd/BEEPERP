/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.biz.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.thinkgem.jeesite.common.config.Global;
import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.web.BaseController;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.modules.biz.entity.BizPurchaseLog;
import com.thinkgem.jeesite.modules.biz.service.BizPurchaseLogService;

import java.util.List;

/**
 * 采购单日志的ControllerController
 * @author tiancheng
 * @version 2018-07-17
 */
@Controller
@RequestMapping(value = "${adminPath}/biz/bizPurchaseLog")
public class BizPurchaseLogController extends BaseController {

	@Autowired
	private BizPurchaseLogService bizPurchaseLogService;
	
	@ModelAttribute
	public BizPurchaseLog get(@RequestParam(required=false) String id) {
		BizPurchaseLog entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = bizPurchaseLogService.get(id);
		}
		if (entity == null){
			entity = new BizPurchaseLog();
		}
		return entity;
	}
	
	@RequiresPermissions("biz:bizPurchaseLog:view")
	@RequestMapping(value = {"list", ""})
	public String list(BizPurchaseLog bizPurchaseLog, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<BizPurchaseLog> page = bizPurchaseLogService.findPage(new Page<BizPurchaseLog>(request, response), bizPurchaseLog); 
		model.addAttribute("page", page);
		return "modules/biz/bizPurchaseLogList";
	}

	@RequiresPermissions("biz:bizPurchaseLog:view")
	@RequestMapping(value = "form")
	public String form(BizPurchaseLog bizPurchaseLog, Model model) {
		model.addAttribute("bizPurchaseLog", bizPurchaseLog);
		return "modules/biz/bizPurchaseLogForm";
	}

	@RequiresPermissions("biz:bizPurchaseLog:edit")
	@RequestMapping(value = "save")
	public String save(BizPurchaseLog bizPurchaseLog, Model model, RedirectAttributes redirectAttributes) {
		if (!beanValidator(model, bizPurchaseLog)){
			return form(bizPurchaseLog, model);
		}
		bizPurchaseLogService.save(bizPurchaseLog);
		addMessage(redirectAttributes, "保存采购单日志成功");
		return "redirect:"+Global.getAdminPath()+"/biz/bizPurchaseLog/?repage";
	}
	
	@RequiresPermissions("biz:bizPurchaseLog:edit")
	@RequestMapping(value = "delete")
	public String delete(BizPurchaseLog bizPurchaseLog, RedirectAttributes redirectAttributes) {
		bizPurchaseLogService.delete(bizPurchaseLog);
		addMessage(redirectAttributes, "删除采购单日志成功");
		return "redirect:"+Global.getAdminPath()+"/biz/bizPurchaseLog/?repage";
	}


	 /**
	  * 根据采购单id查询日志
	  * @author tiancheng  
	  * @date 2018/7/17 11:13N
	  * @param purchaseId 采购单的id
	  * @return   
	  */
	@RequestMapping(value = "getBizPurchaseLogListByPurchaseId")
	@ResponseBody
	public List<BizPurchaseLog> getBizPurchaseLogListByPurchaseId(String purchaseId){
		BizPurchaseLog bizPurchaseLog = new BizPurchaseLog();
		bizPurchaseLog.setPurchaseId(purchaseId);
		List<BizPurchaseLog> bizPurchaseLogList = bizPurchaseLogService.findList(bizPurchaseLog);
		//页面createBy.name 不能获取
		for (BizPurchaseLog biz:bizPurchaseLogList) {
			biz.setCreateName(biz.getCreateBy().getName());
		}
		return bizPurchaseLogList;
	}
}