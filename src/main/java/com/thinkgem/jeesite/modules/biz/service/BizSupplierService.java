/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.biz.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.service.CrudService;
import com.thinkgem.jeesite.modules.biz.entity.BizSupplier;
import com.thinkgem.jeesite.modules.biz.dao.BizSupplierDao;

/**
 * 供应商的ControllerService
 * @author tiancheng
 * @version 2018-07-13
 */
@Service
@Transactional(readOnly = true)
public class BizSupplierService extends CrudService<BizSupplierDao, BizSupplier> {

	@Autowired
	private BizSupplierDao bizSupplierDao;

	public BizSupplier get(String id) {
		return super.get(id);
	}
	
	public List<BizSupplier> findList(BizSupplier bizSupplier) {
		return super.findList(bizSupplier);
	}
	
	public Page<BizSupplier> findPage(Page<BizSupplier> page, BizSupplier bizSupplier) {
		return super.findPage(page, bizSupplier);
	}
	
	@Transactional(readOnly = false)
	public void save(BizSupplier bizSupplier) {
		if(null == bizSupplier.getSupplierId() || bizSupplier.getSupplierId().equals("")){
			//bizSupplier.setAppState("5");
			//添加直接通过
			bizSupplier.setAppState("15");
		}
		super.save(bizSupplier);
	}
	
	@Transactional(readOnly = false)
	public void delete(BizSupplier bizSupplier) {
		super.delete(bizSupplier);
	}

	/**
	 * 审批供应商	XC
	 * @param after	操作后的状态
	 * @param supplierId
	 */
	@Transactional(readOnly = false)
	public void approval(String supplierId,String after) {
		bizSupplierDao.approval(supplierId,after);
	}
}