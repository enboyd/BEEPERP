/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.biz.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.thinkgem.jeesite.common.config.Global;
import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.web.BaseController;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.modules.biz.entity.BizGoodsBrand;
import com.thinkgem.jeesite.modules.biz.service.BizGoodsBrandService;

import java.util.List;

/**
 * 商品品牌的ControllerController
 * @author tiancheng
 * @version 2018-07-12
 */
@Controller
@RequestMapping(value = "${adminPath}/biz/bizGoodsBrand")
public class BizGoodsBrandController extends BaseController {

	@Autowired
	private BizGoodsBrandService bizGoodsBrandService;
	
	@ModelAttribute
	public BizGoodsBrand get(@RequestParam(required=false) String id) {
		BizGoodsBrand entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = bizGoodsBrandService.get(id);
		}
		if (entity == null){
			entity = new BizGoodsBrand();
		}
		return entity;
	}
	
	@RequiresPermissions("biz:bizGoodsBrand:view")
	@RequestMapping(value = {"list", ""})
	public String list(BizGoodsBrand bizGoodsBrand, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<BizGoodsBrand> page = bizGoodsBrandService.findPage(new Page<BizGoodsBrand>(request, response), bizGoodsBrand); 
		model.addAttribute("page", page);
		return "modules/biz/bizGoodsBrandList";
	}

	@RequiresPermissions("biz:bizGoodsBrand:view")
	@RequestMapping(value = "form")
	public String form(BizGoodsBrand bizGoodsBrand, Model model) {
		model.addAttribute("bizGoodsBrand", bizGoodsBrand);
		return "modules/biz/bizGoodsBrandForm";
	}

	@RequiresPermissions(value={"biz:bizGoodsBrand:add","biz:bizGoodsBrand:edit"},logical= Logical.OR)
	@RequestMapping(value = "save")
	public String save(BizGoodsBrand bizGoodsBrand, Model model, RedirectAttributes redirectAttributes) {
		if (!beanValidator(model, bizGoodsBrand)){
			return form(bizGoodsBrand, model);
		}
		bizGoodsBrandService.save(bizGoodsBrand);
		addMessage(redirectAttributes, "保存商品品牌成功");
		return "redirect:"+Global.getAdminPath()+"/biz/bizGoodsBrand/?repage";
	}
	
	@RequiresPermissions("biz:bizGoodsBrand:remove")
	@RequestMapping(value = "delete")
	public String delete(BizGoodsBrand bizGoodsBrand, RedirectAttributes redirectAttributes) {
		bizGoodsBrandService.delete(bizGoodsBrand);
		addMessage(redirectAttributes, "删除商品品牌成功");
		return "redirect:"+Global.getAdminPath()+"/biz/bizGoodsBrand/?repage";
	}

	/**
	 * 添加商品品牌的ajax
	 * @author tiancheng
	 * @date 2018/7/12 17:07
	 * @param bizGoodsBrand
	 * @return
	 */
	@RequestMapping(value = "saveBizGoodsBrand")
	@ResponseBody
	public String saveBizGoodsCategory(BizGoodsBrand bizGoodsBrand){
		String msg = "";
		try {
			bizGoodsBrandService.save(bizGoodsBrand);
			msg = "添加商品品牌成功！";
		} catch (Exception e) {
			e.printStackTrace();
			msg = "添加商品品牌失败";
		}
		return msg;
	}


	 /**
	  * 查询全部的商品品牌
	  * @author tiancheng  
	  * @date 2018/7/12 17:18
	  * @param
	  * @return   
	  */
	 @RequestMapping(value = "findBizGoodsBrandList")
	 @ResponseBody
	public List<BizGoodsBrand> findBizGoodsBrandList(){
		List<BizGoodsBrand> bizGoodsBrandList = bizGoodsBrandService.findAllList(new BizGoodsBrand());
		return bizGoodsBrandList;
	}

}