/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.biz.vo;

import lombok.Data;

import java.util.List;

/**
 * 库位管理Entity
 * @author xuchen
 * @version 2018-07-17
 */
@Data
public class BizStorageLocationVo  {

	private static final long serialVersionUID = 1L;
	private String id;
	private String name;		// 名称
	private List<BizStorageLocationVo> bizStorageLocationVoList; //下级的库位
}