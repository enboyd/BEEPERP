/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.biz.entity;
import com.thinkgem.jeesite.common.persistence.DataEntity;

import java.util.Date;

/**
 * 废弃商品Entity
 * @author xuchen
 * @version 2018-11-02
 */
public class BizDiscardedGoods extends DataEntity<BizDiscardedGoods> {
	
	private static final long serialVersionUID = 1L;
	private String goodsId;		// 商品ID
	private String goodsCode;		// 商品编号
	private String goodsName;		// 商品名称
	private String barcodeId;		// 条形码id
	private String barcodeName;		// 条形码编号
	private String batch;		// 批次
	private Integer number;		// 数量
	private String specification;		// 规格
	private String storageLocationId;		// 库区id
	private String storageLocationName;		// 库区名称
	private Integer expirationDate;		// 保质期/月
	private Date manufactureDate;	//生产日期
	private String type;		// 类型(已报废,已过期待报废)
	private String createName;		// 创建者名称
	private String updateName;		// 更新者名称
	
	public BizDiscardedGoods() {
		super();
	}

	public BizDiscardedGoods(String id){
		super(id);
	}

	public String getGoodsId() {
		return goodsId;
	}

	public void setGoodsId(String goodsId) {
		this.goodsId = goodsId;
	}
	
	public String getGoodsCode() {
		return goodsCode;
	}

	public void setGoodsCode(String goodsCode) {
		this.goodsCode = goodsCode;
	}
	
	public String getGoodsName() {
		return goodsName;
	}

	public void setGoodsName(String goodsName) {
		this.goodsName = goodsName;
	}
	
	public String getBarcodeId() {
		return barcodeId;
	}

	public void setBarcodeId(String barcodeId) {
		this.barcodeId = barcodeId;
	}
	
	public String getBarcodeName() {
		return barcodeName;
	}

	public void setBarcodeName(String barcodeName) {
		this.barcodeName = barcodeName;
	}
	
	public String getBatch() {
		return batch;
	}

	public void setBatch(String batch) {
		this.batch = batch;
	}
	
	public Integer getNumber() {
		return number;
	}

	public void setNumber(Integer number) {
		this.number = number;
	}
	
	public String getSpecification() {
		return specification;
	}

	public void setSpecification(String specification) {
		this.specification = specification;
	}
	
	public String getStorageLocationId() {
		return storageLocationId;
	}

	public void setStorageLocationId(String storageLocationId) {
		this.storageLocationId = storageLocationId;
	}
	
	public String getStorageLocationName() {
		return storageLocationName;
	}

	public void setStorageLocationName(String storageLocationName) {
		this.storageLocationName = storageLocationName;
	}
	
	public Integer getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(Integer expirationDate) {
		this.expirationDate = expirationDate;
	}
	
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	public String getCreateName() {
		return createName;
	}

	public void setCreateName(String createName) {
		this.createName = createName;
	}
	
	public String getUpdateName() {
		return updateName;
	}

	public void setUpdateName(String updateName) {
		this.updateName = updateName;
	}

	public Date getManufactureDate() {
		return manufactureDate;
	}

	public void setManufactureDate(Date manufactureDate) {
		this.manufactureDate = manufactureDate;
	}
}