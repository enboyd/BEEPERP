/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.biz.entity;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

import com.thinkgem.jeesite.common.persistence.DataEntity;

import java.util.Date;

/**
 * 出库日志Entity
 * @author tiancheng
 * @version 2018-09-19
 */
@Data
public class BizBarcodeLog extends DataEntity<BizBarcodeLog> {
	
	private static final long serialVersionUID = 1L;
	private String orderId;		// 订单id
	private String goodsId;	// 商品id
	private String barcodeId;		// 条码id
	private String number;		// 出库数
	private Date beginOutDate;   //开始出库日期(用于查询)
	private Date endOutDate;   //结束出库日期(用于查询)
	
}