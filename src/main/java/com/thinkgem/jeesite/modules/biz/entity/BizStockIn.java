/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.biz.entity;

import org.hibernate.validator.constraints.Length;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.thinkgem.jeesite.modules.sys.entity.User;
import javax.validation.constraints.NotNull;

import com.thinkgem.jeesite.common.persistence.DataEntity;

/**
 * 入库单的ControllerEntity
 * @author tianCheng
 * @version 2018-07-16
 */
public class BizStockIn extends DataEntity<BizStockIn> {
	
	private static final long serialVersionUID = 1L;
	private String stockInId;		// 入库单ID
	private String stockInCode;		// 入库单编号
	private String purchaseId;		// 采购单的id
	private Date stockInDate;		// 入库日期

	private String status;  //入库单的状态（0，未分配托盘，1，已分配托盘）
	private int pageSize;		//条数
	private int pageNo;			//页码

	private List<BizStockInDetail> bizStockInDetailList;//入库单明细

	private String purchaseCode; //采购单的编号
	private Date stockInDateStart; // 入库日期开始时间
	private Date stockInDateEnd; // 入库日期结束时间

	public BizStockIn() {
		super();
	}

	public BizStockIn(String id){
		super(id);
	}

	@Length(min=1, max=20, message="入库单ID长度必须介于 1 和 20 之间")
	public String getStockInId() {
		return stockInId;
	}

	public void setStockInId(String stockInId) {
		this.stockInId = stockInId;
	}
	
	@Length(min=1, max=24, message="入库单编号长度必须介于 1 和 24 之间")
	public String getStockInCode() {
		return stockInCode;
	}

	public void setStockInCode(String stockInCode) {
		this.stockInCode = stockInCode;
	}
	
	@Length(min=0, max=32, message="采购单的id长度必须介于 0 和 32 之间")
	public String getPurchaseId() {
		return purchaseId;
	}

	public void setPurchaseId(String purchaseId) {
		this.purchaseId = purchaseId;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	public Date getStockInDate() {
		return stockInDate;
	}

	public void setStockInDate(Date stockInDate) {
		this.stockInDate = stockInDate;
	}

	public List<BizStockInDetail> getBizStockInDetailList() {
		return bizStockInDetailList;
	}

	public void setBizStockInDetailList(List<BizStockInDetail> bizStockInDetailList) {
		this.bizStockInDetailList = bizStockInDetailList;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public int getPageNo() {
		return pageNo;
	}

	public void setPageNo(int pageNo) {
		this.pageNo = pageNo;
	}

	public String getPurchaseCode() {
		return purchaseCode;
	}

	public void setPurchaseCode(String purchaseCode) {
		this.purchaseCode = purchaseCode;
	}

	public Date getStockInDateStart() {
		return stockInDateStart;
	}

	public void setStockInDateStart(Date stockInDateStart) {
		this.stockInDateStart = stockInDateStart;
	}

	public Date getStockInDateEnd() {
		return stockInDateEnd;
	}

	public void setStockInDateEnd(Date stockInDateEnd) {
		this.stockInDateEnd = stockInDateEnd;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
}