/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.biz.dao;

import com.thinkgem.jeesite.common.persistence.CrudDao;
import com.thinkgem.jeesite.common.persistence.annotation.MyBatisDao;
import com.thinkgem.jeesite.modules.biz.entity.BizPurchase;
import com.thinkgem.jeesite.modules.biz.entity.BizPurchaseDetail;
import com.thinkgem.jeesite.modules.biz.vo.BizPurchaseDetailVo;

import java.util.List;

/**
 * 采购列表单明细的controllerDAO接口
 * @author tiancheng
 * @version 2018-07-13
 */
@MyBatisDao
public interface BizPurchaseDetailDao extends CrudDao<BizPurchaseDetail> {

    void deleteByPurchaseId(String purchaseId);

    void deleteByPurchaseIdLJ(String purchaseId);

    void updateAmount(BizPurchaseDetail bizPurchaseDetail);

    Integer selectByPurchaseIdAndDifferentAmount(String purchaseId);

    List<BizPurchaseDetailVo> findListApi(BizPurchaseDetail bizPurchaseDetail);

    List<BizPurchaseDetail> findListByParent(BizPurchase bizPurchase);

    Integer selectCountByPurchaseId(String purchaseId);

    List<String> selectByParIdGroupBySuppiler(String purchaseId);
}