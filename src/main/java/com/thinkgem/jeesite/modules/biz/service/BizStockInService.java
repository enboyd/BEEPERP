/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.biz.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

import com.thinkgem.jeesite.common.utils.IdGen;
import com.thinkgem.jeesite.common.utils.SendOaNotifyUtil;
import com.thinkgem.jeesite.modules.biz.dao.BizOrderdetailDao;
import com.thinkgem.jeesite.modules.biz.dao.BizStockInDetailDao;
import com.thinkgem.jeesite.modules.biz.entity.*;
import com.thinkgem.jeesite.modules.biz.form.*;
import com.thinkgem.jeesite.modules.biz.vo.BizStockInDetailVo;
import com.thinkgem.jeesite.modules.biz.vo.BizStockInVo;
import com.thinkgem.jeesite.modules.oa.entity.OaNotify;
import com.thinkgem.jeesite.modules.oa.service.OaNotifyService;
import com.thinkgem.jeesite.modules.sys.entity.User;
import com.thinkgem.jeesite.modules.sys.utils.AutoNameUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.service.CrudService;
import com.thinkgem.jeesite.modules.biz.dao.BizStockInDao;

/**
 * 入库单的ControllerService
 * @author tianCheng
 * @version 2018-07-16
 */
@Service
@Transactional(readOnly = true)
public class BizStockInService extends CrudService<BizStockInDao, BizStockIn> {

	@Autowired
	private BizGoodsService bizGoodsService;
	@Autowired
	private BizPurchaseDetailService bizPurchaseDetailService;
	@Autowired
	private BizPurchaseService bizPurchaseService;
	@Autowired
	private BizStockInDetailDao bizStockInDetailDao;
	@Autowired
	private BizStockInDao bizStockInDao;
	@Autowired
	private BizOrderdetailDao bizOrderdetailDao;
	@Autowired
	private OaNotifyService oaNotifyService;

	public BizStockIn get(String id) {
		return super.get(id);
	}
	
	public List<BizStockIn> findList(BizStockIn bizStockIn) {
		return super.findList(bizStockIn);
	}
	
	public Page<BizStockIn> findPage(Page<BizStockIn> page, BizStockIn bizStockIn) {
		return super.findPage(page, bizStockIn);
	}
	
	@Transactional(readOnly = false)
	public void save(BizStockIn bizStockIn) {

		super.save(bizStockIn);
	}

	@Transactional(readOnly = false)
	public void updateStatus(BizStockIn bizStockIn) {
		bizStockInDao.updateStatus(bizStockIn);
	}

	@Transactional(readOnly = false)
	public void delete(BizStockIn bizStockIn) {
		super.delete(bizStockIn);
	}


	 /**
	  * 手机端添加入库单
	  * @author tiancheng
	  * @date 2018/8/28 16:00
	  * @param
	  * @return
	  */
	@Transactional(readOnly = false)
	public void saveApi(BizStockInSaveForm bizStockInSaveForm) {
		//添加入库单
		BizStockIn bizStockIn = new BizStockIn();
		String stockInId = IdGen.uuid(); // 入库单的id
		String code = AutoNameUtils.createAutoNameByTableName("biz_stock_in");
		bizStockIn.setStockInCode(code);
		bizStockIn.setStockInId(stockInId);
		bizStockIn.setPurchaseId(bizStockInSaveForm.getPurchaseId());
		bizStockIn.setStockInDate(new Date());
		User user = new User();
		user.setId(bizStockInSaveForm.getCreateById());
		user.setName(bizStockInSaveForm.getCreateByName());
		bizStockIn.setStatus("0");
		bizStockIn.setCreateBy(user);
		bizStockIn.setUpdateBy(user);
		bizStockIn.setCreateDate(new Date());
		bizStockIn.setUpdateDate(new Date());
		bizStockIn.setDelFlag("0");
		//添加入库单
		bizStockInDao.insert(bizStockIn);

		//定义一个开光，判断商品是否全部到货
		boolean a = true;
		List<BizStockInDetailForm> bizStockInDetailFormList = bizStockInSaveForm.getBizStockInDetailFormList();
		//待出库的商品
		List<String> orderNameLists = new ArrayList<>();

		if(null != bizStockInDetailFormList){
			for (BizStockInDetailForm bizStockInDetailForm:bizStockInDetailFormList) {
				//如果实际入库数不为空
				if(null != bizStockInDetailForm.getActualAmount()){
					//采购单的明细
					BizStockInDetail bizStockInDetail = new BizStockInDetail();
					BeanUtils.copyProperties(bizStockInDetailForm, bizStockInDetail);

					//修改商品的库存,可用库存和采购中的数量
					BizGoods goods = bizGoodsService.get(bizStockInDetail.getGoodsId());
					Integer num = goods.getStockAmount() + Integer.valueOf(bizStockInDetail.getActualAmount());
					Integer sureStockAmount = goods.getSureStockAmount() + Integer.valueOf(bizStockInDetail.getActualAmount());
					Integer purchasingAmount = Integer.valueOf(goods.getPurchasingAmount())-Integer.valueOf(bizStockInDetail.getActualAmount());

					goods.setStockAmount(num);//库存数量
					goods.setSureStockAmount(sureStockAmount);//可用库存数量
					goods.setPurchasingAmount(purchasingAmount.toString());//采购中数量

					bizGoodsService.updateInfo(goods);
					//查询出库单中是否存在数量不够的订单
					BizOrderdetail  bizOrderdetail = new BizOrderdetail();
					bizOrderdetail.setProjectdetailId(goods.getGoodsId());
					List<String> orderNameList = bizOrderdetailDao.findListByIsDeliver(bizOrderdetail);
					orderNameLists.addAll(orderNameList);

					//修改采购单明细
					BizPurchaseDetail bizPurchaseDetailAll = bizPurchaseDetailService.get(bizStockInDetail.getPurchaseDetailId());
					//采购单明细相差数量
					Integer differentAmount = Integer.valueOf(bizPurchaseDetailAll.getDifferentAmount()) - Integer.valueOf(bizStockInDetail.getActualAmount());
					bizPurchaseDetailAll.setDifferentAmount(differentAmount.toString());
					//bizStockInDetail.getActualAmount() 这是入库单的实际入库数
					Integer sum = Integer.valueOf(bizPurchaseDetailAll.getActualAmount()) + Integer.valueOf(bizStockInDetail.getActualAmount());
					bizPurchaseDetailAll.setActualAmount(sum.toString());
					bizPurchaseDetailService.updateAmount(bizPurchaseDetailAll);
					//如果相差数量大于0就是部分到货
					if(differentAmount > 0){
						a = false;
						bizStockInDetail.setStatus("0");
					}else{
						bizStockInDetail.setStatus("1");
					}

					//添加入库单明细
					bizStockInDetail.setStockInId(stockInId);
					bizStockInDetail.setStockInDetailId(IdGen.uuid());
					bizStockInDetail.setManufactureDate(bizStockInDetailForm.getManufactureDate());
					//bizStockInDetail.setExpirationDate(bizStockInDetailForm.getExpirationDate());
					bizStockInDetail.setStorageLocationId(bizStockInDetail.getStorageLocationId());//仓库库位的id
					bizStockInDetail.setPurchaseDetailId(bizStockInDetail.getPurchaseDetailId());
					bizStockInDetail.setGoodsId(bizPurchaseDetailAll.getGoodsId());//采购单中的商品id
					bizStockInDetail.setWaitDistributionAmount(bizStockInDetail.getActualAmount());//待分配托盘数
					bizStockInDetail.setActualAmount(bizStockInDetail.getActualAmount());
					bizStockInDetail.setCreateBy(user);
					bizStockInDetail.setUpdateBy(user);
					bizStockInDetail.setCreateDate(new Date());
					bizStockInDetail.setUpdateDate(new Date());
					//设置入库明细单价
					if(bizPurchaseDetailAll.getUnitPrice() != null){
						bizStockInDetail.setUnitPrice(Double.valueOf(bizPurchaseDetailAll.getUnitPrice()));
					}
					bizStockInDetailDao.insert(bizStockInDetail);
				}
			}
			//修改采购单的状态
			BizPurchase bizPurchase = new BizPurchase();
			bizPurchase.setPurchaseId(bizStockInSaveForm.getPurchaseId());
			//查询采购单中相差数不为0的条数
			Integer num = bizPurchaseDetailService.selectCountByPurchaseId(bizStockInSaveForm.getPurchaseId());
			//修改采购单的状态 (11,部分入库,  12,已完成)
			if(num > 0){
				bizPurchase.setStatus("11");
			}else{
				bizPurchase.setStatus("12");
				bizPurchase.setFinancialStatus("10");
			}
			bizPurchaseService.updateStatus(bizPurchase);
		}
		//判断是否有数量不够的订单
		if(orderNameLists.size() > 0){
			HashSet h = new HashSet(orderNameLists);
			orderNameLists.clear();
			orderNameLists.addAll(h);
			StringBuffer stringBuffer = new StringBuffer();
			stringBuffer.append("销售出库单【");
			for (String orderName:orderNameLists) {
				stringBuffer.append(orderName+",");
			}
			stringBuffer.substring(0,stringBuffer.length()-1);
			stringBuffer.append("】可出库，请及时查看！");

			String content = stringBuffer.toString();
			String title = "商品到货";
			OaNotify oaNotify = SendOaNotifyUtil.findOaNotifyDan("4", "1",content,title);
			// 保存通知记录，发送通知
			oaNotifyService.save(oaNotify);
		}

	}


	 /**
	  * 手机端获取入库单的列表
	  * @author tiancheng
	  * @date 2018/8/29 9:37
	  * @param
	  * @return
	  */
	public List<BizStockInVo> findListApi(BizStockInListForm bizStockInListForm){
		BizStockIn bizStockIn = new BizStockIn();
		bizStockIn.setPageSize(bizStockInListForm.getPageSize());
		bizStockIn.setPageNo((bizStockInListForm.getPageNo()-1)*bizStockInListForm.getPageSize());
		return bizStockInDao.findListApi(bizStockIn);
	}


	 /**
	  * 手机端获取入库单的详情
	  * @author tiancheng  
	  * @date 2018/8/29 10:10
	  * @param
	  * @return   
	  */  
	public BizStockInVo getApi(BizStockInGetForm bizStockInGetForm){
		//入库单的详情
		BizStockInVo bizStockInVo = bizStockInDao.getApi(bizStockInGetForm.getStockInId());

		//查询入库单的明细
		BizStockInDetail bizStockInDetail = new BizStockInDetail();
		bizStockInDetail.setStockInId(bizStockInVo.getStockInId());
		List<BizStockInDetailVo> bizStockInDetailVoList = bizStockInDetailDao.findListApi(bizStockInDetail);
		bizStockInVo.setBizStockInDetailList(bizStockInDetailVoList);

		return bizStockInVo;
	}

}