/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.biz.form;


import lombok.Data;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * 订单Entity
 * @author tiancheng
 * @version 2018-09-17
 */
@Data
public class BizOrderGetForm {

	@NotEmpty(message = "出库的编号必填")
	private String name;
}