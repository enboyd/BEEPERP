/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.biz.service;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.thinkgem.jeesite.common.config.Global;
import com.thinkgem.jeesite.common.utils.BarcodeUtil;
import com.thinkgem.jeesite.common.utils.DateUtils;
import com.thinkgem.jeesite.common.utils.FileUtils;
import com.thinkgem.jeesite.common.utils.IdGen;
import com.thinkgem.jeesite.modules.biz.dao.BizGoodsDao;
import com.thinkgem.jeesite.modules.biz.dao.BizOrderDao;
import com.thinkgem.jeesite.modules.biz.entity.BizGoods;
import com.thinkgem.jeesite.modules.biz.entity.BizOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.service.CrudService;
import com.thinkgem.jeesite.modules.biz.entity.BizOrderdetail;
import com.thinkgem.jeesite.modules.biz.dao.BizOrderdetailDao;

/**
 * 订单Service
 * @author tiancheng
 * @version 2018-09-17
 */
@Service
@Transactional(readOnly = true)
public class BizOrderdetailService extends CrudService<BizOrderdetailDao, BizOrderdetail> {

	@Autowired
	private BizOrderService bizOrderService;
	@Autowired
	private BizGoodsDao bizGoodsDao;

	public BizOrderdetail get(String id) {
		return super.get(id);
	}
	
	public List<BizOrderdetail> findList(BizOrderdetail bizOrderdetail) {
		return super.findList(bizOrderdetail);
	}

	public Page<BizOrderdetail> findPage(Page<BizOrderdetail> page, BizOrderdetail bizOrderdetail) {
		return super.findPage(page, bizOrderdetail);
	}
	
	@Transactional(readOnly = false)
	public void save(BizOrderdetail bizOrderdetail) {
		super.save(bizOrderdetail);
	}
	
	@Transactional(readOnly = false)
	public void delete(BizOrderdetail bizOrderdetail) {
		super.delete(bizOrderdetail);
	}


	 /**
	  * 领取物料的详情
	  * @author tiancheng
	  * @date 2018/12/4 14:44
	  * @param
	  * @return
	  */
	public List<BizOrderdetail> findListReceive(BizOrderdetail  bizOrderdetail){
		return dao.findListReceive(bizOrderdetail);
	}


	 /**
	  * 物料领取的保存
	  * @author tiancheng
	  * @date 2018/12/4 14:01
	  * @param
	  * @return
	  */
	 @Transactional(readOnly = false)
	public void receiveSave(BizOrder bizOrder){
		 //判断是添加还是修改
		 if(null != bizOrder && (bizOrder.getId().equals("") || null == bizOrder.getId())){
			 SimpleDateFormat si = new SimpleDateFormat("yyyyMMddHHmmss");
			 //物料领取的标识
			 bizOrder.setDelFlag("2");
			 bizOrder.setOrderGenre("70");
			 bizOrder.setOrderStatus("0");
			 bizOrder.setDeliverType("0");
			 bizOrder.setOrderApprovalStatus("10");
			 bizOrder.setIsCanOut("1");
			 bizOrder.setTaskState("0");
			 //生成条码
			 String name = "DHD"+si.format(new Date())+(10 + (int)(Math.random()*90));
			 bizOrder.setName(name);
			 //生成条形码的地址
			 String txmPath = exportDir()+name+".jpg";
			 BarcodeUtil.createBarcode(name, txmPath);
			 //修改出库单的条码路径
			 if(txmPath.indexOf("userfiles") != -1){
				 String[] paths = txmPath.split("userfiles");
				 bizOrder.setTmUrl("/userfiles"+paths[1]);
			 }
			 //添加订货单
			 bizOrderService.save(bizOrder);
			 //添加商品的明细
			 for (BizOrderdetail bizOrderdetail:bizOrder.getBizOrderDetailList()) {
				 if(bizOrderdetail.getQuantity() > 0 && !bizOrderdetail.getDelFlag().equals("1")){
					 // 扣库存
					 BizGoods bizGoods = bizGoodsDao.get(bizOrderdetail.getProjectdetailId());
					 //修改可用库存数和代发货数
					 bizGoods.setSureStockAmount(bizGoods.getSureStockAmount() - bizOrderdetail.getQuantity());
					 bizGoods.setWaitStockAmount(bizGoods.getWaitStockAmount() + bizOrderdetail.getQuantity());
					 //刷新库存信息
					 bizGoodsDao.updateInfo(bizGoods);

					 bizOrderdetail.setOutboundOrderPid(bizOrder.getId());
					 bizOrderdetail.setIsDeliver("0");
					 bizOrderdetail.setSurplusQuantity(bizOrderdetail.getQuantity());
					 save(bizOrderdetail);
				 }

			 }
		 }else{
			 //修改订货单
			 bizOrderService.save(bizOrder);
			 //获取以前的领取物料的明细
			 BizOrderdetail bizOrderdetail = new BizOrderdetail();
			 bizOrderdetail.setOutboundOrderPid(bizOrder.getId());
			 List<BizOrderdetail> bizOrderdetailList = dao.findList(bizOrderdetail);
			 for (BizOrderdetail orderdetail:bizOrderdetailList) {
				 // 库存回填
				 BizGoods bizGoods = bizGoodsDao.get(orderdetail.getProjectdetailId());
				 //修改可用库存数和代发货数
				 bizGoods.setSureStockAmount(bizGoods.getSureStockAmount() + orderdetail.getQuantity());
				 bizGoods.setWaitStockAmount(bizGoods.getWaitStockAmount() - orderdetail.getQuantity());
				 //刷新库存信息
				 bizGoodsDao.updateInfo(bizGoods);
			 }
			 //删除以前的订单明细
			 dao.deleteByOrderId(bizOrder.getId());
			 //重新添加库存
			 for (BizOrderdetail bizOrderdetailNew:bizOrder.getBizOrderDetailList()) {
				 if(bizOrderdetailNew.getQuantity() > 0 && !bizOrderdetailNew.getDelFlag().equals("1")){
					 // 扣库存
					 BizGoods bizGoods = bizGoodsDao.get(bizOrderdetailNew.getProjectdetailId());
					 //修改可用库存数和代发货数
					 bizGoods.setSureStockAmount(bizGoods.getSureStockAmount() - bizOrderdetailNew.getQuantity());
					 bizGoods.setWaitStockAmount(bizGoods.getWaitStockAmount() + bizOrderdetailNew.getQuantity());
					 //刷新库存信息
					 bizGoodsDao.updateInfo(bizGoods);

					 bizOrderdetailNew.setOutboundOrderPid(bizOrder.getId());
					 bizOrderdetailNew.setIsDeliver("0");
					 bizOrderdetailNew.setSurplusQuantity(bizOrderdetail.getQuantity());
					 save(bizOrderdetailNew);
				 }

			 }
		 }

	}


	/**
	 * 生成条形码的路径
	 * @author tiancheng
	 * @date 2018/9/5 14:02
	 * @param
	 * @return
	 */
	public String exportDir(){
		//默认传到系统管理员的明下
		String exportDir =  FileUtils.path(Global.getUserfilesBaseDir() + Global.USERFILES_BASE_URL + "1/files/exportFiles/" + DateUtils.getDate("yyyy-MM-dd") + "/");
		if (!new File(exportDir).exists()) {
			new File(exportDir).mkdirs();
		}
		return exportDir;
	}
}