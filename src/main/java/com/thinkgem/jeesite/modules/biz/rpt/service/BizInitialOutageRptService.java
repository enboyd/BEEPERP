package com.thinkgem.jeesite.modules.biz.rpt.service;

import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.modules.biz.entity.BizBarcode;
import com.thinkgem.jeesite.modules.biz.entity.BizStockLog;
import com.thinkgem.jeesite.modules.biz.rpt.dao.BizInitialOutageRptDao;
import com.thinkgem.jeesite.modules.biz.rpt.entity.BizInitialOutage;
import com.thinkgem.jeesite.modules.biz.service.BizBarcodeService;
import com.thinkgem.jeesite.modules.biz.service.BizStockLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author : xuchen
 * @date : 2018/10/22 9:13
 */
@Service
@Transactional(readOnly = true)
public class BizInitialOutageRptService {

    @Autowired
    private BizInitialOutageRptDao bizInitialStorageDao;

    public Page<BizInitialOutage> findPage(Page<BizInitialOutage> page, BizInitialOutage bizInitialOutage) {
        bizInitialOutage.setPage(page);
        List<BizInitialOutage> bizInitialOutageList = bizInitialStorageDao.findList(bizInitialOutage);
        page.setList(bizInitialOutageList);
        return page;
    }

    public List<BizInitialOutage> findAllListByDate(Date beginOutDate, Date endOutDate,String goodsCode,String classifyId) {
        return bizInitialStorageDao.findAllListByDate(beginOutDate,endOutDate,goodsCode,classifyId);
    }
}
