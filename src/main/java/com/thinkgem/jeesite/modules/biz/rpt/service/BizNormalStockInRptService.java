package com.thinkgem.jeesite.modules.biz.rpt.service;

import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.service.CrudService;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.modules.biz.entity.*;
import com.thinkgem.jeesite.modules.biz.rpt.dao.BizNormalStockInRptDao;
import com.thinkgem.jeesite.modules.biz.rpt.entity.BizNormalStockInRpt;
import com.thinkgem.jeesite.modules.biz.service.*;
import com.thinkgem.jeesite.modules.sys.utils.UserUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author : xuchen
 * @date : 2018/10/22 9:13
 */
@Service
@Transactional(readOnly = true)
public class BizNormalStockInRptService  extends CrudService<BizNormalStockInRptDao, BizNormalStockInRpt> {

    @Autowired
    private BizNormalStockInRptDao bizNormalStockInRptDao;

    public Page<BizNormalStockInRpt> findPage(Page<BizNormalStockInRpt> page, BizNormalStockInRpt bizNormalStockIn) {
        bizNormalStockIn.setPage(page);
        List<BizNormalStockInRpt> normalStockInList = bizNormalStockInRptDao.findList(bizNormalStockIn);
        page.setList(normalStockInList);
        return page;
    }

    public List<BizNormalStockInRpt> findAllListByDate(Date stockInDateStart, Date stockInDateEnd,String goodsCode,String classifyId) {
        return bizNormalStockInRptDao.findAllListByDate(stockInDateStart,stockInDateEnd,goodsCode,classifyId);
    }
}
