package com.thinkgem.jeesite.modules.biz.vo;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * Description:
 * Author: xuchen
 * Create Time: 2018-10-12
 */
@Data
public class BizGoodsClassifyChildVO implements Serializable {
    private String id;
    private String name;
}
