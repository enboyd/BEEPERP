/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.biz.web;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.thinkgem.jeesite.modules.biz.dao.BizStockInDetailDao;
import com.thinkgem.jeesite.modules.biz.entity.BizBarcode;
import com.thinkgem.jeesite.modules.biz.service.BizBarcodeService;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.thinkgem.jeesite.common.config.Global;
import com.thinkgem.jeesite.common.web.BaseController;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.modules.biz.entity.BizStorageLocation;
import com.thinkgem.jeesite.modules.biz.service.BizStorageLocationService;

/**
 * 库位管理Controller
 * @author xuchen
 * @version 2018-07-17
 */
@Controller
@RequestMapping(value = "${adminPath}/biz/bizStorageLocation")
public class BizStorageLocationController extends BaseController {

	@Autowired
	private BizStorageLocationService bizStorageLocationService;
	@Autowired
	private BizBarcodeService bizBarcodeService;
	@Autowired
	private BizStockInDetailDao bizStockInDetailDao;
	
	@ModelAttribute
	public BizStorageLocation get(@RequestParam(required=false) String id) {
		BizStorageLocation entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = bizStorageLocationService.get(id);
		}
		if (entity == null){
			entity = new BizStorageLocation();
		}
		return entity;
	}
	
	@RequiresPermissions("biz:bizStorageLocation:view")
	@RequestMapping(value = {"list", ""})
	public String list(BizStorageLocation bizStorageLocation, HttpServletRequest request, HttpServletResponse response, Model model) {
		List<BizStorageLocation> list = bizStorageLocationService.findList(bizStorageLocation); 
		model.addAttribute("list", list);
		return "modules/biz/bizStorageLocationList";
	}

	@RequiresPermissions("biz:bizStorageLocation:view")
	@RequestMapping(value = "form")
	public String form(BizStorageLocation bizStorageLocation, Model model) {
		if (bizStorageLocation.getParent()!=null && StringUtils.isNotBlank(bizStorageLocation.getParent().getId())){
			bizStorageLocation.setParent(bizStorageLocationService.get(bizStorageLocation.getParent().getId()));
			// 获取排序号，最末节点排序号+30
			if (StringUtils.isBlank(bizStorageLocation.getId())){
				BizStorageLocation bizStorageLocationChild = new BizStorageLocation();
				bizStorageLocationChild.setParent(new BizStorageLocation(bizStorageLocation.getParent().getId()));
				List<BizStorageLocation> list = bizStorageLocationService.findList(bizStorageLocation); 
				if (list.size() > 0){
					bizStorageLocation.setSort(list.get(list.size()-1).getSort());
					if (bizStorageLocation.getSort() != null){
						bizStorageLocation.setSort(bizStorageLocation.getSort() + 30);
					}
				}
			}
		}
		if (bizStorageLocation.getSort() == null){
			bizStorageLocation.setSort(30);
		}
		model.addAttribute("bizStorageLocation", bizStorageLocation);
		return "modules/biz/bizStorageLocationForm";
	}

	@RequiresPermissions(value={"biz:bizStorageLocation:edit","biz:bizStorageLocation:add"},logical= Logical.OR)
	@RequestMapping(value = "save")
	public String save(BizStorageLocation bizStorageLocation, Model model, RedirectAttributes redirectAttributes) {
		if (!beanValidator(model, bizStorageLocation)){
			return form(bizStorageLocation, model);
		}
		bizStorageLocationService.save(bizStorageLocation);
		addMessage(redirectAttributes, "保存库位管理成功");
		return "redirect:"+Global.getAdminPath()+"/biz/bizStorageLocation/?repage";
	}
	
	@RequiresPermissions("biz:bizStorageLocation:remove")
	@RequestMapping(value = "delete")
	public String delete(BizStorageLocation bizStorageLocation, RedirectAttributes redirectAttributes) {
		//判断下面是否有子集的库位
		BizStorageLocation storageLocation = new BizStorageLocation();
		BizStorageLocation parent = new BizStorageLocation();
		parent.setId(bizStorageLocation.getId());
		storageLocation.setParent(parent);
		List<BizStorageLocation> bizStorageLocationList = bizStorageLocationService.findList(storageLocation);
		if(bizStorageLocationList.size() > 0){
			addMessage(redirectAttributes, "该库位下面有子库位，请进行确认！");
			return "redirect:"+Global.getAdminPath()+"/biz/bizStorageLocation/?repage";
		}

		//判断该库位下面有没有条码
		BizBarcode bizBarcode = new BizBarcode();
		bizBarcode.setStorageLocationId(bizStorageLocation.getId());
		List<BizBarcode> bizBarcodeList = bizBarcodeService.findList(bizBarcode);
		if(bizBarcodeList.size() > 0){
			addMessage(redirectAttributes, "该库位下面有未出库的商品！");
			return "redirect:"+Global.getAdminPath()+"/biz/bizStorageLocation/?repage";
		}
		Integer num = bizStockInDetailDao.selectCountStorageLocationId(bizStorageLocation.getId());
		if(num > 0){
			addMessage(redirectAttributes, "该库位下面有未分配托盘的入库单！");
			return "redirect:"+Global.getAdminPath()+"/biz/bizStorageLocation/?repage";
		}
		bizStorageLocationService.delete(bizStorageLocation);
		return "redirect:"+Global.getAdminPath()+"/biz/bizStorageLocation/?repage";
	}

	@RequiresPermissions("user")
	@ResponseBody
	@RequestMapping(value = "treeData")
	public List<Map<String, Object>> treeData(@RequestParam(required=false) String extId, HttpServletResponse response) {
		List<Map<String, Object>> mapList = Lists.newArrayList();
		List<BizStorageLocation> list = bizStorageLocationService.findList(new BizStorageLocation());
		for (int i=0; i<list.size(); i++){
			BizStorageLocation e = list.get(i);
			if (StringUtils.isBlank(extId) || (extId!=null && !extId.equals(e.getId()) && e.getParentIds().indexOf(","+extId+",")==-1)){
				Map<String, Object> map = Maps.newHashMap();
				map.put("id", e.getId());
				map.put("pId", e.getParentId());
				map.put("name", e.getName());
				mapList.add(map);
			}
		}
		return mapList;
	}
	
}