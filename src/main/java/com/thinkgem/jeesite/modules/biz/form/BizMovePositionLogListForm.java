/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.biz.form;

import lombok.Data;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * 移库Entity
 * @author tiancheng
 * @version 2018-09-12
 */
@Data
public class BizMovePositionLogListForm {

    private int pageSize;		//条数
    private int pageNo;			//页码
}