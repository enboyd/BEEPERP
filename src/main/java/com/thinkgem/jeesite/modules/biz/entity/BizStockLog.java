/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.biz.entity;

import org.hibernate.validator.constraints.Length;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;

import com.thinkgem.jeesite.common.persistence.DataEntity;

/**
 * 期出期入的logEntity
 * @author tiancheng
 * @version 2018-09-13
 */
public class BizStockLog extends DataEntity<BizStockLog> {
	
	private static final long serialVersionUID = 1L;
	private String name; //编号
	private String goodsId;		// 商品id
	private String storageLocationId;		// 库位id
	private String number;		// 数量
	private String stockAmountFront;		// 操作前库存
	private String stockAmountAfter;		// 操作后库存
	private String type;		// 类型（0，期初，1，期入，2，正常发货）
	private String barcodeId;  //条码的id

	private int pageSize;		//条数
	private int pageNo;			//页码
	private String isTm; //是否生成条码（0，不生成，1，生成）

	private String storageLocationName;  // 库位的名称
	private String goodsName;   // 商品的名称
	private String goodsCode;   // 商品的编号
	private String createByName;   // 创建人名称
	private Date beginCreateDate;	//创建时间--开始
	private Date endCreateDate;		//创建时间--结束

	public BizStockLog() {
		super();
	}

	public BizStockLog(String id){
		super(id);
	}

	@Length(min=0, max=32, message="商品id长度必须介于 0 和 32 之间")
	public String getGoodsId() {
		return goodsId;
	}

	public void setGoodsId(String goodsId) {
		this.goodsId = goodsId;
	}
	
	@Length(min=0, max=32, message="库位id长度必须介于 0 和 32 之间")
	public String getStorageLocationId() {
		return storageLocationId;
	}

	public void setStorageLocationId(String storageLocationId) {
		this.storageLocationId = storageLocationId;
	}
	
	@Length(min=0, max=11, message="数量长度必须介于 0 和 11 之间")
	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}
	
	@Length(min=0, max=10, message="操作前库存长度必须介于 0 和 10 之间")
	public String getStockAmountFront() {
		return stockAmountFront;
	}

	public void setStockAmountFront(String stockAmountFront) {
		this.stockAmountFront = stockAmountFront;
	}
	
	@Length(min=0, max=10, message="操作后库存长度必须介于 0 和 10 之间")
	public String getStockAmountAfter() {
		return stockAmountAfter;
	}

	public void setStockAmountAfter(String stockAmountAfter) {
		this.stockAmountAfter = stockAmountAfter;
	}
	
	@Length(min=0, max=1, message="类型（0，期初，1，期入，2，正常发货）长度必须介于 0 和 1 之间")
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public int getPageNo() {
		return pageNo;
	}

	public void setPageNo(int pageNo) {
		this.pageNo = pageNo;
	}

	public String getStorageLocationName() {
		return storageLocationName;
	}

	public void setStorageLocationName(String storageLocationName) {
		this.storageLocationName = storageLocationName;
	}

	public String getGoodsName() {
		return goodsName;
	}

	public void setGoodsName(String goodsName) {
		this.goodsName = goodsName;
	}

	public String getGoodsCode() {
		return goodsCode;
	}

	public void setGoodsCode(String goodsCode) {
		this.goodsCode = goodsCode;
	}

	public String getCreateByName() {
		return createByName;
	}

	public void setCreateByName(String createByName) {
		this.createByName = createByName;
	}

	public String getIsTm() {
		return isTm;
	}

	public void setIsTm(String isTm) {
		this.isTm = isTm;
	}

	public String getBarcodeId() {
		return barcodeId;
	}

	public void setBarcodeId(String barcodeId) {
		this.barcodeId = barcodeId;
	}

	public Date getBeginCreateDate() {
		return beginCreateDate;
	}

	public void setBeginCreateDate(Date beginCreateDate) {
		this.beginCreateDate = beginCreateDate;
	}

	public Date getEndCreateDate() {
		return endCreateDate;
	}

	public void setEndCreateDate(Date endCreateDate) {
		this.endCreateDate = endCreateDate;
	}
}