/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.biz.form;

import com.thinkgem.jeesite.common.annotation.FieldName;
import com.thinkgem.jeesite.modules.sys.entity.User;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 订单Entity
 * @author tiancheng
 * @version 2018-09-17
 */
@Data
public class BizOrderdetailForm{

	private String id;		// 编码
	private String name;		// 编码
	private String orderId;		// 所属订单
	private String materielId;		// 物料
	private Double unitPrice;		// 单价
	private Integer quantity;		// 数量
	private Double amountOfMoney;		// 金额
	private String materielName;		// 物料name
	private String projectdetailId;		// 项目明细id
	private String projectdetailName;		// 项目明细name
	private String outboundOrderPid;		// 所属订货单ID
	private String code;		// code
	private BigDecimal discount;		// 折扣
	private String payType;		// 结算方式
	private String type;  // 类型， M，冷链  P，酱料  E，设备  S，收音机
	private String isDeliver; // 是否足够发货

	private String specifications; //规格
	private String createBy;	// 创建者
	private String createByName;	// 创建者
	private Date createDate;	// 创建日期
	private String updateBy;	// 更新者
	private String updateByName;	// 更新者
	private Date updateDate;	// 更新日期
	private String remarks;	//备注
}