/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.biz.entity;

import org.hibernate.validator.constraints.Length;
import com.fasterxml.jackson.annotation.JsonBackReference;

import com.thinkgem.jeesite.common.persistence.TreeEntity;

/**
 * 产品类型Entity
 * @author xuchen
 * @version 2018-08-28
 */
public class BizGoodsCategory extends TreeEntity<BizGoodsCategory> {
	
	private static final long serialVersionUID = 1L;
	private String name;		// 名称
	private BizGoodsCategory parent;		// 父级编号
	private String parentIds;		// 所有父级编码
	private Integer sort;		// 排序
	private String isenable;		// 是否启用
	
	public BizGoodsCategory() {
		super();
	}

	public BizGoodsCategory(String id){
		super(id);
	}

	@Length(min=0, max=100, message="名称长度必须介于 0 和 100 之间")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	@JsonBackReference
	public BizGoodsCategory getParent() {
		return parent;
	}

	public void setParent(BizGoodsCategory parent) {
		this.parent = parent;
	}
	
	@Length(min=0, max=2000, message="所有父级编码长度必须介于 0 和 2000 之间")
	public String getParentIds() {
		return parentIds;
	}

	public void setParentIds(String parentIds) {
		this.parentIds = parentIds;
	}
	
	public Integer getSort() {
		return sort;
	}

	public void setSort(Integer sort) {
		this.sort = sort;
	}
	
	@Length(min=0, max=1, message="是否启用长度必须介于 0 和 1 之间")
	public String getIsenable() {
		return isenable;
	}

	public void setIsenable(String isenable) {
		this.isenable = isenable;
	}
	
	public String getParentId() {
		return parent != null && parent.getId() != null ? parent.getId() : "0";
	}
}