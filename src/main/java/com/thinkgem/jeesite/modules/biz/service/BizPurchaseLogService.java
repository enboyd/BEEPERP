/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.biz.service;

import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.service.CrudService;
import com.thinkgem.jeesite.modules.biz.dao.BizPurchaseLogDao;
import com.thinkgem.jeesite.modules.biz.entity.BizPurchaseLog;
import com.thinkgem.jeesite.modules.sys.dao.DictDao;
import com.thinkgem.jeesite.modules.sys.entity.Dict;
import com.thinkgem.jeesite.modules.sys.entity.User;
import com.thinkgem.jeesite.modules.sys.utils.UserUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 采购单日志的ControllerService
 * @author tiancheng
 * @version 2018-07-17
 */
@Service
@Transactional(readOnly = true)
public class BizPurchaseLogService extends CrudService<BizPurchaseLogDao, BizPurchaseLog> {

	@Autowired
	private DictDao dictDao;

	public BizPurchaseLog get(String id) {
		return super.get(id);
	}
	
	public List<BizPurchaseLog> findList(BizPurchaseLog bizPurchaseLog) {
		return super.findList(bizPurchaseLog);
	}
	
	public Page<BizPurchaseLog> findPage(Page<BizPurchaseLog> page, BizPurchaseLog bizPurchaseLog) {
		return super.findPage(page, bizPurchaseLog);
	}
	
	@Transactional(readOnly = false)
	public void save(BizPurchaseLog bizPurchaseLog) {
		super.save(bizPurchaseLog);
	}
	
	@Transactional(readOnly = false)
	public void delete(BizPurchaseLog bizPurchaseLog) {
		super.delete(bizPurchaseLog);
	}


	 /**
	  * 添加采购单的日志
	  * @author tiancheng
	  * @date 2018/7/17 10:27
	  * @param type 操作
	  * @param purchaseId 采购单的id
	  * @return
	  */
	@Transactional(readOnly = false)
	public void addBizPurchaseLog(String type,String purchaseId){
		if(StringUtils.isNotEmpty(type) && StringUtils.isNotEmpty(purchaseId)){
			BizPurchaseLog bizPurchaseLog = new BizPurchaseLog();
			bizPurchaseLog.setPurchaseId(purchaseId);
			bizPurchaseLog.setType(type);
			super.save(bizPurchaseLog);
		}
	}
}