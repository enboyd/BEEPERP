/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.biz.entity;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

import com.thinkgem.jeesite.common.persistence.DataEntity;

/**
 * 退换货明细Entity
 * @author xuchen
 * @version 2018-09-11
 */
@Data
public class SmReturnBarterDetail extends DataEntity<SmReturnBarterDetail> {
	
	private static final long serialVersionUID = 1L;
	private String returnBarterId;		// 退换货单id
	private String goodsId;		// 商品id
	private String goodsCode;		// 商品编号
	private String goodsName;		// 商品名称
	private String specifications;		// 规格
	private Integer quantity;		// 数量
	private String type;		// 退货\换货
	private Double unitPrice;	//单价
	private Integer stockAmount;	//库存
	private String isDeliver;  // 是否发货（0，立即发货，1，等待发货）
}