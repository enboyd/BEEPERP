/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.biz.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.service.CrudService;
import com.thinkgem.jeesite.modules.biz.entity.BizApk;
import com.thinkgem.jeesite.modules.biz.dao.BizApkDao;

/**
 * 安卓apkService
 * @author tiancheng
 * @version 2018-08-28
 */
@Service
@Transactional(readOnly = true)
public class BizApkService extends CrudService<BizApkDao, BizApk> {

	@Autowired
	BizApkDao bizApkDao;

	public BizApk get(String id) {
		return super.get(id);
	}
	
	public List<BizApk> findList(BizApk bizApk) {
		return super.findList(bizApk);
	}
	
	public Page<BizApk> findPage(Page<BizApk> page, BizApk bizApk) {
		return super.findPage(page, bizApk);
	}
	
	@Transactional(readOnly = false)
	public void save(BizApk bizApk) {
		super.save(bizApk);
	}
	
	@Transactional(readOnly = false)
	public void delete(BizApk bizApk) {
		super.delete(bizApk);
	}


	public BizApk findListApi(){
		return bizApkDao.findListApi();
	}
}