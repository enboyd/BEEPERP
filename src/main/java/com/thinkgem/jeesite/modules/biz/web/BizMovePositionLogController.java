/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.biz.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.thinkgem.jeesite.common.config.Global;
import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.web.BaseController;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.modules.biz.entity.BizMovePositionLog;
import com.thinkgem.jeesite.modules.biz.service.BizMovePositionLogService;

/**
 * 移库Controller
 * @author tiancheng
 * @version 2018-09-12
 */
@Controller
@RequestMapping(value = "${adminPath}/biz/bizMovePositionLog")
public class BizMovePositionLogController extends BaseController {

	@Autowired
	private BizMovePositionLogService bizMovePositionLogService;
	
	@ModelAttribute
	public BizMovePositionLog get(@RequestParam(required=false) String id) {
		BizMovePositionLog entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = bizMovePositionLogService.get(id);
		}
		if (entity == null){
			entity = new BizMovePositionLog();
		}
		return entity;
	}
	
	@RequiresPermissions("biz:bizMovePositionLog:view")
	@RequestMapping(value = {"list", ""})
	public String list(BizMovePositionLog bizMovePositionLog, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<BizMovePositionLog> page = bizMovePositionLogService.findPage(new Page<BizMovePositionLog>(request, response), bizMovePositionLog); 
		model.addAttribute("page", page);
		return "modules/biz/bizMovePositionLogList";
	}

	@RequiresPermissions("biz:bizMovePositionLog:view")
	@RequestMapping(value = "form")
	public String form(BizMovePositionLog bizMovePositionLog, Model model) {
		model.addAttribute("bizMovePositionLog", bizMovePositionLog);
		return "modules/biz/bizMovePositionLogForm";
	}

	@RequiresPermissions("biz:bizMovePositionLog:edit")
	@RequestMapping(value = "save")
	public String save(BizMovePositionLog bizMovePositionLog, Model model, RedirectAttributes redirectAttributes) {
		if (!beanValidator(model, bizMovePositionLog)){
			return form(bizMovePositionLog, model);
		}
		bizMovePositionLogService.save(bizMovePositionLog);
		addMessage(redirectAttributes, "保存移库成功");
		return "redirect:"+Global.getAdminPath()+"/biz/bizMovePositionLog/?repage";
	}
	
	@RequiresPermissions("biz:bizMovePositionLog:edit")
	@RequestMapping(value = "delete")
	public String delete(BizMovePositionLog bizMovePositionLog, RedirectAttributes redirectAttributes) {
		bizMovePositionLogService.delete(bizMovePositionLog);
		addMessage(redirectAttributes, "删除移库成功");
		return "redirect:"+Global.getAdminPath()+"/biz/bizMovePositionLog/?repage";
	}

}