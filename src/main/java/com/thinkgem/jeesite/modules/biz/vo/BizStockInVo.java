/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.biz.vo;

import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * 入库单的ControllerEntity
 * @author tianCheng
 * @version 2018-07-16
 */
@Data
public class BizStockInVo{
	
	private static final long serialVersionUID = 1L;
	private String stockInId;		// 入库单ID
	private String stockInCode;		// 入库单编号
	private Date stockInDate;		// 入库日期
	private String createByName;   //入库单的创建人

	private List<BizStockInDetailVo> bizStockInDetailList;//入库单明细

}