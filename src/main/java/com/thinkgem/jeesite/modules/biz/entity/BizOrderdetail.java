/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.biz.entity;

import org.hibernate.validator.constraints.Length;

import com.thinkgem.jeesite.common.persistence.DataEntity;

import java.math.BigDecimal;

/**
 * 订单Entity
 * @author tiancheng
 * @version 2018-09-17
 */
public class BizOrderdetail extends DataEntity<BizOrderdetail> {
	
	private static final long serialVersionUID = 1L;
	private String name;		// 编码
	private String orderId;		// 所属订单
	private Double unitPrice;		// 单价
	private Integer quantity;		// 数量
	private Integer surplusQuantity;  // 出库剩余数
	private Integer outNum;		//已出库数
	private Double amountOfMoney;		// 金额
	private String materielId;		// 物料
	private String materielName;		// 物料name
	private String projectdetailId;		// 商品id
	private String projectdetailName;		// 商品name
	private String outboundOrderPid;		// 所属订货单ID
	private String code;		// code
	private BigDecimal discount;		// 折扣
	private String payType;		// 结算方式
	private String specification;		// 规格
	private String isDeliver;  // 是否发货（0，立即发货，1，等待发货）
	private String unit;	//单位
	private String projectId;	//项目id
	private String projectName;	//项目name

	private String supplierName;  //供应商
	private String goodsCategoryName; //项目
	private String stockAmount; //库存数量

	public BizOrderdetail() {
		super();
	}

	public BizOrderdetail(String id){
		super(id);
	}

	@Length(min=1, max=64, message="编码长度必须介于 1 和 64 之间")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	@Length(min=0, max=64, message="所属订单长度必须介于 0 和 64 之间")
	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	
	@Length(min=0, max=64, message="物料长度必须介于 0 和 64 之间")
	public String getMaterielId() {
		return materielId;
	}

	public void setMaterielId(String materielId) {
		this.materielId = materielId;
	}

	public Double getUnitPrice() {
		return unitPrice;
	}

	public void setUnitPrice(Double unitPrice) {
		this.unitPrice = unitPrice;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public Double getAmountOfMoney() {
		return amountOfMoney;
	}

	public void setAmountOfMoney(Double amountOfMoney) {
		this.amountOfMoney = amountOfMoney;
	}

	@Length(min=0, max=64, message="物料name长度必须介于 0 和 64 之间")
	public String getMaterielName() {
		return materielName;
	}

	public void setMaterielName(String materielName) {
		this.materielName = materielName;
	}
	
	@Length(min=0, max=64, message="项目明细id长度必须介于 0 和 64 之间")
	public String getProjectdetailId() {
		return projectdetailId;
	}

	public void setProjectdetailId(String projectdetailId) {
		this.projectdetailId = projectdetailId;
	}
	
	@Length(min=0, max=64, message="项目明细name长度必须介于 0 和 64 之间")
	public String getProjectdetailName() {
		return projectdetailName;
	}

	public void setProjectdetailName(String projectdetailName) {
		this.projectdetailName = projectdetailName;
	}

	public String getOutboundOrderPid() {
		return outboundOrderPid;
	}

	public void setOutboundOrderPid(String outboundOrderPid) {
		this.outboundOrderPid = outboundOrderPid;
	}

	@Length(min=0, max=64, message="code长度必须介于 0 和 64 之间")
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public BigDecimal getDiscount() {
		return discount;
	}

	public void setDiscount(BigDecimal discount) {
		this.discount = discount;
	}

	@Length(min=0, max=64, message="结算方式长度必须介于 0 和 64 之间")
	public String getPayType() {
		return payType;
	}

	public void setPayType(String payType) {
		this.payType = payType;
	}

	public String getIsDeliver() {
		return isDeliver;
	}

	public void setIsDeliver(String isDeliver) {
		this.isDeliver = isDeliver;
	}

	public String getSpecification() {
		return specification;
	}

	public void setSpecification(String specification) {
		this.specification = specification;
	}

	public Integer getSurplusQuantity() {
		return surplusQuantity;
	}

	public void setSurplusQuantity(Integer surplusQuantity) {
		this.surplusQuantity = surplusQuantity;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public String getProjectId() {
		return projectId;
	}

	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public Integer getOutNum() {
		return this.quantity - this.surplusQuantity;
	}

	public void setOutNum(Integer outNum) {
		this.outNum = outNum;
	}

	public String getSupplierName() {
		return supplierName;
	}

	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}

	public String getGoodsCategoryName() {
		return goodsCategoryName;
	}

	public void setGoodsCategoryName(String goodsCategoryName) {
		this.goodsCategoryName = goodsCategoryName;
	}

	public String getStockAmount() {
		return stockAmount;
	}

	public void setStockAmount(String stockAmount) {
		this.stockAmount = stockAmount;
	}
}