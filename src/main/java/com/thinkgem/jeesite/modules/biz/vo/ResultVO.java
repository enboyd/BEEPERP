package com.thinkgem.jeesite.modules.biz.vo;

import lombok.Data;

import java.io.Serializable;

/**
 * Description:
 * Author: LTY
 * Create Time: 2018-08-09 14:07
 */
@Data
public class ResultVO<T> implements Serializable {

    private static final long serialVersionUID = 5062944374365078774L;

    private Integer code = 0;

    private String msg = "";

    private T data;
}
