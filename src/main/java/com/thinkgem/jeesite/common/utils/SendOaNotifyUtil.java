package com.thinkgem.jeesite.common.utils;

import com.thinkgem.jeesite.modules.oa.entity.OaNotify;
import com.thinkgem.jeesite.modules.sys.dao.UserDao;
import com.thinkgem.jeesite.modules.sys.entity.User;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * SendOaNotifyUtil 通知通告
 * @author 张增亮
 * @version 2017-12-22
 */

public class SendOaNotifyUtil {

	private static UserDao userDao = SpringContextHolder.getBean(UserDao.class);

	/**
	 * 单个接受人的发送
	 * @param  type       通知类型        1,会议通告,2,奖惩通告,3,活动通告，4，对接通知
	 * @param customerServiceId        通知接收人的id
	 */
	//返回通知通告管理对象
	public static OaNotify findOaNotifyDan(String type, String customerServiceId, String content, String title) {
		User user = userDao.getNameById(customerServiceId);
		OaNotify oaNotify = new OaNotify();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try {
			oaNotify.setType(type);
			oaNotify.setTitle(title);
			oaNotify.setStatus("1"); //	0,草稿 1,发布
			oaNotify.setContent(content +"\n"+ "入库时间："+sdf.format(new Date()));
			oaNotify.setDelFlag("0");
			oaNotify.setIsNewRecord(false);
			oaNotify.setReadFlag("0");//0,未读,1,已读
			oaNotify.setSelf(false);
			oaNotify.setUser(user);//设置接受人
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("发送通知异常");
		}
		return oaNotify;
	}

}
