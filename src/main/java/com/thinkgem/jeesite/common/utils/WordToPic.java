package com.thinkgem.jeesite.common.utils;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.imageio.ImageIO;
import sun.swing.SwingUtilities2;

public class WordToPic {

    //
    public static int getLength(String text) {
        int length = 0;
        for (int i = 0; i < text.length(); i++) {
            if (new String(text.charAt(i) + "").getBytes().length > 1) {
                length += 2;
            } else {
                length += 1;
            }
        }
        return length / 2;
    }

    public static String TextToPic(Map<String,String> map, int width, int height, int fontSize) {
        try {
            BufferedImage small = ImageIO.read(new File(map.get("txmPath")));

            String filepath = map.get("path");
            File file = new File(filepath);
            Font font = new Font("Microsoft YaHei", Font.ITALIC, fontSize);
            BufferedImage bi = new BufferedImage(width, height,BufferedImage.TYPE_INT_RGB);
            Graphics2D g2 = (Graphics2D) bi.getGraphics();
            g2.setBackground(null);
            g2.clearRect(0, 0, width, height);
            g2.setFont(font);
            g2.setPaint(Color.BLACK);
            //g2.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_ATOP,0.3f));
            g2.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING,RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
            printString(g2, "存货编码:",  20,30, fontSize);
            printString(g2, map.get("goodsCode"),  120,30, fontSize/2);
            if(null != map.get("manufactureDate")){
                printString(g2, map.get("manufactureDate"), 270,30, fontSize/2);
            }
            printString(g2, "存货名称："+map.get("goodsName"),  20,65, fontSize);
            printString(g2, "批  次:", 20,100, fontSize);
            printString(g2, map.get("piCi"), 120,100, fontSize/2);
            printString(g2, "数量:", 270,100, fontSize);
            printString(g2, map.get("num"), 335,100, fontSize/2);
            g2.drawImage(small, -10, 120, 425, small.getHeight(), null);
            g2.dispose();
            ImageIO.write(bi, "png", file);
            return "image" + getDate() + ".png";

        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    private static void printString(Graphics2D g2d, String str, int x, int y,int fontSize) {
        FontMetrics metrics = SwingUtilities2.getFontMetrics(null,g2d.getFont());
        for (char ca : str.toCharArray()) {
            int px = metrics.stringWidth("" + ca);
            g2d.drawString("" + ca, x + (fontSize - px) / 2, y);
            x += fontSize;
        }
    }

    public static String getDate() {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss");
        return formatter.format(new Date());
    }

    public static void main(String[] args) throws IOException {
        Map<String,String> mapInfo = new HashMap<>();
        //生成综合条形码的地址
        String path = "D:/ph/J100071811270001Z.png";
        mapInfo.put("path",path);
        mapInfo.put("txmPath","D:/ph/J100071811270001.png");
        mapInfo.put("goodsCode","J10007");


        mapInfo.put("goodsName","包子馅煸肉2");
        mapInfo.put("piCi","J100071811270001");
        mapInfo.put("num","J100081811250001");
        TextToPic(mapInfo,400, 200, 20);
    }

}
