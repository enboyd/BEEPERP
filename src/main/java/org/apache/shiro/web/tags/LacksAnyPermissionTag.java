package org.apache.shiro.web.tags;

import org.apache.shiro.subject.Subject;

/**
 * Shiro LacksAnyPermission Tag.
 * @author xuchen
 * @version 2018年3月5日
 */
public class LacksAnyPermissionTag extends PermissionTag {

	
	private static final long serialVersionUID = 1L;
	private static final String PERMISSION_NAMES_DELIMETER = ",";

	@Override
	protected boolean showTagBody(String permissionNames) {
		boolean hasAnyPermission = true;

		Subject subject = getSubject();

		if (subject != null) {
			//用，切割。如果有这个权限返回false
			for (String permission : permissionNames.split(PERMISSION_NAMES_DELIMETER)) {
				if (subject.isPermitted(permission.trim())) {
					hasAnyPermission = false;
					break;
				}

			}
		}
		return hasAnyPermission;
	}
}
