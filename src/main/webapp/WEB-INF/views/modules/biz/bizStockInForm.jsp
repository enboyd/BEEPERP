<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>采购单管理</title>
	<meta name="decorator" content="default"/>
	<script src="${ctxStatic}/dist/zoomify.min.js"></script>
	<link rel="stylesheet" href="${ctxStatic}/dist/zoomify.min.css">
	<script type="text/javascript">
		$(document).ready(function() {
			$('.example img').zoomify();
			//清空tbody
		/*	$("#goodsList").html("");*/
			//$("#name").focus();
			$("#inputForm").validate({
				submitHandler: function(form){
					loading('正在提交，请稍等...');
					form.submit();
				},
				errorContainer: "#messageBox",
				errorPlacement: function(error, element) {
					$("#messageBox").text("输入有误，请先更正。");
					if (element.is(":checkbox")||element.is(":radio")||element.parent().is(".input-append")){
						error.appendTo(element.parent().parent());
					} else {
						error.insertAfter(element);
					}
				}
			});

			<%--$("#save").click(function () {--%>
                <%--//提交表单--%>
                <%--$.ajax({--%>
                    <%--type: "POST",--%>
                    <%--url: "${ctx}/biz/bizStockIn/ajaxSave",--%>
                    <%--data:$("#inputForm").serialize(),--%>
                    <%--dataType: "text",--%>
                    <%--success: function(data){--%>
                        <%--if(data == 'success'){--%>
                            <%--//提示--%>
                            <%--top.$.jBox.tip("${'保存成功'}","${'success'}",{persistent:true,opacity:0});--%>

                        <%--}else{--%>
                            <%--top.$.jBox.tip("${'保存失败'}","${'error'}",{persistent:true,opacity:0});--%>
                        <%--}--%>
                    <%--}--%>
                <%--})--%>
			<%--})--%>

		});

		/**
		 * 单个条形码的修改
		 */
		function addBizBarcode(barcodeId,stockInDetailId,index,statusIndex) {
			loading("正在修改，请稍等...");
			var number = $("#bizStockInDetail"+statusIndex+"_bizBarcode_"+index).val();
			$.ajax({
				type: "POST",
				url: "${ctx}/biz/bizStockIn/saveBarcode",
				data: {
						"barcodeId":barcodeId,
						"stockInDetailId":stockInDetailId,
						"number":number
					},
				dataType: "text",
				success: function(data){
					closeLoading("");
					if(data != null){
						//提示
						top.$.jBox.tip("${'保存成功！'}","${'success'}",{persistent:true,opacity:0});
						$("#"+stockInDetailId+"_url"+index).attr("src",data);
					}else{
						top.$.jBox.tip("${'保存失败！'}","${'error'}",{persistent:true,opacity:0});
					}
				}
			})
		}

		/**
		 * 删除条形码
		 */
		function deleteBizBarcode(barcodeId,stockInDetailId,index) {
			if(confirm("确认删除此条形码吗？")){
				$.ajax({
					type: "POST",
					url: "${ctx}/biz/bizBarcode/delete",
					data: {
						"barcodeId":barcodeId
					},
					dataType: "text",
					success: function(data){
						if(data == 'success'){
							//提示
							top.$.jBox.tip("${'删除成功！'}","${'success'}",{persistent:true,opacity:0});
							$("#"+stockInDetailId+"_tr"+index).css("display","none");
						}else{
							top.$.jBox.tip("${'删除失败！'}","${'error'}",{persistent:true,opacity:0});
						}
					}
				})
			}
		}
		
		function saveBarcode(tr,index) {
			var barcodeNum = $("#barcode"+index).val();
			//托盘的信息
			var td = "<tr><td colspan='7'><input type='text' onkeyup='cal("+index+",this.id)' id='bizStockInDetail"+index+"_bizBarcode_"+barcodeNum+"' placeholder='请输入托盘存放量' name='bizStockInDetailList["+index+"].bizBarcodeList["+barcodeNum+"].number'></td></tr>";
			$("#"+tr).after(td);
			var barcodeNumNew = Number(barcodeNum)+1;
			$("#barcode"+index).val(barcodeNumNew);
		}


		/**
		 * 当输入托盘数时自动计算待分配托盘数
		 */
		function cal(index,id) {
			//获取此采购单的	实际入库数（不能直接获取待分配托盘数，因为它是随时在变的）
			var actualAmount = $("#actualAmount"+index).val();
			//获取托盘的数量
			var num = $("#barcode"+index).val();
			var sum = 0;
			for (var i = 0 ; i < num ;i ++){
				var number = $("#bizStockInDetail"+index+"_bizBarcode_"+i).val();
				sum += Number(number);
			}
			var cha = Number(actualAmount) - sum;
			if(cha < 0){
				alert("请认真核对待分配托盘数！");
				$("#"+id).val(0);
				//清空当前的输入框，重新计算待分配托盘数
				sum = 0;
				for (var i = 0 ; i < num ;i ++){
					var number = $("#bizStockInDetail"+index+"_bizBarcode_"+i).val();
					sum += Number(number);
				}
				var cha = Number(actualAmount) - sum;
				$("#waitDistributionAmount"+index).val(cha);

			}else{
				//设置待分配托盘数
				$("#waitDistributionAmount"+index).val(cha);
			}
		}


		/**
		 * 修改实际入库数时修改待分配托盘数
		 */
		function updateWaitDistributionAmount(val,index) {
			var num = $("#barcode"+index).val();
			var sum = 0;
			for (var i = 0 ; i < num ;i ++){
				var number = $("#bizStockInDetail"+index+"_bizBarcode_"+i).val();
				sum += Number(number);
			}
			var cha = Number(val) - sum;
			$("#waitDistributionAmount"+index).val(cha);
		}


		/**
		 * 单个打印条码
		 */
		function dayinBizBarcode(id) {
			loading("正在打印，请稍等...");
			$.ajax({
				type: "POST",
				url: "${ctx}/biz/bizStockIn/getInfoDan",
				data: {
					"id":id
				},
				success: function(data){
					closeLoading("");
					var info = data;
					var goodsCode = info.goodsCode;
					var manufactureDate = info.manufactureDate;
					var goodsName = info.goodsName;
					var pici = info.name.substring(0,12);
					var number = info.number;
					var name = info.name;
					printQRcode(goodsCode,manufactureDate,goodsName,pici,number,name);
                    printNumberAdd(name);	//条码打印次数增加1

				}
			})
		}
		
		/**
		 * 批量打印条码
		 */
		function printQRcodeSum(stockInId) {
			$.ajax({
				type: "POST",
				url: "${ctx}/biz/bizStockIn/getInfo",
				data: {
					"stockInId":stockInId
				},
				success: function(data){
					for(var i = 0;i < data.length; i++){
						var info= data[i];
						var goodsCode = info.goodsCode;
						var manufactureDate = info.manufactureDate;
						var goodsName = info.goodsName;
						var pici = info.name.substring(0,12);
						var number = info.number;
						var name = info.name;
						printQRcode(goodsCode,manufactureDate,goodsName,pici,number,name);
                        printNumberAdd(name);	//条码打印次数增加1
                    }
				}
			})
		}


		/**
		 * 打印接的打印
		 * goodsCode 商品编号
		 * date 生产日期
		 * goodsName 商品名称
		 * pici 批次
		 * number 数量
		 * code 条形码编号
		 */
		function printQRcode(goodsCode,date,goodsName,pici,number,code){
			var TSCObj = new ActiveXObject("TSCActiveX.TSCLIB");//打印机使用的dll
			TSCObj.ActiveXopenport ("TSC TTP-243E Pro");
			TSCObj.ActiveXsetup("70","40","4","15","0","2.5","0");//打印机设置
			TSCObj.ActiveXsendcommand("DIRECTION 1");  //设置标签方向 ,DIRECTION 1 左上角 (x,y)={0,0};DIRECTION 0 右下角 (x,y)={0,0};
			TSCObj.ActiveXsendcommand ("SET TEAR ON");
			TSCObj.ActiveXclearbuffer();
			TSCObj.ActiveXwindowsfont (65, 20, 30, 0, 2, 0, "Arial", "存货编码："+goodsCode);
			TSCObj.ActiveXwindowsfont (350, 20, 30, 0, 2, 0, "Arial", date);
			TSCObj.ActiveXwindowsfont (65, 60, 30, 0, 2, 0, "Arial", "存货名称："+goodsName);
			TSCObj.ActiveXwindowsfont (65, 100, 30, 0, 2, 0, "Arial", "批  次："+pici);
			TSCObj.ActiveXwindowsfont (365, 100, 30, 0, 2, 0, "Arial", "数量："+number);
			TSCObj.ActiveXbarcode ("35", "150", "128", "100", "1", "0", "3", "3", code);
			TSCObj.ActiveXprintlabel ("1","1");//（打印份数,每页打印张数）
			TSCObj.ActiveXclearbuffer();//清除
			TSCObj.ActiveXcloseport();//关闭打印端口
		}

        /**
		 * 条形码打印次数增加1
         * @param name
         */
        function printNumberAdd(name){
            $.ajax({
                type: "POST",
                url: "${ctx}/biz/bizBarcode/printNumberAdd",
                data: {
                    "barCode":name
                }
            })
        }

	</script>
</head>
<body>
	<ul class="nav nav-tabs">
		<li><a href="${ctx}/biz/bizStockIn/list">入库单列表</a></li>
		<li class="active"><a href="${ctx}/biz/bizStockIn/form?id=${bizStockIn.stockInId}">入库单详情</a></li>
	</ul><br/>
	<form:form id="inputForm" modelAttribute="bizStockIn" action="${ctx}/biz/bizStockIn/ajaxSave" method="post" class="form-horizontal">
		<form:hidden path="id"/>
		<sys:message content="${message}"/>

		<div class="control-group">
			<label class="control-label">入库明细：</label>
			<div class="controls">
				<table id="contentTable"
					   class="table table-bordered table-condensed">
					<thead>
					<tr>
						<th>商品名称</th>
						<th>规格</th>
						<th>生产日期</th>
						<th>货物存放区</th>
						<th>实际入库数量</th>
						<th>待分配托盘数</th>
						<th>操作</th>
					</tr>
					</thead>
					<tbody>
						<c:forEach items="${bizStockInDetailList}" var="bizStockInDetailList" varStatus="status">
							<tr id="bizStockInDetailList${status.index}">
								<input type="hidden" id="bizStockInDetailList${status.index}" name="bizStockInDetailList[${status.index}].stockInDetailId" value="${bizStockInDetailList.stockInDetailId}">
								<input type="hidden"  name="bizStockInDetailList[${status.index}].goodsId" value="${bizStockInDetailList.goodsId}">
								<input type="hidden"  name="bizStockInDetailList[${status.index}].manufactureDate" value="${bizStockInDetailList.manufactureDate}">
								<input type="hidden"  name="bizStockInDetailList[${status.index}].storageLocationId" value="${bizStockInDetailList.storageLocationId}">
								<c:if test="${empty bizStockInDetailList.bizBarcodeNum}"><input type="hidden" id="barcode${status.index}" value="0"></c:if>
								<c:if test="${not empty bizStockInDetailList.bizBarcodeNum}"><input type="hidden" id="barcode${status.index}" value="${bizStockInDetailList.bizBarcodeNum}"></c:if>
								<td>${bizStockInDetailList.goodsName}</td>
								<td>${bizStockInDetailList.specification}</td>
								<td>${bizStockInDetailList.manufactureDate}</td>
								<td>${bizStockInDetailList.storageLocationName}</td>
								<td><input id="actualAmount${status.index}" onkeyup="updateWaitDistributionAmount(this.value,${status.index})" name="bizStockInDetailList[${status.index}].actualAmount" type="text" value="${bizStockInDetailList.actualAmount}"></td>
								<td><input style="color:red" id="waitDistributionAmount${status.index}" name="bizStockInDetailList[${status.index}].waitDistributionAmount" readonly type="text" value="${bizStockInDetailList.waitDistributionAmount}"></td>
								<td onclick="saveBarcode('bizStockInDetailList${status.index}',${status.index})"><img src="${ctxStatic}/images/save.png" style="width: 30px;height: 30px"></td>
							</tr>
							<c:forEach  items="${bizStockInDetailList.bizBarcodeList}" var="bizBarcode" varStatus="ind">
								<tr id="${bizStockInDetailList.stockInDetailId}_tr${ind.index}">
									<td><input type='text' value="${bizBarcode.name}" readonly></td>
									<td><input onkeyup='cal(${status.index},this.id)' type='text' id="bizStockInDetail${status.index}_bizBarcode_${ind.index}" value="${bizBarcode.number}" ></td>
									<td>
										<div class="example">
											<p>
												<img class="img-rounded" id="${bizStockInDetailList.stockInDetailId}_url${ind.index}" style="height: 90px" src="${bizBarcode.url}">
											</p>
										</div>
									</td>
									<td colspan='4'>
										<img onclick="addBizBarcode('${bizBarcode.id}','${bizStockInDetailList.stockInDetailId}','${ind.index}',${status.index})" sty src="${ctxStatic}/images/add.png" alt="保存" style="width: 30px;height: 30px;margin-left: 10px">
										<img onclick="deleteBizBarcode('${bizBarcode.id}','${bizStockInDetailList.stockInDetailId}','${ind.index}')" src="${ctxStatic}/images/delete.png" alt="删除" style="width: 30px;height: 30px;margin-left: 10px">
										<img onclick="dayinBizBarcode('${bizBarcode.id}')" src="${ctxStatic}/images/dayin.png" alt="打印" style="width: 30px;height: 30px;margin-left: 10px">
									</td>
								</tr>
							</c:forEach>
						</c:forEach>
					</tbody>
				</table>
			</div>
		</div>

		<div colspan=5 class="text-center">
			<input class="btn btn-primary" type="submit" value="保 存"/>&nbsp;
			<input id="btnCancel" class="btn" type="button" value="返 回" onclick="history.go(-1)"/>
			<input class="btn btn-primary" type="button" onclick="printQRcodeSum('${bizStockIn.stockInId}')" value="打 印"/>&nbsp;
		</div>
	</form:form>

</body>
</html>