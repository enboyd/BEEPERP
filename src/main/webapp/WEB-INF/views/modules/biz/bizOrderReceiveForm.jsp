<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>订单管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
			//$("#name").focus();
			$("#inputForm").validate({
				submitHandler: function(form){
					loading('正在提交，请稍等...');
					form.submit();
				},
				errorContainer: "#messageBox",
				errorPlacement: function(error, element) {
					$("#messageBox").text("输入有误，请先更正。");
					if (element.is(":checkbox")||element.is(":radio")||element.parent().is(".input-append")){
						error.appendTo(element.parent().parent());
					} else {
						error.insertAfter(element);
					}
				}
			});


			$("#btnSubmitFrom").click(function(){
				//表单校验

				//提交表单
				$.ajax({
					type: "POST",
					url: "${ctx}/biz/bizGoods/ajaxSave",
					data:$("#inputFormGoods").serialize(),
					dataType: "text",
					success: function(data){
						//关闭formDiv
						$("#goodsFormHtml").hide();
						//清空form表单
						//TODO
						//打开listDiv
						$("#add").removeClass('active');
						$("#list").addClass('active');
						$("#goodsListHtml").show();
						//调用ajax查询商品列表
						$("#addGoods").click();
						if(data == 'success'){
							//提示
							top.$.jBox.tip("${'保存成功'}","${'success'}",{persistent:true,opacity:0});
						}else{
							top.$.jBox.tip("${'保存失败'}","${'error'}",{persistent:true,opacity:0});
						}
					}
				})
			});

			$("#addGoods").click(function () {
				$("#addGoodsModal").modal('show');
				var goodsCode = $("#goodsCode").val();
				var goodsName = $("#goodsName").val();
				//获取商品的列表
				$.ajax({
					type: "POST",
					url: "${ctx}/biz/bizGoods/modalPage",
					data: {
						'goodsCode': goodsCode,
						'goodsName': goodsName
					},
					success: function(data){
						//清空tbody
						$("#goodsInfo").html("");
						//添加分页
						$("#level").html(data.html);
						var list = data.list;
						var rows = "";
						for (var i = 0 ; i < list.length ; i++){
							var specification = isNanVal(list[i].specification);
							var stockAmount = isNanVal(list[i].sureStockAmount);
							var row = "<tr>"
									+ "<td><input name='checkId' type='checkbox' value='"+list[i].goodsId+"'/></td>"
									+ "<td>"+ list[i].goodsCode+ "</td>"
									+ "<td>"+ list[i].goodsName+"</td>"
									+ "<td>"+ list[i].supplierName+"</td>"
									+ "<td>"+ list[i].goodsCategoryName+ "</td>"
									+ "<td>"+ specification+ "</td>"
									+ "<td>"+ list[i].unit+ "</td>"
									+ "<td>"+ stockAmount+ "</td>"
									+ "</tr>";
							rows += row;
						}
						$("#goodsInfo").html(rows);
					}
				});
			})

			//全选
			$("#selectAll").click(function(){
				if($('#selectAll').is(':checked')) {
					$("#goodsInfo :checkbox").attr("checked", true);
				}else{
					$("#goodsInfo :checkbox").attr("checked", false);
				}
			});

			//商品的查询
			$("#select").click(function () {
				var goodsCode = $("#goodsCode").val();
				var goodsName = $("#goodsName").val();
				//获取商品的列表
				$.ajax({
					type: "POST",
					url: "${ctx}/biz/bizGoods/modalPage",
					data: {
						'goodsCode': goodsCode,
						'goodsName': goodsName
					},
					success: function(data){
						//清空tbody
						$("#goodsInfo").html("");
						//添加分页
						$("#level").html(data.html);
						var list = data.list;
						var rows = "";
						for (var i = 0 ; i < list.length ; i++){
							var specification = isNanVal(list[i].specification);
							var stockAmount = isNanVal(list[i].sureStockAmount);
							var row = "<tr>"
									+ "<td><input name='checkId' type='checkbox' value='"+list[i].goodsId+"' /></td>"
									+ "<td>"+ list[i].goodsCode+ "</td>"
									+ "<td>"+ list[i].goodsName+"</td>"
									+ "<td>"+ list[i].supplierName+"</td>"
									+ "<td>"+ list[i].goodsCategoryName+ "</td>"
									+ "<td>"+ specification+ "</td>"
									+ "<td>"+ list[i].unit+ "</td>"
									+ "<td>"+ stockAmount+ "</td>"
									+ "</tr>";
							rows += row;
						}
						$("#goodsInfo").html(rows);
					}
				});
			});

			//添加商品到领取物料单
			$("#addGoodsToPurchase").click(function () {
				$("#addGoodsModal").modal('hide');
				//获取到选择的商品
				var dataCheckId = [];
				$("input[name='checkId']:checked").each(function(i) {
					dataCheckId.push($(this).val());
				});
				if(dataCheckId.length == 0){
					alert("请选择要下单的商品！");
				}else{
					$.ajax({
						type: "POST",
						url: "${ctx}/biz/bizGoods/getGoodsListByIds",
						data: {
							'goodsIds[]': dataCheckId
						},
						success: function(data){
							//清空tbody
							//$("#goodsList").html("");
							var rows = "";
							var goodsNum = $("#goodsNum").val();
							//当前采购单明细有几条
							var num = 0;
							if(null == goodsNum){
								goodsNum = data.length;
							}else{
								num = goodsNum;
								goodsNum = Number(goodsNum) + data.length;
							}
							//用于商品小计和总价的计算
							$("#goodsNum").val(goodsNum);
							for (var i = 0 ; i < data.length ; i++){
								var specification = isNanVal(data[i].specification);
								var stockAmount = isNanVal(data[i].sureStockAmount);
								var unit = isNanVal(data[i].unit);
								//获取是第几行
								var rowNum = Number(num)+Number(i);
								var row = "<tr id='bizOrderDetail"+rowNum+"'>"
										+ "<td>"+ data[i].goodsName+"</td>"
										+ "<td>"+ data[i].supplierName+"</td>"
										+ "<td>"+ data[i].goodsCategoryName+ "</td>"
										+ "<td>"+ specification+ "</td>"
										+ "<td>"+ unit + "</td>"
										+ "<td>"+ stockAmount+ "</td>"
										+ "<td>"
										+ "<input type='hidden' id='stockAmount"+rowNum+"' value='"+ data[i].sureStockAmount+"' name='bizOrderDetailList["+rowNum+"].amountOfMoney'>"
										+ "<input type='hidden' id='projectdetailId"+rowNum+"' name='bizOrderDetailList["+rowNum+"].projectdetailId' value='"+ data[i].goodsId+"'>"
										+ "<input type='hidden' id='projectdetailName"+rowNum+"' name='bizOrderDetailList["+rowNum+"].projectdetailName' value='"+ data[i].goodsName+"'>"
										+ "<input type='hidden' id='code"+rowNum+"' name='bizOrderDetailList["+rowNum+"].code' value='"+ data[i].goodsCode+"'>"
										+ "<input type='hidden' id='specification"+rowNum+"' name='bizOrderDetailList["+rowNum+"].specification' value='"+ data[i].specification+"'>"
										+ "<input type='hidden' id='delFlag"+rowNum+"' name='bizOrderDetailList["+rowNum+"].delFlag' value='0'>"
										+ "<input type='text' class='digits required' style='width: 100px' onkeyup='errorCheck("+rowNum+")' id='quantity"+rowNum+"' name='bizOrderDetailList["+rowNum+"].quantity' />"
										+ "<span class='help-inline'><font color='red'>*</font> </span>"
										+ "</td>"
										+ "<shiro:hasPermission name='biz:buyingRequisition:buyingRequisitiondetailRemove'>"
										+ "<td class='text-center' width='10'>"
										+ "<span class='close' onclick='delRow(this.id)' id='"+rowNum+"' title='删除'>&times;</span>"
										+ "</td>"
										+ "</shiro:hasPermission>>"
										+ "</tr>";
								rows += row;
							}
							$("#goodsList").append(rows);
						}
					});
				}
			})

		});

		function isNanVal(val) {
			if(typeof(val)=="undefined"){
				return "";
			}
			return val;
		}

		function page(n,s){
			var goodsCode = $("#goodsCode").val();
			var goodsName = $("#goodsName").val();
			var pageNo = n;
			var pageSize = s;
			//获取商品的列表
			$.ajax({
				type: "POST",
				url: "${ctx}/biz/bizGoods/modalPage",
				data: {
					'goodsCode': goodsCode,
					'goodsName': goodsName,
					'pageNo': pageNo,
					'pageSize': pageSize
				},
				success: function(data){
					closeLoading("");
					//清空tbody
					$("#goodsInfo").html("");
					//添加分页
					$("#level").html(data.html);
					var list = data.list;
					var rows = "";
					for (var i = 0 ; i < list.length ; i++){
						var stockAmount = isNanVal(list[i].stockAmount);
						var supplierName = isNanVal(list[i].supplierName);
						var goodsCategoryName = isNanVal(list[i].goodsCategoryName);
						var purchasingAmount = isNanVal(list[i].purchasingAmount);
						var unit = isNanVal(list[i].unit);
						var minPurchaseQuantity = isNanVal(list[i].minPurchaseQuantity);
						var specification = isNanVal(list[i].specification);
						var row = "<tr>"
								+ "<td><input name='checkId' type='checkbox' value='"+list[i].goodsId+"'/></td>"
								+ "<td>"+ list[i].goodsCode+ "</td>"
								+ "<td>"+ list[i].goodsName+"</td>"
								+ "<td>"+ supplierName+"</td>"
								+ "<td>"+ goodsCategoryName+ "</td>"
								+ "<td>"+ specification+ "</td>"
								+ "<td>"+ unit+ "</td>"
								+ "<td>"+ stockAmount+ "</td>"
								+ "<td>"+ purchasingAmount+ "</td>"
								+ "<td>"+ minPurchaseQuantity+ "</td>"
								+ "</tr>";
						rows += row;
					}
					$("#goodsInfo").html(rows);
				}
			});
			return false;
		}

		//删除行
		function delRow(row) {
			$("#bizOrderDetail"+row).css("display","none");
			//删除标识
			$("#delFlag"+row).val("1");
		}

		function showGoodsDiv(id) {
			$(".goodsDiv").removeClass("active");
			$("#"+id).addClass("active");
			if(id == "add"){
				$("#goodsFormHtml").show();
				$("#goodsListHtml").hide();
			}else if(id == "list"){
				$("#goodsFormHtml").hide();
				$("#goodsListHtml").show();
			}
		}

		/*
		 *	领取数量不能大于库存数量
		 */
		function errorCheck(num){
			var stockAmount = $("#stockAmount"+num).val();
			var quantity = $("#quantity"+num).val();
			if(Number(quantity) > Number(stockAmount)){
				alert("领取数量不能大于库存数量！");
				$("#quantity"+num).val("0");
			}

		}
	</script>
</head>
<body>
<ul class="nav nav-tabs">
	<li><a href="${ctx}/biz/bizOrder/receiveList">领取物料列表</a></li>
	<li class="active"><a href="${ctx}/biz/bizOrder/receiveForm?id=${bizOrder.id}">领取物料<shiro:hasPermission name="biz:bizOrder:edit">${not empty bizOrder.id?'修改':'添加'}</shiro:hasPermission><shiro:lacksPermission name="biz:bizOrder:edit">查看</shiro:lacksPermission></a></li>
</ul><br/>
<form:form id="inputForm" modelAttribute="bizOrder" action="${ctx}/biz/bizOrder/receiveSave" method="post" class="form-horizontal">
	<form:hidden path="id"/>
	<input type="hidden" id="goodsNum" value="${goodsNum}">
	<sys:message content="${message}"/>
	<div class="control-group">
		<label class="control-label">客户名称：</label>
		<div class="controls">
			<form:input path="customerName" htmlEscape="false" maxlength="64" class="input-xlarge "/>
		</div>
	</div>
	<div class="control-group">
		<label class="control-label">电话：</label>
		<div class="controls">
			<form:input path="phone" htmlEscape="false" maxlength="64" class="input-xlarge "/>
		</div>
	</div>
	<div class="control-group">
		<label class="control-label">订货时间：</label>
		<div class="controls">
			<input name="orderTime" type="text" readonly="readonly" maxlength="20" class="input-medium Wdate "
				   value="<fmt:formatDate value="${bizOrder.orderTime}" pattern="yyyy-MM-dd"/>"
				   onclick="WdatePicker({dateFmt:'yyyy-MM-dd',isShowClear:false});"/>
		</div>
	</div>

	<div class="control-group">
		<label class="control-label"></label>
		<div class="controls">
			<input id="addGoods" class="btn btn-primary" value="批量挑选商品" autocomplete="off"/>
		</div>
		</div>
	</div>
	<div class="control-group">
		<label class="control-label">商品明细：</label>
		<div class="controls demo" <%--id="editInfo"--%>>
			<table  id="thetable"
					class="table table-striped table-bordered table-condensed">
				<thead>
				<tr>
					<th>商品名称</th>
					<th>供货商</th>
					<th>项目</th>
					<th>规格</th>
					<th>单位</th>
					<th>库存数量</th>
					<th>领取数量</th>
					<shiro:hasPermission name="biz:buyingRequisition:buyingRequisitiondetailRemove"><th></th></shiro:hasPermission>
				</tr>
				</thead>
				<tbody id="goodsList">
				<c:forEach items="${orderDetailList}" var="bizOrderDetail" varStatus="status">
					<tr id="bizOrderDetail${status.index}">
						<td>${bizOrderDetail.projectdetailName}</td>
						<td>${bizOrderDetail.supplierName}</td>
						<td>${bizOrderDetail.goodsCategoryName}</td>
						<td>${bizOrderDetail.specification}</td>
						<td>${bizOrderDetail.unit}</td>
						<td>
							${bizOrderDetail.amountOfMoney}
						</td>
						<td>
							<input type="hidden" id="amountOfMoney${status.index}" name="bizOrderDetailList[${status.index}].amountOfMoney" value="${bizOrderDetail.amountOfMoney}" />
							<input type="hidden" id="projectdetailId${status.index}" name="bizOrderDetailList[${status.index}].projectdetailId" value="${bizOrderDetail.projectdetailId}" />
							<input type="hidden" id="projectdetailName${status.index}" name="bizOrderDetailList[${status.index}].projectdetailName" value="${bizOrderDetail.projectdetailName}" />
							<input type="hidden" id="code${status.index}" name="bizOrderDetailList[${status.index}].code" value="${bizOrderDetail.code}"/>
							<input type="hidden" id="specification${status.index}" name="bizOrderDetailList[${status.index}].specification" value="${bizOrderDetail.specification}"/>
							<input type="hidden" id="delFlag${status.index}" name="bizOrderDetailList[${status.index}].delFlag" value="${bizOrderDetail.delFlag}"/>
							<input type="text" class="digits required" style="width: 100px" onkeyup="errorCheck(${status.index})" id="quantity${status.index}" name="bizOrderDetailList[${status.index}].quantity" value="${bizOrderDetail.quantity}" />
							<span class="help-inline"><font color="red">*</font> </span>
						</td>
						<shiro:hasPermission name="biz:buyingRequisition:buyingRequisitiondetailRemove">
							<td class="text-center" width="10">
								<span class="close" onclick="delRow(${status.index})" title="删除">&times;</span>
							</td>
						</shiro:hasPermission>
					</tr>
				</c:forEach>

				</tbody>
			</table>
		</div>
	</div>
	<div class="control-group">
		<label class="control-label">收货地址：</label>
		<div class="controls">
			<form:textarea path="address" htmlEscape="false" rows="4"  class="input-xxlarge "/>
		</div>
	</div>
	<div class="control-group">
		<label class="control-label">备注：</label>
		<div class="controls">
			<form:textarea path="remarks" htmlEscape="false" rows="4" class="input-xxlarge "/>
		</div>
	</div>
	<div class="form-actions">
		<%--<shiro:hasPermission name="biz:bizOrder:tiJiao">--%>
			<%--<c:if test="${bizOrder.taskState == '0'}">--%>
				<%--<a href="${ctx}/biz/bizOrder/updateTaskState?id=${bizOrder.id}&taskState=1"  class="btn btn-primary" >提 交&nbsp;</a>--%>
			<%--</c:if>--%>
		<%--</shiro:hasPermission>--%>
		<shiro:hasPermission name="biz:bizOrder:shen">
			<c:if test="${bizOrder.taskState == '1'}">
				<a href="${ctx}/biz/bizOrder/updateTaskState?id=${bizOrder.id}&taskState=2" class="btn btn-primary" >通 过&nbsp;</a>
				<a href="${ctx}/biz/bizOrder/updateTaskState?id=${bizOrder.id}&taskState=3" class="btn btn-primary" >驳 回&nbsp;</a>
			</c:if>
		</shiro:hasPermission>

		<shiro:hasPermission name="biz:bizOrder:receiveListSave">
			<input id="btnSubmit" class="btn btn-primary" type="submit" value="保 存"/>&nbsp;
		</shiro:hasPermission>
		<input id="btnCancel" class="btn" type="button" value="返 回" onclick="history.go(-1)"/>
	</div>
</form:form>

<div class="modal fade bs-example-modal-lg" id="addGoodsModal"  tabindex="-99" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"
	 style="display:none;width: 850px;">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				批量挑选
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			</div>
			<div class="modal-body">
				<ul class="nav nav-tabs">
					<li class="active goodsDiv" onclick="showGoodsDiv(this.id)" id="list"><a href="#" class="disabled">商品列表</a></li>
					<%--<li class="goodsDiv" onclick="showGoodsDiv(this.id)" id="add"><a href="#" class="disabled">商品添加</a></li>--%>
				</ul>
				<div id="goodsListHtml">
					<form:form id="searchForm" modelAttribute="bizGoods" action="" method="post" class="breadcrumb form-search">
						<input id="
							pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
						<input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/>
						<ul class="ul-form">
							<li><label>商品编号：</label>
								<form:input path="goodsCode" htmlEscape="false" maxlength="64" class="input-medium"/>
							</li>
							<li><label>商品名称：</label>
								<form:input path="goodsName" htmlEscape="false" maxlength="64" class="input-medium"/>
							</li>
							<li class="btns"><input id="select" class="btn btn-primary" type="button" value="查询"/></li>
							<li class="clearfix"></li>
						</ul>
					</form:form>
					<sys:message content="${message}"/>
					<table id="contentTable" class="table table-striped table-bordered table-condensed">
						<thead>
						<tr>
							<th><input type="checkbox" id="selectAll" /></th>
							<th>商品编号</th>
							<th>商品名称</th>
							<th>供货商</th>
							<th>项目</th>
							<th>规格</th>
							<th>单位</th>
							<th>库存数量</th>
						</tr>
						</thead>
						<tbody id="goodsInfo">
						</tbody>
					</table>
					<div class="pagination" id="level"></div>
				</div>
			</div>
			<div></div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
				<button type="button" class="btn btn-primary" id="addGoodsToPurchase"> 添加</button>
			</div>
		</div>
	</div>
</div>
</body>
</html>