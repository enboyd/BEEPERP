<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>采购单管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
			$("#inputForm").validate({
				submitHandler: function (form) {
					loading('正在提交，请稍等...');
					form.submit();
				},
				errorContainer: "#messageBox",
				errorPlacement: function (error, element) {
					$("#messageBox").text("输入有误，请先更正。");
					if (element.is(":checkbox") || element.is(":radio") || element.parent().is(".input-append")) {
						error.appendTo(element.parent().parent());
					} else {
						error.insertAfter(element);
					}
				}
			});

            var outstandingAmount = $('#outstandingAmount').val();
            if(outstandingAmount == ""){
                $('#outstandingAmount').val($("#totalPrice").val());
            }

		})
        function total(){
            var totalPrice = $("#totalPrice").val();
			var settledAmount = $("#settledAmount").val();
			if(typeof settledAmount == "undefined"){
                settledAmount = 0;
			}
			var outstandingAmount = totalPrice - settledAmount;
			if(typeof outstandingAmount == "undefined"){
                outstandingAmount = totalPrice;
			}
			$('#outstandingAmount').val(outstandingAmount);
        }
	</script>
</head>
<body>
	<ul class="nav nav-tabs">
		<li><a href="${ctx}/biz/bizPurchase/listForFinance?type=${bizPurchase.type}">采购单列表</a></li>
		<li class="active"><a href="${ctx}/biz/bizPurchase/formForFinance?id=${bizPurchase.purchaseId}&type=${bizPurchase.type}">采购单<shiro:hasPermission name="biz:bizPurchase:edit">${not empty bizPurchase.purchaseId?'修改':'添加'}</shiro:hasPermission><shiro:lacksPermission name="biz:bizPurchase:edit">查看</shiro:lacksPermission></a></li>
	</ul><br/>
	<form:form id="inputForm" modelAttribute="bizPurchase" action="${ctx}/biz/bizPurchase/save?source=finance" method="post" class="form-horizontal">
		<form:hidden path="id"/>
		<sys:message content="${message}"/>
		<input type="hidden" id="goodsNum" value="${bizPurchaseDetailCount}">
		<div class="control-group">
			<label class="control-label"></label>
		</div>
		<div class="control-group">
			<label class="control-label">商品明细：</label>
			<div class="controls">
				<table id="contentTable"
					   class="table table-striped table-bordered table-condensed">
					<thead>
					<tr>
						<th>商品名称</th>
						<th>供货商</th>
						<th>商品类型</th>
						<th>库存数量</th>
						<th>采购中数量</th>
						<th>采购数量</th>
						<th>实际入库数量</th>
						<th>相差数量</th>
					</tr>
					</thead>
					<tbody id="goodsList">
							<c:forEach items="${bizPurchaseDetailList}" var="bizPurchaseDetail" varStatus="status">
								<tr id="bizPurchaseDetailList${status.index}">
									<td>${bizPurchaseDetail.goodsName}</td>
									<td>${bizPurchaseDetail.supplierName}</td>
									<td>${bizPurchaseDetail.goodsCategoryName}</td>
									<td>${bizPurchaseDetail.stockAmount}</td>
									<td>${bizPurchaseDetail.purchasingAmount}</td>
									<td>
										<input type="hidden" id="goodsId${status.index}" name="purchaseDetailList[${status.index}].goodsId" value="${bizPurchaseDetail.goodsId}">
										<input type="hidden" id="purchaseDetailId${status.index}" name="purchaseDetailList[${status.index}].purchaseDetailId" value="${bizPurchaseDetail.purchaseDetailId}">
										<input type="hidden" id="delFlag${status.index}" name="purchaseDetailList[${status.index}].delFlag" value="${bizPurchaseDetail.delFlag}">
										<input type="text" readonly class="number" style="width: 100px"  id="expertAmount${status.index}" name="purchaseDetailList[${status.index}].expertAmount" value="${bizPurchaseDetail.expertAmount}">
									</td>
									<td>
										<input type="text" readonly id="actualAmount${status.index}" class="number required" style="width: 100px" name="purchaseDetailList[${status.index}].actualAmount" value="${bizPurchaseDetail.actualAmount}">
									</td>
									<td>
										<input type="text" readonly id="differentAmount${status.index}" style="width: 100px" name="purchaseDetailList[${status.index}].differentAmount" value="${bizPurchaseDetail.differentAmount}">
									</td>
								</tr>
							</c:forEach>
					</tbody>
				</table>
			</div>
		</div>
		<!-- 采购单总金额 -->
		<div class="control-group">
			<label class="control-label">采购总额：</label>
			<div class="controls">
				<form:input path="totalPrice" htmlEscape="false" class="input-xlarge "/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">采购单类型：</label>
			<div class="controls">
				<form:select path="purchaseType" class="input-xlarge required" >
					<form:option value="" label="请选择" />
					<form:options items="${fns:getDictList('biz_purchase_type')}"
								  itemLabel="label" itemValue="value" htmlEscape="false" />
				</form:select>
				<span class="help-inline"><font color="red">*</font> </span>
			</div>
		</div>
		<!-- 已结算金额 -->
		<div class="control-group">
			<label class="control-label">已结算金额：</label>
			<div class="controls">
				<form:input path="settledAmount" onkeyup="total()" htmlEscape="false" class="input-xlarge "/>
			</div>
		</div>
		<!-- 待结算金额 -->
		<div class="control-group">
			<label class="control-label">待结算金额：</label>
			<div class="controls">
				<form:input path="outstandingAmount" htmlEscape="false" class="input-xlarge "/>
			</div>
		</div>

		<div class="control-group">
			<label class="control-label">备注信息：</label>
			<div class="controls">
				<form:textarea path="remarks" htmlEscape="false" rows="4" maxlength="255" class="input-xxlarge "/>
			</div>
		</div>
		<div style="margin-left: 650px">
			<h3>入库明细</h3>
		</div>
		<c:if test="${!empty bizStockInList}">
			<c:forEach items="${bizStockInList}" var="bizStockIn">
			<div class="control-group" style="margin-top: 20px">
				<label class="control-label"><fmt:formatDate value="${bizStockIn.stockInDate}" pattern="yyyy-MM-dd"/>：</label>
				<div class="controls">
					<table id="contentTable" class="table table-striped table-bordered table-condensed">
						<thead>
						<tr>
							<th>商品名称</th>
							<th>实际入库数量</th>
						</tr>
						</thead>
						<tbody>
						<c:forEach items="${bizStockIn.bizStockInDetailList}" var="bizStockInDetail">
						<tr>
							<td>${bizStockInDetail.goodsName}</td>
							<td>${bizStockInDetail.actualAmount}</td>
						</tr>
						</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
			</c:forEach>
		</c:if>
		<div colspan=5 class="text-center" style="margin-top: 50px">
			<input id="btnSubmit" class="btn btn-primary" type="submit" value="保 存"/>&nbsp;
			<input id="btnCancel" class="btn" type="button" value="返 回" onclick="history.go(-1)"/>
		</div>
	</form:form>
</body>
</html>