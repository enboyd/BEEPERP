<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>采购单管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {

		});
		function page(n,s){
			$("#pageNo").val(n);
			$("#pageSize").val(s);
			$("#searchForm").submit();
        	return false;
        }
        //日志的查看
        function showLog(purchaseId) {
            //获取日志
            $.ajax({
                type: "POST",
                url: "${ctx}/biz/bizPurchaseLog/getBizPurchaseLogListByPurchaseId",
                data: {
                    'purchaseId': purchaseId
                },
                success: function(data){
                    $("#logInfo").html("");
                    $("#logModal").modal('show');
                    var rows = "";
                    debugger
                    for (var i = 0 ; i < data.length ; i++){
                        var row = "<tr>"
                            + "<td>"+ data[i].createName+ "</td>"
                            + "<td>"+ data[i].type+"</td>"
                            + "<td>"+ data[i].createDate+"</td>"
                            + "</tr>";
                        rows += row;
                    }
                    $("#logInfo").html(rows);
                }
            });
        }
	</script>
</head>
<body>
	<ul class="nav nav-tabs">
		<li class="active"><a href="${ctx}/biz/bizPurchase/listForFinance">采购单列表</a></li>
	</ul>
	<form:form id="searchForm" modelAttribute="bizPurchase" action="${ctx}/biz/bizPurchase/listForFinance" method="post" class="breadcrumb form-search">
		<input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
		<input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/>
		<ul class="ul-form">
			<li><label>采购单编号：</label>
				<form:input path="purchaseCode" htmlEscape="false" maxlength="24" class="input-medium"/>
			</li>
			<li><label>采购日期：</label>
				<input name="beginPurchaseDate" type="text" readonly="readonly" maxlength="20" class="input-medium Wdate"
					value="<fmt:formatDate value="${bizPurchase.beginPurchaseDate}" pattern="yyyy-MM-dd"/>"
					onclick="WdatePicker({dateFmt:'yyyy-MM-dd',isShowClear:false});"/> -
				<input name="endPurchaseDate" type="text" readonly="readonly" maxlength="20" class="input-medium Wdate"
					value="<fmt:formatDate value="${bizPurchase.endPurchaseDate}" pattern="yyyy-MM-dd"/>"
					onclick="WdatePicker({dateFmt:'yyyy-MM-dd',isShowClear:false});"/>
			</li>
			<li><label>财务状态：</label>
				<form:select path="financialStatus" class="input-medium" >
					<form:option value="" label="请选择" />
					<form:options items="${fns:getDictList('biz_financial_status')}" itemLabel="label" itemValue="value" htmlEscape="false" />
				</form:select>
			</li>
			<li class="btns"><input id="btnSubmit" class="btn btn-primary" type="submit" value="查询"/></li>
			<li class="clearfix"></li>
		</ul>
	</form:form>
	<sys:message content="${message}"/>
	<table id="contentTable" class="table table-striped table-bordered table-condensed">
		<thead>
			<tr>
				<th>采购单编号</th>
				<th>采购日期</th>
				<th>采购状态</th>
				<th>采购单类型</th>
				<th>财务状态</th>
				<th>采购总额</th>
				<th>备注信息</th>
				<shiro:hasAnyPermissions name="biz:bizPurchase:edit,biz:bizPurchase:remove,biz:bizPurchase:flatSheet"><th>操作</th></shiro:hasAnyPermissions>
			</tr>
		</thead>
		<tbody>
		<c:forEach items="${page.list}" var="bizPurchase">
			<tr>
				<td>
					<a href="${ctx}/biz/bizPurchase/formForFinance?id=${bizPurchase.purchaseId}">${bizPurchase.purchaseCode}</a>
				</td>
				<td><fmt:formatDate value="${bizPurchase.purchaseDate}" pattern="yyyy-MM-dd"/></td>
				<td>${fns:getDictLabel(bizPurchase.status, 'biz_purchase_status', '')}</td>
				<td>${fns:getDictLabel(bizPurchase.purchaseType, 'biz_purchase_type', '')}</td>
				<td>${fns:getDictLabel(bizPurchase.financialStatus, 'biz_financial_status', '')}</td>
				<td>${bizPurchase.totalPrice}</td>
				<td>${bizPurchase.remarks}</td>
				<shiro:hasAnyPermissions name="biz:bizPurchase:edit,biz:bizPurchase:remove,biz:bizPurchase:flatSheet"><td>
					<shiro:hasPermission name="biz:bizPurchase:edit">
						<a href="${ctx}/biz/bizPurchase/formForFinance?id=${bizPurchase.purchaseId}">修改</a>
					</shiro:hasPermission>
					<shiro:hasPermission name="biz:bizPurchase:log">
						<a href="#" id="level" onclick="showLog(${bizPurchase.purchaseId})">日志</a>
					</shiro:hasPermission>
				</td></shiro:hasAnyPermissions>
			</tr>
		</c:forEach>
		</tbody>
	</table>
	<div class="pagination">${page}</div>

	<%-- 日志的modal --%>
	<div class="modal fade" id="logModal" tabindex="-99" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display:none;width: 700px;">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					采购单日志
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				</div>
				<div class="modal-body">
					<table border="1" cellspacing="0" cellpadding="0" style="width: 100%">
						<thead>
						<tr>
							<th>操作人</th>
							<th>日志</th>
							<th>时间</th>
						</tr>
						</thead>
						<tbody id="logInfo">
						</tbody>
					</table>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
				</div>
			</div>
		</div>
	</div>
</body>
</html>