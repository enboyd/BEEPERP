<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>正常出库信息</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
            /* 导出 */
            $("#btnExport").click(function(){
                top.$.jBox.confirm("确认要导出数据吗？", "系统提示", function(v,h,f){
                    if(v=="ok"){
                        $("#searchForm").attr("action","${ctx}/biz/rpt/BizNormalOutRpt/export");
                        $("#searchForm").submit();
                        loading("正在将文件打包，请耐心等待，勿刷新页面...");
                    }
                },{buttonsFocus:1});
            });
		});
		function page(n,s){
			$("#pageNo").val(n);
			$("#pageSize").val(s);
			$("#searchForm").submit();
        	return false;
        }

	</script>
</head>
<body>
	<ul class="nav nav-tabs">
		<shiro:hasPermission name="biz:BizNormalOutRpt:view">
			<li  class="active"><a href="${ctx}/biz/rpt/BizNormalOutRpt/list">正常出库信息</a></li>
		</shiro:hasPermission>
	</ul>
	<form:form id="searchForm" modelAttribute="bizNormalOutGoing" action="${ctx}/biz/rpt/BizNormalOutRpt/list" method="post" class="breadcrumb form-search">
		<input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
		<input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/>
		<sys:tableSort id="orderBy" name="orderBy" value="${page.orderBy}" callback="page();"/>
		<ul class="ul-form">
			<li><label>商品编号：</label>
				<form:input path="goodsCode" htmlEscape="false" maxlength="64" class="input-medium"/>
			</li>
			<li>
				<label>商品分类：</label>
				<sys:treeselect id="classifyId" name="classifyId" value="${bizNormalOutGoing.classifyId}"
								labelName="classifyName" labelValue="${bizNormalOutGoing.classifyName}"  title="商品分类"
								url="/biz/bizGoodsClassify/treeData?isEnabled=true" cssClass="input-medium" allowClear="true" notAllowSelectParent="true" />
			</li>
			<li><label>出库时间：</label>
				<input autocomplete = "off" name="beginOutDate" type="text" maxlength="20" class="input-medium Wdate"
					   value="<fmt:formatDate value='${bizNormalOutGoing.beginOutDate}' pattern='yyyy-MM-dd'/>"
					   onclick="WdatePicker({dateFmt:'yyyy-MM-dd',isShowClear:false});" />
				-
				<input autocomplete = "off" name="endOutDate" type="text" maxlength="20" class="input-medium Wdate"
					   value="<fmt:formatDate value='${bizNormalOutGoing.endOutDate}' pattern='yyyy-MM-dd'/>"
					   onclick="WdatePicker({dateFmt:'yyyy-MM-dd',isShowClear:false});" />
			</li>
			<li class="btns"><input id="btnSubmit" class="btn btn-primary" type="submit" value="查询"/></li>
			<shiro:hasPermission name="sm:BizNormalOutRpt:export">
				<li class="btns"><input id="btnExport" class="btn btn-primary" type="button" value="导出"/></li>
			</shiro:hasPermission>
		</ul>
	</form:form>
	<sys:message content="${message}"/>
	<table id="contentTable" class="table table-striped table-bordered table-condensed">
		<thead>
			<tr>
				<th class="sort-column orderCode">单号</th>
				<td>客户</td>
				<td>身份证号码</td>
				<th class="sort-column bbl.create_date">出库日期</th>
				<th>条码号</th>
				<th class="sort-column goodsCode">存货编码</th>
				<th>存货名称</th>
				<th>规格</th>
				<th>商品分类</th>
				<th>批次</th>
				<th>出库单价</th>
				<th class="sort-column number">数量</th>
				<th class="sort-column storage_location_name">货位</th>
				<th>操作人</th>
			</tr>
		</thead>
		<tbody>
		<c:forEach items="${page.list}" var="bizOut">
			<tr>
				<td>${bizOut.orderCode}</td>
				<td>${bizOut.customerName}</td>
				<td>${bizOut.idNumber}</td>
				<td><fmt:formatDate value="${bizOut.outDate}" pattern="yyyy-MM-dd"/></td>
				<td>${bizOut.barcode}</td>
				<td>${bizOut.goodsCode}</td>
				<td>${bizOut.goodsName}</td>
				<td>${bizOut.specification}</td>
				<td>${bizOut.classifyName}</td>
				<td>${bizOut.batch}</td>
				<td>${bizOut.unitPrice}</td>
				<td>${bizOut.number}</td>
				<td>${bizOut.storageLocName}</td>
				<td>${bizOut.operatorName}</td>
			</tr>
		</c:forEach>
		</tbody>
	</table>
	<table id="contentTable" class="table table-striped ">
		<tbody>
		<tr>
			<td width="11%"></td>
			<td width="11%"></td>
			<td width="11%"></td>
			<td width="11%"></td>
			<td width="11%"></td>
			<td width="11%"></td>
			<td width="12%"></td>
			<td>总数：${sumNum}</td>
			<td width="11%"></td>
		</tr>
		</tbody>
	</table>
	<div class="pagination">${page}</div>
</body>
</html>