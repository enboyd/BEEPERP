<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>货位现存量报表</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
			/* 导出 */
			$("#btnExport").click(function(){
				top.$.jBox.confirm("确认要导出数据吗？", "系统提示", function(v,h,f){
					if(v=="ok"){
						$("#searchForm").attr("action","${ctx}/biz/rpt/bizBarcode/export?type=2");
						$("#searchForm").submit();
						loading("正在将文件打包，请耐心等待，勿刷新页面...");
					}
				},{buttonsFocus:1});
			});
		});
		function page(n,s){
			$("#pageNo").val(n);
			$("#pageSize").val(s);
			$("#searchForm").submit();
        	return false;
        }

	</script>
</head>
<body>
	<ul class="nav nav-tabs">
		<li  class="active"><a href="${ctx}/biz/rpt/bizBarcode/list?type=2">货位现存量报表</a></li>
	</ul>
	<form:form id="searchForm" modelAttribute="bizBarcodeRpt" action="${ctx}/biz/rpt/bizBarcode/list?type=2" method="post" class="breadcrumb form-search">
		<input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
		<input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/>
		<sys:tableSort id="orderBy" name="orderBy" value="${page.orderBy}" callback="page();"/>
		<ul class="ul-form">
			<li><label>商品编号：</label>
				<form:input path="goodsCode" htmlEscape="false" maxlength="64" class="input-medium"/>
			</li>
			<li>
				<label>商品分类：</label>
				<sys:treeselect id="classifyId" name="classifyId" value="${bizBarcodeRpt.classifyId}"
								labelName="classifyName" labelValue="${bizBarcodeRpt.classifyName}"  title="商品分类"
								url="/biz/bizGoodsClassify/treeData?isEnabled=true" cssClass="input-medium" allowClear="true" notAllowSelectParent="true" />
			</li>
			<li class="btns"><input id="btnSubmit" class="btn btn-primary" type="submit" value="查询"/></li>
			<shiro:hasPermission name="sm:bizBarcodeRpt:export">
				<li class="btns"><input id="btnExport" class="btn btn-primary" type="button" value="导出"/></li>
			</shiro:hasPermission>
			<li class="clearfix"></li>
		</ul>
	</form:form>
	<sys:message content="${message}"/>
	<table id="contentTable" class="table table-striped table-bordered table-condensed">
		<thead>
			<tr>
				<th class="sort-column goods_code">商品编号</th>
				<th class="sort-column goods_name">商品名称</th>
				<th>商品分类</th>
				<th>规格型号</th>
				<th>单位</th>
				<th class="sort-column manufacture_date">生产日期</th>
				<th class="sort-column pici">批次</th>
				<th class="sort-column sl.name">仓库名称</th>
				<th class="sort-column number">可用数量</th>
			</tr>
		</thead>
		<tbody>
		<c:forEach items="${page.list}" var="bizBarcodeRpt">
			<tr>
				<td>${bizBarcodeRpt.goodsCode}</td>
				<td>${bizBarcodeRpt.goodsName}</td>
				<td>${bizBarcodeRpt.classifyName}</td>
				<td>${bizBarcodeRpt.specification}</td>
				<td>${bizBarcodeRpt.unit}</td>
				<td>${bizBarcodeRpt.manufactureDate}</td>
				<td>${bizBarcodeRpt.pici}</td>
				<td>${bizBarcodeRpt.storageLocationName}</td>
				<td>${bizBarcodeRpt.number}</td>
			</tr>
		</c:forEach>
		</tbody>
	</table>
	<table id="contentTable" class="table table-striped ">
		<tbody>
		<tr>
			<td width="13%"></td>
			<td width="13%"></td>
			<td width="13%"></td>
			<td width="13%"></td>
			<td width="13%"></td>
			<td width="13%"></td>
			<td width="13%"></td>
			<td>总数：${sum}</td>
		</tr>
		</tbody>
	</table>
	<div class="pagination">${page}</div>
</body>
</html>