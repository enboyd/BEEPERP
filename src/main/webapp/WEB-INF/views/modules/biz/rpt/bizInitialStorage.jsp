<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>期初入库信息</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
        $(document).ready(function() {
            /* 导出 */
            $("#btnExport").click(function(){
                top.$.jBox.confirm("确认要导出数据吗？", "系统提示", function(v,h,f){
                    if(v=="ok"){
                        $("#searchForm").attr("action","${ctx}/biz/rpt/bizInitialStorage/export");
                        $("#searchForm").submit();
                        loading("正在将文件打包，请耐心等待，勿刷新页面...");
                    }
                },{buttonsFocus:1});
            });
        });
        function page(n,s){
            $("#pageNo").val(n);
            $("#pageSize").val(s);
            $("#searchForm").submit();
            return false;
        }

	</script>
</head>
<body>
<ul class="nav nav-tabs">
	<shiro:hasPermission name="biz:bizInitialStorage:view">
		<li  class="active"><a href="${ctx}/biz/rpt/bizInitialStorage/list">期初入库信息</a></li>
	</shiro:hasPermission>
</ul>
<form:form id="searchForm" modelAttribute="bizInitialStorage" action="${ctx}/biz/rpt/bizInitialStorage/list" method="post" class="breadcrumb form-search">
	<input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
	<input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/>
	<sys:tableSort id="orderBy" name="orderBy" value="${page.orderBy}" callback="page();"/>
	<ul class="ul-form">
		<li><label>商品编号：</label>
			<form:input path="goodsCode" htmlEscape="false" maxlength="64" class="input-medium"/>
		</li>
		<li>
			<label>商品分类：</label>
			<sys:treeselect id="classifyId" name="classifyId" value="${bizInitialStorage.classifyId}"
							labelName="classifyName" labelValue="${bizInitialStorage.classifyName}"  title="商品分类"
							url="/biz/bizGoodsClassify/treeData?isEnabled=true" cssClass="input-medium" allowClear="true" notAllowSelectParent="true" />
		</li>
		<li><label>入库时间：</label>
			<input autocomplete = "off" name="stockInDateStart" type="text" maxlength="20" class="input-medium Wdate"
				   value="<fmt:formatDate value='${bizInitialStorage.stockInDateStart}' pattern='yyyy-MM-dd'/>"
				   onclick="WdatePicker({dateFmt:'yyyy-MM-dd',isShowClear:false});" />
			-
			<input autocomplete = "off" name="stockInDateEnd" type="text" maxlength="20" class="input-medium Wdate"
				   value="<fmt:formatDate value='${bizInitialStorage.stockInDateEnd}' pattern='yyyy-MM-dd'/>"
				   onclick="WdatePicker({dateFmt:'yyyy-MM-dd',isShowClear:false});" />
		</li>
		<li class="btns"><input id="btnSubmit" class="btn btn-primary" type="submit" value="查询"/></li>
		<shiro:hasPermission name="sm:bizInitialStorage:export">
			<li class="btns"><input id="btnExport" class="btn btn-primary" type="button" value="导出"/></li>
		</shiro:hasPermission>
	</ul>
</form:form>
<sys:message content="${message}"/>
<table id="contentTable" class="table table-striped table-bordered table-condensed">
	<thead>
	<tr>
		<th class="sort-column bsl.name">单号</th>
		<th class="sort-column bsl.create_date">入库日期</th>
		<th>条码号</th>
		<th class="sort-column bg.goods_code">存货编码</th>
		<th>存货名称</th>
		<th>商品分类</th>
		<th>批次</th>
		<th class="sort-column number">数量</th>
		<th class="sort-column bsl.storage_location_name">货位</th>
		<th>操作人</th>
	</tr>
	</thead>
	<tbody>
	<c:forEach items="${page.list}" var="bizIn">
		<tr>
			<td>${bizIn.stockInCode}</td>
			<td><fmt:formatDate value="${bizIn.inDate}" pattern="yyyy-MM-dd"/></td>
			<td>${bizIn.barcode}</td>
			<td>${bizIn.goodsCode}</td>
			<td>${bizIn.goodsName}</td>
			<td>${bizIn.classifyName}</td>
			<td>${bizIn.batch}</td>
			<td>${bizIn.number}</td>
			<td>${bizIn.storageLocName}</td>
			<td>${bizIn.operatorName}</td>
		</tr>
	</c:forEach>
	</tbody>
</table>
<table id="contentTable" class="table table-striped ">
	<tbody>
	<tr>
		<td width="11%"></td>
		<td width="11%"></td>
		<td width="11%"></td>
		<td width="11%"></td>
		<td width="11%"></td>
		<td width="11%"></td>
		<td width="10%"></td>
		<td>总数：${sumNum}</td>
		<td width="11%"></td>
	</tr>
	</tbody>
</table>
<div class="pagination">${page}</div>
</body>
</html>