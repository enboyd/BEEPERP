<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
  <title>商品管理</title>
  <meta name="decorator" content="default"/>
  <script type="text/javascript">
      $(document).ready(function() {
          //$("#name").focus();
          $("#inputForm").validate({
              submitHandler: function(form){
                  loading('正在提交，请稍等...');
                  form.submit();
              },
              errorContainer: "#messageBox",
              errorPlacement: function(error, element) {
                  $("#messageBox").text("输入有误，请先更正。");
                  if (element.is(":checkbox")||element.is(":radio")||element.parent().is(".input-append")){
                      error.appendTo(element.parent().parent());
                  } else {
                      error.insertAfter(element);
                  }
              }
          });

          //添加商品品牌
          $("#addGoodsBrand").click(function () {
              $("#goodsBrandModal").modal('show');
              $("#goodsBrandError").html("");
          })
          //添加商品品牌的ajax
          $("#addGoodsBrandModal").click(function () {
              var goodsBrand = $("#goodsBrand").val();
              if(null == goodsBrand || "" == goodsBrand){
                  $("#goodsBrandError").html("请输入商品品牌！");
              }else{
                  $.ajax({
                      type: "POST",
                      url: "${ctx}/biz/bizGoodsBrand/saveBizGoodsBrand",
                      data: {
                          'goodsBrandName': goodsBrand
                      },
                      success: function(data){
                          $("#goodsBrandModal").modal('hide');
                          top.$.jBox.info(data,'提示');
                          //重新加载商品的品牌
                          $.ajax({
                              type: "POST",
                              url: "${ctx}/biz/bizGoodsBrand/findBizGoodsBrandList",
                              success: function(data){
                                  $("#goodsBrandId").html("");
                                  //动态赋值
                                  for (var i = 0;i < data.length;i++){
                                      $("#goodsBrandId").append("<option value='"+data[i].goodsBrandId+"'>"+data[i].goodsBrandName+"</option>");
                                  }
                              }
                          });
                      }
                  });
              }
          })

          /*$("#btnSubmit").click(function(){
              //表单校验 TODO
              //提交表单
              $.ajax({
                  type: "POST",
                  url: "${ctx}/biz/bizGoods/ajaxSave",
                  data:$("#inputForm").serialize(),
                  dataType: "text",
                  success: function(data){
                      //调用ajax查询商品列表
                      if(data == 'success'){
                          top.$.jBox.tip("${'保存成功'}","${'success'}",{persistent:true,opacity:0});
                          window.location.href = '${ctx}/biz/bizGoods/';
                      }else if(data == 'codeRepeat'){
                          top.$.jBox.tip("${'保存失败，商品编码已存在'}","${'error'}",{persistent:true,opacity:0});
                      }else if(data == 'codeIsNull'){
                          top.$.jBox.tip("${'保存失败,商品编码不允许为空！'}","${'error'}",{persistent:true,opacity:0});
                      }else{
                          top.$.jBox.tip("${'保存失败'}","${'error'}",{persistent:true,opacity:0});
                      }
                  }
              })
          });*/
      });

      //检查商品编码重复
      function checkRepeat(){
          var goodsCode = $("#goodsCode").val();
          $.ajax({
              type: "POST",
              url: "${ctx}/biz/bizGoods/checkCodeRepeat",
              data:{
                  'goodsCode': goodsCode
              },
              dataType: "text",
              success: function(data){
                  //调用ajax查询商品列表
                  if(data == 'codeRepeat'){
                      $("#codeFont").html(" * 商品编码重复");
                      $("#btnSubmit").attr("disabled", true)
                  }else{
                      $("#btnSubmit").attr("disabled", false);
                      $("#codeFont").html("*")
                  }
              }
          })
      }
  </script>
</head>
<body>
<ul class="nav nav-tabs">
  <li><a href="${ctx}/biz/bizGoods/">商品列表</a></li>
  <li class="active"><a href="${ctx}/biz/bizGoods/form?id=${bizGoods.goodsId}">商品<shiro:hasPermission name="biz:bizGoods:edit">${not empty bizGoods.goodsId?'修改':'添加'}</shiro:hasPermission><shiro:lacksPermission name="biz:bizGoods:edit">查看</shiro:lacksPermission></a></li>
</ul><br/>
<form:form id="inputForm" modelAttribute="bizGoods" action="${ctx}/biz/bizGoods/save" method="post" class="form-horizontal">
  <form:hidden path="id"/>
  <sys:message content="${message}"/>
  <div class="control-group">
    <label class="control-label">商品编码：</label>
    <div class="controls">
      <form:input path="goodsCode" onkeyup="checkRepeat()" htmlEscape="false" maxlength="64" class="input-xlarge required"/>
      <span class="help-inline"><font id="codeFont" color="red">*</font> </span>
    </div>
  </div>
  <div class="control-group">
    <label class="control-label">商品名称：</label>
    <div class="controls">
      <form:input path="goodsName" htmlEscape="false" maxlength="64" class="input-xlarge required"/>
      <span class="help-inline"><font color="red">*</font> </span>
    </div>
  </div>
  <div class="control-group">
    <label class="control-label">供应商：</label>
    <div class="controls">
      <form:select path="supplierId" class="input-xlarge required">
        <form:option value="" label=""/>
        <c:forEach items="${bizSupplierList}" varStatus="bizSupplier">
          <c:set var="et" value="${bizSupplier.current}"/>
          <c:if test="${bizGoods.supplierId eq et.supplierId}">
            <option value="${et.supplierId}" selected>${et.supplierName}</option>
          </c:if>
          <c:if test="${bizGoods.supplierId != et.supplierId}">
            <option value="${et.supplierId}">${et.supplierName}</option>
          </c:if>
        </c:forEach>
      </form:select>
      <span class="help-inline"><font color="red">*</font> </span>
    </div>
  </div>
  <div class="control-group">
    <label class="control-label">项目：</label>
    <div class="controls">
      <sys:treeselect id="goodsCategoryId" name="goodsCategoryId" value="${bizGoods.goodsCategoryId}"
                      labelName="goodsCategoryName" labelValue="${bizGoods.goodsCategoryName}" title="项目"
                      url="/biz/bizGoodsCategory/treeData?isEnabled=true"  extId="0" cssClass=""
                      allowClear="true" checked="true" notAllowSelectParent="false"/>
    </div>
  </div>
  <div class="control-group">
    <label class="control-label">商品分类：</label>
    <div class="controls">
      <sys:treeselect id="classifyId" name="classifyId" value="${bizGoods.classifyId}"
                      labelName="classifyName" labelValue="${bizGoods.classifyName}" title="商品分类"
                      url="/biz/bizGoodsClassify/treeData?isEnabled=true"  extId="0" cssClass="required"
                      allowClear="true"/>
      <span class="help-inline"><font color="red">*</font> </span>
    </div>
  </div>


  <div class="control-group">
    <label class="control-label">仓库：</label>
    <div class="controls">
      <form:select path="storageId" class="input-xlarge required">
        <form:option value="" label=""/>
        <c:forEach items="${bizStorageList}" varStatus="bizStorageList">
          <c:set var="et" value="${bizStorageList.current}"/>
          <c:if test="${bizGoods.storageId eq et.storageId}">
            <option value="${et.storageId}" selected>${et.storageName}</option>
          </c:if>
          <c:if test="${bizGoods.storageId != et.storageId}">
            <option value="${et.storageId}">${et.storageName}</option>
          </c:if>
        </c:forEach>
      </form:select>
      <span class="help-inline"><font color="red">*</font> </span>
    </div>
  </div>
  <div class="control-group">
    <label class="control-label">规格：</label>
    <div class="controls">
      <form:input path="specification" htmlEscape="false" maxlength="64" class="input-xlarge"/>
    </div>
  </div>
  <div class="control-group">
    <label class="control-label">单位：</label>
    <div class="controls">
      <form:input path="unit" htmlEscape="false" maxlength="10" class="input-xlarge required"/>
      <span class="help-inline"><font color="red">*</font> </span>
    </div>
  </div>
  <div class="control-group">
    <label class="control-label">保质期/月：</label>
    <div class="controls">
      <form:input path="expirationDate" htmlEscape="false" maxlength="10" class="input-xlarge digits"/>
    </div>
  </div>
  <div class="control-group">
    <label class="control-label">预警数量：</label>
    <div class="controls">
      <form:input path="warningAmount" htmlEscape="false" maxlength="10" class="input-xlarge  digits"/>
      <span class="help-inline"><font color="red">设置为0则不预警</font></span>
    </div>
  </div>
  <div class="control-group">
    <label class="control-label">最小起购数量：</label>
    <div class="controls">
      <form:input path="minPurchaseQuantity" htmlEscape="false" maxlength="10" class="input-xlarge digits required"/>
      <span class="help-inline"><font color="red">*</font> </span>
    </div>
  </div>
  <div class="control-group">
    <label class="control-label">备注信息：</label>
    <div class="controls">
      <form:textarea path="remarks" htmlEscape="false" rows="4" maxlength="255" class="input-xxlarge "/>
    </div>
  </div>
  <div class="form-actions">
    <shiro:hasAnyPermissions name="biz:bizGoods:edit,">
      <input id="btnSubmit" class="btn btn-primary" type="submit" value="保 存"/>&nbsp;
    </shiro:hasAnyPermissions>
    <input id="btnCancel" class="btn" type="button" value="返 回" onclick="history.go(-1)"/>
  </div>
</form:form>
</body>
</html>