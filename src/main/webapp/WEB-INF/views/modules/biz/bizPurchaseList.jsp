<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>采购单管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
            /* 导出 */
            $("#btnExport").click(function(){
                top.$.jBox.confirm("确认要导出数据吗？", "系统提示", function(v,h,f){
                    if(v=="ok"){
                        $("#searchForm").attr("action","${ctx}/biz/bizPurchase/export");
                        $("#searchForm").submit();
                        loading("正在将文件打包，请耐心等待，勿刷新页面...");
                    }
                },{buttonsFocus:1});
            });
		});
		function page(n,s){
			$("#pageNo").val(n);
			$("#pageSize").val(s);
			$("#searchForm").submit();
        	return false;
        }

		//日志的查看
		function showLog(purchaseId) {
			//获取日志
			$.ajax({
				type: "POST",
				url: "${ctx}/biz/bizPurchaseLog/getBizPurchaseLogListByPurchaseId",
				data: {
					'purchaseId': purchaseId
				},
				success: function(data){
					$("#logInfo").html("");
					$("#logModal").modal('show');
					var rows = "";
					for (var i = 0 ; i < data.length ; i++){
						var row = "<tr>"
								+ "<td>"+ data[i].createName+ "</td>"
								+ "<td>"+ data[i].type+"</td>"
								+ "<td>"+ data[i].createDate+"</td>"
								+ "</tr>";
						rows += row;
					}
					$("#logInfo").html(rows);
					closeLoading();
				}
			});
		}
	</script>
</head>
<body>
	<ul class="nav nav-tabs">
		<li class="active"><a href="${ctx}/biz/bizPurchase/">采购单列表</a></li>
		<shiro:hasPermission name="biz:bizPurchase:edit"><li><a href="${ctx}/biz/bizPurchase/form">采购单添加</a></li></shiro:hasPermission>
	</ul>
	<form:form id="searchForm" modelAttribute="bizPurchase" action="${ctx}/biz/bizPurchase/" method="post" class="breadcrumb form-search">
		<input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
		<input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/>
		<ul class="ul-form">
			<li><label>采购单编号：</label>
				<form:input path="purchaseCode" htmlEscape="false" maxlength="24" class="input-medium"/>
			</li>
			<li><label>请购单编号：</label>
				<form:input path="buyingCode" htmlEscape="false" maxlength="24" class="input-medium"/>
			</li>
			<li><label>采购日期：</label>
				<input name="beginPurchaseDate" type="text" readonly="readonly" maxlength="20" class="input-medium Wdate"
					value="<fmt:formatDate value="${bizPurchase.beginPurchaseDate}" pattern="yyyy-MM-dd"/>"
					onclick="WdatePicker({dateFmt:'yyyy-MM-dd',isShowClear:false});"/> -
				<input name="endPurchaseDate" type="text" readonly="readonly" maxlength="20" class="input-medium Wdate"
					value="<fmt:formatDate value="${bizPurchase.endPurchaseDate}" pattern="yyyy-MM-dd"/>"
					onclick="WdatePicker({dateFmt:'yyyy-MM-dd',isShowClear:false});"/>
			</li>
			<li><label>采购状态：</label>
				<form:select path="status" class="input-medium" >
					<form:option value="" label="请选择" />
					<form:options items="${fns:getDictList('biz_purchase_status')}" itemLabel="label" itemValue="value" htmlEscape="false" />
				</form:select>
			</li>
			<li class="btns"><input id="btnSubmit" class="btn btn-primary" type="submit" value="查询"/></li>
			<shiro:hasPermission name="sm:bizPurchase:export">
				<li class="btns"><input id="btnExport" class="btn btn-primary" type="button" value="导出"/></li>
			</shiro:hasPermission>
			<li class="clearfix"></li>
		</ul>
	</form:form>
	<sys:message content="${message}"/>
	<table id="contentTable" class="table table-striped table-bordered table-condensed">
		<thead>
			<tr>
				<th>采购单编号</th>
				<th>请购单编号</th>
				<th>采购日期</th>
				<th>采购状态</th>
				<th>采购单类型</th>
				<th>采购总额</th>
				<th>审核状态</th>
				<shiro:hasAnyPermissions name="biz:bizPurchase:edit,biz:bizPurchase:remove,biz:bizPurchase:flatSheet"><th>操作</th></shiro:hasAnyPermissions>
			</tr>
		</thead>
		<tbody>
		<c:forEach items="${page.list}" var="bizPurchase">
			<tr>
				<td>
					<a href="${ctx}/biz/bizPurchase/form?id=${bizPurchase.purchaseId}">${bizPurchase.purchaseCode}</a>
				</td>
				<td>
					<a href="${ctx}/biz/bizBuyingRequisition/form?buyingId=${bizPurchase.buyingId}">${bizPurchase.buyingCode}</a>
				</td>
				<td>
					<fmt:formatDate value="${bizPurchase.purchaseDate}" pattern="yyyy-MM-dd"/>
				</td>
				<td>
					${fns:getDictLabel(bizPurchase.status, 'biz_purchase_status', '')}
				</td>
				<td>${fns:getDictLabel(bizPurchase.purchaseType, 'biz_purchase_type', '')}</td>
				<td>
					${bizPurchase.totalPrice}
				</td>
				<td>
					<c:if test="${(bizPurchase.auditStatus eq '10') || (bizPurchase.auditStatus eq '20') || (bizPurchase.auditStatus eq '40')}">
						<span style="color: red">${fns:getDictLabel(bizPurchase.auditStatus, 'biz_purchase_auditStatus','')}</span>
					</c:if>
					<c:if test="${(bizPurchase.auditStatus eq '30')}">
						<span style="color: green">${fns:getDictLabel(bizPurchase.auditStatus, 'biz_purchase_auditStatus','')}</span>
					</c:if>
				</td>
				<shiro:hasAnyPermissions name="biz:bizPurchase:edit,biz:bizPurchase:remove,biz:bizPurchase:flatSheet,
										biz:bizPurchase:submit,biz:bizPurchase:toExamine"><td>
					<shiro:hasPermission name="biz:bizPurchase:edit,biz:bizPurchase:reject,biz:bizPurchase:resubmit">
						<a href="${ctx}/biz/bizPurchase/form?id=${bizPurchase.purchaseId}">修改</a>
					</shiro:hasPermission>
					<shiro:hasPermission name="biz:bizPurchase:remove">
						<a href="${ctx}/biz/bizPurchase/delete?id=${bizPurchase.purchaseId}" onclick="return confirmx('确认要删除该采购单吗？', this.href)">删除</a>
					</shiro:hasPermission>
					<c:if test="${bizPurchase.status != '12'}">
						<shiro:hasPermission name="biz:bizPurchase:flatSheet">
							<a href="${ctx}/biz/bizPurchase/flatSheet?id=${bizPurchase.purchaseId}&source=list" onclick="return confirmx('是否确定平单？', this.href)">平单</a>
						</shiro:hasPermission>
					</c:if>
					<c:if test="${bizPurchase.auditStatus eq '10'}">
						<shiro:hasPermission name="biz:bizPurchase:submit">
							<a href="${ctx}/biz/bizPurchase/updateAuditStatus?auditStatus=20&purchaseId=${bizPurchase.purchaseId}">提交</a>
						</shiro:hasPermission>
					</c:if>
					<c:if test="${bizPurchase.auditStatus eq '20'}">
						<shiro:hasPermission name="biz:bizPurchase:withdraw">
							<a href="${ctx}/biz/bizPurchase/updateAuditStatus?auditStatus=10&purchaseId=${bizPurchase.purchaseId}">撤回</a>
						</shiro:hasPermission>
						<shiro:hasPermission name="biz:bizPurchase:toExamine">
							<a href="${ctx}/biz/bizPurchase/updateAuditStatus?auditStatus=30&purchaseId=${bizPurchase.purchaseId}">审批通过</a>
						</shiro:hasPermission>
						<shiro:hasPermission name="biz:bizPurchase:reject">
							<a href="${ctx}/biz/bizPurchase/updateAuditStatus?auditStatus=40&purchaseId=${bizPurchase.purchaseId}">驳回</a>
						</shiro:hasPermission>
					</c:if>
					<c:if test="${bizPurchase.auditStatus eq '40'}">
						<shiro:hasPermission name="biz:bizPurchase:resubmit">
							<a href="${ctx}/biz/bizPurchase/updateAuditStatus?auditStatus=20&purchaseId=${bizPurchase.purchaseId}">重新提交</a>
						</shiro:hasPermission>
					</c:if>
					<shiro:hasPermission name="biz:bizPurchase:log">
						<a href="#" id="${bizPurchase.purchaseId}" onclick="showLog(this.id)">日志</a>
					</shiro:hasPermission>
				</td></shiro:hasAnyPermissions>
			</tr>
		</c:forEach>
		</tbody>
	</table>
	<div class="pagination">${page}</div>

	<%-- 日志的modal --%>
	<div class="modal fade" id="logModal" tabindex="-99" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display:none;width: 700px;">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					采购单日志
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				</div>
				<div class="modal-body">
					<table border="1" cellspacing="0" cellpadding="0" style="width: 100%">
						<thead>
						<tr>
							<th>操作人</th>
							<th>日志</th>
							<th>时间</th>
						</tr>
						</thead>
						<tbody id="logInfo">
						</tbody>
					</table>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
				</div>
			</div>
		</div>
	</div>
</body>
</html>