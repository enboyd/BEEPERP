<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>退换货管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
			//$("#name").focus();
			$("#inputForm").validate({
				submitHandler: function(form){
					loading('正在提交，请稍等...');
					form.submit();
				},
				errorContainer: "#messageBox",
				errorPlacement: function(error, element) {
					$("#messageBox").text("输入有误，请先更正。");
					if (element.is(":checkbox")||element.is(":radio")||element.parent().is(".input-append")){
						error.appendTo(element.parent().parent());
					} else {
						error.insertAfter(element);
					}
				}
			});
		});
		
		
		function addRow(list, idx, tpl, row){
			$(list).append(Mustache.render(tpl, {
				idx: idx, delBtn: true, row: row
			}));
			$(list+idx).find("select").each(function(){
				$(this).val($(this).attr("data-value"));
			});
			$(list+idx).find("input[type='checkbox'], input[type='radio']").each(function(){
				var ss = $(this).attr("data-value").split(',');
				for (var i=0; i<ss.length; i++){
					if($(this).val() == ss[i]){
						$(this).attr("checked","checked");
					}
				}
			});
		}
		
		function delRow(obj, prefix){
			debugger;
			var id = $(prefix+"_id");
			var delFlag = $(prefix+"_delFlag");
			if (id.val() == ""){
				$(obj).parent().parent().remove();
			}else if(delFlag.val() == "0"){
				delFlag.val("1");
				$(obj).html("&divide;").attr("title", "撤销删除");
				$(obj).parent().parent().addClass("error");
			}else if(delFlag.val() == "1"){
				delFlag.val("0");
				$(obj).html("&times;").attr("title", "删除");
				$(obj).parent().parent().removeClass("error");
			}
		}
	</script>
</head>
<body>
	<ul class="nav nav-tabs">
		<li><a href="${ctx}/biz/smReturnBarter/">退换货列表</a></li>
		<li class="active">
			<a href="${ctx}/biz/smReturnBarter/form?id=${smReturnBarter.id}">
				<shiro:hasAnyPermissions name="sm:smReturnBarter:edit,sm:smReturnBarter:add">${not empty smReturnBarter.id?'退换货修改':'退换货添加'}</shiro:hasAnyPermissions>
				<shiro:LacksAnyPermission name="sm:smReturnBarter:edit,sm:smReturnBarter:add">退换货查看</shiro:LacksAnyPermission>
			</a>
		</li>
	</ul><br/>
	<form:form id="inputForm" modelAttribute="smReturnBarter" action="${ctx}/biz/smReturnBarter/save" method="post" class="form-horizontal">
		<form:hidden path="id"/>
		<form:hidden path="customerId"/>
		<sys:message content="${message}"/>		
		<div class="control-group">
			<label class="control-label">客户：</label>
			<div class="controls">
				<form:input path="customerName" htmlEscape="false" maxlength="24" class="input-xlarge " readonly="true"/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">手机号：</label>
			<div class="controls">
				<form:input path="phone" htmlEscape="false" maxlength="24" class="input-xlarge digits"/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">分类：</label>
			<div class="controls">
				<form:select path="category" class="input-xlarge required" >
					<form:option value="" label="请选择" />
					<form:options items="${fns:getDictList('sm_return_barter_category')}"
						itemLabel="label" itemValue="value" htmlEscape="false" />
				</form:select>
				<span class="help-inline"><font color="red">*</font> </span>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">发货方式：</label>
			<div class="controls">
				<form:select path="deliverType" class="input-xlarge " >
					<form:option value="" label="请选择" />
					<form:options items="${fns:getDictList('sm_outbound_order_deliver_type')}"
								  itemLabel="label" itemValue="value" htmlEscape="false" />
				</form:select>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">退货类型：</label>
			<div class="controls">
				<form:select path="goodsType" class="input-xlarge required" >
					<form:option value="" label="请选择" />
					<form:options items="${fns:getDictList('sm_return_barter_goodsType')}"
						itemLabel="label" itemValue="value" htmlEscape="false" />
				</form:select>
				<span class="help-inline"><font color="red">*</font> </span>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">退货商品：</label>
			<div class="controls">
				<td colspan="4"  class="controls">
					<!-- <div class="controls"> -->
					<table id="contentTable"
						class="table table-striped table-bordered table-condensed">
						<thead>
							<tr>
								<th class="hide"></th>
								<th>产品</th>
								<th>规格</th>
								<th>单价</th>
								<th>数量</th>
								<th>备注</th>
								<%--<shiro:hasPermission name="sm:smReturnBarter:edit">
									<th width="10">&nbsp;</th>
								</shiro:hasPermission>--%>
							</tr>
						</thead>
						<tbody id="smReturnDetailList">
						</tbody>
						<%--<shiro:hasPermission name="sm:smReturnBarter:add">
							<tfoot>
								<tr>
									<td colspan="4">
										<a href="javascript:" 
											onclick="addRow('#smReturnDetailList', smReturnDetailListRowIdx, smReturnDetailListTpl);smReturnDetailListRowIdx = smReturnDetailListRowIdx + 1;"
											class="btn">新增
										</a>
									</td>
								</tr>
							</tfoot>
						</shiro:hasPermission>--%>
					</table> 
					<script type="text/template" id="smReturnDetailListTpl">//<!--
						<tr id="smReturnDetailList{{idx}}">
							<td class="hide">
								<input id="smReturnDetailList{{idx}}_id" name="smReturnDetailList[{{idx}}].id" type="hidden" value="{{row.id}}"/>
								<input id="smReturnDetailList{{idx}}_delFlag" name="smReturnDetailList[{{idx}}].delFlag" type="hidden" value="0"/>
							</td>
							<td style="text-align:center;vertical-align:middle; width: 30%;">
                            	<sys:treeselectCopy id="smReturnDetailList{{idx}}_goodsId" name="smReturnDetailList[{{idx}}].goodsId" value="{{row.goodsId}}"  labelName="smReturnDetailList[{{idx}}].goodsName" labelValue="{{row.goodsName}}"
									title="产品" url="/biz/smProductInformation/treeData" cssClass=""  allowClear="true" notAllowSelectParent="true" />
							</td>
							<td style="text-align:center;vertical-align:middle; width: 15%;">
								<input id="smReturnDetailList{{idx}}_specifications" name="smReturnDetailList[{{idx}}].specifications" type="text" value="{{row.specifications}}" maxlength="64" class="input-small " readOnly="true"  placeholder=""/>
							</td>
							<td style="text-align:center;vertical-align:middle; width: 15%;">
								<input id="smReturnDetailList{{idx}}_unitPrice" name="smReturnDetailList[{{idx}}].unitPrice" type="text" value="{{row.unitPrice}}" maxlength="64" class="input-small " readOnly="true"  placeholder=""/>
							</td>
							<td style="text-align:center;vertical-align:middle; width: 15%;">
								<input id="smReturnDetailList{{idx}}_quantity" name="smReturnDetailList[{{idx}}].quantity" type="text" value="{{row.quantity}}" maxlength="64" class="input-small "  placeholder=""/>
							</td>
							<td style="text-align:center;vertical-align:middle; width: 30%;">
                                <textarea id="smReturnDetailList{{idx}}_remarks" name="smReturnDetailList[{{idx}}].remarks" rows="1" maxlength="255" class="input-xlarge " style="resize: none;">{{row.remarks}}</textarea>
                             </td>
							<%--<shiro:hasPermission name="sm:smReturnBarter:edit"><td class="text-center" width="">
								{{#delBtn}}<span class="close" onclick="delRow(this, '#smReturnDetailList{{idx}}')" title="删除">&times;</span>{{/delBtn}}
							</td></shiro:hasPermission>--%>
						</tr>//-->
					</script> 
					<script type="text/javascript">
						var smReturnDetailListRowIdx = 0, smReturnDetailListTpl = $("#smReturnDetailListTpl").html().replace(/(\/\/\<!\-\-)|(\/\/\-\->)/g,"");
						$(document).ready(function() {
							var data = ${fns:toJson(smReturnDetailList)};
							for (var i=0; i<data.length; i++){
								addRow('#smReturnDetailList', smReturnDetailListRowIdx, smReturnDetailListTpl, data[i]);
								smReturnDetailListRowIdx = smReturnDetailListRowIdx + 1;
							}
						});
					</script>
				</td>
			</div>
		</div>
		<!-- 换货商品 -->
		<div class="control-group">
			<label class="control-label">换货商品：</label>
			<div class="controls">
				<td colspan="4"  class="controls">
					<!-- <div class="controls"> -->
					<table id="contentTable"
						class="table table-striped table-bordered table-condensed">
						<thead>
							<tr>
								<th class="hide"></th>
								<th>产品</th>
								<th>规格</th>
								<th>单价</th>
								<th>数量</th>
								<th>库存</th>
								<th>备注</th>
								<%--<shiro:hasPermission name="sm:smReturnBarter:edit">
									<th width="10">&nbsp;</th>
								</shiro:hasPermission>--%>
							</tr>
						</thead>
						<tbody id="smBarterDetailList">
						</tbody>
						<%--<shiro:hasPermission name="sm:smReturnBarter:add">
							<tfoot>
								<tr>
									<td colspan="4">
										<a href="javascript:" 
											onclick="addRow('#smBarterDetailList', smBarterDetailListRowIdx, smBarterDetailListTpl);smBarterDetailListRowIdx = smBarterDetailListRowIdx + 1;"
											class="btn">新增
										</a>
									</td>
								</tr>
							</tfoot>
						</shiro:hasPermission>--%>
					</table> 
					<script type="text/template" id="smBarterDetailListTpl">//<!--
						<tr id="smBarterDetailList{{idx}}">
							<td class="hide">
								<input id="smBarterDetailList{{idx}}_id" name="smBarterDetailList[{{idx}}].id" type="hidden" value="{{row.id}}"/>
								<input id="smBarterDetailList{{idx}}_delFlag" name="smBarterDetailList[{{idx}}].delFlag" type="hidden" value="0"/>
							</td>
							<td style="text-align:center;vertical-align:middle; width: 25%;">
                            	<sys:treeselectCopy id="smBarterDetailList{{idx}}_goodsId" name="smBarterDetailList[{{idx}}].goodsId" value="{{row.goodsId}}"  labelName="smBarterDetailList[{{idx}}].goodsName" labelValue="{{row.goodsName}}"
									title="产品" url="/biz/smProductInformation/treeData" cssClass=""  allowClear="true" notAllowSelectParent="true" />
							</td>
							<td style="text-align:center;vertical-align:middle; width: 10%;">
								<input id="smBarterDetailList{{idx}}_specifications" name="smBarterDetailList[{{idx}}].specifications" type="text" value="{{row.specifications}}" maxlength="64" class="input-small " readOnly="true"  placeholder=""/>
							</td>
							<td style="text-align:center;vertical-align:middle; width: 10%;">
								<input id="smBarterDetailList{{idx}}_unitPrice" name="smBarterDetailList[{{idx}}].unitPrice" type="text" value="{{row.unitPrice}}" maxlength="64" class="input-small " readOnly="true"  placeholder=""/>
							</td>
							<td style="text-align:center;vertical-align:middle; width: 10%;">
								<input id="smBarterDetailList{{idx}}_quantity" name="smBarterDetailList[{{idx}}].quantity" type="text" value="{{row.quantity}}" maxlength="64" class="input-small "  readOnly="true"  placeholder=""/>
							</td>
							<td style="text-align:center;vertical-align:middle; width: 10%;">
								<input id="smBarterDetailList{{idx}}_stockAmount" name="smBarterDetailList[{{idx}}].stockAmount" type="text" value="{{row.stockAmount}}" maxlength="64" class="input-small " placeholder=""/>
							</td>
							<td style="text-align:center;vertical-align:middle; width: 40%;">
                                <textarea id="smBarterDetailList{{idx}}_remarks" name="smBarterDetailList[{{idx}}].remarks" rows="1" maxlength="255" class="input-xlarge " style="resize: none;">{{row.remarks}}</textarea>
                             </td>
							<%--<shiro:hasPermission name="sm:smReturnBarter:edit"><td class="text-center" width="">
								{{#delBtn}}<span class="close" onclick="delRow(this, '#smBarterDetailList{{idx}}')" title="删除">&times;</span>{{/delBtn}}
							</td></shiro:hasPermission>--%>
						</tr>//-->
					</script> 
					<script type="text/javascript">
						var smBarterDetailListRowIdx = 0, smBarterDetailListTpl = $("#smBarterDetailListTpl").html().replace(/(\/\/\<!\-\-)|(\/\/\-\->)/g,"");
						$(document).ready(function() {
							var data = ${fns:toJson(smBarterDetailList)};
							for (var i=0; i<data.length; i++){
								addRow('#smBarterDetailList', smBarterDetailListRowIdx, smBarterDetailListTpl, data[i]);
								smBarterDetailListRowIdx = smBarterDetailListRowIdx + 1;
							}
						});
					</script>
				</td>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">审核状态：</label>
			<div class="controls">
				<form:select path="auditStatus" class="input-xlarge" disabled="true">
					<form:option value="" label="请选择" />
					<form:options items="${fns:getDictList('sm_return_barter_auditStatus')}"
						itemLabel="label" itemValue="value" htmlEscape="false" />
				</form:select>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">收货地址：</label>
			<div class="controls">
				<form:textarea path="address" htmlEscape="false" rows="4" maxlength="1024" class="input-xxlarge "/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">退换货原因：</label>
			<div class="controls">
				<form:textarea path="remarks" htmlEscape="false" rows="4" maxlength="1024" class="input-xxlarge "/>
			</div>
		</div>
		<div class="form-actions">
			<c:if test="${smReturnBarter.auditStatus != '10' && smReturnBarter.auditStatus != '20' }">
				<shiro:hasAnyPermissions name="sm:smReturnBarter:edit,sm:smReturnBarter:add">
					<input id="btnSubmit" class="btn btn-primary" type="submit" value="保 存"/>&nbsp;
				</shiro:hasAnyPermissions>
			</c:if>
			<%--<c:if test="${smReturnBarter.auditStatus eq '10'}">
				<shiro:hasPermission name="sm:smReturnBarter:approve">
					<a href="${ctx}/biz/smReturnBarter/approve?id=${smReturnBarter.id}" class="btn btn-primary">审批通过</a>
				</shiro:hasPermission>
				<shiro:hasPermission name="sm:smReturnBarter:dismiss">
					<a href="${ctx}/biz/smReturnBarter/dismiss?id=${smReturnBarter.id}" class="btn btn-primary">驳回</a>
				</shiro:hasPermission>
			</c:if>--%>
			<input id="btnCancel" class="btn" type="button" value="返 回" onclick="history.go(-1)"/>
		</div>
	</form:form>
</body>
</html>