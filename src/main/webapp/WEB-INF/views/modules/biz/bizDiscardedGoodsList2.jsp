<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>废弃商品管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {

			//变量定义区域
            var cbArr = $(":input[name='checkId']");
            var flag = 0;
            function checkStatus() {
                for (var i = 0; i < cbArr.length; i++) {
                    if (cbArr.get(i).checked == true) {
                        flag = 1;
                        break;
                    }
                }
                return;
            }

            //全选全不选
            $(":input#cbHandle").on("click", function() {
                if ($(this)[0].checked == true) {
                    for (var i = 0; i < cbArr.length; i++) {
                        cbArr.get(i).checked = true;
                    }
                    flag = 2;
                }
                if ($(this)[0].checked == false) {
                    for (var i = 0; i < cbArr.length; i++) {
                        cbArr.get(i).checked = false;
                    }
                    flag = 0;
                }
            });
            /**
             * 确认报废
             */
            $("#discarding").click(function() {
                //选中的客户 customerId
                var checkedBox = $("input[name='checkId']:checked");
                if(checkedBox.length == 0){
                    top.$.jBox.tip("请勾选商品", "warning", {
                        persistent : true,
                        opacity : 0
                    });
                }else{
                    if(confirm("确定要废弃吗？？")){
                        var res = checkedBox.map(function(){
                            return this.value;
                        });
                        var ids = res.toArray().join(",");
                        //alert(ids);
                        $.ajax({
                            type: "POST",
                            url: "${ctx}/biz/bizDiscardedGoods/discarding",
                            data: {
                                'ids': ids
                            },
                            success: function(data){
                                top.$.jBox.info(data,'提示');
                                $('#searchForm').submit();
                            }
                        });
                    }
                }
            });

		});
		function page(n,s){
			$("#pageNo").val(n);
			$("#pageSize").val(s);
			$("#searchForm").submit();
        	return false;
        }
	</script>
</head>
<body>
	<ul class="nav nav-tabs">
		<li><a href="${ctx}/biz/bizDiscardedGoods/list">废弃商品</a></li>
		<li class="active"><a href="${ctx}/biz/bizDiscardedGoods/list?type=1">过期待废弃</a>
	</ul>
	<form:form id="searchForm" modelAttribute="bizDiscardedGoods" action="${ctx}/biz/bizDiscardedGoods/list?type=1" method="post" class="breadcrumb form-search">
		<input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
		<input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/>
		<ul class="ul-form">
			<%--<li class="btns"><input id="btnSubmit" class="btn btn-primary" type="submit" value="查询"/></li>--%>
			<li class="btns"><input id="discarding" class="btn btn-primary" type="button" value="确认报废"/>
			<li class="clearfix"></li>
		</ul>
	</form:form>
	<sys:message content="${message}"/>
	<table id="contentTable" class="table table-striped table-bordered table-condensed">
		<thead>
			<tr>
				<th><input type="checkbox" id="cbHandle" /></th>
				<th>商品编号</th>
				<th>商品名称</th>
				<th>条形码编号</th>
				<th>批次</th>
				<th>生产日期</th>
				<th>保质期/月</th>
				<th>数量</th>
				<th>规格</th>
				<th>库区名称</th>
				<th>状态</th>
				<%--<shiro:hasPermission name="biz:bizDiscardedGoods:edit"><th>操作</th></shiro:hasPermission>--%>
			</tr>
		</thead>
		<tbody>
		<c:forEach items="${page.list}" var="bizDiscardedGoods">
			<tr>
				<td>
					<input name="checkId" type="checkbox" value="${bizDiscardedGoods.id}" />
				</td>
				<td><a href="${ctx}/biz/bizGoods/form?id=${bizDiscardedGoods.goodsId}">
					${bizDiscardedGoods.goodsCode}
				</a></td>
				<td>
					${bizDiscardedGoods.goodsName}
				</td>
				<td>
					${bizDiscardedGoods.barcodeName}
				</td>
				<td>
					${bizDiscardedGoods.batch}
				</td>
				<td>
					<fmt:formatDate value="${bizDiscardedGoods.manufactureDate}" pattern="yyyy-MM-dd"/>
				</td>
				<td>
					${bizDiscardedGoods.expirationDate}
				</td>
				<td>
					${bizDiscardedGoods.number}
				</td>
				<td>
					${bizDiscardedGoods.specification}
				</td>
				<td>
					${bizDiscardedGoods.storageLocationName}
				</td>
				<td>
					<span style="color: black">${fns:getDictLabel(bizDiscardedGoods.type, 'biz_discardedGoods_type', '')}</span>
				</td>
				<%--<shiro:hasPermission name="biz:bizDiscardedGoods:edit"><td>
    				<a href="${ctx}/biz/bizDiscardedGoods/discarding?id=${bizDiscardedGoods.id}">确认报废</a>
				</td></shiro:hasPermission>--%>
			</tr>
		</c:forEach>
		</tbody>
	</table>
	<div class="pagination">${page}</div>
</body>
</html>