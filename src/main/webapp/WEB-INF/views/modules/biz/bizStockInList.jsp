<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>入库单管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
            /* 导出 */
            $("#btnExport").click(function(){
                top.$.jBox.confirm("确认要导出数据吗？", "系统提示", function(v,h,f){
                    if(v=="ok"){
                        $("#searchForm").attr("action","${ctx}/biz/bizStockIn/export");
                        $("#searchForm").submit();
                        loading("正在将文件打包，请耐心等待，勿刷新页面...");
                    }
                },{buttonsFocus:1});
            });
		});
		function page(n,s){
			$("#pageNo").val(n);
			$("#pageSize").val(s);
			$("#searchForm").submit();
        	return false;
        }
	</script>
</head>
<body>
	<ul class="nav nav-tabs">
		<li class="active"><a href="${ctx}/biz/bizStockIn/">入库单列表</a></li>
	</ul>
	<form:form id="searchForm" modelAttribute="bizStockIn" action="${ctx}/biz/bizStockIn/" method="post" class="breadcrumb form-search">
		<input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
		<input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/>
		<ul class="ul-form">
			<li><label>采购单编号：</label>
				<form:input path="purchaseCode" htmlEscape="false" maxlength="64" class="input-medium"/>
			</li>
			<li><label>入库单编号：</label>
				<form:input path="stockInCode" htmlEscape="false" maxlength="64" class="input-medium"/>
			</li>
			<li><label>入库单状态：</label>
				<form:select path="status" class="input-medium">
					<form:option value="" label=""/>
					<form:options items="${fns:getDictList('biz_stock_in_status')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
				</form:select>
			</li>
			<li><label>创建人名称：</label>
				<form:input path="createBy.name" htmlEscape="false" maxlength="64" class="input-medium"/>
			</li>
			<li><label>入库日期：</label>
				<input name="stockInDateStart" type="text" maxlength="20" class="input-medium Wdate"
					   value="<fmt:formatDate value="${bizStockIn.stockInDateStart}" pattern="yyyy-MM-dd HH:mm:ss"/>"
					   onclick="WdatePicker({dateFmt:'yyyy-MM-dd 00:00:00',isShowClear:false});"/> -
				<input name="stockInDateEnd" type="text" maxlength="20" class="input-medium Wdate"
					   value="<fmt:formatDate value="${bizStockIn.stockInDateEnd}" pattern="yyyy-MM-dd HH:mm:ss"/>"
					   onclick="WdatePicker({dateFmt:'yyyy-MM-dd 23:59:59',isShowClear:false});"/>
			</li>
			<li class="btns"><input id="btnSubmit" class="btn btn-primary" type="submit" value="查询"/></li>
			<shiro:hasPermission name="sm:bizStockIn:export">
				<li class="btns"><input id="btnExport" class="btn btn-primary" type="button" value="导出"/></li>
			</shiro:hasPermission>
			<li class="clearfix"></li>
		</ul>
	</form:form>
	<sys:message content="${message}"/>
	<table id="contentTable" class="table table-striped table-bordered table-condensed">
		<thead>
			<tr>
				<th>入库单编号</th>
				<th>采购单编号</th>
				<th>入库单状态</th>
				<th>入库时间</th>
				<th>创建者</th>
				<shiro:hasAnyPermissions name="biz:bizStockIn:edit,biz:bizStockIn:remove"><th>操作</th></shiro:hasAnyPermissions>
			</tr>
		</thead>
		<tbody>
		<c:forEach items="${page.list}" var="bizStockIn">
			<tr>
				<td><a href="${ctx}/biz/bizStockIn/form?id=${bizStockIn.stockInId}">
					${bizStockIn.stockInCode}
				</a></td>
				<td>${bizStockIn.purchaseCode}</td>
				<td>${fns:getDictLabel(bizStockIn.status, 'biz_stock_in_status', '')}</td>
				<td><fmt:formatDate value="${bizStockIn.stockInDate}" pattern="yyyy-MM-dd HH:mm:ss"/></td>
				<td>${bizStockIn.createBy.name}</td>
				<shiro:hasAnyPermissions name="biz:bizStockIn:edit,biz:bizStockIn:remove">
					<td>
						<shiro:hasPermission name="biz:bizStockIn:edit">
							<a href="${ctx}/biz/bizStockIn/form?id=${bizStockIn.stockInId}">修改</a>
						</shiro:hasPermission>
						<shiro:hasPermission name="biz:bizStockIn:remove">
							<a href="${ctx}/biz/bizStockIn/delete?id=${bizStockIn.stockInId}" onclick="return confirmx('确认要删除该商品吗？', this.href)">删除</a>
						</shiro:hasPermission>
					</td>
				</shiro:hasAnyPermissions>
			</tr>
		</c:forEach>
		</tbody>
	</table>
	<div class="pagination">${page}</div>
</body>
</html>