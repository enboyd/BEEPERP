<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>采购单管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
			$("#inputForm").validate({
				submitHandler: function (form) {
					loading('正在提交，请稍等...');
					form.submit();
				},
				errorContainer: "#messageBox",
				errorPlacement: function (error, element) {
					$("#messageBox").text("输入有误，请先更正。");
					if (element.is(":checkbox") || element.is(":radio") || element.parent().is(".input-append")) {
						error.appendTo(element.parent().parent());
					} else {
						error.insertAfter(element);
					}
				}
			});
		})

		function total(){
			debugger
			var row = $("#goodsNum").val();
			for(var i = 0 ; i < row; i++){
				var expertAmount = Number($("#expertAmount"+i).val());//申请采购数量
				var actualAmount = Number($("#actualAmount"+i).val());//实际入库数量
				if(expertAmount > actualAmount){
					$("#differentAmount"+i).val(expertAmount-actualAmount);
				}else if(expertAmount < actualAmount){
					$("#actualAmount"+i).val(expertAmount);
					$("#differentAmount"+i).val("0");
					alert("实际入库数量已大于申请采购数量，请进行核查！");
				}else{
					$("#differentAmount"+i).val("0");
				}
			}
		}
	</script>
</head>
<body>
	<ul class="nav nav-tabs">
		<li><a href="${ctx}/biz/bizPurchase/list?type=${bizPurchase.type}">采购单列表</a></li>
		<li class="active"><a href="${ctx}/biz/bizPurchase/form?id=${bizPurchase.purchaseId}&type=${bizPurchase.type}">采购单<shiro:hasPermission name="biz:bizPurchase:edit">${not empty bizPurchase.purchaseId?'修改':'添加'}</shiro:hasPermission><shiro:lacksPermission name="biz:bizPurchase:edit">查看</shiro:lacksPermission></a></li>
	</ul><br/>
	<form:form id="inputForm" modelAttribute="bizPurchase" action="${ctx}/biz/bizPurchase/save" method="post" class="form-horizontal">
		<form:hidden path="id"/>
		<sys:message content="${message}"/>
		<input type="hidden" id="goodsNum" value="${bizPurchaseDetailCount}">
		<div class="control-group">
			<label class="control-label"></label>
		</div>
		<div class="control-group">
			<label class="control-label">商品明细：</label>
			<div class="controls">
				<table id="contentTable"
					   class="table table-striped table-bordered table-condensed">
					<thead>
					<tr>
						<th>商品名称</th>
						<th>供货商</th>
						<th>商品类型</th>
						<shiro:hasPermission name="biz:bizPurchase:formField">
						<th>规格</th>
						<th>单位</th>
						<th>采购单价</th>
						<th>税率</th>
						</shiro:hasPermission>
						<th>需求时间</th>
						<th>库存数量</th>
						<th>采购中数量</th>
						<th>采购数量</th>
						<th>实际入库数量</th>
						<th>相差数量</th>
						<shiro:hasPermission name="biz:bizPurchase:formField">
						<th>小计</th>
						</shiro:hasPermission>
					</tr>
					</thead>
					<tbody id="goodsList">
							<c:forEach items="${bizPurchaseDetailList}" var="bizPurchaseDetail" varStatus="status">
								<tr id="bizPurchaseDetailList${status.index}">
									<td>${bizPurchaseDetail.goodsName}</td>
									<td>${bizPurchaseDetail.supplierName}</td>
									<td>${bizPurchaseDetail.goodsCategoryName}</td>
									<shiro:hasPermission name="biz:bizPurchase:formField">
									<td>${bizPurchaseDetail.specification}</td>
									<td>${bizPurchaseDetail.unit}</td>
									<td>
										<input type="text" id="unitPrice${status.index}" style="width: 100px"  name="purchaseDetailList[${status.index}].unitPrice" value="${bizPurchaseDetail.unitPrice}">
									</td>
									<td>
										<input type="text" readonly id="taxRate${status.index}" style="width: 100px" name="purchaseDetailList[${status.index}].taxRate" value="${bizPurchaseDetail.taxRate}">
									</td>
									</shiro:hasPermission>
									<%--需求时间--%>
									<td>
										<input id="demandTime${status.index}" name="purchaseDetailList[${status.index}].demandTime"
											   type="text" maxlength="20" autocomplete="off" class="input-medium Wdate "
											   value="<fmt:formatDate value="${bizPurchaseDetail.demandTime}" pattern="yyyy-MM-dd"/>"
											   readonly="readonly"/>
									</td>
									<td>${bizPurchaseDetail.stockAmount}</td>
									<td>${bizPurchaseDetail.purchasingAmount}</td>
									<td>
										<input type="hidden" id="goodsId${status.index}" name="purchaseDetailList[${status.index}].goodsId" value="${bizPurchaseDetail.goodsId}">
										<input type="hidden" id="purchaseDetailId${status.index}" name="purchaseDetailList[${status.index}].purchaseDetailId" value="${bizPurchaseDetail.purchaseDetailId}">
										<input type="hidden" id="delFlag${status.index}" name="purchaseDetailList[${status.index}].delFlag" value="${bizPurchaseDetail.delFlag}">
										<input type="text" class="number" style="width: 100px"  id="expertAmount${status.index}" name="purchaseDetailList[${status.index}].expertAmount" value="${bizPurchaseDetail.expertAmount}">
									</td>
									<td>
										<input type="text" readonly id="actualAmount${status.index}" class="number required" style="width: 100px" name="purchaseDetailList[${status.index}].actualAmount" value="${bizPurchaseDetail.actualAmount}">
									</td>
									<td>
										<input type="text" readonly id="differentAmount${status.index}" style="width: 100px" name="purchaseDetailList[${status.index}].differentAmount" value="${bizPurchaseDetail.differentAmount}">
									</td>
									<shiro:hasPermission name="biz:bizPurchase:formField">
									<td>
										<input type="text" readonly id="totalPrice${status.index}" style="width: 100px" name="purchaseDetailList[${status.index}].totalPrice" value="${bizPurchaseDetail.totalPrice}">
									</td>
									</shiro:hasPermission>
								</tr>
							</c:forEach>
					</tbody>
				</table>
			</div>
		</div>
		<%--<div class="control-group">
			<label class="control-label">出款方：</label>
			<div class="controls">
				<form:select path="warehouse" class="input-xlarge required" >
					<form:option value="" label="请选择" />
					<form:options items="${fns:getDictList('biz_purchase_warehouse')}"
								  itemLabel="label" itemValue="value" htmlEscape="false" />
				</form:select>
				<span class="help-inline"><font color="red">*</font> </span>
			</div>
		</div>--%>
		<div class="control-group">
			<label class="control-label">采购单类型：</label>
			<div class="controls">
				<form:select path="purchaseType" class="input-xlarge required" >
					<form:option value="" label="请选择" />
					<form:options items="${fns:getDictList('biz_purchase_type')}"
								  itemLabel="label" itemValue="value" htmlEscape="false" />
				</form:select>
				<span class="help-inline"><font color="red">*</font> </span>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">采购总额：</label>
			<div class="controls">
				<form:input path="totalPrice" htmlEscape="false" class="input-xlarge required" readonly="true"/>
				<span class="help-inline"><font color="red">*</font> </span>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">备注信息：</label>
			<div class="controls">
				<form:textarea path="remarks" htmlEscape="false" rows="4" maxlength="255" class="input-xxlarge "/>
			</div>
		</div>
		<div style="margin-left: 650px">
			<h3>入库明细</h3>
		</div>
		<c:if test="${!empty bizStockInList}">
			<c:forEach items="${bizStockInList}" var="bizStockIn">
			<div class="control-group" style="margin-top: 20px">
				<label class="control-label"><fmt:formatDate value="${bizStockIn.stockInDate}" pattern="yyyy-MM-dd"/>：</label>
				<div class="controls">
					<table id="contentTable" class="table table-striped table-bordered table-condensed">
						<thead>
						<tr>
							<th width="30%">商品名称</th>
							<th width="30%">实际入库数量</th>
							<th width="40%">备注</th>
						</tr>
						</thead>
						<tbody>
						<c:forEach items="${bizStockIn.bizStockInDetailList}" var="bizStockInDetail">
							<tr>
								<td width="30%">${bizStockInDetail.goodsName}</td>
								<td width="30%">${bizStockInDetail.actualAmount}</td>
								<td width="40%">${bizStockInDetail.remarks}</td>
							</tr>
						</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
			</c:forEach>
		</c:if>
		<div colspan=5 class="text-center" style="margin-top: 50px">
			<c:if test="${bizPurchase.auditStatus != '20' && bizPurchase.auditStatus != '30'}">
				<shiro:hasAnyPermissions name="biz:bizPurchase:edit,biz:bizPurchase:add">
					<!-- 审核中和审核通过不可修改该单 -->
					<input id="btnSubmit" class="btn btn-primary" type="submit" value="保 存"/>&nbsp;
				</shiro:hasAnyPermissions>
			</c:if>
			<c:if test="${bizPurchase.status != '12'}">
				<shiro:hasPermission name="biz:bizPurchase:flatSheet">
					<a href="${ctx}/biz/bizPurchase/flatSheet?id=${bizPurchase.purchaseId}&source=form" class="btn btn-primary"
					   onclick="return confirmx('是否确定平单？', this.href)">平单</a>
				</shiro:hasPermission>
			</c:if>
			<input id="btnCancel" class="btn" type="button" value="返 回" onclick="history.go(-1)"/>
		</div>
	</form:form>

</body>
</html>
 ``