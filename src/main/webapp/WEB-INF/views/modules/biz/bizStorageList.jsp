<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>仓库管理管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
			
		});
		function page(n,s){
			$("#pageNo").val(n);
			$("#pageSize").val(s);
			$("#searchForm").submit();
        	return false;
        }
	</script>
</head>
<body>
	<ul class="nav nav-tabs">
		<li class="active"><a href="${ctx}/biz/bizStorage/">仓库管理列表</a></li>
		<shiro:hasPermission name="biz:bizStorage:edit"><li><a href="${ctx}/biz/bizStorage/form">仓库管理添加</a></li></shiro:hasPermission>
	</ul>
	<form:form id="searchForm" modelAttribute="bizStorage" action="${ctx}/biz/bizStorage/" method="post" class="breadcrumb form-search">
		<input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
		<input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/>
		<ul class="ul-form">
			<li><label>仓库名称：</label>
				<form:input path="storageName" htmlEscape="false" maxlength="64" class="input-medium"/>
			</li>
			<li><label>联系人：</label>
				<form:input path="contact" htmlEscape="false" maxlength="64" class="input-medium"/>
			</li>
			<li><label>联系电话：</label>
				<form:input path="phone" htmlEscape="false" maxlength="18" class="input-medium"/>
			</li>
			<li><label>仓库类型：</label>
				<form:select path="type" class="input-medium">
					<form:option value="" label=""/>
					<form:options items="${fns:getDictList('biz_storage_type')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
				</form:select>
			</li>
			<li class="btns"><input id="btnSubmit" class="btn btn-primary" type="submit" value="查询"/></li>
			<li class="clearfix"></li>
		</ul>
	</form:form>
	<sys:message content="${message}"/>
	<table id="contentTable" class="table table-striped table-bordered table-condensed">
		<thead>
			<tr>
				<th>仓库名称</th>
				<th>联系人</th>
				<th>联系电话</th>
				<th>仓库类型</th>
				<th>省</th>
				<th>市</th>
				<th>区</th>
				<th>地址</th>
				<th>备注信息</th>
				<shiro:hasPermission name="biz:bizStorage:edit"><th>操作</th></shiro:hasPermission>
			</tr>
		</thead>
		<tbody>
		<c:forEach items="${page.list}" var="bizStorage">
			<tr>
				<td><a href="${ctx}/biz/bizStorage/form?id=${bizStorage.storageId}">
					${bizStorage.storageName}
				</a></td>
				<td>
					${bizStorage.contact}
				</td>
				<td>
					${bizStorage.phone}
				</td>
				<td>
					${fns:getDictLabel(bizStorage.type, 'biz_storage_type', '')}
				</td>
				<td>
					${bizStorage.province.name}
				</td>
				<td>
					${bizStorage.city.name}
				</td>
				<td>
					${bizStorage.district.name}
				</td>
				<td>
					${bizStorage.address}
				</td>
				<td>
					${bizStorage.remarks}
				</td>
				<td>
					<shiro:hasPermission name="biz:bizStorage:edit">
						<a href="${ctx}/biz/bizStorage/form?id=${bizStorage.storageId}">修改</a>
					</shiro:hasPermission>
					<shiro:hasPermission name="biz:bizStorage:remove">
						<a href="${ctx}/biz/bizStorage/delete?id=${bizStorage.storageId}" onclick="return confirmx('确认要删除该仓库管理吗？', this.href)">删除</a>
					</shiro:hasPermission>
				</td>
			</tr>
		</c:forEach>
		</tbody>
	</table>
	<div class="pagination">${page}</div>
</body>
</html>