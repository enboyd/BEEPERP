<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>请购单管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
            /* 导出 */
            $("#btnExport").click(function(){
                top.$.jBox.confirm("确认要导出商品数据吗？", "系统提示", function(v,h,f){
                    if(v=="ok"){
                        var checked = $("input[type='checkbox']").is(':checked')
                        if(checked){
                            $("#stockAmount").val(0);
                        }
                        $("#searchForm").attr("action","${ctx}/biz/bizBuyingRequisition/export");
                        $("#searchForm").submit();
                        loading("正在将文件打包，请耐心等待，勿刷新页面...");
                    }
                },{buttonsFocus:1});
            });
		});
		function page(n,s){
			$("#pageNo").val(n);
			$("#pageSize").val(s);
			$("#searchForm").submit();
        	return false;
        }
	</script>
</head>
<body>
	<ul class="nav nav-tabs">
		<li class="active"><a href="${ctx}/biz/bizBuyingRequisition/">请购单列表</a></li>
		<shiro:hasPermission name="biz:bizBuyingRequisition:edit"><li><a href="${ctx}/biz/bizBuyingRequisition/form">请购单添加</a></li></shiro:hasPermission>
	</ul>
	<form:form id="searchForm" modelAttribute="bizBuyingRequisition" action="${ctx}/biz/bizBuyingRequisition/" method="post" class="breadcrumb form-search">
		<input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
		<input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/>
		<ul class="ul-form">
			<li><label>请购单编号：</label>
				<form:input path="buyingCode" htmlEscape="false" maxlength="24" class="input-medium"/>
			</li>
			<li><label>请购日期：</label>
				<input name="beginBuyingDate" type="text" readonly="readonly" maxlength="20" class="input-medium Wdate"
					value="<fmt:formatDate value="${bizBuyingRequisition.beginBuyingDate}" pattern="yyyy-MM-dd HH:mm:ss"/>"
					onclick="WdatePicker({dateFmt:'yyyy-MM-dd 00:00:00',isShowClear:false});"/> -
				<input name="endBuyingDate" type="text" readonly="readonly" maxlength="20" class="input-medium Wdate"
					value="<fmt:formatDate value="${bizBuyingRequisition.endBuyingDate}" pattern="yyyy-MM-dd HH:mm:ss"/>"
					onclick="WdatePicker({dateFmt:'yyyy-MM-dd 23:59:59',isShowClear:false});"/>
			</li>
			<li>
				<label>请购单状态：</label>
				<form:select path="status" class="input-medium" >
					<form:option value="" label="请选择" />
					<form:options items="${fns:getDictList('biz_buying_requisition_status')}"
								  itemLabel="label" itemValue="value" htmlEscape="false" />
				</form:select>
			</li>
			<li class="btns"><input id="btnSubmit" class="btn btn-primary" type="submit" value="查询"/></li>
			<shiro:hasPermission name="sm:bizBuyingRequisition:export">
				<li class="btns"><input id="btnExport" class="btn btn-primary" type="button" value="导出"/></li>
			</shiro:hasPermission>
			<li class="clearfix"></li>
		</ul>
	</form:form>
	<sys:message content="${message}"/>
	<table id="contentTable" class="table table-striped table-bordered table-condensed">
		<thead>
			<tr>
				<th>请购单编号</th>
				<th>请购日期</th>
				<th>请购单状态</th>
				<th>请购单类型</th>
				<th>更新时间</th>
				<th>备注信息</th>
				<shiro:hasAnyPermissions name="biz:bizBuyingRequisition:edit,biz:bizBuyingRequisition:remove,biz:bizBuyingRequisition:toExamine,biz:bizBuyingRequisition:reject,biz:bizBuyingRequisition:quicklyCreatePurOrders">
					<th>操作</th>
				</shiro:hasAnyPermissions>
			</tr>
		</thead>
		<tbody>
		<c:forEach items="${page.list}" var="bizBuyingRequisition">
			<tr>
				<td><a href="${ctx}/biz/bizBuyingRequisition/form?buyingId=${bizBuyingRequisition.buyingId}">
					${bizBuyingRequisition.buyingCode}
				</a></td>
				<td>
					<fmt:formatDate value="${bizBuyingRequisition.buyingDate}" pattern="yyyy-MM-dd HH:mm:ss"/>
				</td>
				<td>
				<c:if test="${(bizBuyingRequisition.status eq '10') || (bizBuyingRequisition.status eq '20') || (bizBuyingRequisition.status eq '5')}">
					<span style="color: red">${fns:getDictLabel(bizBuyingRequisition.status, 'biz_buying_requisition_status','')}</span>
				</c:if>
				<c:if test="${(bizBuyingRequisition.status eq '30') || (bizBuyingRequisition.status eq '40') || (bizBuyingRequisition.status eq '50')}">
					<span style="color: green">${fns:getDictLabel(bizBuyingRequisition.status, 'biz_buying_requisition_status','')}</span>

				</c:if>
				</td>
				<td>${fns:getDictLabel(bizBuyingRequisition.type, 'biz_buying_requisition_type', '')}</td>
				<td>
					<fmt:formatDate value="${bizBuyingRequisition.updateDate}" pattern="yyyy-MM-dd HH:mm:ss"/>
				</td>
				<td>
					${bizBuyingRequisition.remarks}
				</td>
				<shiro:hasAnyPermissions name="biz:bizBuyingRequisition:edit,biz:bizBuyingRequisition:remove,biz:bizBuyingRequisition:toExamine,biz:bizBuyingRequisition:reject,biz:bizBuyingRequisition:quicklyCreatePurOrders">
					<td>
						<%--<shiro:hasPermission name="biz:bizBuyingRequisition:edit">
							<a href="${ctx}/biz/bizBuyingRequisition/form?buyingId=${bizBuyingRequisition.buyingId}">修改</a>
						</shiro:hasPermission>--%>
						<shiro:hasPermission name="biz:bizBuyingRequisition:remove">
							<a href="${ctx}/biz/bizBuyingRequisition/delete?buyingId=${bizBuyingRequisition.buyingId}" onclick="return confirmx('确认要删除该请购单吗？', this.href)">删除</a>
						</shiro:hasPermission>
						<c:if test="${bizBuyingRequisition.status eq '5'}">
							<shiro:hasPermission name="biz:bizBuyingRequisition:submit">
								<a href="${ctx}/biz/bizBuyingRequisition/submit?buyingId=${bizBuyingRequisition.buyingId}">提交</a>
							</shiro:hasPermission>
						</c:if>
						<c:if test="${bizBuyingRequisition.status eq '10'}">
							<shiro:hasPermission name="biz:bizBuyingRequisition:toExamine">
								<a href="${ctx}/biz/bizBuyingRequisition/toExamine?buyingId=${bizBuyingRequisition.buyingId}">审批通过</a>
							</shiro:hasPermission>
							<shiro:hasPermission name="biz:bizBuyingRequisition:reject">
								<a href="${ctx}/biz/bizBuyingRequisition/reject?buyingId=${bizBuyingRequisition.buyingId}">驳回</a>
							</shiro:hasPermission>
						</c:if>
						<c:if test="${bizBuyingRequisition.status eq '20'}">
							<shiro:hasPermission name="biz:bizBuyingRequisition:resubmit">
								<a href="${ctx}/biz/bizBuyingRequisition/resubmit?buyingId=${bizBuyingRequisition.buyingId}">重新提交</a>
							</shiro:hasPermission>
						</c:if>
						<c:if test="${bizBuyingRequisition.status == '30' || bizBuyingRequisition.status == '40'}">
							<shiro:hasPermission name="biz:bizBuyingRequisition:quicklyCreatePurOrders">
								<a href="${ctx}/biz/bizBuyingRequisition/quicklyCreatePurOrders?buyingId=${bizBuyingRequisition.buyingId}">快捷采购</a>
							</shiro:hasPermission>
						</c:if>
					</td>
				</shiro:hasAnyPermissions>
			</tr>
		</c:forEach>
		</tbody>
	</table>
	<div class="pagination">${page}</div>
</body>
</html>