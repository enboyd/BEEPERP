<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>库位管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
			$("#name").focus();
			$("#inputForm").validate({
				submitHandler: function(form){
					loading('正在提交，请稍等...');
					form.submit();
				},
				errorContainer: "#messageBox",
				errorPlacement: function(error, element) {
					$("#messageBox").text("输入有误，请先更正。");
					if (element.is(":checkbox") || element.is(":radio") || element.parent().is(".input-append")){
						error.appendTo(element.parent().parent());
					} else {
						error.insertAfter(element);
					}
				}
			});

            //新增库位时是否启用默认选中是
            var val=$("input:radio[name='isEnable']:checked").val();
            if(val==null){
                $("#isEnable1").attr("checked","checked");
            }
		});
	</script>
</head>
<body>
	<ul class="nav nav-tabs">
		<li><a href="${ctx}/biz/bizStorageLocation/">库位管理列表</a></li>
		<li class="active"><a href="${ctx}/biz/bizStorageLocation/form?id=${bizStorageLocation.id}&parent.id=${bizStorageLocationparent.id}">库位管理<shiro:hasPermission name="biz:bizStorageLocation:edit">${not empty bizStorageLocation.id?'修改':'添加'}</shiro:hasPermission><shiro:lacksPermission name="biz:bizStorageLocation:edit">查看</shiro:lacksPermission></a></li>
	</ul><br/>
	<form:form id="inputForm" modelAttribute="bizStorageLocation" action="${ctx}/biz/bizStorageLocation/save" method="post" class="form-horizontal">
		<form:hidden path="id"/>
		<sys:message content="${message}"/>		
		<div class="control-group">
			<label class="control-label">名称：</label>
			<div class="controls">
				<form:input path="name" htmlEscape="false" maxlength="100" class="input-xlarge "/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">上级父级编号:</label>
			<div class="controls">
				<sys:treeselect id="parent" name="parent.id" value="${bizStorageLocation.parent.id}" labelName="parent.name" labelValue="${bizStorageLocation.parent.name}"
					title="父级编号" url="/biz/bizStorageLocation/treeData" extId="${bizStorageLocation.id}" cssClass="" allowClear="true"/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">排序：</label>
			<div class="controls">
				<form:input path="sort" htmlEscape="false" maxlength="10" class="input-xlarge  digits"/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">备注信息：</label>
			<div class="controls">
				<form:textarea path="remarks" htmlEscape="false" rows="4" maxlength="1000" class="input-xxlarge "/>
			</div>
		</div>
		 <div class="control-group">
			<label class="control-label">是否启用：</label>
			<div class="controls">
				<form:radiobuttons path="isEnable"
								   items="${fns:getDictList('yes_no')}" itemLabel="label"
								   itemValue="value" delimiter="&nbsp&nbsp&nbsp&nbsp&nbsp"
								   htmlEscape="false" class="" />
			</div>
		</div>
		<div class="form-actions">
			<shiro:hasPermission name="biz:bizStorageLocation:edit"><input id="btnSubmit" class="btn btn-primary" type="submit" value="保 存"/>&nbsp;</shiro:hasPermission>
			<input id="btnCancel" class="btn" type="button" value="返 回" onclick="history.go(-1)"/>
		</div>
	</form:form>
</body>
</html>