<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>仓库管理管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
			//$("#name").focus();
			$("#inputForm").validate({
				submitHandler: function(form){
					loading('正在提交，请稍等...');
					form.submit();
				},
				errorContainer: "#messageBox",
				errorPlacement: function(error, element) {
					$("#messageBox").text("输入有误，请先更正。");
					if (element.is(":checkbox")||element.is(":radio")||element.parent().is(".input-append")){
						error.appendTo(element.parent().parent());
					} else {
						error.insertAfter(element);
					}
				}
			});
		});
	</script>
</head>
<body>
	<ul class="nav nav-tabs">
		<li><a href="${ctx}/biz/bizStorage/">仓库管理列表</a></li>
		<li class="active"><a href="${ctx}/biz/bizStorage/form?id=${bizStorage.id}">仓库管理<shiro:hasPermission name="biz:bizStorage:edit">${not empty bizStorage.id?'修改':'添加'}</shiro:hasPermission><shiro:lacksPermission name="biz:bizStorage:edit">查看</shiro:lacksPermission></a></li>
	</ul><br/>
	<form:form id="inputForm" modelAttribute="bizStorage" action="${ctx}/biz/bizStorage/save" method="post" class="form-horizontal">
		<form:hidden path="id"/>
		<sys:message content="${message}"/>		
		<div class="control-group">
			<label class="control-label">仓库名称：</label>
			<div class="controls">
				<form:input path="storageName" htmlEscape="false" maxlength="64" class="input-xlarge required"/>
				<span class="help-inline"><font color="red">*</font> </span>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">联系人：</label>
			<div class="controls">
				<form:input path="contact" htmlEscape="false" maxlength="64" class="input-xlarge required"/>
				<span class="help-inline"><font color="red">*</font> </span>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">联系电话：</label>
			<div class="controls">
				<form:input path="phone" htmlEscape="false" maxlength="18" class="input-xlarge required"/>
				<span class="help-inline"><font color="red">*</font> </span>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">仓库类型：</label>
			<div class="controls">
				<form:select path="type" class="input-xlarge required">
					<form:option value="" label=""/>
					<form:options items="${fns:getDictList('biz_storage_type')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
				</form:select>
				<span class="help-inline"><font color="red">*</font> </span>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">省市区：</label>
			<div class="controls">
				<input id="province" name="province.id" class="" type="hidden" value="${bizStorage.province.id}">
				<input id="city" name="city.id" class="" type="hidden" value="${bizStorage.city.id}">
				<input id="district" name="district.id" class="" type="hidden" value="${bizStorage.district.id}">
				<input id="pcaName" name="pcaName" type="text"
					   value="${not empty bizStorage.district.id ? bizStorage.province.name.concat(',').concat(bizStorage.city.name).concat(',').concat(bizStorage.district.name):''}"
					   data-msg-required="" class="valid" readonly="true">
				<a id="pcaButton" href="javascript:void(0)" class="btn" style="">&nbsp;
					<i class="icon-search"></i>&nbsp;
				</a>&nbsp;&nbsp;
			</div>
			<script type="text/javascript">
                $("#pcaButton").click(function() {
                    // 是否限制选择，如果限制，设置为disabled
                    if ($("#pcaButton").hasClass("disabled")) {
                        return true;
                    }
                    // 正常打开
                    top.$.jBox.open(
                        "iframe:${ctx}/tag/treeselect?url="
                        + encodeURIComponent("/sys/area/treeData")
                        + "&module=&checked=&extId=&isAll=",
                        "选择省市区",300,420,
                        {
                            ajaxData : {
                                selectIds : $("#district").val()
                            },
                            buttons : {
                                "确定" : "ok",
                                "清除" : "clear",
                                "关闭" : true
                            },
                            submit : function(v,h,f) {
                                if (v == "ok") {
                                    var tree = h.find("iframe")[0].contentWindow.tree;//h.find("iframe").contents();
                                    var ids = [], names = [], nodes = [], pNode = '', ppNode = '';
                                    if ("" == "true") {
                                        nodes = tree.getCheckedNodes(true);
                                    } else {
                                        nodes = tree.getSelectedNodes();
                                    }
                                    for (var i = 0; i < nodes.length; i++) {//
                                        if (nodes[i].isParent) {
                                            top.$.jBox.tip("不能选择父节点（" + nodes[i].name + "）请重新选择。");
                                            return false;
                                        }
                                        ids.push(nodes[i].id);
                                        names.push(nodes[i].name);//
                                        break; // 如果为非复选框选择，则返回第一个选择
                                    }

                                    if (nodes.length > 0) {
                                        if (nodes[0].level == 3) {
                                            pNode = nodes[0].getParentNode();
                                            ppNode = pNode.getParentNode();
                                            // console.log(ppNode.id + ',' + pNode.id + ',' + nodes[0].id);
                                            // console.log(ppNode.name + ',' + pNode.name + ',' + nodes[0].name);
                                            $("#province").val(ppNode.id);
                                            $("#city").val(pNode.id);
                                            $("#district").val(nodes[0].id);
                                            $('#pcaName').val(ppNode.name + ',' + pNode.name + ',' + nodes[0].name);
                                        } else {
                                            $("#province").val(nodes[0].id);
                                            $("#city").val(nodes[0].id);
                                            $("#district").val(nodes[0].id);
                                            $('#pcaName').val(nodes[0].name + ',' + nodes[0].name + ',' + nodes[0].name);
                                        }
                                    }
                                }else if (v == "clear") {
                                    $("#province").val('');
                                    $("#city").val('');
                                    $("#district").val('');
                                    $('#pcaName').val('');
                                }
                                if (typeof areaTreeselectCallBack == 'function') {
                                    areaTreeselectCallBack(v,h,f);
                                }
                            },
                            loaded : function(h) {
                                $(".jbox-content", top.document).css("overflow-y", "hidden");
                            }
                        }
                    );
                });
			</script>
		</div>



		<div class="control-group">
			<label class="control-label">地址：</label>
			<div class="controls">
				<form:input path="address" htmlEscape="false" maxlength="256" class="input-xlarge "/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">备注信息：</label>
			<div class="controls">
				<form:textarea path="remarks" htmlEscape="false" rows="4" maxlength="255" class="input-xxlarge "/>
			</div>
		</div>
		<div class="form-actions">
			<shiro:hasPermission name="biz:bizStorage:edit"><input id="btnSubmit" class="btn btn-primary" type="submit" value="保 存"/>&nbsp;</shiro:hasPermission>
			<input id="btnCancel" class="btn" type="button" value="返 回" onclick="history.go(-1)"/>
		</div>
	</form:form>
</body>
</html>