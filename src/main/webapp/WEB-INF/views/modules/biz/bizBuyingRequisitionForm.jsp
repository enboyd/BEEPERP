<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
    <title>请购单管理</title>
    <script src="${ctxStatic}/jquery/jquery.tablescroll.js" type="text/javascript"></script>
    <script src="${ctxStatic}/jquery/jquery.freezeheader.js" type="text/javascript"></script>
    <style type="text/css">
        div#addGoodsModal{
            line-height:30px;
            text-align:center;
            cursor:default;
            top:0;
            left:350px;
        }
        /*#gudingdw{box-sizing: border-box;}*/
        /*#gudingdw tbody {display: block;height: 195px;overflow-y: scroll;}*/
        /*#gudingdw thead,tbody tr {display: table;width: 100%;table-layout: fixed;box-sizing: border-box;}*/
        /*#gudingdw thead,tbody td {display: table;table-layout: fixed;box-sizing: border-box;}*/
        /*#gudingdw thead {width: calc(100% - 1em)}*/
        /*#gudingdw thead th {background: #ccc;}*/
    </style>
    <meta name="decorator" content="default"/>
    <script type="text/javascript">
        $(document).ready(function() {
            $("#gudingdw").freezeHeader({ 'height': '300px' });

            //$("#name").focus();

            $('#thetable').tableScroll({
                width:980,
                height:300
            });

            $("#inputForm").validate({
                submitHandler: function(form){
                    loading('正在提交，请稍等...');
                    form.submit();
                },
                errorContainer: "#messageBox",
                errorPlacement: function(error, element) {
                    $("#messageBox").text("输入有误，请先更正。");
                    if (element.is(":checkbox")||element.is(":radio")||element.parent().is(".input-append")){
                        error.appendTo(element.parent().parent());
                    } else {
                        error.insertAfter(element);
                    }
                }
            });

            $("#btnSubmitFrom").click(function(){
                //表单校验

                //提交表单
                $.ajax({
                    type: "POST",
                    url: "${ctx}/biz/bizGoods/ajaxSave",
                    data:$("#inputFormGoods").serialize(),
                    dataType: "text",
                    success: function(data){
                        //关闭formDiv
                        $("#goodsFormHtml").hide();
                        //清空form表单
                        //TODO
                        //打开listDiv
                        $("#add").removeClass('active');
                        $("#list").addClass('active');
                        $("#goodsListHtml").show();
                        //调用ajax查询商品列表
                        $("#addGoods").click();
                        if(data == 'success'){
                            //提示
                            top.$.jBox.tip("${'保存成功'}","${'success'}",{persistent:true,opacity:0});
                        }else{
                            top.$.jBox.tip("${'保存失败'}","${'error'}",{persistent:true,opacity:0});
                        }
                    }
                })
            });


            $("#addGoods").click(function () {
                $("#addGoodsModal").modal('show');
                var goodsCode = $("#goodsCode").val();
                var goodsName = $("#goodsName").val();
                //获取商品的列表
                $.ajax({
                    type: "POST",
                    url: "${ctx}/biz/bizGoods/modalPage",
                    data: {
                        'goodsCode': goodsCode,
                        'goodsName': goodsName
                    },
                    success: function(data){
                        //清空tbody
                        $("#goodsInfo").html("");
                        //添加分页
                        $("#level").html(data.html);
                        var list = data.list;
                        var rows = "";
                        for (var i = 0 ; i < list.length ; i++){
                            var specification = isNanVal(list[i].specification);
                            var stockAmount = isNanVal(list[i].stockAmount);
                            var purchasingAmount = isNanVal(list[i].purchasingAmount);
                            var minPurchaseQuantity = isNanVal(list[i].minPurchaseQuantity);
                            var row = "<tr>"
                                + "<td><input name='checkId' type='checkbox' value='"+list[i].goodsId+"'/></td>"
                                + "<td>"+ list[i].goodsCode+ "</td>"
                                + "<td>"+ list[i].goodsName+"</td>"
                                + "<td>"+ list[i].supplierName+"</td>"
                                + "<td>"+ list[i].goodsCategoryName+ "</td>"
                                + "<td>"+ specification+ "</td>"
                                + "<td>"+ list[i].unit+ "</td>"
                                + "<td>"+ stockAmount+ "</td>"
                                + "<td>"+ purchasingAmount+ "</td>"
                                + "<td>"+ minPurchaseQuantity+ "</td>"
                                + "</tr>";
                            rows += row;
                        }
                        $("#goodsInfo").html(rows);
                    }
                });
            })

            //全选
            $("#selectAll").click(function(){
                if($('#selectAll').is(':checked')) {
                    $("#goodsInfo :checkbox").attr("checked", true);
                }else{
                    $("#goodsInfo :checkbox").attr("checked", false);
                }
            });

            //商品的查询
            $("#select").click(function () {
                var goodsCode = $("#goodsCode").val();
                var goodsName = $("#goodsName").val();
                //获取商品的列表
                $.ajax({
                    type: "POST",
                    url: "${ctx}/biz/bizGoods/modalPage",
                    data: {
                        'goodsCode': goodsCode,
                        'goodsName': goodsName
                    },
                    success: function(data){
                        //清空tbody
                        $("#goodsInfo").html("");
                        //添加分页
                        $("#level").html(data.html);
                        var list = data.list;
                        var rows = "";
                        for (var i = 0 ; i < list.length ; i++){
                            var specification = isNanVal(list[i].specification);
                            var stockAmount = isNanVal(list[i].stockAmount);
                            var purchasingAmount = isNanVal(list[i].purchasingAmount);
                            var minPurchaseQuantity = isNanVal(list[i].minPurchaseQuantity);
                            var row = "<tr>"
                                + "<td><input name='checkId' type='checkbox' value='"+list[i].goodsId+"' /></td>"
                                + "<td>"+ list[i].goodsCode+ "</td>"
                                + "<td>"+ list[i].goodsName+"</td>"
                                + "<td>"+ list[i].supplierName+"</td>"
                                + "<td>"+ list[i].goodsCategoryName+ "</td>"
                                + "<td>"+ specification+ "</td>"
                                + "<td>"+ list[i].unit+ "</td>"
                                + "<td>"+ stockAmount+ "</td>"
                                + "<td>"+ purchasingAmount+ "</td>"
                                + "<td>"+ minPurchaseQuantity+ "</td>"
                                + "</tr>";
                            rows += row;
                        }
                        $("#goodsInfo").html(rows);
                    }
                });
            });


            //添加商品到请购单
            $("#addGoodsToPurchase").click(function () {
                $("#addGoodsModal").modal('hide');
                //获取到选择的商品
                var dataCheckId = [];
                $("input[name='checkId']:checked").each(function(i) {
                    dataCheckId.push($(this).val());
                });
                if(dataCheckId.length == 0){
                    alert("请选择要下单的商品！");
                }else{
                    $.ajax({
                        type: "POST",
                        url: "${ctx}/biz/bizGoods/getGoodsListByIds",
                        data: {
                            'goodsIds[]': dataCheckId
                        },
                        success: function(data){
                            //清空tbody
                            //$("#goodsList").html("");
                            var rows = "";
                            var goodsNum = $("#goodsNum").val();
                            //当前采购单明细有几条
                            var num = 0;
                            if(null == goodsNum){
                                goodsNum = data.length;
                            }else{
                                num = goodsNum;
                                goodsNum = Number(goodsNum) + data.length;
                            }
                            //用于商品小计和总价的计算
                            $("#goodsNum").val(goodsNum);
                            for (var i = 0 ; i < data.length ; i++){
                                var specification = isNanVal(data[i].specification);
                                var stockAmount = isNanVal(data[i].stockAmount);
                                var purchasingAmount = isNanVal(data[i].purchasingAmount);
                                var minPurchaseQuantity = isNanVal(data[i].minPurchaseQuantity);
                                var unit = isNanVal(data[i].unit);
                                //获取是第几行
                                var rowNum = Number(num)+Number(i);
                                var row = "<tr id='bizBuyingRequisitionList"+rowNum+"'>"
                                    + "<td>"+ data[i].goodsName+"</td>"
                                    + "<td>"
                                    + "<input type='hidden' id='supplierId"+rowNum+"' name='bizBuyingRequisitionList["+rowNum+"].supplierId' value='"+ data[i].supplierId+"' />"
                                    + "<input type='hidden' id='supplierName"+rowNum+"' name='bizBuyingRequisitionList["+rowNum+"].supplierName' value='"+ data[i].supplierName+"' />"
                                    + data[i].supplierName+"</td>"
                                    + "<td>"+ data[i].goodsCategoryName+ "</td>"
                                    + "<td>"+ specification+ "</td>"
                                    + "<td>"+ unit + "</td>"
                                    + "<td>"+ stockAmount+ "</td>"
                                    + "<td>"+ purchasingAmount+ "</td>"
                                    + "<td>"
                                    + "<input type='text' readonly='readonly' class='number' style='width: 100px' id='minPurchaseQuantity"+rowNum+"' name='bizBuyingRequisitionList["+rowNum+"].minPurchaseQuantity' value = '"+minPurchaseQuantity+"'>"
                                    + "</td>"
                                    + "<td>"
                                    + "<input id='demandTime"+rowNum+"' type='text' name='bizBuyingRequisitionList["+rowNum+"].demandTime' "
                                    + "maxlength='20' autocomplete='off' class='input-medium Wdate '"
                                    + "value=\"<fmt:formatDate value='${buyingRequisitiondetail.demandTime}' pattern='yyyy-MM-dd'/>\""
                                    + "onclick=\"WdatePicker({dateFmt:'yyyy-MM-dd',isShowClear:false});\" />"
                                    + "</td>"
                                    + "<td>"
                                    + "<input type='hidden' id='goodsId"+rowNum+"' name='bizBuyingRequisitionList["+rowNum+"].goodsId' value='"+ data[i].goodsId+"'>"
                                    + "<input type='hidden' id='delFlag"+rowNum+"' name='bizBuyingRequisitionList["+rowNum+"].delFlag' value='0'>"
                                    + "<input type='text' class='digits required' style='width: 100px' onchange='errorCheck()' id='expertAmount"+rowNum+"' name='bizBuyingRequisitionList["+rowNum+"].expertAmount' />"
                                    + "<span class='help-inline'><font color='red'>*</font> </span>"
                                    + "</td>"
                                    + "<shiro:hasPermission name='biz:buyingRequisition:buyingRequisitiondetailRemove'>"
                                    + "<td class='text-center' width='10'>"
                                    + "<span class='close' onclick='delRow(this.id)' id='"+rowNum+"' title='删除'>&times;</span>"
                                    + "</td>"
                                    + "</shiro:hasPermission>>"
                                    + "</tr>";
                                rows += row;
                            }
                            $("#goodsList").append(rows);
                        }
                    });
                }
                setTimeout(function () {
                    $("#hdScrollgudingdw").scrollTop($("#goodsList").height())
                },100)

            })
        });
        function isNanVal(val) {
            if(typeof(val)=="undefined"){
                return "";
            }
            return val;
        }

        function page(n,s){
            var goodsCode = $("#goodsCode").val();
            var goodsName = $("#goodsName").val();
            var pageNo = n;
            var pageSize = s;
            //获取商品的列表
            $.ajax({
                type: "POST",
                url: "${ctx}/biz/bizGoods/modalPage",
                data: {
                    'goodsCode': goodsCode,
                    'goodsName': goodsName,
                    'pageNo': pageNo,
                    'pageSize': pageSize
                },
                success: function(data){
                    closeLoading("");
                    //清空tbody
                    $("#goodsInfo").html("");
                    //添加分页
                    $("#level").html(data.html);
                    var list = data.list;
                    var rows = "";
                    for (var i = 0 ; i < list.length ; i++){
                        var stockAmount = isNanVal(list[i].stockAmount);
                        var supplierName = isNanVal(list[i].supplierName);
                        var goodsCategoryName = isNanVal(list[i].goodsCategoryName);
                        var purchasingAmount = isNanVal(list[i].purchasingAmount);
                        var unit = isNanVal(list[i].unit);
                        var minPurchaseQuantity = isNanVal(list[i].minPurchaseQuantity);
                        var specification = isNanVal(list[i].specification);
                        var row = "<tr>"
                            + "<td><input name='checkId' type='checkbox' value='"+list[i].goodsId+"'/></td>"
                            + "<td>"+ list[i].goodsCode+ "</td>"
                            + "<td>"+ list[i].goodsName+"</td>"
                            + "<td>"+ supplierName+"</td>"
                            + "<td>"+ goodsCategoryName+ "</td>"
                            + "<td>"+ specification+ "</td>"
                            + "<td>"+ unit+ "</td>"
                            + "<td>"+ stockAmount+ "</td>"
                            + "<td>"+ purchasingAmount+ "</td>"
                            + "<td>"+ minPurchaseQuantity+ "</td>"
                            + "</tr>";
                        rows += row;
                    }
                    $("#goodsInfo").html(rows);
                }
            });
            return false;
        }

        //删除行
        function delRow(row) {
            $("#bizBuyingRequisitionList"+row).css("display","none");
            //删除标识
            $("#delFlag"+row).val("1");
        }

        function showGoodsDiv(id) {
            $(".goodsDiv").removeClass("active");
            $("#"+id).addClass("active");
            if(id == "add"){
                $("#goodsFormHtml").show();
                $("#goodsListHtml").hide();
            }else if(id == "list"){
                $("#goodsFormHtml").hide();
                $("#goodsListHtml").show();
            }
        }

        /*
         *	清单数量输入校验 不允许输入负数和小数
         * 	请购数量不能小于最小起购数量
         *
         */
        function errorCheck(){
            var num = $("#goodsNum").val();
            for (var i = 0 ; i < num ;i ++){
                var rowId = "expertAmount"+i;
                var minPurNumberId = "minPurchaseQuantity" + i;
                var display = $("#"+rowId).css("display");
                if(display != "none"){
                    var expertAmount = $("#"+rowId).val();
                    var minPurNumber = $("#"+minPurNumberId).val();
                    if(expertAmount.indexOf("-") != -1 || expertAmount.indexOf(".") != -1){
                        //包含 - 和  . 清空input框的值
                        $("#"+rowId).val('');
                    }
                    if(parseInt(expertAmount) < parseInt(minPurNumber)){
                        alert("请购数量不能小于最小起购数量!")
                        $("#"+rowId).val(minPurNumber);
                    }
                }
            }
        }
    </script>
</head>
<body>
<ul class="nav nav-tabs">
    <li><a href="${ctx}/biz/bizBuyingRequisition/">请购单列表</a></li>
    <li class="active"><a href="${ctx}/biz/bizBuyingRequisition/form?buyingId=${bizBuyingRequisition.buyingId}">请购单<shiro:hasPermission name="biz:bizBuyingRequisition:edit">${not empty bizBuyingRequisition.buyingId?'修改':'添加'}</shiro:hasPermission><shiro:lacksPermission name="biz:bizBuyingRequisition:edit">查看</shiro:lacksPermission></a></li>
</ul><br/>
<form:form id="inputForm" modelAttribute="bizBuyingRequisition" action="${ctx}/biz/bizBuyingRequisition/save" method="post" class="form-horizontal" autocomplete="off">
    <form:hidden path="buyingId"/>
    <form:hidden path="buyingCode"/>
    <sys:message content="${message}"/>
    <input type="hidden" id="goodsNum" value="${bizBuyingRequisitionCount}">
    <div class="control-group">
        <label class="control-label"></label>
        <div class="controls">
            <input id="addGoods" class="btn btn-primary" value="批量挑选商品" autocomplete="off"/>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label">商品明细：</label>
        <div class="controls demo" <%--id="editInfo"--%>>
            <table  <%--id="thetable"--%> id="gudingdw"
                    class="table table-striped table-bordered table-condensed">
                <thead>
                <tr>
                    <th>商品名称</th>
                    <th>供货商</th>
                    <th>项目</th>
                    <th>规格</th>
                    <th>单位</th>
                    <th>库存数量</th>
                    <th>采购中数量</th>
                    <th>最小起购数量</th>
                    <th>需求时间</th>
                    <th>请购数量</th>
                    <shiro:hasPermission name="biz:buyingRequisition:buyingRequisitiondetailRemove"><th></th></shiro:hasPermission>
                </tr>
                </thead>
                <tbody id="goodsList">
                <c:forEach items="${bizBuyingRequisitionList}" var="buyingRequisitiondetail" varStatus="status">
                    <tr id="bizPurchaseDetailList${status.index}">
                        <td>${buyingRequisitiondetail.goodsName}</td>

                        <td>
                            <input type="hidden" id="supplierId${status.index}" name="bizBuyingRequisitionList[${status.index}].supplierId" value="${buyingRequisitiondetail.supplierId}" />
                            <input type="hidden" id="supplierName${status.index}" name="bizBuyingRequisitionList[${status.index}].supplierName" value="${buyingRequisitiondetail.supplierName}" />
                                ${buyingRequisitiondetail.supplierName}
                        </td>
                        <td>${buyingRequisitiondetail.goodsCategoryName}</td>
                        <td>${buyingRequisitiondetail.specification}</td>
                        <td>${buyingRequisitiondetail.unit}</td>
                        <td>${buyingRequisitiondetail.stockAmount}</td>
                        <td>${buyingRequisitiondetail.purchasingAmount}</td>
                        <td>
                            <input type="text" readonly="readonly" style="width: 100px"  id="minPurchaseQuantity${status.index}"
                                   name="purchaseDetailList[${status.index}].minPurchaseQuantity"
                                   value="${buyingRequisitiondetail.minPurchaseQuantity}">
                        </td>
                        <td>
                            <input id="demandTime${status.index}" name="bizBuyingRequisitionList[${status.index}].demandTime"
                                   type="text" maxlength="20" autocomplete="off" class="input-medium Wdate "
                                   value="<fmt:formatDate value="${buyingRequisitiondetail.demandTime}" pattern="yyyy-MM-dd"/>"
                                   onclick="WdatePicker({dateFmt:'yyyy-MM-dd',isShowClear:false});" />
                        </td>
                        <td>
                            <input type="hidden" id="goodsId${status.index}" name="bizBuyingRequisitionList[${status.index}].goodsId" value="${buyingRequisitiondetail.goodsId}" />
                            <input type="hidden" id="buyingDetailId${status.index}" name="bizBuyingRequisitionList[${status.index}].buyingDetailId" value="${buyingRequisitiondetail.buyingDetailId}"/>
                            <input type="hidden" id="delFlag${status.index}" name="bizBuyingRequisitionList[${status.index}].delFlag" value="${buyingRequisitiondetail.delFlag}"/>
                            <input type="text" class="digits required" style="width: 100px" onchange="errorCheck()" id="expertAmount${status.index}" name="bizBuyingRequisitionList[${status.index}].expertAmount" value="${buyingRequisitiondetail.expertAmount}" />
                            <span class="help-inline"><font color="red">*</font> </span>
                        </td>
                        <shiro:hasPermission name="biz:buyingRequisition:buyingRequisitiondetailRemove">
                            <td class="text-center" width="10">
                                <span class="close" onclick="delRow(${status.index})" title="删除">&times;</span>
                            </td>
                        </shiro:hasPermission>
                    </tr>
                </c:forEach>

                </tbody>
            </table>
        </div>
    </div>

    <div class="control-group">
        <label class="control-label">请购单类型：</label>
        <div class="controls">
            <form:select path="type" class="input-xlarge required" >
                <form:option value="" label="请选择" />
                <form:options items="${fns:getDictList('biz_buying_requisition_type')}"
                              itemLabel="label" itemValue="value" htmlEscape="false" />
            </form:select>
            <span class="help-inline"><font color="red">*</font> </span>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label">备注信息：</label>
        <div class="controls">
            <form:textarea path="remarks" htmlEscape="false" rows="4" maxlength="255" class="input-xxlarge "/>
        </div>
    </div>

    <div colspan=5 class="text-center">
        <c:if test="${not empty bizBuyingRequisition.buyingId && bizBuyingRequisition.status eq '30'}">
            <shiro:hasPermission name="biz:bizBuyingRequisition:quicklyCreatePurOrders">
                <a href="${ctx}/biz/bizBuyingRequisition/quicklyCreatePurOrders?buyingId=${bizBuyingRequisition.buyingId}" class="btn btn-primary">快捷采购</a>
            </shiro:hasPermission>
        </c:if>
        <!-- 审核中和审核通过不可修改该请购单 -->
        <c:if test="${empty bizBuyingRequisition.buyingId || bizBuyingRequisition.status == '5' || bizBuyingRequisition.status == '20'}">
            <shiro:hasPermission name="biz:bizBuyingRequisition:edit">
                <input id="btnSubmit" class="btn btn-primary" type="submit" value="保 存"/>&nbsp;
            </shiro:hasPermission>
        </c:if>
        <c:if test="${bizBuyingRequisition.status eq '10'}">
            <shiro:hasPermission name="biz:bizBuyingRequisition:toExamine">
                <a href="${ctx}/biz/bizBuyingRequisition/toExamine?buyingId=${bizBuyingRequisition.buyingId}" class="btn btn-primary">审批通过</a>
            </shiro:hasPermission>
            <shiro:hasPermission name="biz:bizBuyingRequisition:reject">
                <a href="${ctx}/biz/bizBuyingRequisition/reject?buyingId=${bizBuyingRequisition.buyingId}" class="btn btn-primary">驳回</a>
            </shiro:hasPermission>
        </c:if>
        <c:if test="${bizBuyingRequisition.status eq '20'}">
            <shiro:hasPermission name="biz:bizBuyingRequisition:resubmit">
                <a href="${ctx}/biz/bizBuyingRequisition/resubmit?buyingId=${bizBuyingRequisition.buyingId}" class="btn btn-primary">重新提交</a>
            </shiro:hasPermission>
        </c:if>
        <input id="btnCancel" class="btn" type="button" value="返 回" onclick="history.go(-1)"/>
    </div>
</form:form>


<div class="modal fade bs-example-modal-lg" id="addGoodsModal"  tabindex="-99" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"
     style="display:none;width: 850px;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                批量挑选
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <ul class="nav nav-tabs">
                    <li class="active goodsDiv" onclick="showGoodsDiv(this.id)" id="list"><a href="#" class="disabled">商品列表</a></li>
                    <li class="goodsDiv" onclick="showGoodsDiv(this.id)" id="add"><a href="#" class="disabled">商品添加</a></li>
                </ul>
                <div id="goodsListHtml">
                    <form:form id="searchForm" modelAttribute="bizGoods" action="" method="post" class="breadcrumb form-search">
                        <input id="
							pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
                        <input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/>
                        <ul class="ul-form">
                            <li><label>商品编号：</label>
                                <form:input path="goodsCode" htmlEscape="false" maxlength="64" class="input-medium"/>
                            </li>
                            <li><label>商品名称：</label>
                                <form:input path="goodsName" htmlEscape="false" maxlength="64" class="input-medium"/>
                            </li>
                            <li class="btns"><input id="select" class="btn btn-primary" type="button" value="查询"/></li>
                            <li class="clearfix"></li>
                        </ul>
                    </form:form>
                    <sys:message content="${message}"/>
                    <table id="contentTable" class="table table-striped table-bordered table-condensed">
                        <thead>
                        <tr>
                            <th><input type="checkbox" id="selectAll" /></th>
                            <th>商品编号</th>
                            <th>商品名称</th>
                            <th>供货商</th>
                            <th>项目</th>
                            <th>规格</th>
                            <th>单位</th>
                            <th>库存数量</th>
                            <th>采购中数量</th>
                            <th>最小起购数量</th>
                        </tr>
                        </thead>
                        <tbody id="goodsInfo">
                        </tbody>
                    </table>
                    <div class="pagination" id="level"></div>
                </div>


                <div id="goodsFormHtml" style="display: none;">
                    <form:form id="inputFormGoods" modelAttribute="bizGoods" class="form-horizontal">
                        <form:hidden path="id"/>
                        <sys:message content="${message}"/>
                        <div class="control-group">
                            <label class="control-label">商品编码：</label>
                            <div class="controls">
                                <form:input path="goodsCode" htmlEscape="false" maxlength="64" class="input-xlarge required"/>
                                <span class="help-inline"><font color="red">*</font> </span>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">商品名称：</label>
                            <div class="controls">
                                <form:input path="goodsName" htmlEscape="false" maxlength="64" class="input-xlarge required"/>
                                <span class="help-inline"><font color="red">*</font> </span>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">供应商：</label>
                            <div class="controls">
                                <form:select path="supplierId" class="input-xlarge required">
                                    <form:option value="" label=""/>
                                    <c:forEach items="${bizSupplierList}" varStatus="bizSupplier">
                                        <c:set var="et" value="${bizSupplier.current}"/>
                                        <c:if test="${bizGoods.supplierId eq et.supplierId}">
                                            <option value="${et.supplierId}" selected>${et.supplierName}</option>
                                        </c:if>
                                        <c:if test="${bizGoods.supplierId != et.supplierId}">
                                            <option value="${et.supplierId}">${et.supplierName}</option>
                                        </c:if>
                                    </c:forEach>
                                </form:select>
                                <span class="help-inline"><font color="red">*</font> </span>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">项目：</label>
                            <div class="controls">
                                <sys:treeselect id="goodsCategoryId" name="goodsCategoryId" value="${bizGoods.goodsCategoryId}"
                                                labelName="goodsCategoryName" labelValue="${bizGoods.goodsCategoryName}" title="产品类型"
                                                url="/biz/bizGoodsCategory/treeData?isEnabled=true" extId="0" cssClass="required" allowClear="true" />

                                <span class="help-inline"><font color="red">*</font> </span>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">商品分类：</label>
                            <div class="controls">
                                <sys:treeselect id="classifyId" name="classifyId" value="${bizGoods.classifyId}"
                                                labelName="classifyName" labelValue="${bizGoods.classifyName}" title="商品类型"
                                                url="/biz/bizGoodsClassify/treeData?isEnabled=true"  extId="0" cssClass="required"
                                                allowClear="true"/>
                                <span class="help-inline"><font color="red">*</font> </span>
                            </div>
                        </div>

                        <div class="control-group">
                            <label class="control-label">仓库：</label>
                            <div class="controls">
                                <form:select path="storageId" class="input-xlarge required">
                                    <form:option value="" label=""/>
                                    <c:forEach items="${bizStorageList}" varStatus="bizStorageList">
                                        <c:set var="et" value="${bizStorageList.current}"/>
                                        <c:if test="${bizGoods.storageId eq et.storageId}">
                                            <option value="${et.storageId}" selected>${et.storageName}</option>
                                        </c:if>
                                        <c:if test="${bizGoods.storageId != et.storageId}">
                                            <option value="${et.storageId}">${et.storageName}</option>
                                        </c:if>
                                    </c:forEach>
                                </form:select>
                                <span class="help-inline"><font color="red">*</font> </span>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">规格：</label>
                            <div class="controls">
                                <form:input path="specification" htmlEscape="false" maxlength="64" class="input-xlarge "/>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">单位：</label>
                            <div class="controls">
                                <form:input path="unit" htmlEscape="false" maxlength="10" class="input-xlarge required"/>
                                <span class="help-inline"><font color="red">*</font> </span>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">保质期/月：</label>
                            <div class="controls">
                                <form:input path="expirationDate" htmlEscape="false" maxlength="10" class="input-xlarge digits"/>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">预警数量：</label>
                            <div class="controls">
                                <form:input path="warningAmount" htmlEscape="false" maxlength="10" class="input-xlarge digits"/>
                                <span class="help-inline"><font color="red">*设置为0则不预警</font> </span>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">备注信息：</label>
                            <div class="controls">
                                <form:textarea path="remarks" htmlEscape="false" rows="4" maxlength="255" class="input-xxlarge "/>
                            </div>
                        </div>
                        <div class="form-actions">
                            <shiro:hasAnyPermissions name="biz:bizGoods:edit">
                                <input id="btnSubmitFrom" class="btn btn-primary" value="保 存"/>
                            </shiro:hasAnyPermissions>
                        </div>
                    </form:form>
                </div>
            </div>
            <div></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                <button type="button" class="btn btn-primary" id="addGoodsToPurchase"> 添加</button>
            </div>
        </div>
    </div>
</div>
</body>
</html>