<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>条码管理</title>
	<meta name="decorator" content="default"/>
	<script src="${ctxStatic}/dist/zoomify.min.js"></script>
	<link rel="stylesheet" href="${ctxStatic}/dist/zoomify.min.css">
	<script type="text/javascript">
		$(document).ready(function() {
			$('.example img').zoomify();
		});
		function page(n,s){
			$("#pageNo").val(n);
			$("#pageSize").val(s);
			$("#searchForm").submit();
        	return false;
        }

		/**
		 * 单个打印条码
		 */
		function dayinBizBarcode(id) {
			loading("正在打印，请稍等...");
			$.ajax({
				type: "POST",
				url: "${ctx}/biz/bizStockIn/getInfoDan",
				data: {
					"id":id
				},
				success: function(data){
					closeLoading("");
					var info = data;
					var goodsCode = info.goodsCode;
					var manufactureDate = info.manufactureDate;
					var goodsName = info.goodsName;
					var pici = info.name.substring(0,12);
					var number = info.number;
					var name = info.name;
					printQRcode(goodsCode,manufactureDate,goodsName,pici,number,name);
					//打印次数加1
                    printNumberAdd(name);	//条码打印次数增加1
				}
			})
		}
		/**
		 * 打印接的打印
		 * goodsCode 商品编号
		 * date 生产日期
		 * goodsName 商品名称
		 * pici 批次
		 * number 数量
		 * code 条形码编号
		 */
		function printQRcode(goodsCode,date,goodsName,pici,number,code){
			var TSCObj = new ActiveXObject("TSCActiveX.TSCLIB");//打印机使用的dll
			TSCObj.ActiveXopenport ("TSC TTP-243E Pro");
			TSCObj.ActiveXsetup("70","40","4","15","0","2.5","0");//打印机设置
			TSCObj.ActiveXsendcommand("DIRECTION 1");  //设置标签方向 ,DIRECTION 1 左上角 (x,y)={0,0};DIRECTION 0 右下角 (x,y)={0,0};
			TSCObj.ActiveXsendcommand ("SET TEAR ON");
			TSCObj.ActiveXclearbuffer();
			TSCObj.ActiveXwindowsfont (65, 20, 30, 0, 2, 0, "Arial", "存货编码："+goodsCode);
			TSCObj.ActiveXwindowsfont (350, 20, 30, 0, 2, 0, "Arial", date);
			TSCObj.ActiveXwindowsfont (65, 60, 30, 0, 2, 0, "Arial", "存货名称："+goodsName);
			TSCObj.ActiveXwindowsfont (65, 100, 30, 0, 2, 0, "Arial", "批  次："+pici);
			TSCObj.ActiveXwindowsfont (365, 100, 30, 0, 2, 0, "Arial", "数量："+number);
			TSCObj.ActiveXbarcode ("55", "150", "128", "100", "1", "0", "3", "3", code);
			TSCObj.ActiveXprintlabel ("1","1");//（打印份数,每页打印张数）
			TSCObj.ActiveXclearbuffer();//清除
			TSCObj.ActiveXcloseport();//关闭打印端口
		}

        /**
         * 条形码打印次数增加1
         * @param name
         */
        function printNumberAdd(name){
            $.ajax({
                type: "POST",
                url: "${ctx}/biz/bizBarcode/printNumberAdd",
                data: {
                    "barCode":name
                }
            })
        }

	</script>
</head>
<body>
	<ul class="nav nav-tabs">
		<li class="active"><a href="${ctx}/biz/bizBarcode/">条码列表</a></li>
	</ul>
	<form:form id="searchForm" modelAttribute="bizBarcode" action="${ctx}/biz/bizBarcode/" method="post" class="breadcrumb form-search">
		<input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
		<input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/>
		<ul class="ul-form">
			<li><label>条码编号：</label>
				<form:input path="name" htmlEscape="false" maxlength="64" class="input-medium"/>
			</li>
			<li><label>商品编号：</label>
				<form:input path="goodsCode" htmlEscape="false" maxlength="64" class="input-medium"/>
			</li>
			<li><label>商品名称：</label>
				<form:input path="goodsName" htmlEscape="false" maxlength="64" class="input-medium"/>
			</li>
			<li><label>生产日期：</label>
				<input name="manufactureDateDa" type="text" readonly="readonly" maxlength="20" class="input-medium Wdate"
					   value="<fmt:formatDate value="${bizBarcode.manufactureDateDa}" pattern="yyyy-MM-dd"/>"
					   onclick="WdatePicker({dateFmt:'yyyy-MM-dd',isShowClear:false});"/>
			</li>
			<li class="btns"><input id="btnSubmit" class="btn btn-primary" type="submit" value="查询"/></li>
			<li class="clearfix"></li>
		</ul>
	</form:form>
	<sys:message content="${message}"/>
	<table id="contentTable" class="table table-striped table-bordered table-condensed">
		<thead>
			<tr>
				<th>条码编号</th>
				<th>当前存放商品</th>
				<th>商品编号</th>
				<th>当前存放数量</th>
				<th>生产日期</th>
				<th>区域</th>
				<th>条形码</th>
				<th>打印次数</th>
				<th>打印</th>
			</tr>
		</thead>
		<tbody>
		<c:forEach items="${page.list}" var="bizBarcode">
			<tr>
				<td>${bizBarcode.name}</td>
				<td>${bizBarcode.goodsName}</td>
				<td>${bizBarcode.goodsCode}</td>
				<td>${bizBarcode.number}</td>
				<td>${bizBarcode.manufactureDate}</td>
				<td>${bizBarcode.storageLocationName}</td>
				<td>
					<div class="example">
						<p><img src="${bizBarcode.url}" style="width: 150px;height: 75px" class="img-rounded" alt=""></p>
					</div>
				</td>
				<td>${bizBarcode.printNumber}</td>
				<td>
					<img onclick="dayinBizBarcode('${bizBarcode.id}')" src="${ctxStatic}/images/dayin.png" alt="打印" style="width: 30px;height: 30px;margin-left: 10px">
				</td>
			</tr>
		</c:forEach>
		</tbody>
	</table>
	<div class="pagination">${page}</div>
</body>
</html>