<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>订单管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
			//$("#name").focus();
			$("#inputForm").validate({
				submitHandler: function(form){
					loading('正在提交，请稍等...');
					form.submit();
				},
				errorContainer: "#messageBox",
				errorPlacement: function(error, element) {
					$("#messageBox").text("输入有误，请先更正。");
					if (element.is(":checkbox")||element.is(":radio")||element.parent().is(".input-append")){
						error.appendTo(element.parent().parent());
					} else {
						error.insertAfter(element);
					}
				}
			});
		});

        function addRow(list, idx, tpl, row){
            $(list).append(Mustache.render(tpl, {
                idx: idx, delBtn: true, row: row
            }));
            $(list+idx).find("select").each(function(){
                $(this).val($(this).attr("data-value"));
            });
            $(list+idx).find("input[type='checkbox'], input[type='radio']").each(function(){
                var ss = $(this).attr("data-value").split(',');
                for (var i=0; i<ss.length; i++){
                    if($(this).val() == ss[i]){
                        $(this).attr("checked","checked");
                    }
                }
            });
        }

        function delRow(obj, prefix){
            var id = $(prefix+"_id");
            var delFlag = $(prefix+"_delFlag");
            if (id.val() == ""){
                $(obj).parent().parent().remove();
            }else if(delFlag.val() == "0"){
                delFlag.val("1");
                $(obj).html("&divide;").attr("title", "撤销删除");
                $(obj).parent().parent().addClass("error");
            }else if(delFlag.val() == "1"){
                delFlag.val("0");
                $(obj).html("&times;").attr("title", "删除");
                $(obj).parent().parent().removeClass("error");
            }
        }
        function print(id) {
            window.top.mainFrame.location.href = "${ctx}/biz/bizOrder/print?id=" + id;
        }
	</script>
</head>
<body>
	<ul class="nav nav-tabs">
		<li><a href="${ctx}/biz/bizOrder/">销售出库单列表</a></li>
		<li class="active"><a href="${ctx}/biz/bizOrder/form?id=${bizOrder.id}">销售出库单<shiro:hasPermission name="biz:bizOrder:edit">${not empty bizOrder.id?'修改':'添加'}</shiro:hasPermission><shiro:lacksPermission name="biz:bizOrder:edit">查看</shiro:lacksPermission></a></li>
	</ul><br/>
	<form:form id="inputForm" modelAttribute="bizOrder" action="${ctx}/biz/bizOrder/save" method="post" class="form-horizontal">
		<form:hidden path="id"/>
		<sys:message content="${message}"/>
		<div class="control-group">
			<label class="control-label">客户名称：</label>
			<div class="controls">
				<form:input path="customerName" htmlEscape="false" maxlength="64" class="input-xlarge "/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">电话：</label>
			<div class="controls">
				<form:input path="phone" htmlEscape="false" maxlength="64" class="input-xlarge "/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">团队名称：</label>
			<div class="controls">
				<form:input path="teamName" htmlEscape="false" maxlength="64" class="input-xlarge "/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">订货时间：</label>
			<div class="controls">
				<input name="orderTime" type="text" readonly="readonly" maxlength="20" class="input-medium Wdate "
					   value="<fmt:formatDate value="${bizOrder.orderTime}" pattern="yyyy-MM-dd"/>"
					   onclick="WdatePicker({dateFmt:'yyyy-MM-dd',isShowClear:false});"/>
			</div>
		</div>

		<div class="control-group">
			<label class="control-label">出库明细：</label>
			<div class="controls">
				<td colspan="4"  class="controls">
					<!-- <div class="controls"> -->
					<table id="contentTable"
						   class="table table-striped table-bordered table-condensed">
						<thead>
						<tr>
							<th class="hide"></th>
							<th>商品</th>
							<th>规格</th>
							<th>数量</th>
							<th>已出库数</th>
							<th>备注</th>
							<shiro:hasPermission name="biz:bizOrder:edit">
								<th width="10">&nbsp;</th>
							</shiro:hasPermission>
						</tr>
						</thead>
						<tbody id="orderDetailList">
						</tbody>
						<%--<shiro:hasPermission name="biz:bizOrder:edit">
							<tfoot>
							<tr>
								<td colspan="4">
									<a href="javascript:"
									   onclick="addRow('#orderDetailList', orderDetailListRowIdx, orderDetailListTpl);orderDetailListRowIdx = orderDetailListRowIdx + 1;"
									   class="btn">新增
									</a>
								</td>
							</tr>
							</tfoot>
						</shiro:hasPermission>--%>
					</table>
					<script type="text/template" id="orderDetailListTpl">//<!--
						<tr id="orderDetailList{{idx}}">
							<td class="hide">
								<input id="orderDetailList{{idx}}_id" name="orderDetailList[{{idx}}].id" type="hidden" value="{{row.id}}"/>
								<input id="orderDetailList{{idx}}_delFlag" name="orderDetailList[{{idx}}].delFlag" type="hidden" value="0"/>
							</td>
							<td style="text-align:center;vertical-align:middle; width: 15%;">
								<input id="orderDetailList{{idx}}projectdetailName" name="orderDetailList[{{idx}}].projectdetailName" type="text" value="{{row.projectdetailName}}" maxlength="64" class="input-small " readonly="true"  placeholder=""/>
							</td>
							<td style="text-align:center;vertical-align:middle; width: 15%;">
								<input id="orderDetailList{{idx}}specification" name="orderDetailList[{{idx}}].specification" type="text" value="{{row.specification}}" maxlength="64" class="input-small " readonly="true" placeholder=""/>
							</td>
							<td style="text-align:center;vertical-align:middle; width: 15%;">
								<input id="orderDetailList{{idx}}_quantity" name="orderDetailList[{{idx}}].quantity" type="text" value="{{row.quantity}}" maxlength="64" class="input-small " readonly="true" placeholder=""/>
							</td>
							<td style="text-align:center;vertical-align:middle; width: 15%;">
								<input id="orderDetailList{{idx}}_outNum" name="orderDetailList[{{idx}}].outNum" type="text" value="{{row.outNum}}" maxlength="64" class="input-small " readonly="true" placeholder=""/>
							</td>
							<td style="text-align:center;vertical-align:middle; width: 35%;">
                                <textarea id="orderDetailList{{idx}}_remarks" name="orderDetailList[{{idx}}].remarks" rows="1" maxlength="255" class="input-xlarge " style="resize: none;">{{row.remarks}}</textarea>
                             </td>
						</tr>//-->
					</script>
					<script type="text/javascript">
                        var orderDetailListRowIdx = 0, orderDetailListTpl = $("#orderDetailListTpl").html().replace(/(\/\/\<!\-\-)|(\/\/\-\->)/g,"");
                        $(document).ready(function() {
                            var data = ${fns:toJson(orderDetailList)};
                            for (var i=0; i<data.length; i++){
                                addRow('#orderDetailList', orderDetailListRowIdx, orderDetailListTpl, data[i]);
                                orderDetailListRowIdx = orderDetailListRowIdx + 1;
                            }
                        });
					</script>
				</td>
			</div>
		</div>

		<div class="control-group">
			<label class="control-label">收货地址：</label>
			<div class="controls">
				<form:textarea path="address" htmlEscape="false" rows="4"  class="input-xxlarge "/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">备注：</label>
			<div class="controls">
				<form:textarea path="remarks" htmlEscape="false" rows="4" class="input-xxlarge "/>
			</div>
		</div>
		<div class="form-actions">
			<shiro:hasPermission name="biz:bizOrder:print">
				<input id="btnPrint" class="btn btn-primary" type="button" onclick="print('${bizOrder.id}')" <c:if test="${bizOrder.orderApprovalStatus eq '10'}">disabled="true"</c:if> value="打 印"/>&nbsp;
			</shiro:hasPermission>
			<shiro:hasPermission name="biz:bizOrder:edit"><input id="btnSubmit" class="btn btn-primary" type="submit" value="保 存"/>&nbsp;</shiro:hasPermission>
			<input id="btnCancel" class="btn" type="button" value="返 回" onclick="history.go(-1)"/>
		</div>
	</form:form>
</body>
</html>