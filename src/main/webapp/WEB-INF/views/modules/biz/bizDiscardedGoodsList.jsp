<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>废弃商品管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
			
		});
		function page(n,s){
			$("#pageNo").val(n);
			$("#pageSize").val(s);
			$("#searchForm").submit();
        	return false;
        }
	</script>
</head>
<body>
	<ul class="nav nav-tabs">
		<li class="active"><a href="${ctx}/biz/bizDiscardedGoods/list">废弃商品</a></li>
		<li><a href="${ctx}/biz/bizDiscardedGoods/list?type=1">过期待废弃</a>
	</ul>
	<form:form id="searchForm" modelAttribute="bizDiscardedGoods" action="${ctx}/biz/bizDiscardedGoods/" method="post" class="breadcrumb form-search">
		<input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
		<input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/>
		<ul class="ul-form">
			<li class="btns"><input id="btnSubmit" class="btn btn-primary" type="submit" value="查询"/></li>
			<li class="clearfix"></li>
		</ul>
	</form:form>
	<sys:message content="${message}"/>
	<table id="contentTable" class="table table-striped table-bordered table-condensed">
		<thead>
			<tr>
				<th>商品编号</th>
				<th>商品名称</th>
				<th>条形码编号</th>
				<th>批次</th>
				<th>生产日期</th>
				<th>保质期/月</th>
				<th>数量</th>
				<th>规格</th>
				<th>库区名称</th>
				<th>状态</th>
			</tr>
		</thead>
		<tbody>
		<c:forEach items="${page.list}" var="bizDiscardedGoods">
			<tr>
				<td><a href="${ctx}/biz/bizGoods/form?id=${bizDiscardedGoods.goodsId}">
					${bizDiscardedGoods.goodsCode}
				</a></td>
				<td>
					${bizDiscardedGoods.goodsName}
				</td>
				<td>
					${bizDiscardedGoods.barcodeName}
				</td>
				<td>
					${bizDiscardedGoods.batch}
				</td>
				<td>
					<fmt:formatDate value="${bizDiscardedGoods.manufactureDate}" pattern="yyyy-MM-dd"/>
				</td>
				<td>
					${bizDiscardedGoods.expirationDate}
				</td>
				<td>
					${bizDiscardedGoods.number}
				</td>
				<td>
					${bizDiscardedGoods.specification}
				</td>
				<td>
					${bizDiscardedGoods.storageLocationName}
				</td>
				<td>
					${fns:getDictLabel(bizDiscardedGoods.type, 'biz_discardedGoods_type', '')}
				</td>
			</tr>
		</c:forEach>
		</tbody>
	</table>
	<div class="pagination">${page}</div>
</body>
</html>