<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>商品品牌管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
			
		});
		function page(n,s){
			$("#pageNo").val(n);
			$("#pageSize").val(s);
			$("#searchForm").submit();
        	return false;
        }
	</script>
</head>
<body>
	<ul class="nav nav-tabs">
		<li class="active"><a href="${ctx}/biz/bizGoodsBrand/">品牌列表</a></li>
		<shiro:hasPermission name="biz:bizGoodsBrand:edit"><li><a href="${ctx}/biz/bizGoodsBrand/form">品牌添加</a></li></shiro:hasPermission>
	</ul>
	<form:form id="searchForm" modelAttribute="bizGoodsBrand" action="${ctx}/biz/bizGoodsBrand/" method="post" class="breadcrumb form-search">
		<input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
		<input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/>
		<ul class="ul-form">
			<li><label>品牌名称：</label>
				<form:input path="goodsBrandName" htmlEscape="false" maxlength="64" class="input-medium"/>
			</li>
			<li class="btns"><input id="btnSubmit" class="btn btn-primary" type="submit" value="查询"/></li>
			<li class="clearfix"></li>
		</ul>
	</form:form>
	<sys:message content="${message}"/>
	<table id="contentTable" class="table table-striped table-bordered table-condensed">
		<thead>
			<tr>
				<th>品牌名称</th>
				<th>创建者</th>
				<th>创建时间</th>
				<th>更新者</th>
				<th>更新时间</th>
				<th>备注</th>
				<shiro:hasAnyPermissions name="biz:bizGoodsBrand:edit, biz:bizGoodsBrand:remove"><th>操作</th></shiro:hasAnyPermissions>
			</tr>
		</thead>
		<tbody>
		<c:forEach items="${page.list}" var="bizGoodsBrand">
			<tr>
				<td><a href="${ctx}/biz/bizGoodsBrand/form?id=${bizGoodsBrand.goodsBrandId}">
					${bizGoodsBrand.goodsBrandName}
				</a></td>
				<td>
					${bizGoodsBrand.createBy.name}
				</td>
				<td>
					<fmt:formatDate value="${bizGoodsBrand.createDate}" pattern="yyyy-MM-dd HH:mm:ss"/>
				</td>
				<td>
					${bizGoodsBrand.updateBy.name}
				</td>
				<td>
					<fmt:formatDate value="${bizGoodsBrand.updateDate}" pattern="yyyy-MM-dd HH:mm:ss"/>
				</td>
				<td>
						${bizGoodsBrand.remarks}
				</td>
				<shiro:hasAnyPermissions name="biz:bizGoodsBrand:edit, biz:bizGoodsBrand:remove"><td>
    				<a href="${ctx}/biz/bizGoodsBrand/form?id=${bizGoodsBrand.goodsBrandId}">修改</a>
					<a href="${ctx}/biz/bizGoodsBrand/delete?id=${bizGoodsBrand.goodsBrandId}" onclick="return confirmx('确认要删除该商品品牌吗？', this.href)">删除</a>
				</td></shiro:hasAnyPermissions>
			</tr>
		</c:forEach>
		</tbody>
	</table>
	<div class="pagination">${page}</div>
</body>
</html>