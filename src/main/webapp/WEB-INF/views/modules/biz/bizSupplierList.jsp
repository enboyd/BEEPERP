<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>供应商管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
			
		});
		function page(n,s){
			$("#pageNo").val(n);
			$("#pageSize").val(s);
			$("#searchForm").submit();
        	return false;
        }
	</script>
</head>
<body>
	<ul class="nav nav-tabs">
		<li class="active"><a href="${ctx}/biz/bizSupplier/">供应商列表</a></li>
		<shiro:hasPermission name="biz:bizSupplier:edit"><li><a href="${ctx}/biz/bizSupplier/form">供应商添加</a></li></shiro:hasPermission>
	</ul>
	<form:form id="searchForm" modelAttribute="bizSupplier" action="${ctx}/biz/bizSupplier/" method="post" class="breadcrumb form-search">
		<input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
		<input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/>
		<ul class="ul-form">
			<li><label>供应商名称：</label>
				<form:input path="supplierName" htmlEscape="false" maxlength="64" class="input-medium"/>
			</li>
			<li><label>联系电话：</label>
				<form:input path="phone" htmlEscape="false" maxlength="18" class="input-medium"/>
			</li>
			<li class="btns"><input id="btnSubmit" class="btn btn-primary" type="submit" value="查询"/></li>
			<li class="clearfix"></li>
		</ul>
	</form:form>
	<sys:message content="${message}"/>
	<table id="contentTable" class="table table-striped table-bordered table-condensed">
		<thead>
			<tr>
				<th>供应商名称</th>
				<th>联系人</th>
				<th>联系电话</th>
				<th>地址</th>
				<th>审批状态</th>
				<th>备注信息</th>
				<shiro:hasAnyPermissions name="biz:bizSupplier:edit,biz:bizSupplier:remove,biz:supplier:startApproval,biz:supplier:passApproval,biz:supplier:dismissApproval"><th>操作</th></shiro:hasAnyPermissions>
			</tr>
		</thead>
		<tbody>
		<c:forEach items="${page.list}" var="bizSupplier">
			<tr>
				<td>
					<a href="${ctx}/biz/bizSupplier/form?id=${bizSupplier.supplierId}">
						${bizSupplier.supplierName}
					</a>
				</td>
				<td>
					${bizSupplier.contact}
				</td>
				<td>
					${bizSupplier.phone}
				</td>
				<td>
					${bizSupplier.address}
				</td>
				<td>
					<c:if test="${(bizSupplier.appState eq '5') || (bizSupplier.appState eq '10')}">
						<span style="color: red">${fns:getDictLabel(bizSupplier.appState, 'supplier_app_state', '')}</span>
					</c:if>
					<c:if test="${(bizSupplier.appState eq '15')}">
						<span style="color: green">${fns:getDictLabel(bizSupplier.appState, 'supplier_app_state', '')}</span>
					</c:if>
				</td>
				<td>
					${bizSupplier.remarks}
				</td>
				<shiro:hasAnyPermissions name="biz:bizSupplier:edit,biz:bizSupplier:remove,biz:supplier:startApproval,biz:supplier:passApproval,biz:supplier:dismissApproval">
					<td>
						<shiro:hasPermission name="biz:bizSupplier:edit">
    						<a href="${ctx}/biz/bizSupplier/form?id=${bizSupplier.supplierId}">修改</a>
						</shiro:hasPermission>
						<shiro:hasPermission name="biz:bizSupplier:remove">
							<a href="${ctx}/biz/bizSupplier/delete?id=${bizSupplier.supplierId}" onclick="return confirmx('确认要删除该供应商吗？', this.href)">删除</a>
						</shiro:hasPermission>
						<c:if test="${bizSupplier.appState eq '10'}">
							<shiro:hasPermission name="biz:supplier:startApproval">
								<a id="startApproval" href="${ctx}/biz/bizSupplier/approval?id=${bizSupplier.supplierId}&type=anew">重新提交</a>
							</shiro:hasPermission>
						</c:if>
						<c:if test="${bizSupplier.appState eq '5'}">
							<shiro:hasPermission name="biz:supplier:passApproval">
								<a id="passApproval" href="${ctx}/biz/bizSupplier/approval?id=${bizSupplier.supplierId}&type=pass">审批通过</a>
							</shiro:hasPermission>
							<shiro:hasPermission name="biz:supplier:dismissApproval">
								<a id="dismissApproval" href="${ctx}/biz/bizSupplier/approval?id=${bizSupplier.supplierId}&type=dismiss">驳回</a>
							</shiro:hasPermission>
						</c:if>
					</td>
				</shiro:hasAnyPermissions>
			</tr>
		</c:forEach>
		</tbody>
	</table>
	<div class="pagination">${page}</div>
</body>
</html>