<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>退换货管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
			
		});
		function page(n,s){
			$("#pageNo").val(n);
			$("#pageSize").val(s);
			$("#searchForm").submit();
        	return false;
        }

		//驳回
        function dismiss(id) {
            top.$.jBox.confirm('是否确认驳回','系统提示',function(v,h,f){
                closeLoading();
                debugger

                if(v=='ok'){
                    $.ajax({
                        type: "POST",
                        url: "${ctx}/biz/smReturnBarter/dismiss",
                        data: {
                            id:id
                        },
                        success: function(data){
                            top.$.jBox.info(data,'提示');
                            $('#searchForm').submit();
                        }
                    });
                }
            },{buttonsFocus:1, closed:function(){
                closeLoading();
            }
            });
        }

        //通过
        function approve(id) {
            top.$.jBox.confirm('是否确认通过','系统提示',function(v,h,f){
                closeLoading();
                if(v=='ok'){
                    $.ajax({
                        type: "POST",
                        url: "${ctx}/biz/smReturnBarter/approve",
                        data: {
                            id:id
                        },
                        success: function(data){
                            top.$.jBox.info(data,'提示');
                            $('#searchForm').submit();
                        }
                    });
                }
            },{buttonsFocus:1, closed:function(){
                closeLoading();
            }
            });
        }
	</script>
</head>
<body>
	<ul class="nav nav-tabs">
		<li class="active"><a href="${ctx}/biz/smReturnBarter/">退换货列表</a></li>
		<shiro:hasPermission name="sm:smReturnBarter:add">
			<li><a href="${ctx}/biz/smReturnBarter/form">退换货添加</a></li>
		</shiro:hasPermission>
	</ul>
	<form:form id="searchForm" modelAttribute="smReturnBarter" action="${ctx}/biz/smReturnBarter/" method="post" class="breadcrumb form-search">
		<input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
		<input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/>
		<ul class="ul-form">
			<li><label>编码：</label>
				<form:input path="name" htmlEscape="false" maxlength="64" class="input-medium"/>
			</li>
			<li><label>客户：</label>
				<form:input path="customerName" htmlEscape="false" maxlength="24" class="input-medium"/>
			</li>
			<li class="btns"><input id="btnSubmit" class="btn btn-primary" type="submit" value="查询"/></li>
			<li class="clearfix"></li>
		</ul>
	</form:form>
	<sys:message content="${message}"/>
	<table id="contentTable" class="table table-striped table-bordered table-condensed">
		<thead>
			<tr>
				<th>编码</th>
				<th>客户</th>
				<th>退/换货</th>
				<th>审核状态</th>
				<shiro:hasAnyPermissions name="sm:smReturnBarter:approve,sm:smReturnBarter:dismiss,sm:smReturnBarter:remove"><th width="200px">操作</th></shiro:hasAnyPermissions>
			</tr>
		</thead>
		<tbody>
		<c:forEach items="${page.list}" var="smReturnBarter">
			<tr>
				<td><a href="${ctx}/biz/smReturnBarter/form?id=${smReturnBarter.id}">
					${smReturnBarter.name}
				</a></td>
				<td>
					${smReturnBarter.customerName}
				</td>
				<td>
					${fns:getDictLabel(smReturnBarter.category, 'sm_return_barter_category', '')}
				</td>
				<td>
					${fns:getDictLabel(smReturnBarter.auditStatus, 'sm_return_barter_auditStatus', '')}
				</td>
				<shiro:hasAnyPermissions name="sm:smReturnBarter:approve,sm:smReturnBarter:dismiss,sm:smReturnBarter:remove">
					<td>
	    				<shiro:hasPermission name="sm:smReturnBarter:remove">
							<a href="${ctx}/biz/smReturnBarter/delete?id=${smReturnBarter.id}" onclick="return confirmx('确认要删除该退换货吗？', this.href)">删除</a>
						</shiro:hasPermission>
						<c:if test="${smReturnBarter.auditStatus eq '10'}">
							<shiro:hasPermission name="sm:smReturnBarter:approve">
								<a id="approve" onclick="approve('${smReturnBarter.id}')" href="javaScript:void(0)">审批通过</a>
							</shiro:hasPermission>
							<shiro:hasPermission name="sm:smReturnBarter:dismiss">
								<a id="dismiss" onclick="dismiss('${smReturnBarter.id}')" href="javaScript:void(0)">驳回</a>
							</shiro:hasPermission>
						</c:if>
					</td>
				</shiro:hasAnyPermissions>
			</tr>
		</c:forEach>
		</tbody>
	</table>
	<div class="pagination">${page}</div>
</body>
</html>