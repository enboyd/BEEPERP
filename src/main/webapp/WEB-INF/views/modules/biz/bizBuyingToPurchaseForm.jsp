<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>采购单管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
			//清空tbody
		/*	$("#goodsList").html("");*/
			//$("#name").focus();
			$("#inputForm").validate({
				submitHandler: function(form){
					loading('正在提交，请稍等...');
					form.submit();
				},
				errorContainer: "#messageBox",
				errorPlacement: function(error, element) {
					$("#messageBox").text("输入有误，请先更正。");
					if (element.is(":checkbox")||element.is(":radio")||element.parent().is(".input-append")){
						error.appendTo(element.parent().parent());
					} else {
						error.insertAfter(element);
					}
				}
			});
		});

		//计算金额
		function total(){
			debugger
			var num = $("#goodsNum").val();
			var sum = 0;
			for (var i = 0 ; i < num ;i ++){
				//判断这行有没有被删除
				var rowId = "bizPurchaseDetailList"+i;
				var display = $("#"+rowId).css("display");
				if(display != "none"){
					var unitPrice = "unitPrice"+i;
					var expertAmount = "expertAmount"+i;
					var requisitionAmount = "requisitionAmount"+i;
					var minPurchaseQuantity = "minPurchaseQuantity"+i;
					var unitPriceNum = $("#"+unitPrice).val();
					var expertAmountNum = $("#"+expertAmount).val();
                    var requisitionAmount = $("#"+requisitionAmount).val();
                    var minPurchaseQuantityNum = $("#"+minPurchaseQuantity).val();
                    if(parseInt(expertAmountNum) < parseInt(minPurchaseQuantityNum)){
                        alert("采购数量不能小于最小起购数量!")
                        $("#"+expertAmount).val(minPurchaseQuantityNum);
                    }else{
                        if(Number(expertAmountNum) > Number(requisitionAmount)){
                            $("#"+expertAmount).val(requisitionAmount);
                            alert("采购数量不能大于可请购数量!")
                            if(null != unitPriceNum){
                                sum += unitPriceNum*requisitionAmount;
                                var totalPrice = "totalPrice"+i;
                                $("#"+totalPrice).val(sum);
                            }
                        }else{
                            if(null != unitPriceNum && null != expertAmountNum){
                                sum += unitPriceNum*expertAmountNum;
                                var totalPrice = "totalPrice"+i;
                                $("#"+totalPrice).val(unitPriceNum*expertAmountNum);
                            }
                        }
					}


				}
			}
			$("#totalPrice").val(sum);
		}

		//删除行
		function delRow(row) {
			$("#bizPurchaseDetailList"+row).css("display","none");
			//删除标识
			$("#delFlag"+row).val("1");
			//计算总金额
			total();
		}

	</script>
</head>
<body>
	<ul class="nav nav-tabs">
		<li><a href="${ctx}/biz/bizPurchase/list?type=${bizPurchase.type}">采购单列表</a></li>
		<li class="active"><a href="${ctx}/biz/bizBuyingRequisition/quicklyCreatePurOrders?buyingId=${bizBuyingRequisition.buyingId}">采购单<shiro:hasPermission name="biz:bizPurchase:edit">${not empty bizPurchase.purchaseId?'修改':'添加'}</shiro:hasPermission><shiro:lacksPermission name="biz:bizPurchase:edit">查看</shiro:lacksPermission></a></li>
	</ul><br/>
	<form:form id="inputForm" modelAttribute="bizPurchase" action="${ctx}/biz/bizPurchase/save" method="post" class="form-horizontal" autocomplete="off">
		<form:hidden path="id"/>
		<input type="hidden" name="buyingId" value="${bizBuyingRequisition.buyingId}">
		<sys:message content="${message}"/>
		<input type="hidden" id="goodsNum" value="${bizPurchaseDetailCount}">

		<div class="control-group">
			<label class="control-label">商品明细：</label>
			<div class="controls">
				<table id="contentTable"
					   class="table table-striped table-bordered table-condensed">
					<thead>
					<tr>
						<th>商品名称</th>
						<th>供货商</th>
						<th>商品类型</th>
						<th>规格</th>
						<th>单位</th>
						<th>库存数量</th>
						<th>采购中数量</th>
						<th>采购单价</th>
						<th>税率</th>
						<th>需求时间</th>
						<th>可请购数量</th>
						<th>最小起购数量</th>
						<th>采购数量</th>
						<th>小计</th>
						<shiro:hasPermission name="biz:bizPurchase:purchaseDetailRemove"><th></th></shiro:hasPermission>
					</tr>
					</thead>
					<tbody id="goodsList">

							<c:forEach items="${bizBuyingRequisitionDetails}" var="bizBuyingRequisitionDetail" varStatus="status">
								<tr id="bizPurchaseDetailList${status.index}">
								<td>${bizBuyingRequisitionDetail.goodsName}</td>
								<td>
									<input type="hidden" id="supplierId${status.index}" name="purchaseDetailList[${status.index}].supplierId" value="${bizBuyingRequisitionDetail.supplierId}" />
									<input type="hidden" id="supplierName${status.index}" name="purchaseDetailList[${status.index}].supplierName" value="${bizBuyingRequisitionDetail.supplierName}" />
									${bizBuyingRequisitionDetail.supplierName}
								</td>
								<td>${bizBuyingRequisitionDetail.goodsCategoryName}</td>
								<td>${bizBuyingRequisitionDetail.specification}</td>
								<td>${bizBuyingRequisitionDetail.unit}</td>
								<td>${bizBuyingRequisitionDetail.stockAmount}</td>
								<td>${bizBuyingRequisitionDetail.purchasingAmount}</td>
								<td>
									<input type="text" class="number" onkeyup="total()" style="width: 100px" id="unitPrice${status.index}" name="purchaseDetailList[${status.index}].unitPrice" />
								</td>
								<td>
									<input type="text" style="width: 100px" id="taxRate${status.index}" name="purchaseDetailList[${status.index}].taxRate" >
								</td>
								<td>
									<input id="demandTime${status.index}" name="purchaseDetailList[${status.index}].demandTime"
										   type="text" maxlength="20" autocomplete="off" class="input-medium Wdate "
										   value="<fmt:formatDate value="${bizBuyingRequisitionDetail.demandTime}" pattern="yyyy-MM-dd"/>"
										   onclick="WdatePicker({dateFmt:'yyyy-MM-dd',isShowClear:false});" />
								</td>
								<td>
									<input type="text" readonly="readonly" style="width: 100px"  id="requisitionAmount${status.index}"
										   name="purchaseDetailList[${status.index}].requisitionAmount"
										   value="${bizBuyingRequisitionDetail.surplusExpertAmount}">
								</td>
								<td>
									<input type="text" readonly="readonly" style="width: 100px"  id="minPurchaseQuantity${status.index}"
										   name="purchaseDetailList[${status.index}].minPurchaseQuantity"
										   value="${bizBuyingRequisitionDetail.minPurchaseQuantity}">
								</td>
								<td>
									<input type="hidden" id="delFlag${status.index}" name="purchaseDetailList[${status.index}].delFlag" value="0">
									<input type="hidden" id="goodsId${status.index}" name="purchaseDetailList[${status.index}].goodsId" value="${bizBuyingRequisitionDetail.goodsId}">
									<input type="hidden"  name="purchaseDetailList[${status.index}].buyingDetailId" value="${bizBuyingRequisitionDetail.buyingDetailId}">
									<input type="text" class="digits" style="width: 100px" onchange="total()" id="expertAmount${status.index}" name="purchaseDetailList[${status.index}].expertAmount" value="${bizPurchaseDetail.expertAmount}">
								</td>
								<td>
									<input type="text" readonly id="totalPrice${status.index}" style="width: 100px" name="purchaseDetailList[${status.index}].totalPrice" value="${bizPurchaseDetail.totalPrice}">
								</td>
								<shiro:hasPermission name="biz:bizPurchase:purchaseDetailRemove">
									<td class="text-center" width="10">
										<span class="close" onclick="delRow(${status.index})" title="删除">&times;</span>
									</td>
								</shiro:hasPermission>
								</tr>
							</c:forEach>
					</tbody>
				</table>
			</div>
		</div>
		<%--<div class="control-group">
			<label class="control-label">出款方：</label>
			<div class="controls">
				<form:select path="warehouse" class="input-xlarge required" >
					<form:option value="" label="请选择" />
					<form:options items="${fns:getDictList('biz_purchase_warehouse')}"
								  itemLabel="label" itemValue="value" htmlEscape="false" />
				</form:select>
				<span class="help-inline"><font color="red">*</font> </span>
			</div>
		</div>--%>
		<div class="control-group">
			<label class="control-label">采购单类型：</label>
			<div class="controls">
				<form:select path="purchaseType" class="input-xlarge required" >
					<form:option value="" label="请选择" />
					<form:options items="${fns:getDictList('biz_purchase_type')}"
								  itemLabel="label" itemValue="value" htmlEscape="false" />
				</form:select>
				<span class="help-inline"><font color="red">*</font> </span>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">采购总额：</label>
			<div class="controls">
				<form:input path="totalPrice" htmlEscape="false" class="input-xlarge required" readonly="true"/>
				<span class="help-inline"><font color="red">*</font> </span>
			</div>
		</div>

		<div class="control-group">
			<label class="control-label">备注信息：</label>
			<div class="controls">
				<form:textarea path="remarks" htmlEscape="false" rows="4" maxlength="255" class="input-xxlarge "/>
			</div>
		</div>
		<div colspan=5 class="text-center">
			<shiro:hasAnyPermissions name="biz:bizPurchase:edit,biz:bizPurchase:add">
				<c:if test="${empty bizPurchase.purchaseId || bizPurchase.status == '01'}">
					<input id="btnSubmit" class="btn btn-primary" type="submit" value="保 存"/>&nbsp;
				</c:if>
			</shiro:hasAnyPermissions>
			<input id="btnCancel" class="btn" type="button" value="返 回" onclick="history.go(-1)"/>
		</div>
	</form:form>
</body>
</html>