<%@ taglib prefix="C" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
			closeLoading("");
		});

		function printOrder(){
            window.print();
        }
	<!-- 241mm*138mm -->
	</script>
	<style type="text/css">
		.orderPrin div,#orderPrin p,#orderPrin img,#orderPrin h1,#orderPrin div,#orderPrin div{margin: 0;padding: 0;}
		.orderPrin{width: 964px;height:500px; margin: 0 auto;font-size: 14px;color: #333;border: 1px solid #dedede;position:relative;}
		.orderPrin .cleaboth{clear: both;width: 0 !important;height: 0 !important;}
		.orderPrin .fl{float: left;box-sizing: border-box;}
		.orderPrin .orderPrin_T{width: 25%;line-height: 30px;height: 80px;}
		.orderPrin .orderPrin_T:first-child{width: 37.5%;}
		.orderPrin .orderPrin_T:nth-child(3){width: 37.5%;}
		.orderPrin .orderPrin_T div{height: 20px;}
		.orderPrin .orderTxm{max-width: 90%;height: auto;display: block;margin: 0 auto;}
		.orderPrin .orderNum{padding-left: 50px;;text-overflow:ellipsis; white-space:nowrap;}
		.orderPrin .title{font-size: 24px;text-align: center;line-height: 100px;}
		.orderPrin .orderPrin_Top p{display: inline-block;width: 60px;text-align: justify;text-align-last: justify}
		.orderPrin .orderInfor{padding-left: 50px;padding-top: 13px;line-height: 22px;}
		.orderPrin .orderInfor>div{padding: 3px 0;}
		.orderPrin .orderInforNa{width: 50%;}
		.orderPrin table{display: block;width: 800px;margin: 0 auto;}
		.orderPrin table,#orderPrin table tr th{ border:1px solid #0094ff; }
		.orderPrin .orderEnd{ width: 840px;position:absolute;margin-left:100px;bottom:0;margin-bottom:10px}
		.orderPrin .orderEnd>div{width:33% ; }
	</style>
	<style type="text/css" media="print">
		.noprint { display:none;}
	</style>
</head>
<body>
<c:forEach items="${orderMapList}" var="orderMap" varStatus="status">
<div id="orderPrin${status.index}" class="orderPrin" <c:if test="${!status.last}">style="page-break-after:always"</c:if>>
		<div class="orderPrin_Top" style="height: 90px;margin-top: 10px">
			<div class="fl orderPrin_T">
				<img class="orderTxm" src="${orderMap.tmUrl}"/>
				<div class="orderNum">编号：<span>${orderMap.name}</span></div>
				<div class="orderNum fl">客户：<span>${orderMap.customer}</span></div>
				<div class="orderNum fl">部门：<span>${orderMap.teamName}</span></div>
				<div class="cleaboth"></div>
				<div class="orderNum">地址：<span>${orderMap.address}</span></div>
			</div>
			<div class="fl orderPrin_T">
				<h1 class="title">${orderMap.title}</h1>
			</div>
			<div class="fl orderPrin_T">
				<div><p>出库类别</p>：<span>${orderMap.businessType}</span></div>
				<div><p>发货方式</p>：<span>${orderMap.outgoingCategory}</span></div>
				<div><p>仓库</p>：<span>${orderMap.warehouse}</span></div>
				<div><p>出库日期</p>：<span>${orderMap.outDate}</span></div>
			</div>
			<div class="cleaboth"></div>
		</div>
		<div class="orderInfor">

			<c:if test="${!empty orderMap.remarks}">
				<div>
					备注：<span>${orderMap.remarks}</span>
				</div>
			</c:if>
				<%--<div>
                    备注：<span>${orderMap.remarks}</span>
                </div>--%>
		</div>
		<table border="1" cellspacing="0" cellpadding="3">
			<tr>
				<th width="100px">存货编码</th>
				<th width="130px">存货名称</th>
				<th width="150px">规格型号</th>
				<th width="100px">单位</th>
				<th width="90px">数量</th>
				<th width="300px">项目</th>
			</tr>
			<C:forEach items="${orderMap.orderDetails}" var="orderDetail">
				<tr>
					<td>${orderDetail.code}</td>
					<td>${orderDetail.projectdetailName}</td>
					<td>${orderDetail.specification}</td>
					<td>${orderDetail.unit}</td>
					<td>${orderDetail.quantity}</td>
					<td>${orderDetail.projectName}</td>
				</tr>
			</C:forEach>
			<tr>
				<td>合计</td>
				<td></td>
				<td></td>
				<td></td>
				<td>${orderMap.totalQuantity}</td>
				<td></td>
			</tr>
		</table>
		<div id="orderEnd${status.index}" class="orderEnd">
			<div class="fl">制单人:<span>${orderMap.createBy}</span></div>
			<div class="fl">审核人:<span>${orderMap.auditorName}</span></div>
			<div class="fl">审核日期:<span>${orderMap.auditDate}</span></div>
			<div class="cleaboth"></div>
		</div>
	</div>
	<%--<br>--%>
	<%--<br>--%>
</c:forEach>
<br>
<center id="printId" class="noprint">
	<%--<input id="btnPrint" class="btn btn-primary" type="button" value="打 印" onclick="javascript:window.print();"/>--%>
	<input id="btnPrint" class="btn btn-primary" type="button" value="打 印" onclick="printOrder()"/>
		<input id="btnCancel" class="btn" type="button" value="返 回" onclick="history.go(-1)"/>
</center>
</body>
</html>