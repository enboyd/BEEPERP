<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>采购单管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
			//清空tbody
		/*	$("#goodsList").html("");*/
			//$("#name").focus();
			$("#inputForm").validate({
				submitHandler: function(form){
					loading('正在提交，请稍等...');
					form.submit();
				},
				errorContainer: "#messageBox",
				errorPlacement: function(error, element) {
					$("#messageBox").text("输入有误，请先更正。");
					if (element.is(":checkbox")||element.is(":radio")||element.parent().is(".input-append")){
						error.appendTo(element.parent().parent());
					} else {
						error.insertAfter(element);
					}
				}
			});
		});

		//计算金额
		function total(){
			var num = $("#goodsNum").val();
			var sum = 0;
			for (var i = 0 ; i < num ;i ++){
				//判断这行有没有被删除
				var rowId = "bizPurchaseDetailList"+i;
				var display = $("#"+rowId).css("display");
				if(display != "none"){
					var unitPrice = "unitPrice"+i;
					var expertAmount = "expertAmount"+i;
					var requisitionAmount = "requisitionAmount"+i;
					var unitPriceNum = $("#"+unitPrice).val();
					var expertAmountNum = $("#"+expertAmount).val();
                    var requisitionAmount = $("#"+requisitionAmount).val();
                    if(Number(expertAmountNum) > Number(requisitionAmount)){
                        $("#"+expertAmount).val(requisitionAmount);
                        alert("采购数量不能大于请购数量!")
						if(null != unitPriceNum){
                            sum += unitPriceNum*requisitionAmount;
                            var totalPrice = "totalPrice"+i;
                            $("#"+totalPrice).val(sum);
						}
					}else{
                        if(null != unitPriceNum && null != expertAmountNum){
                            sum += unitPriceNum*expertAmountNum;
                            var totalPrice = "totalPrice"+i;
                            $("#"+totalPrice).val(unitPriceNum*expertAmountNum);
                        }
					}

				}
			}
			$("#totalPrice").val(sum);
		}

		function page(n,s){
			var goodsCode = $("#goodsCode").val();
			var goodsName = $("#goodsName").val();
			var pageNo = n;
			var pageSize = s;
			//获取商品的列表
			$.ajax({
				type: "POST",
				url: "${ctx}/biz/bizGoods/modalPage",
				data: {
					'goodsCode': goodsCode,
					'goodsName': goodsName,
					'pageNo': pageNo,
					'pageSize': pageSize
				},
				success: function(data){
					closeLoading("");
					//清空tbody
					$("#goodsInfo").html("");
					//添加分页
					$("#level").html(data.html);
					var list = data.list;
					var rows = "";
					for (var i = 0 ; i < list.length ; i++){
                        var specification = "";
                        if(typeof(list[i].specification)=="undefined"){
                        }else{
                            specification = list[i].specification;
                        }
						var row = "<tr>"
								+ "<td><input name='checkId' type='checkbox' /></td>"
								+ "<td>"+ list[i].goodsCode+ "</td>"
								+ "<td>"+ list[i].goodsName+"</td>"
								+ "<td>"+ list[i].supplierName+"</td>"
								+ "<td>"+ list[i].goodsCategoryName+ "</td>"
								+ "<td>"+ list[i].goodsBrandName+ "</td>"
								+ "<td>"+ specification+ "</td>"
								+ "<td>"+ list[i].unit+ "</td>"
								+ "<td>"+ list[i].stockAmount+ "</td>"
								+ "<td>"+ list[i].purchasingAmount+ "</td>"
								+ "<td>"+ list[i].warningAmount+ "</td>"
								+ "</tr>";
						rows += row;
					}
					$("#goodsInfo").html(rows);
				}
			});
			return false;
		}

		//删除行
		function delRow(row) {
			$("#bizPurchaseDetailList"+row).css("display","none");
			//删除标识
			$("#delFlag"+row).val("1");
			//计算总金额
			total();
		}

	</script>
</head>
<body>
	<ul class="nav nav-tabs">
		<li><a href="${ctx}/biz/bizPurchase/list?type=${bizPurchase.type}">采购单列表</a></li>
		<li class="active"><a href="${ctx}/biz/bizPurchase/form?id=${bizPurchase.purchaseId}&type=${bizPurchase.type}">采购单<shiro:hasPermission name="biz:bizPurchase:edit">${not empty bizPurchase.purchaseId?'修改':'添加'}</shiro:hasPermission><shiro:lacksPermission name="biz:bizPurchase:edit">查看</shiro:lacksPermission></a></li>
	</ul><br/>
	<form:form id="inputForm" modelAttribute="bizPurchase" action="${ctx}/biz/bizPurchase/save" method="post" class="form-horizontal" autocomplete="off">
		<form:hidden path="id"/>
		<sys:message content="${message}"/>
		<input type="hidden" id="goodsNum" value="${bizPurchaseDetailCount}">

		<div class="control-group">
			<label class="control-label">商品明细：</label>
			<div class="controls">
				<table id="contentTable"
					   class="table table-striped table-bordered table-condensed">
					<thead>
					<tr>
						<th>商品名称</th>
						<th>供货商</th>
						<th>商品类型</th>
						<th>规格</th>
						<th>单位</th>
						<th>库存数量</th>
						<th>采购中数量</th>
						<th>预警数量</th>
						<th>采购单价</th>
						<th>请购数量</th>
						<th>采购数量</th>
						<th>小计</th>
						<shiro:hasPermission name="biz:bizPurchase:purchaseDetailRemove"><th></th></shiro:hasPermission>
					</tr>
					</thead>
					<tbody id="goodsList">

							<c:forEach items="${bizPurchaseDetailList}" var="bizPurchaseDetail" varStatus="status">
								<tr id="bizPurchaseDetailList${status.index}">
								<td>${bizPurchaseDetail.goodsName}</td>
								<td>${bizPurchaseDetail.supplierName}</td>
								<td>${bizPurchaseDetail.goodsCategoryName}</td>
								<td>${bizPurchaseDetail.specification}</td>
								<td>${bizPurchaseDetail.unit}</td>
								<td>${bizPurchaseDetail.stockAmount}</td>
								<td>${bizPurchaseDetail.purchasingAmount}</td>
								<td>${bizPurchaseDetail.warningAmount}</td>
								<td>
									<input type="text" onkeyup="total()" style="width: 100px" id="unitPrice${status.index}" name="purchaseDetailList[${status.index}].unitPrice" value="${bizPurchaseDetail.unitPrice}">
								</td>
								<td>
									<input type="text" readonly="readonly" style="width: 100px"  id="requisitionAmount${status.index}" name="purchaseDetailList[${status.index}].requisitionAmount" value="${bizPurchaseDetail.requisitionAmount}">
								</td>
								<td>
									<input type="hidden" id="goodsId${status.index}" name="purchaseDetailList[${status.index}].goodsId" value="${bizPurchaseDetail.goodsId}">
									<input type="hidden" id="purchaseDetailId${status.index}" name="purchaseDetailList[${status.index}].purchaseDetailId" value="${bizPurchaseDetail.purchaseDetailId}">
									<input type="hidden" id="delFlag${status.index}" name="purchaseDetailList[${status.index}].delFlag" value="${bizPurchaseDetail.delFlag}">
									<input type="text" class="number" style="width: 100px" onkeyup="total()" id="expertAmount${status.index}" name="purchaseDetailList[${status.index}].expertAmount" value="${bizPurchaseDetail.expertAmount}">
								</td>
								<td>
									<input type="text" readonly id="totalPrice${status.index}" style="width: 100px" name="purchaseDetailList[${status.index}].totalPrice" value="${bizPurchaseDetail.totalPrice}">
								</td>
								<shiro:hasPermission name="biz:bizPurchase:purchaseDetailRemove">
									<td class="text-center" width="10">
										<span class="close" onclick="delRow(${status.index})" title="删除">&times;</span>
									</td>
								</shiro:hasPermission>
								</tr>
							</c:forEach>
					</tbody>
				</table>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">采购单类型：</label>
			<div class="controls">
				<form:select path="purchaseType" class="input-xlarge required" >
					<form:option value="" label="请选择" />
					<form:options items="${fns:getDictList('biz_purchase_type')}"
								  itemLabel="label" itemValue="value" htmlEscape="false" />
				</form:select>
				<span class="help-inline"><font color="red">*</font> </span>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">采购总额：</label>
			<div class="controls">
				<form:input path="totalPrice" htmlEscape="false" class="input-xlarge required" readonly="true"/>
				<span class="help-inline"><font color="red">*</font> </span>
			</div>
		</div>

		<div class="control-group">
			<label class="control-label">备注信息：</label>
			<div class="controls">
				<form:textarea path="remarks" htmlEscape="false" rows="4" maxlength="255" class="input-xxlarge "/>
			</div>
		</div>
		<div colspan=5 class="text-center">
			<c:if test="${bizPurchase.auditStatus == '10' && bizPurchase.auditStatus == '40'}">
				<shiro:hasAnyPermissions name="biz:bizPurchase:edit,biz:bizPurchase:add">
					<!-- 审核中和审核通过不可修改该单 -->
					<input id="btnSubmit" class="btn btn-primary" type="submit" value="保 存"/>&nbsp;
				</shiro:hasAnyPermissions>
			</c:if>
			<input id="btnCancel" class="btn" type="button" value="返 回" onclick="history.go(-1)"/>
		</div>
	</form:form>
</body>
</html>