<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>订单管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">

		$(document).ready(function() {
			/**
			 * 全选，反选
			 */
			$('#cbHandle').on('click',function(){
				if(this.checked) {
					$("input[name='checkId']").attr('checked',true);
				}else {
					$("input[name='checkId']").attr('checked',false);
				}
			});

			/**
			 * 批量打印
			 */
			$("#printing").on('click',function(){
				loading("请稍等，勿刷新页面...");
				//选中的订单
				var orderIds = [];
				var flag = true;
				$("input[name='checkId']:checked").each(function(i) {
                    var info = $(this).val();
                    var arr = info.split(",");
                    if(arr[2] == 10 || arr[3] == 0){
                        flag = false;
					}
					orderIds.push(arr[0]);
				});

				if(flag){
                    if(orderIds.length > 0){
                        window.location.href = "${ctx}/biz/bizOrder/prints?orderIds="+orderIds
                    }else{
                        top.$.jBox.tip("请选择要打印的销售出库单出库单！", "warning", {
                            persistent : true,
                            opacity : 0
                        });
                    }
				}else{
                    top.$.jBox.tip("未审核通过或不可出库的出库单不可打印！", "warning", {
                        persistent : true,
                        opacity : 0
                    });
				}
			})

			$("#btnBatchSign").on('click',function(){
				var orderId = $("#orderId").val();
				var content = $("#content").val();
				window.location.href = "${ctx}/biz/bizOrder/discarded?id="+orderId+"&content="+content;
			})
		});

		function discarded(id) {
			$("#orderId").val(id);
			$("#content").text("");
			$("#task").modal('show');
			//${ctx}/biz/bizOrder/discarded?id=${bizOrder.id}
		}

		function page(n,s){
			$("#pageNo").val(n);
			$("#pageSize").val(s);
			$("#searchForm").submit();
        	return false;
        }

        //ajax可出库校验
        function secondary(orderId) {
            top.$.jBox.confirm('是否确认校验','系统提示',function(v,h,f){
                closeLoading();
                if(v=='ok'){
                    $.ajax({
                        type: "POST",
                        url: "${ctx}/biz/bizOrder/secondary",
                        data: {
                            id:orderId
                        },
                        success: function(data){
                            top.$.jBox.info(data,'提示');
                            $('#searchForm').submit();
                        }
                    });
                }
            },{buttonsFocus:1, closed:function(){closeLoading();}});
        }

	</script>
</head>
<body>
	<ul class="nav nav-tabs">
		<li class="active"><a href="${ctx}/biz/bizOrder/">销售出库单列表</a></li>
		<shiro:hasPermission name="biz:bizOrder:edit"><li><a href="${ctx}/biz/bizOrder/form">销售出库单添加</a></li></shiro:hasPermission>
	</ul>
	<form:form id="searchForm" modelAttribute="bizOrder" action="${ctx}/biz/bizOrder/" method="post" class="breadcrumb form-search">
		<input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
		<input id="" name="noIsCanOut" type="hidden" value="1"/>
		<input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/>
		<ul class="ul-form">
			<li><label>订货编号：</label>
				<form:input path="name" htmlEscape="false" maxlength="64" class="input-medium"/>
			</li>
			<li><label>是否可出库：</label>
				<form:select path="isCanOut" class="input-medium" >
					<form:option value="" label="请选择" />
					<form:options items="${fns:getDictList('biz_order_isCanOut')}" itemLabel="label" itemValue="value" htmlEscape="false" />
				</form:select>
			</li>
			<li><label>出库类别：</label>
				<form:select path="orderType" class="input-medium" >
					<form:option value="" label="请选择" />
					<form:options items="${fns:getDictList('biz_order_order_type')}" itemLabel="label" itemValue="value" htmlEscape="false" />
				</form:select>
			</li>
			<li><label>审核状态：</label>
				<form:select path="orderApprovalStatus" class="input-medium" >
					<form:option value="" label="请选择" />
					<form:options items="${fns:getDictList('order_approval_status')}" itemLabel="label" itemValue="value" htmlEscape="false" />
				</form:select>
			</li>
			<li><label>订单状态：</label>
				<form:select path="orderStatus" class="input-medium" >
					<form:option value="" label="请选择" />
					<form:options items="${fns:getDictList('biz_order_status')}" itemLabel="label" itemValue="value" htmlEscape="false" />
				</form:select>
			</li>
			<li class="btns"><input id="btnSubmit" class="btn btn-primary" type="submit" value="查询"/></li>
			<li class="btns"><input id="printing" class="btn btn-primary" type="button" value="打印"/></li>
			<li class="clearfix"></li>
		</ul>
	</form:form>
	<sys:message content="${message}"/>
	<table id="contentTable" class="table table-striped table-bordered table-condensed">
		<thead>
			<tr>
				<th><input type="checkbox" id="cbHandle" /></th>
				<th>订货编号</th>
				<th>客户</th>
				<th>是否可出库</th>
				<th>出库类型</th>
				<th>出库类别</th>
				<th>订单状态</th>
				<th>审核状态</th>
				<shiro:hasPermission name="biz:bizOrder:edit"><th>操作</th></shiro:hasPermission>
			</tr>
		</thead>
		<tbody>
		<c:forEach items="${page.list}" var="bizOrder">
			<tr>
				<td>
					<input name="checkId" type="checkbox" value="${bizOrder.id},${bizOrder.name},${bizOrder.orderApprovalStatus},${bizOrder.isCanOut}" />
				</td>
				<td><a href="${ctx}/biz/bizOrder/form?id=${bizOrder.id}">
					${bizOrder.name}
				</a></td>
				<td>${bizOrder.customerName}</td>
				<td>${fns:getDictLabel(bizOrder.isCanOut, 'biz_order_isCanOut','')}</td>
				<td>${fns:getDictLabel(bizOrder.orderGenre, 'biz_orderGenre','')}</td>
				<td>${fns:getDictLabel(bizOrder.orderType, 'biz_order_order_type','')}</td>
				<td>${fns:getDictLabel(bizOrder.orderStatus, 'biz_order_status', '')}</td>
				<td>${fns:getDictLabel(bizOrder.orderApprovalStatus, 'order_approval_status', '')}</td>
				<shiro:hasAnyPermissions name="biz:bizOrder:edit,biz:bizOrder:discarded">
					<td>
						<shiro:hasPermission name="biz:bizOrder:edit">
							<a href="${ctx}/biz/bizOrder/form?id=${bizOrder.id}">修改</a>
							<a href="${ctx}/biz/bizOrder/delete?id=${bizOrder.id}" onclick="return confirmx('确认要删除该订单吗？', this.href)">删除</a>
							<c:if test="${bizOrder.isCanOut == '0'}">
								<a id="secondary" href="javaScript:void(0)" onclick="secondary('${bizOrder.id}')">可出库校验</a>
							</c:if>
						</shiro:hasPermission>
						<c:if test="${bizOrder.orderApprovalStatus == '20'}">
							<shiro:hasPermission name="biz:bizOrder:discarded">
								<a href="#" class="disabled" onclick="discarded('${bizOrder.id}')">作废</a>
							</shiro:hasPermission>
						</c:if>
					</td>
				</shiro:hasAnyPermissions>
			</tr>
		</c:forEach>
		</tbody>
	</table>
	<div class="pagination">${page}</div>
	<!-- 模态框（Modal） -->
	<div class="modal fade" id="task" tabindex="-99" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display:none;">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					请填写作废原因
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				</div>
				<div class="modal-body">
					<input id="orderId" name="id" type="hidden" />
					作废原因：
					<textarea name="content" id="content" style="margin-left: 50px;width: 360px;height: 70px">

					</textarea>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
					<button type="button" class="btn btn-primary" id="btnBatchSign"> 提交</button>
				</div>
			</div>
		</div>
	</div>
</body>
</html>