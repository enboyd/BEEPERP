<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>商品管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
            //导入按钮
            $("#btnImport").click(function() {
                $.jBox($("#importBox").html(),
                    {
                        title : "导入数据",
                        buttons : {
                            "关闭" : true
                        },
                        bottomText : "导入文件不能超过5M，仅允许导入“csv”格式文件！"
                    });
            });

			/* 导出 */
			$("#btnExport").click(function(){
				top.$.jBox.confirm("确认要导出商品数据吗？", "系统提示", function(v,h,f){
					if(v=="ok"){
						var checked = $("input[type='checkbox']").is(':checked')
						if(checked){
							$("#stockAmount").val(0);
						}
						$("#searchForm").attr("action","${ctx}/biz/bizGoods/export");
						$("#searchForm").submit();
					}
				},{buttonsFocus:1});
			});


			$("#btnSubmit").click(function(){
				var checked = $("input[type='checkbox']").is(':checked')
				if(checked){
					$("#stockAmount").val(0);
				}
				$("#searchForm").submit();
			})

		});
		function page(n,s){
			$("#pageNo").val(n);
			$("#pageSize").val(s);
			$("#searchForm").submit();
        	return false;
        }

        //库存信息的查看
        function showStockInfo(goodsId) {
            $.ajax({
                type: "POST",
                url: "${ctx}/biz/bizGoods/stockInfo",
                data: {
                    'goodsId': goodsId
                },
                success: function(data){
                    top.$.jBox.closeTip();
                    $("#stockInfo").html("");
                    $("#stockModal").modal('show');
                    var rows = "";
                    for (var i = 0 ; i < data.length ; i++){
                        var goodsName = data[i].goodsName;//商品名字
                        var storageLocationName = data[i].storageLocationName;//库位
                        var surplusNumber = data[i].surplusNumber;//剩余数
                        var row = "<tr>"
                            + "<td>"+ goodsName+ "</td>"
                            + "<td>"+ storageLocationName+ "</td>"
                            + "<td>"+ surplusNumber +"</td>"
                            + "</tr>";
                        rows += row;
                    }
                    $("#stockInfo").html(rows);
                }
            });
        }
	</script>
</head>
<body>
<div id="importBox" class="hide">
	<form id="importForm" action="${ctx}/biz/bizGoods/importGoods" method="post" enctype="multipart/form-data"
		  class="form-search" style="padding-left: 20px; text-align: center;"
		  onsubmit="loading('正在导入，请稍等...');setTimeout(function(){window.location.reload()}, 999000);">
		<br />
		<input id="uploadFile" name="file" type="file" style="width: 330px" />
		<br /> <br />
		<input id="btnImportSubmit" class="btn btn-primary" type="submit" value="   导    入   " />
		<a href="${ctx}/biz/bizGoods/import/template">下载模板</a>
	</form>
</div>
	<ul class="nav nav-tabs">
		<li class="active"><a href="${ctx}/biz/bizGoods/">商品列表</a></li>
		<shiro:hasPermission name="biz:bizGoods:edit"><li><a href="${ctx}/biz/bizGoods/form">商品添加</a></li></shiro:hasPermission>
	</ul>
	<form:form id="searchForm" modelAttribute="bizGoods" action="${ctx}/biz/bizGoods/" method="post" class="breadcrumb form-search">
		<input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
		<input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/>
		<ul class="ul-form">
			<li><label>商品编号：</label>
				<form:input path="goodsCode" htmlEscape="false" maxlength="64" class="input-medium"/>
			</li>
			<li><label>商品名称：</label>
				<form:input path="goodsName" htmlEscape="false" maxlength="64" class="input-medium"/>
			</li>
			<li><label>零库存展示：</label>
				<input type="hidden" name="stockAmount" id="stockAmount" >
				<input type="checkbox" id="is0" <c:if test="${!empty bizGoods.stockAmount}">checked</c:if> >
			</li>
			<li class="btns" style="margin-left: 50px"><input id="btnSubmit" class="btn btn-primary" type="button" value="查询"/></li>
			<shiro:hasPermission name="biz:goods:import">
				<li class="btns">
					<input id="btnImport" class="btn btn-primary" type="button" value="导入"/>
				</li>
			</shiro:hasPermission>
			<shiro:hasPermission name="sm:goods:export">
				<li class="btns"><input id="btnExport" class="btn btn-primary" type="button" value="导出"/></li>
			</shiro:hasPermission>
			<li class="clearfix"></li>
		</ul>
	</form:form>
	<sys:message content="${message}"/>
	<table id="contentTable" class="table table-striped table-bordered table-condensed">
		<thead>
			<tr>
				<th>商品编号</th>
				<th>商品名称</th>
				<th>供应商</th>
				<th>项目</th>
				<th>规格</th>
				<th>库存数量</th>
				<th>单位</th>
				<th>采购中数量</th>
				<th>预警数量</th>
				<th>最小请购数</th>
				<shiro:hasAnyPermissions name="biz:bizGoods:edit,biz:bizGoods:remove"><th>操作</th></shiro:hasAnyPermissions>
			</tr>
		</thead>
		<tbody>
		<c:forEach items="${page.list}" var="bizGoods">
			<tr>
				<td><a href="${ctx}/biz/bizGoods/form?id=${bizGoods.goodsId}">
					${bizGoods.goodsCode}
				</a></td>
				<td>${bizGoods.goodsName}</td>
				<td>${bizGoods.supplierName}</td>
				<td>${bizGoods.goodsCategoryName}</td>
				<td>${bizGoods.specification}</td>
				<td>${bizGoods.stockAmount}</td>
				<td>${bizGoods.unit}</td>
				<td>${bizGoods.purchasingAmount}</td>
				<td>${bizGoods.warningAmount}</td>
				<td>${bizGoods.minPurchaseQuantity}</td>
				<shiro:hasAnyPermissions name="biz:bizGoods:edit,biz:bizGoods:remove,biz:bizGoods:stockInfo">
					<td>
						<shiro:hasPermission name="biz:bizGoods:edit">
							<a href="${ctx}/biz/bizGoods/form?id=${bizGoods.goodsId}">修改</a>
						</shiro:hasPermission>
						<shiro:hasPermission name="biz:bizGoods:remove">
							<a href="${ctx}/biz/bizGoods/delete?id=${bizGoods.goodsId}" onclick="return confirmx('确认要删除该商品吗？', this.href)">删除</a>
						</shiro:hasPermission>
						<%--<shiro:hasPermission name="biz:bizGoods:stockInfo">--%>
							<%--<a href="#" id="aStockInfo" onclick="showStockInfo('${bizGoods.goodsId}')">库存信息</a>--%>
						<%--</shiro:hasPermission>--%>
					</td>
				</shiro:hasAnyPermissions>
			</tr>
		</c:forEach>
		</tbody>
	</table>
	<div class="pagination">${page}</div>

	<%-- 库存信息的modal --%>
	<div class="modal fade" id="stockModal" tabindex="-99" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display:none;width: 700px;">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					库存信息
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				</div>
				<div class="modal-body">
					<table border="1" cellspacing="0" cellpadding="0" style="width: 100%">
						<thead>
						<tr>
							<th>商品</th>
							<th>库位</th>
							<th>数量</th>
						</tr>
						</thead>
						<tbody id="stockInfo">
						</tbody>
					</table>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
				</div>
			</div>
		</div>
	</div>
</body>
</html>