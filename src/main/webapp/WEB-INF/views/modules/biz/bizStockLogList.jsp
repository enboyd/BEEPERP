<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>期出期入管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
			
		});
		function page(n,s){
			$("#pageNo").val(n);
			$("#pageSize").val(s);
			$("#searchForm").submit();
        	return false;
        }

		/**
		 * 单个打印条码
		 */
		function dayinBizBarcode(id) {
			loading("正在打印，请稍等...");
			$.ajax({
				type: "POST",
				url: "${ctx}/biz/bizStockIn/getInfoDan",
				data: {
					"id":id
				},
				success: function(data){
					closeLoading("");
					var info = data;
					var goodsCode = info.goodsCode;
					var manufactureDate = info.manufactureDate;
					var goodsName = info.goodsName;
					var pici = info.name.substring(0,12);
					var number = info.number;
					var name = info.name;
					printQRcode(goodsCode,manufactureDate,goodsName,pici,number,name);
				}
			})
		}

		/**
		 * 打印接的打印
		 * goodsCode 商品编号
		 * date 生产日期
		 * goodsName 商品名称
		 * pici 批次
		 * number 数量
		 * code 条形码编号
		 */
		function printQRcode(goodsCode,date,goodsName,pici,number,code){
			var TSCObj = new ActiveXObject("TSCActiveX.TSCLIB");//打印机使用的dll
			TSCObj.ActiveXopenport ("TSC TTP-243E Pro");
			TSCObj.ActiveXsetup("70","40","4","15","0","2.5","0");//打印机设置
			TSCObj.ActiveXsendcommand("DIRECTION 1");  //设置标签方向 ,DIRECTION 1 左上角 (x,y)={0,0};DIRECTION 0 右下角 (x,y)={0,0};
			TSCObj.ActiveXsendcommand ("SET TEAR ON");
			TSCObj.ActiveXclearbuffer();
			TSCObj.ActiveXwindowsfont (65, 20, 30, 0, 2, 0, "Arial", "存货编码："+goodsCode);
			TSCObj.ActiveXwindowsfont (350, 20, 30, 0, 2, 0, "Arial", date);
			TSCObj.ActiveXwindowsfont (65, 60, 30, 0, 2, 0, "Arial", "存货名称："+goodsName);
			TSCObj.ActiveXwindowsfont (65, 100, 30, 0, 2, 0, "Arial", "批  次："+pici);
			TSCObj.ActiveXwindowsfont (365, 100, 30, 0, 2, 0, "Arial", "数量："+number);
			TSCObj.ActiveXbarcode ("65", "150", "128", "100", "1", "0", "3", "3", code);
			TSCObj.ActiveXprintlabel ("1","1");//（打印份数,每页打印张数）
			TSCObj.ActiveXclearbuffer();//清除
			TSCObj.ActiveXcloseport();//关闭打印端口
		}


	</script>
</head>
<body>
	<ul class="nav nav-tabs">
		<li class="active"><a href="${ctx}/biz/bizStockLog/">期出期入列表</a></li>
		<shiro:hasPermission name="biz:bizStockLog:edit"><li><a href="${ctx}/biz/bizStockLog/form">期出期入添加</a></li></shiro:hasPermission>
	</ul>
	<form:form id="searchForm" modelAttribute="bizStockLog" action="${ctx}/biz/bizStockLog/" method="post" class="breadcrumb form-search">
		<input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
		<input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/>
		<ul class="ul-form">
			<li><label>商品编号：</label>
				<form:input path="goodsId" htmlEscape="false" maxlength="32" class="input-medium"/>
			</li>
			<li><label>类型：</label>
				<form:select path="type" class="input-medium" >
					<form:option value="" label="请选择" />
					<form:options items="${fns:getDictList('biz_stock_log_type')}" itemLabel="label" itemValue="value" htmlEscape="false" />
				</form:select>			</li>
			<li class="btns"><input id="btnSubmit" class="btn btn-primary" type="submit" value="查询"/></li>
			<li class="clearfix"></li>
		</ul>
	</form:form>
	<sys:message content="${message}"/>
	<table id="contentTable" class="table table-striped table-bordered table-condensed">
		<thead>
			<tr>
				<th>商品</th>
				<th>库位</th>
				<th>数量</th>
				<th>操作前库存</th>
				<th>操作后库存</th>
				<th>类型</th>
				<th>创建时间</th>
				<th>创建人</th>
				<shiro:hasPermission name="biz:bizStockLog:printing"><th>打印</th></shiro:hasPermission>
			</tr>
		</thead>
		<tbody>
		<c:forEach items="${page.list}" var="bizStockLog">
			<tr>
				<td>
					${bizStockLog.goodsName}
				</td>
				<td>
					${bizStockLog.storageLocationName}
				</td>
				<td>
					${bizStockLog.number}
				</td>
				<td>
					${bizStockLog.stockAmountFront}
				</td>
				<td>
					${bizStockLog.stockAmountAfter}
				</td>
				<td>
					${fns:getDictLabel(bizStockLog.type, 'biz_stock_log_type', '')}
				</td>
				<td>
					<fmt:formatDate value="${bizStockLog.createDate}" pattern="yyyy-MM-dd hh:mm:ss"/>
				</td>
				<td>
					${bizStockLog.createByName}
				</td>
				<shiro:hasPermission name="biz:bizStockLog:printing">
					<td>
						<c:if test="${bizStockLog.isTm eq '1'}">
							<img onclick="dayinBizBarcode('${bizStockLog.barcodeId}')" src="${ctxStatic}/images/dayin.png" alt="打印" style="width: 30px;height: 30px;margin-left: 10px">
						</c:if>
					</td>
				</shiro:hasPermission>
			</tr>
		</c:forEach>
		</tbody>
	</table>
	<div class="pagination">${page}</div>
</body>
</html>