<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>自动编码管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
			
		});
		function page(n,s){
			$("#pageNo").val(n);
			$("#pageSize").val(s);
			$("#searchForm").submit();
        	return false;
        }
	</script>
</head>
<body>
	<ul class="nav nav-tabs">
		<li class="active"><a href="${ctx}/sys/sysAutoname/">自动编码列表</a></li>
		<shiro:hasPermission name="sys:sysAutoname:edit"><li><a href="${ctx}/sys/sysAutoname/form">自动编码添加</a></li></shiro:hasPermission>
	</ul>
	<form:form id="searchForm" modelAttribute="sysAutoname" action="${ctx}/sys/sysAutoname/" method="post" class="breadcrumb form-search">
		<input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
		<input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/>
		<ul class="ul-form">
			<li><label>表名：</label>
				<form:input path="tableName" htmlEscape="false" maxlength="255" class="input-medium"/>
			</li>
			<li><label>简写名：</label>
				<form:input path="logogramName" htmlEscape="false" maxlength="64" class="input-medium"/>
			</li>
			<li class="btns"><input id="btnSubmit" class="btn btn-primary" type="submit" value="查询"/></li>
			<li class="clearfix"></li>
		</ul>
	</form:form>
	<sys:message content="${message}"/>
	<table id="contentTable" class="table table-striped table-bordered table-condensed">
		<thead>
			<tr>
				<th>表名</th>
				<th>简写名</th>
				<shiro:hasPermission name="sys:sysAutoname:edit"><th>操作</th></shiro:hasPermission>
			</tr>
		</thead>
		<tbody>
		<c:forEach items="${page.list}" var="sysAutoname">
			<tr>
				<td><a href="${ctx}/sys/sysAutoname/form?id=${sysAutoname.id}">
					${sysAutoname.tableName}
				</a></td>
				<td>
					${sysAutoname.logogramName}
				</td>
				<shiro:hasPermission name="sys:sysAutoname:edit"><td>
    				<a href="${ctx}/sys/sysAutoname/form?id=${sysAutoname.id}">修改</a>
					<a href="${ctx}/sys/sysAutoname/delete?id=${sysAutoname.id}" onclick="return confirmx('确认要删除该自动编码吗？', this.href)">删除</a>
				</td></shiro:hasPermission>
			</tr>
		</c:forEach>
		</tbody>
	</table>
	<div class="pagination">${page}</div>
</body>
</html>